package ItemAnimators;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

import Adapters.OperativeAccountsAdapter;
import ViewHolders.OperativeAccountViewHolder;

/**
 * Created by Samvid Mistry on 15/02/2016.
 */
public class OperativeAccountAnimator extends DefaultItemAnimator {
    private boolean mIsExpanding = false;

    @Override
    public boolean canReuseUpdatedViewHolder(RecyclerView.ViewHolder viewHolder) {
        return true;
    }

    @Override
    public boolean animateChange(@NonNull final RecyclerView.ViewHolder oldHolder, @NonNull RecyclerView.ViewHolder newHolder, @NonNull final ItemHolderInfo preInfo, @NonNull final ItemHolderInfo postInfo) {
        if(!(oldHolder instanceof OperativeAccountViewHolder)) return false;

        ValueAnimator valueAnimator = ValueAnimator.ofInt(preInfo.bottom, postInfo.bottom);
        valueAnimator.setDuration(500);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                OperativeAccountViewHolder viewHolder = (OperativeAccountViewHolder) oldHolder;
                ViewGroup.LayoutParams layoutParams = viewHolder.wholeView.getLayoutParams();
                int animatedVal = (int) animation.getAnimatedValue();
                layoutParams.height = animatedVal;
                viewHolder.wholeView.setLayoutParams(layoutParams);
            }
        });
        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                //((OperativeAccountViewHolder) oldHolder).bottomLayout.setVisibility(mIsExpanding ? View.INVISIBLE : View.VISIBLE);
                dispatchChangeStarting(oldHolder, true);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                //((OperativeAccountViewHolder) oldHolder).bottomLayout.setVisibility(mIsExpanding ? View.VISIBLE : View.GONE);
                dispatchChangeFinished(oldHolder, true);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                dispatchChangeFinished(oldHolder, true);
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        valueAnimator.start();

        return true;
    }

    @NonNull
    @Override
    public ItemHolderInfo recordPreLayoutInformation(RecyclerView.State state, RecyclerView.ViewHolder viewHolder, int changeFlags, List<Object> payloads) {
        ItemHolderInfo itemHolderInfo = new ItemHolderInfo();
        itemHolderInfo.setFrom(viewHolder);

        if(payloads.contains(OperativeAccountsAdapter.COLLAPSED)){
            mIsExpanding = false;
        }else if(payloads.contains(OperativeAccountsAdapter.EXPANDED)){
            mIsExpanding = true;
        }

        return itemHolderInfo;
    }

    @NonNull
    @Override
    public ItemHolderInfo recordPostLayoutInformation(@NonNull RecyclerView.State state, @NonNull RecyclerView.ViewHolder viewHolder) {
        ItemHolderInfo itemHolderInfo = new ItemHolderInfo();
        itemHolderInfo.setFrom(viewHolder);
        if(mIsExpanding){
            itemHolderInfo.bottom += 200;
        }else{
            itemHolderInfo.bottom -= 200;
        }

        return itemHolderInfo;
    }
}
