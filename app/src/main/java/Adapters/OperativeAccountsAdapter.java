package Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.waranabank.mobipro.DetailedStatementActivity;
import com.waranabank.mobipro.MiniStatementActivity;
import com.waranabank.mobipro.R;

import java.util.ArrayList;
import java.util.List;

import Model.OperativeAccountResModel;
import Utility.MySharedPreferences;
import ViewHolders.OperativeAccountHeaderViewHolder;
import ViewHolders.OperativeAccountViewHolder;

/**
 * Created by Samvid Mistry on 10/02/2016.
 */
public class OperativeAccountsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final String EXPANDED = "expanded";
    public static final String COLLAPSED = "collapsed";

    private Context mContext;
    private ArrayList<OperativeAccountResModel.Response> mResponses;
    private ArrayList<Integer> mSections;
    private SparseBooleanArray mSparseBooleanArray;
    private static final int VIEW_HEADER = 916;
    private static final int VIEW_ITEM = 651;

    public OperativeAccountsAdapter(Context context, ArrayList<OperativeAccountResModel.Response> responses, ArrayList<Integer> sections) {
        mContext = context;
        mResponses = responses;
        mSections = sections;

        mSparseBooleanArray = new SparseBooleanArray();

        for (int i = 0; i < responses.size(); i++) {
            mSparseBooleanArray.put(i, false);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == VIEW_HEADER) {
            return new OperativeAccountHeaderViewHolder(LayoutInflater.from(mContext).inflate(R.layout.view_header_item_operative_account, parent, false));
        }else{
            return new OperativeAccountViewHolder(LayoutInflater.from(mContext).inflate(R.layout.view_item_operative_account, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position, List<Object> payloads) {
        if(payloads.size() == 0){
            onBindViewHolder(holder, position);
        }else if(payloads.contains(EXPANDED)){
            if(holder instanceof OperativeAccountViewHolder){
                ((OperativeAccountViewHolder) holder).mShowMoreLess.setVisibility(View.VISIBLE);
                ((OperativeAccountViewHolder) holder).mMoreDetailsText.setText("Less Details");
                mSparseBooleanArray.put(position, true);
            }
        }else if(payloads.contains(COLLAPSED)){
            if(holder instanceof OperativeAccountViewHolder){
                ((OperativeAccountViewHolder) holder).mShowMoreLess.setVisibility(View.GONE);
                ((OperativeAccountViewHolder) holder).mMoreDetailsText.setText("More Details");
                mSparseBooleanArray.put(position, false);
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        OperativeAccountResModel.Response response = mResponses.get(position);
        if(holder instanceof OperativeAccountHeaderViewHolder){
            final OperativeAccountHeaderViewHolder viewHolder = (OperativeAccountHeaderViewHolder) holder;

            //viewHolder.mAccountType.setText(StringUtils.capitalizeFully(response.getAccountTypeName(), null));
            viewHolder.mAccountType.setText(response.getAccountTypeName());
        }
        final OperativeAccountViewHolder viewHolder = (OperativeAccountViewHolder) holder;

        viewHolder.mAccountNo.setText(response.getAccountNo(), null);
        viewHolder.mAvailableBalance.setText("₹ " + response.getBalance(), null);
        try {
            viewHolder.mFullName.setText(MySharedPreferences.getCustomerName(mContext), null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        viewHolder.mOpeningDate.setText(response.getOpeningDate(), null);
        viewHolder.mOpeningBal.setText("₹ " + response.getOpeningBalance());
        viewHolder.mUnclearBal.setText("₹ " + response.getUnclearBalance());
        viewHolder.mLienBal.setText("₹ " + response.getLienAmount());
        viewHolder.mAvailableBal.setText("₹ " + response.getBalance());

        if(mSparseBooleanArray.get(position)){
            viewHolder.bottomLayout.setVisibility(View.VISIBLE);
            viewHolder.mMoreDetailsText.setText("Less Details");
        }else{
            viewHolder.bottomLayout.setVisibility(View.GONE);
            viewHolder.mMoreDetailsText.setText("More Details");
        }

        viewHolder.mShowMoreLess.setImageResource(mSparseBooleanArray.get(viewHolder.getAdapterPosition()) ? R.drawable.vector_drawable_show_less : R.drawable.vector_drawable_show_more);

        viewHolder.showMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewHolder.bottomLayout.setVisibility(viewHolder.bottomLayout.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                mSparseBooleanArray.put(viewHolder.getAdapterPosition(), !mSparseBooleanArray.get(viewHolder.getAdapterPosition()));
                viewHolder.mShowMoreLess.setImageResource(mSparseBooleanArray.get(viewHolder.getAdapterPosition()) ? R.drawable.vector_drawable_show_less: R.drawable.vector_drawable_show_more);
                viewHolder.mMoreDetailsText.setText(mSparseBooleanArray.get(viewHolder.getAdapterPosition()) ? "Less Details" : "More Details");
                //notifyItemChanged(viewHolder.getAdapterPosition(), viewHolder.mIsExpanded ? EXPANDED : COLLAPSED);
            }
        });
        viewHolder.mMiniStatement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, MiniStatementActivity.class);
                intent.putExtra(MiniStatementActivity.ACCOUNT_NO, mResponses.get(viewHolder.getAdapterPosition()).getAccountNo());
                mContext.startActivity(intent);
            }
        });
        viewHolder.mDetailedStatement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, DetailedStatementActivity.class);
                intent.putExtra(DetailedStatementActivity.ACCOUNT_NO, mResponses.get(viewHolder.getAdapterPosition()).getAccountNo());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mResponses.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(mSections.contains(position)){
            return VIEW_HEADER;
        }else{
            return VIEW_ITEM;
        }
    }
}
