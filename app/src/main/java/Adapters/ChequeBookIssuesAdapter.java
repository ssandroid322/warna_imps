package Adapters;

import android.content.Context;
import android.support.v4.util.SimpleArrayMap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.waranabank.mobipro.R;

import java.util.ArrayList;

import Model.ChequeBookDetailsResModel;
import ViewHolders.ChequeBookIssuesViewHolder;

/**
 * Created by Samvid Mistry on 26/02/2016.
 */
public class ChequeBookIssuesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String ISSUE_DATE = "ISSUE_TYPE";
    private static final String FROM_CHEQUE = "FROM_CHEQUE";
    private static final String TO_CHEQUE = "TO_CHEQUE";
    private static final String CHEQUE_TOTAL = "CHEQUE_TOTAL";
    private static final String NO_OF_USED = "NO_OF_USED";
    private static final String CHARACTERISTICS = "CHARACTERISTICS";
    private static final String PAYABLE_AT_PAR = "PAYABLE_AT_PAR";

    private Context mContext;
    private int mResCount;
    private ArrayList<SimpleArrayMap<String, String>> mCheques;

    public ChequeBookIssuesAdapter(Context context, ChequeBookDetailsResModel chequeBookDetailsResModel) {
        mContext = context;

        mCheques = new ArrayList<>();

        ChequeBookDetailsResModel.Response[] responses = chequeBookDetailsResModel.getResponse();
        mResCount = responses.length;

        for (int i = 0; i < mResCount; i++) {
            ChequeBookDetailsResModel.Response response = responses[i];
            SimpleArrayMap<String, String> stringStringSimpleArrayMap = new SimpleArrayMap<>();
            stringStringSimpleArrayMap.put(ISSUE_DATE, response.getIssueDate());
            stringStringSimpleArrayMap.put(FROM_CHEQUE, response.getFromCheque());
            stringStringSimpleArrayMap.put(TO_CHEQUE, response.getToCheque());
            stringStringSimpleArrayMap.put(CHEQUE_TOTAL, response.getChequeTotal());
            stringStringSimpleArrayMap.put(NO_OF_USED, response.getNumberOfUsedCheques());
            stringStringSimpleArrayMap.put(CHARACTERISTICS, response.getCharacteristics());
            stringStringSimpleArrayMap.put(PAYABLE_AT_PAR, response.getPayableAtPar());
            mCheques.add(stringStringSimpleArrayMap);
        }

        /*SimpleArrayMap<String, String> stringStringSimpleArrayMap = new SimpleArrayMap<>();
        stringStringSimpleArrayMap.put(ISSUE_DATE, "13-JAN-2014");
        stringStringSimpleArrayMap.put(FROM_CHEQUE, "452");
        stringStringSimpleArrayMap.put(TO_CHEQUE, "485");
        stringStringSimpleArrayMap.put(CHEQUE_TOTAL, "37");
        stringStringSimpleArrayMap.put(NO_OF_USED, "7");
        stringStringSimpleArrayMap.put(CHARACTERISTICS, "Bearer");
        stringStringSimpleArrayMap.put(PAYABLE_AT_PAR, "y");

        mCheques.add(stringStringSimpleArrayMap);
        mCheques.add(stringStringSimpleArrayMap);
        mCheques.add(stringStringSimpleArrayMap);
        mCheques.add(stringStringSimpleArrayMap);
        mCheques.add(stringStringSimpleArrayMap);
        mCheques.add(stringStringSimpleArrayMap);
        mCheques.add(stringStringSimpleArrayMap);
        mCheques.add(stringStringSimpleArrayMap);*/

        mResCount = mCheques.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ChequeBookIssuesViewHolder(LayoutInflater.from(mContext).inflate(R.layout.view_cheque_issue, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(!(holder instanceof ChequeBookIssuesViewHolder)) return;

        ChequeBookIssuesViewHolder viewHolder = (ChequeBookIssuesViewHolder) holder;
        SimpleArrayMap<String, String> data = mCheques.get(position);
        viewHolder.mIssueDate.setText(data.get(ISSUE_DATE));
        viewHolder.mUsedCheques.setText(data.get(NO_OF_USED));
        viewHolder.mTotalCheques.setText(data.get(CHEQUE_TOTAL));
        viewHolder.mFromCheque.setText(data.get(FROM_CHEQUE));
        viewHolder.mToCheque.setText(data.get(TO_CHEQUE));
        viewHolder.mRemarks.setText(data.get(CHARACTERISTICS));
    }

    @Override
    public int getItemCount() {
        return mResCount;
    }
}
