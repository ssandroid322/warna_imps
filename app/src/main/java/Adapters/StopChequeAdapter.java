package Adapters;

import android.content.Context;
import android.support.v4.util.SimpleArrayMap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import com.waranabank.mobipro.R;


import java.util.ArrayList;

import Interface.StopChequeInterface;
import Model.ChequeStopResModel;
import Utility.StringUtils;
import ViewHolders.SelectAccountViewHolder;
import ViewHolders.StopChequeViewHolder;

/**
 * Created by Samvid Mistry on 02/03/2016.
 */
public class StopChequeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int VIEW_ACCOUNT = 0;
    private static final int VIEW_CHEQUE = 1;
    private static final String CHQ_NO = "CHQ_NO";
    private static final String ISSUED_DATE = "ISSUED_DATE";
    private static final String ISSUED_TO = "ISSUED_TO";
    private static final String AMOUNT = "AMOUNT";
    
    private Context mContext;
    private ArrayList<String> mAccounts;
    private StopChequeInterface mStopChequeInterface;
    private ArrayList<SimpleArrayMap<String, String>> mMaps;

    public StopChequeAdapter(Context context, ArrayList<String> accounts, ChequeStopResModel chequeStopResModel, StopChequeInterface stopChequeInterface) {
        mContext = context;
        mAccounts = accounts;
        mStopChequeInterface = stopChequeInterface;

        generateDataFromModel();
    }

    public StopChequeAdapter(Context context, ArrayList<String> accounts, StopChequeInterface stopChequeInterface){
        mStopChequeInterface = stopChequeInterface;
        mContext = context;
        mAccounts = accounts;
    }

    private void generateDataFromModel(){
        mMaps = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            SimpleArrayMap<String, String> map = new SimpleArrayMap<>();
            map.put(CHQ_NO, "1234567");
            map.put(ISSUED_DATE, "10/2/2016");
            map.put(ISSUED_TO, "Mahendra Suthar");
            map.put(AMOUNT, StringUtils.commaSeparated("10000"));

            mMaps.add(map);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == VIEW_ACCOUNT){
            SelectAccountViewHolder selectAccountViewHolder = new SelectAccountViewHolder(LayoutInflater.from(mContext).inflate(R.layout.view_select_account, parent, false));
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(mContext, android.R.layout.simple_list_item_1, mAccounts);
            selectAccountViewHolder.mSpinner.setAdapter(arrayAdapter);
            selectAccountViewHolder.mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if(position == 0) return;
                    mStopChequeInterface.onChequeRequested(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            return selectAccountViewHolder;
        }else{
            return new StopChequeViewHolder(LayoutInflater.from(mContext).inflate(R.layout.view_stop_cheque, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof StopChequeViewHolder){
            final StopChequeViewHolder viewHolder = (StopChequeViewHolder) holder;
            final SimpleArrayMap<String, String> map = mMaps.get(position - 1);
            viewHolder.mChequeNo.setText(map.get(CHQ_NO));
            viewHolder.mIssuedDate.setText(map.get(ISSUED_DATE));
            viewHolder.mIssuedTo.setText(map.get(ISSUED_TO));
            viewHolder.mAmount.setText(map.get(AMOUNT));
            viewHolder.mStopButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mStopChequeInterface.onStopChequeRequested(map.get(CHQ_NO), viewHolder.getAdapterPosition());
                }
            });
        }
    }

    public void removeItem(int deletedPosition){
        mMaps.remove(deletedPosition - 1);
        notifyItemRemoved(deletedPosition);
    }

    @Override
    public int getItemCount() {
        if(mMaps == null) return 1;

        return mMaps.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0){
            return VIEW_ACCOUNT;
        }else{
            return VIEW_CHEQUE;
        }
    }
}
