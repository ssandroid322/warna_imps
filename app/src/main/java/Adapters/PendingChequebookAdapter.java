package Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.util.SimpleArrayMap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.waranabank.mobipro.R;


import java.util.ArrayList;

import Model.PendingChequebookResModel;

/**
 * Created by Samvid Mistry on 25/02/2016.
 */
public class PendingChequebookAdapter extends BaseAdapter {
    private static final String REQ_REF_NO = "REQ_REF_NO";
    private static final String REQ_DATE = "REQ_DATE";
    private static final String NO_OF_LEAF = "NO_OF_LEAF";
    private static final String STATUS = "STATUS";
    
    private Context mContext;
    private int mResCount;
    private PendingChequebookResModel mPendingChequebookResModel;
    private ArrayList<SimpleArrayMap<String, String>> mPendingChequebooks;

    public PendingChequebookAdapter(Context context, PendingChequebookResModel pendingChequebookResModel) {
        mContext = context;
        mPendingChequebookResModel = pendingChequebookResModel;

        mPendingChequebooks = new ArrayList<>();
        PendingChequebookResModel.Response[] responses = pendingChequebookResModel.getResponses();
        mResCount = responses.length;

        for (int i = 0; i < mResCount; i++) {
            PendingChequebookResModel.Response response = responses[i];
            SimpleArrayMap<String, String> map = new SimpleArrayMap<>();
            map.put(REQ_REF_NO, response.getReqRefNo());
            map.put(REQ_DATE, response.getReqDate());
            map.put(NO_OF_LEAF, response.getNoOfLeaf());
            map.put(STATUS, response.getStatus());
            mPendingChequebooks.add(map);
        }
    }

    @Override
    public int getCount() {
        return mResCount;
    }

    @Override
    public Object getItem(int position) {
        return mPendingChequebooks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.view_pending_chequebooks, parent, false);
        }

        TextView reqRefNo = (TextView) convertView.findViewById(R.id.reqRefNo_pendingChequebooks);
        TextView reqDate = (TextView) convertView.findViewById(R.id.reqDate_pendingChequebooks);
        TextView noOfLeaf = (TextView) convertView.findViewById(R.id.noOfLeaf_pendingChequebooks);
        TextView status = (TextView) convertView.findViewById(R.id.status_pendingChequebooks);

        SimpleArrayMap<String, String> map = mPendingChequebooks.get(position);
        reqRefNo.setText(map.get(REQ_REF_NO));
        reqDate.setText(map.get(REQ_DATE));
        noOfLeaf.setText(map.get(NO_OF_LEAF));
        status.setText(map.get(STATUS));

        if(position % 2 == 1){
            convertView.setBackgroundColor(Color.parseColor("#F5F5F5"));
        }else{
            convertView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
        }

        return convertView;
    }
}
