package Adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.waranabank.mobipro.DetailedStatementActivity;
import com.waranabank.mobipro.MiniStatementActivity;
import com.waranabank.mobipro.R;


import java.util.ArrayList;

import Model.DetailedStatementResModel;

/**
 * Created by Samvid Mistry on 13/02/2016.
 */
public class DetailedStatementAdapter extends BaseAdapter {
    private Context mContext;
    private int resCount;
    public ArrayList<String> mDate, mNarration, mAmount, mTransactionCode;
    public ArrayList<Boolean> mIsCredit;

    public DetailedStatementAdapter(Context context, DetailedStatementResModel detailedStatementResModel)
    {
        mContext = context;
        resCount = detailedStatementResModel.getResponses().length;
        DetailedStatementResModel.Response[] responses = detailedStatementResModel.getResponses();
        mDate = new ArrayList<>();
        mNarration = new ArrayList<>();
        mAmount = new ArrayList<>();
        mIsCredit = new ArrayList<>();
        mTransactionCode = new ArrayList<>();

        for (int i = 0; i < resCount; i++)
        {
            DetailedStatementResModel.Response response = responses[i];
            mDate.add(response.getTransactionDate());
            mNarration.add(response.getNarration());
            mAmount.add(response.getTransactionAmount());
            mIsCredit.add(response.getCdDrType().equals("C"));
            mTransactionCode.add(response.getTrasnactionCode());
        }
    }

    @Override
    public int getCount() {
        return resCount;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(convertView == null)
        {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.view_statement_list, parent, false);
        }

        final int pos = position;
        TextView dateView = (TextView) convertView.findViewById(R.id.date_statementList);
        dateView.setText(mDate.get(position));
        final TextView narrationView = (TextView) convertView.findViewById(R.id.narration_statementList);
        narrationView.setText(mNarration.get(position));
        TextView amountView = (TextView) convertView.findViewById(R.id.amount_statementList);
//        amountView.setText("₹ "+ mAmount.get(position));
        amountView.setText("₹ " + mAmount.get(position) + " " + (mIsCredit.get(position) ? "Cr" : "Dr"));
        if(position % 2 == 1)
        {
            convertView.setBackgroundColor(Color.parseColor("#F5F5F5"));
        }
        else
        {
            convertView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(mContext);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.view_edit_narration_dialog);

                final EditText editText = (EditText) dialog.findViewById(R.id.narration_editNarrationDialog);
                final Button button = (Button) dialog.findViewById(R.id.done_editNarrationDialog);
                editText.setText(mNarration.get(pos));
                dialog.setOnDismissListener(
                        new DialogInterface.OnDismissListener()
                        {
                    @Override
                    public void onDismiss(DialogInterface dialog)
                    {
                        if(editText.getText().toString().trim().length() <= 0)
                        {
                            dialog.dismiss();
                            return;
                        }
                        mNarration.add(pos, editText.getText().toString());
                        narrationView.setText(mNarration.get(pos));
                    }
                });
                button.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if(editText.getText().toString().trim().length() <= 0){

                            return;
                        }
                        dialog.dismiss();
                        ((DetailedStatementActivity)mContext).requestUpdateNarration(mTransactionCode.get(pos),editText.getText().toString().trim());
                    }
                });
                dialog.show();
            }
        });

        return convertView;
    }
}
