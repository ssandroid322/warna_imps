package Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.util.SimpleArrayMap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.waranabank.mobipro.BranchDetailActivity;
import com.waranabank.mobipro.R;

import java.util.ArrayList;

import Model.BranchDetailsResModel;

/**
 * Created by Samvid Mistry on 04/03/2016.
 */
public class BranchesAdapter extends BaseAdapter {
    public static final String BRANCH_CD = "BRANCH_CD";
    public static final String BRANCH_NM = "BRANCH_NM";
    public static final String ADD1 = "ADD1";
    public static final String ADD2 = "ADD2";
    public static final String AREA_NM = "AREA_NM";
    public static final String PIN_CODE = "PIN_CODE";
    public static final String CITY_NM = "CITY_NM";
    public static final String STATE_NM = "STATE_NM";
    public static final String COUNTRY_NM = "COUNTRY_NM";
    public static final String CONTACT1 = "CONTACT1";
    public static final String CONTACT2 = "CONTACT2";
    public static final String EMAIL_ID = "EMAIL_ID";
    public static final String IFS_CODE = "IFSC_CODE";
    public static final String MICR_CODE = "MICR_CODE";

    private Context mContext;
    private ArrayList<SimpleArrayMap<String, String>> mMaps;


    public BranchesAdapter(Context context, BranchDetailsResModel branchDetailsResModel) {
        mContext = context;

        mMaps = new ArrayList<>();

        BranchDetailsResModel.Response[] responses = branchDetailsResModel.getResponse();
        for (BranchDetailsResModel.Response response : responses) {
            SimpleArrayMap<String, String> map = new SimpleArrayMap<>();
            map.put(BRANCH_CD, response.getBranchCode());
            map.put(BRANCH_NM, response.getBranchName());
            map.put(ADD1, response.getAddressOne());
            map.put(ADD2, response.getAddressTwo());
            map.put(AREA_NM, response.getAreaName());
            map.put(PIN_CODE, response.getPinCode());
            map.put(CITY_NM, response.getCityName());
            map.put(STATE_NM, response.getStateName());
            map.put(COUNTRY_NM, response.getCountryName());
            map.put(CONTACT1, response.getContactOne());
            map.put(CONTACT2, response.getContactTwo());
            map.put(EMAIL_ID, response.getEmailId());
            map.put(IFS_CODE, response.getIFSCode());
            map.put(MICR_CODE, response.getMICR_CODE());

            mMaps.add(map);
        }
    }

    @Override
    public int getCount() {
        return mMaps.size();
    }

    @Override
    public Object getItem(int position) {
        return mMaps.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.view_branch_name, parent, false);
        }

        final SimpleArrayMap<String, String> map = mMaps.get(position);
        TextView branchName = (TextView) convertView.findViewById(R.id.branchName_branchName);
        if(map.get(CITY_NM) == null || map.get(CITY_NM).length() <= 0){
            branchName.setText(map.get(BRANCH_NM));
        }else {
            branchName.setText(map.get(BRANCH_NM) + ", " + map.get(CITY_NM));
        }
        TextView pinCode = (TextView) convertView.findViewById(R.id.pinCode_branchName);
        pinCode.setText(map.get(PIN_CODE));
        TextView contact = (TextView) convertView.findViewById(R.id.contact_branchName);
        contact.setText(map.get(CONTACT1));

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, BranchDetailActivity.class);
                intent.putExtra(BRANCH_CD, map.get(BRANCH_CD));
                intent.putExtra(BRANCH_NM, map.get(BRANCH_NM));
                intent.putExtra(ADD1, map.get(ADD1));
                intent.putExtra(ADD2, map.get(ADD2));
                intent.putExtra(AREA_NM, map.get(AREA_NM));
                intent.putExtra(PIN_CODE, map.get(PIN_CODE));
                intent.putExtra(CITY_NM, map.get(CITY_NM));
                intent.putExtra(STATE_NM, map.get(STATE_NM));
                intent.putExtra(COUNTRY_NM, map.get(COUNTRY_NM));
                intent.putExtra(CONTACT1, map.get(CONTACT1));
                intent.putExtra(CONTACT2, map.get(CONTACT2));
                intent.putExtra(EMAIL_ID, map.get(EMAIL_ID));
                intent.putExtra(IFS_CODE, map.get(IFS_CODE));
                intent.putExtra(MICR_CODE, map.get(MICR_CODE));
                mContext.startActivity(intent);
            }
        });

        return convertView;
    }
}
