package Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.util.SimpleArrayMap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.waranabank.mobipro.R;


import java.util.ArrayList;

import Model.ChequeStatusInqResModel;

/**
 * Created by Samvid Mistry on 01/03/2016.
 */
public class SearchChequeAdapter extends BaseAdapter {
    private static final String CHEQUE_NO = "CHEQUE_NO";
    private static final String TRN_DATE = "TRN_DATE";
    private static final String AMOUNT = "AMOUNT";
    private static final String STATUS = "STATUS";
    private static final String REMARKS = "REMARKS";
    
    private Context mContext;
    private ArrayList<SimpleArrayMap<String, String>> mMaps;
    private int count;

    public SearchChequeAdapter(Context context, ChequeStatusInqResModel chequeStatusInqResModel) {
        mContext = context;
        mMaps = new ArrayList<>();

        ChequeStatusInqResModel.Response[] responses = chequeStatusInqResModel.getResponses();

        for (int i = 0; i < responses.length; i++) {
            ChequeStatusInqResModel.Response response = responses[i];
            SimpleArrayMap<String, String> map = new SimpleArrayMap<>();
            map.put(CHEQUE_NO, "1234567");
            map.put(TRN_DATE, response.getTransactionDate());
            map.put(AMOUNT, response.getAmount());
            map.put(STATUS, response.getChequeStatus());
            map.put(REMARKS, response.getRemarks());

            mMaps.add(map);
        }

        /*for (int i = 0; i < 10; i++) {
            SimpleArrayMap<String, String> map = new SimpleArrayMap<>();
            map.put(CHEQUE_NO, "1234567");
            map.put(TRN_DATE, "10-1-2016");
            map.put(AMOUNT, "5000");
            map.put(STATUS, "P");
            map.put(REMARKS, "Processed");

            mMaps.add(map);
        }*/

        count = mMaps.size();
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public Object getItem(int position) {
        return mMaps.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.view_cheque_detail, parent, false);
        }

        if(position % 2 == 1){
            convertView.setBackgroundColor(Color.parseColor("#F5F5F5"));
        }else{
            convertView.setBackgroundColor(Color.WHITE);
        }

        TextView mChequeNo = (TextView) convertView.findViewById(R.id.chequeNo_chequeDetail);
        TextView mAmount = (TextView) convertView.findViewById(R.id.amount_chequeDetail);
        TextView mDate = (TextView) convertView.findViewById(R.id.date_chequeDetail);
        TextView mStatus = (TextView) convertView.findViewById(R.id.status_chequeDetail);

        SimpleArrayMap<String, String> map = mMaps.get(position);

        mChequeNo.setText(map.get(CHEQUE_NO));
        mAmount.setText(map.get(AMOUNT));
        mDate.setText(map.get(TRN_DATE));
        mStatus.setText(map.get(REMARKS));

        return convertView;
    }
}
