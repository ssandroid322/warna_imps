package Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.util.SimpleArrayMap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.waranabank.mobipro.R;


import java.util.ArrayList;

import Model.ChequeBookDetailsResModel;

/**
 * Created by Samvid Mistry on 25/02/2016.
 */
public class ChequeBookIssuesAdapterOld extends BaseAdapter {
    private static final String ISSUE_DATE = "ISSUE_TYPE";
    private static final String FROM_CHEQUE = "FROM_CHEQUE";
    private static final String TO_CHEQUE = "TO_CHEQUE";
    private static final String CHEQUE_TOTAL = "CHEQUE_TOTAL";
    private static final String NO_OF_USED = "NO_OF_USED";
    private static final String CHARACTERISTICS = "CHARACTERISTICS";
    private static final String PAYABLE_AT_PAR = "PAYABLE_AT_PAR";
    
    private Context mContext;
    private int mResCount;
    private ChequeBookDetailsResModel mChequeBookDetailsResModel;
    private ArrayList<SimpleArrayMap<String, String>> mCheques;

    public ChequeBookIssuesAdapterOld(Context context, ChequeBookDetailsResModel chequeBookDetailsResModel) {
        mContext = context;
        mChequeBookDetailsResModel = chequeBookDetailsResModel;
        mCheques = new ArrayList<>();

        ChequeBookDetailsResModel.Response[] responses = chequeBookDetailsResModel.getResponse();
        mResCount = responses.length;

        for (int i = 0; i < mResCount; i++) {
            ChequeBookDetailsResModel.Response response = responses[i];
            SimpleArrayMap<String, String> stringStringSimpleArrayMap = new SimpleArrayMap<>();
            stringStringSimpleArrayMap.put(ISSUE_DATE, response.getIssueDate());
            stringStringSimpleArrayMap.put(FROM_CHEQUE, response.getFromCheque());
            stringStringSimpleArrayMap.put(TO_CHEQUE, response.getToCheque());
            stringStringSimpleArrayMap.put(CHEQUE_TOTAL, response.getChequeTotal());
            stringStringSimpleArrayMap.put(NO_OF_USED, response.getNumberOfUsedCheques());
            stringStringSimpleArrayMap.put(CHARACTERISTICS, response.getCharacteristics());
            stringStringSimpleArrayMap.put(PAYABLE_AT_PAR, response.getPayableAtPar());
            mCheques.add(stringStringSimpleArrayMap);
        }
    }

    @Override
    public int getCount() {
        return mResCount;
    }

    @Override
    public Object getItem(int position) {
        return mCheques.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.view_cheque_issue, parent, false);
        }

        ViewHolder viewHolder = (ViewHolder) convertView.getTag();

        if(viewHolder == null){
            viewHolder = new ViewHolder();
            viewHolder.mRemarks = (TextView) convertView.findViewById(R.id.remarks_chequeIssue);
            convertView.setTag(viewHolder);
        }

        SimpleArrayMap<String, String> data = mCheques.get(position);

        viewHolder.mSrNo.setText(String.valueOf(position + 1));
        viewHolder.mIssueDate.setText(data.get(ISSUE_DATE));
        viewHolder.mFromChq.setText(data.get(FROM_CHEQUE));
        viewHolder.mToChq.setText(data.get(TO_CHEQUE));
        viewHolder.mTotalChq.setText(data.get(CHEQUE_TOTAL));
        viewHolder.mRemarks.setText(data.get(CHARACTERISTICS));

        if(position % 2 == 1){
            convertView.setBackgroundColor(Color.parseColor("#F5F5F5"));
        }else{
            convertView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
        }

        convertView.setTag(viewHolder);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    private static class ViewHolder{
        TextView mSrNo, mIssueDate, mFromChq, mToChq, mTotalChq, mRemarks;
    }
}
