package Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.waranabank.mobipro.BaseActivity;
import com.waranabank.mobipro.MiniStatementActivity;
import com.waranabank.mobipro.R;


import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Interface.onReply;
import Model.BaseModel;
import Model.MiniStatementReqModel;
import Model.MiniStatementResModel;
import Model.UpdateNarrationReqModel;
import Model.UpdateNarrationResModel;
import Parser.BaseParser;
import Parser.UpdateNarrationParser;
import Request.MiniStatementRequest;
import Request.UpdateNarrationRequest;
import Utility.ErrorMessage;
import Utility.MyEnum;
import Utility.MySharedPreferences;

/**
 * Created by Samvid Mistry on 11/02/2016.
 */
public class MiniStatementAdapter extends BaseAdapter implements onReply {
    private Activity mContext;
    private int resCount;
    public ArrayList<String> mDate, mNarration, mAmount , mTransactionCode;
    public ArrayList<Boolean> mIsCredit;
    View view ;

    public MiniStatementAdapter(Activity context, MiniStatementResModel miniStatementResModel)
    {
        mContext = context;
        resCount = miniStatementResModel.getResponses().length;
        MiniStatementResModel.Response[] responses = miniStatementResModel.getResponses();
        mDate = new ArrayList<>();
        mNarration = new ArrayList<>();
        mAmount = new ArrayList<>();
        mIsCredit = new ArrayList<>();
        mTransactionCode = new ArrayList<>();

        for (int i = 0; i < resCount; i++) {
            MiniStatementResModel.Response response = responses[i];
            mDate.add(response.getTransactionDate());
            mNarration.add(response.getNarration());
            mAmount.add(response.getTransactionAmount());
            mIsCredit.add(response.getCdDrType().equals("C"));
            mTransactionCode.add(response.getTrasnactionCode());
        }
    }

    @Override
    public int getCount() {
        return resCount;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.view_statement_list, parent, false);
        }

        final int pos = position;

        TextView dateView = (TextView) convertView.findViewById(R.id.date_statementList);
        dateView.setText(mDate.get(position));
        final TextView narrationView = (TextView) convertView.findViewById(R.id.narration_statementList);
        narrationView.setText(mNarration.get(position));
        TextView amountView = (TextView) convertView.findViewById(R.id.amount_statementList);
        amountView.setText("₹ " + mAmount.get(position) + " " + (mIsCredit.get(position) ? "Cr" : "Dr"));
        if(position % 2 == 1){
            convertView.setBackgroundColor(Color.parseColor("#F5F5F5"));
        }else{
            convertView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
        }

        convertView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                view = v;

                final Dialog dialog = new Dialog(mContext);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.view_edit_narration_dialog);
                final EditText editText = (EditText) dialog.findViewById(R.id.narration_editNarrationDialog);
                final Button button = (Button) dialog.findViewById(R.id.done_editNarrationDialog);
                editText.setText(mNarration.get(pos));
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog)
                    {
                        if(editText.getText().toString().trim().length() <= 0)
                        {
                            return;
                        }
                        mNarration.add(pos, editText.getText().toString());
                        narrationView.setText(mNarration.get(pos));
                    }
                });
                button.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if(editText.getText().toString().trim().length() <= 0)
                        {
                            dialog.dismiss();
                            return;
                        }
//                        requestUpdateNarration(" ",editText.getText().toString().trim());
                        dialog.dismiss();

                        ((MiniStatementActivity)mContext).requestUpdateNarration(mTransactionCode.get(pos),editText.getText().toString().trim());
                    }
                });
                dialog.show();
            }
        });

        return convertView;
    }
   /* public void requestUpdateNarration(String transactionCode,String narration)
    {
        try
        {
            UpdateNarrationReqModel updateNarrationReqModel = new UpdateNarrationReqModel(MySharedPreferences.getUserId(mContext),
                    MySharedPreferences.getActivityCode(mContext),
                    transactionCode,
                    narration);

            UpdateNarrationRequest updateNarrationRequest= new UpdateNarrationRequest(this , MyEnum.displayProgress.Show);
            updateNarrationRequest.sendRequest(this, updateNarrationReqModel);
        }
        catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException e) {
            e.printStackTrace();
        }

    }*/

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser)
    {
        UpdateNarrationResModel superModel = (UpdateNarrationResModel) baseParser.doParsing(objJson);

        UpdateNarrationResModel updateNarrationResModel = (UpdateNarrationResModel) superModel.getModelArray().get(0);

         if (updateNarrationResModel.getStatus() == UpdateNarrationRequest.SUCCESSFUL)
        {
            displayErrorMessage(view, "Narration updated successfully.");
        }
        else
        {
            displayErrorMessage(view, "Error in updating narration.Please try again.");
        }
    }

    @Override
    public void networkConnectionError(BaseModel reqModel)
    {
        displayErrorMessage(view, ErrorMessage.getNetworkConnectionError());
    }

    protected void displayErrorMessage(View view,String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
    }
    public void onSessionExpired()
    {

    }

    @Override
    public void onRejectedService(String msg) {

    }

    @Override
    public void onRejectedServiceFinishActivity(String msg) {

    }

    @Override
    public void updateDetail()
    {


    }
}
