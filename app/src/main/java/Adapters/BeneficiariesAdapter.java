package Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.util.SimpleArrayMap;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.waranabank.mobipro.R;

import java.util.ArrayList;

import Interface.OnBeneficiaryDeleteRequested;
import ViewHolders.BeneficiariesViewHolder;

/**
 * Created by Samvid Mistry on 23/02/2016.
 */
public class BeneficiariesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final String TRN_TYPE = "TRN_TYPE";
    public static final String TO_IFSCCODE = "TO_IFSCCODE";
    public static final String TO_ACCT_NO = "TO_ACCT_NO";
    public static final String TO_ACCT_NM = "TO_ACCT_NM";
    public static final String TO_ADD1 = "TO_ADD1";
    public static final String TO_CONTACT_NO = "TO_CONTACT_NO";

    private static final int VIEW_HEADER = 0;
    private static final int VIEW_NORMAL = 1;

    private Context mContext;
    private ArrayList<SimpleArrayMap<String, String>> mSimpleArrayMaps;
    private SparseBooleanArray mSparseBooleanArray;
    private OnBeneficiaryDeleteRequested mOnBeneficiaryDeleteRequested;

    public BeneficiariesAdapter(Context context, ArrayList<SimpleArrayMap<String, String>> simpleArrayMaps, OnBeneficiaryDeleteRequested onBeneficiaryDeleteRequested) {
        mContext = context;
        mSimpleArrayMaps = simpleArrayMaps;
        mOnBeneficiaryDeleteRequested = onBeneficiaryDeleteRequested;

        mSparseBooleanArray = new SparseBooleanArray();

        for (int i = 0; i < simpleArrayMaps.size(); i++) {
            mSparseBooleanArray.put(i, false);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BeneficiariesViewHolder(LayoutInflater.from(mContext).inflate(R.layout.view_beneficiaries, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof BeneficiariesViewHolder){
            final BeneficiariesViewHolder viewHolder = (BeneficiariesViewHolder) holder;
            SimpleArrayMap<String, String> map = mSimpleArrayMaps.get(viewHolder.getAdapterPosition());
            viewHolder.mBeneficiaryName.setText(map.get(TO_ACCT_NM));
            viewHolder.mIfscCode.setText(map.get(TO_IFSCCODE));
            //hide - unhide here
            if(map.get(TRN_TYPE).equals("I")){
                viewHolder.mTransactionType.setText("IFT");
            }else if(map.get(TRN_TYPE).equals("R")){
                viewHolder.mTransactionType.setText("NEFT/RTGS/IMPS");
            }else if(map.get(TRN_TYPE).equals("N")){
                viewHolder.mTransactionType.setText("NEFT/RTGS/IMPS");
            } else if(map.get(TRN_TYPE).equalsIgnoreCase("O")){
                viewHolder.mTransactionType.setText("NEFT/RTGS/IMPS");
            }
            viewHolder.mAccountNo.setText(map.get(TO_ACCT_NO));
            viewHolder.mContactNo.setText(map.get(TO_CONTACT_NO));
            viewHolder.mAddressOne.setText(map.get(TO_ADD1));

            viewHolder.mDeleteBeneficiary.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setTitle("Delete Beneficiary?").setMessage("Are you sure you want to delete this beneficiary?");
                    builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            mOnBeneficiaryDeleteRequested.onDeleteBeneficiary(mSimpleArrayMaps.get(viewHolder.getAdapterPosition()), viewHolder.getAdapterPosition());
                        }
                    });
                    builder.setNegativeButton("Return", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    builder.create().show();
                }
            });

            if(mSparseBooleanArray.get(position)){
                viewHolder.mBottomLayout.setVisibility(View.VISIBLE);
            }else{
                viewHolder.mBottomLayout.setVisibility(View.GONE);
            }

            viewHolder.mShowMoreLess.setImageResource(mSparseBooleanArray.get(viewHolder.getAdapterPosition()) ? R.drawable.vector_drawable_show_less : R.drawable.vector_drawable_show_more);

            viewHolder.mShowMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewHolder.mBottomLayout.setVisibility(viewHolder.mBottomLayout.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                    mSparseBooleanArray.put(viewHolder.getAdapterPosition(), !mSparseBooleanArray.get(viewHolder.getAdapterPosition()));
                    viewHolder.mShowMoreLess.setImageResource(mSparseBooleanArray.get(viewHolder.getAdapterPosition()) ? R.drawable.vector_drawable_show_less : R.drawable.vector_drawable_show_more);
                }
            });
        }
    }

    public void removeItem(int position){
        mSimpleArrayMaps.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public int getItemCount() {
        return mSimpleArrayMaps.size();
    }
}
