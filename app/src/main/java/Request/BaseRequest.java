package Request;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URL;

import Interface.onReply;
import Model.BaseModel;
import Parser.BaseParser;
import Utility.MyEnum;
import Utility.ShowProgressbar;

/**
 * Documentation by Samvid:
 * A base class for all kind of requests sent from application. It handles all the boilerplate code for setting up a request and its
 * parameters. It uses {@link AsyncTask} for sending a request off the main thread. It currently uses single thread execution
 * for all of its requests. To use this class, just extend this class and provide implementation of sendRequest() method. To send
 * request from that method call sendRequestToServer(String json) with the json data you want to send. You can receive the json
 * response and the json parser extending BaseParser in onPopulate method of your {@link BaseRequest} or
 * it's subclass if the result was retrieved successfully.
 * <p/>
 * Created by chitan on 10/17/2015.
 */


public abstract class BaseRequest extends AsyncTask<String, Void, MyEnum.requestError> {
    public static final String OUTPUT = "output";
    public static final int INVALID_CODE = 9;
    public static final int ERROR_MESSAGE_CODE = 99;
    public static final int ERROR_MESSAGE_CODE_FINISH_ACTIVITY= 999;
    public static final String STATUS = "STATUS";
    public static final String MESSAGE = "MESSAGE";
    public static final String MESSAGES = "MESSAGES";

    public abstract void sendRequest(onReply objReplyMethod, BaseModel model);

    public String URL;
    private String imageURL;
    protected Bitmap image;
    protected JSONObject jsonObject;
    protected BaseParser objParser;
    protected onReply onReplyDelegate;
    protected BaseModel reqModel;
    public Activity activityProgress;
    MyEnum.displayProgress objDisplayEnum;
    MyEnum.isMasking objIsMasking;

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        try {
            URL url = new URL(imageURL);
            URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
            imageURL = uri.toASCIIString();
            this.imageURL = imageURL;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void sendRequestToServer(String jsonString) {
        Log.e("Json ", jsonString);
//        objIsMasking = MyEnum.isMasking.mask;
        this.execute(jsonString);
    }

    protected void sendImageToServerASync() {
        new SendImageASync().execute();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (objDisplayEnum == MyEnum.displayProgress.Show) {
            if (objIsMasking == MyEnum.isMasking.notmask) {
                ShowProgressbar.showProgress(activityProgress, "Loading ....");
            } else {
                ShowProgressbar.showMaskProgress(activityProgress, "Loading.....");
            }
        }
    }

    @Override
    protected MyEnum.requestError doInBackground(String... jsonStrings) {
        MyEnum.requestError errorMessage = MyEnum.requestError.completeData;
        try {
            // Create Request
            int timeOutConnection = 60000;

            URI uri = new URI(URL);
            HttpURLConnection conn = (HttpURLConnection) uri.toURL().openConnection();
            conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("androidversion", "1.1");
            conn.setRequestMethod("POST");
            conn.setChunkedStreamingMode(0);
            conn.setConnectTimeout(timeOutConnection);
            conn.setReadTimeout(timeOutConnection);
            conn.setDoInput(true);
            conn.setDoOutput(true);

            conn.connect();

            DataOutputStream out = new DataOutputStream(conn.getOutputStream());
            out.write(jsonStrings[0].getBytes());
            out.flush();



            int code = conn.getResponseCode();

            if (code == HttpURLConnection.HTTP_OK) {
                InputStream objInputStream = conn.getInputStream();

                BufferedReader reader = new BufferedReader(new InputStreamReader(objInputStream));

                StringBuilder responseData = new StringBuilder();
                try {
                    String line;
                    while ((line = reader.readLine()) != null) {
                        responseData.append(line).append("\n");
                    }
                    Log.e("Response Json", responseData.toString());
                } finally {
                    objInputStream.close();
                }
                Log.i("Response", responseData.toString());
                jsonObject = new JSONObject();
                jsonObject.put(OUTPUT, new JSONArray(responseData.toString()));
            } else {
                errorMessage = MyEnum.requestError.connectionError;
            }
        } catch (SocketTimeoutException e) {
            Log.e("exception from socket", "" + e);
            errorMessage = MyEnum.requestError.connectionError;
        } catch (Exception e) {
            Log.e("exception from socket", "" + e);
            jsonObject = null;
            errorMessage = MyEnum.requestError.connectionError;
        }
        return errorMessage;
    }

    protected void onPostExecute(MyEnum.requestError result) {
        super.onPostExecute(result);
        if (objDisplayEnum == MyEnum.displayProgress.Show) {
            ShowProgressbar.dismissDialog();
        } else if (objDisplayEnum == MyEnum.displayProgress.Backshow) {
            Log.i("back show", "temp");
        }
        if (result == MyEnum.requestError.completeData && jsonObject != null) {
            try {
                if (jsonObject.getJSONArray(OUTPUT).getJSONObject(0).optInt(STATUS) == INVALID_CODE) {
                    onReplyDelegate.onSessionExpired();
                    return;
                } else if (jsonObject.getJSONArray(OUTPUT).getJSONObject(0).optInt(STATUS) == ERROR_MESSAGE_CODE) {
                    String msg = jsonObject.getJSONArray(OUTPUT).getJSONObject(0).optString(MESSAGE);
                    if(msg.equals("")){
                        msg = jsonObject.getJSONArray(OUTPUT).getJSONObject(0).optString(MESSAGES);
                    }
                    onReplyDelegate.onRejectedService(msg);
                    return;
                }else if (jsonObject.getJSONArray(OUTPUT).getJSONObject(0).optInt(STATUS) == ERROR_MESSAGE_CODE_FINISH_ACTIVITY) {
                    String msg = jsonObject.getJSONArray(OUTPUT).getJSONObject(0).optString(MESSAGE);
                    if(msg.equals("")){
                        msg = jsonObject.getJSONArray(OUTPUT).getJSONObject(0).optString(MESSAGES);
                    }
                    onReplyDelegate.onRejectedServiceFinishActivity(msg);
                    return;
                }

                if (onReplyDelegate != null) {
                    onReplyDelegate.onPopulate(jsonObject, objParser);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (result == MyEnum.requestError.updateapplication) {
            onReplyDelegate.updateDetail();
        } else {
            if (onReplyDelegate != null)
                onReplyDelegate.networkConnectionError(reqModel);
        }
    }

    // Send Image ASYNC Class Request
    class SendImageASync extends AsyncTask<String, Void, MyEnum.requestError> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (objDisplayEnum == MyEnum.displayProgress.Show) {
                if (objIsMasking == MyEnum.isMasking.notmask) {
                    ShowProgressbar.showProgress(activityProgress, "Loading ....");
                } else {
                    ShowProgressbar.showMaskProgress(activityProgress, "Loading.....");
                }
            }
        }

        @Override
        protected MyEnum.requestError doInBackground(String... jsonStrings) {
            MyEnum.requestError errorMessage = MyEnum.requestError.completeData;
            try {
                int timeoutconnection = 120000;
                URI uri = new URI(imageURL);

                HttpURLConnection conn = (HttpURLConnection) uri.toURL().openConnection();
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestMethod("POST");
                conn.setRequestProperty("androidversion", "1.1"); //added by arpit
                conn.setChunkedStreamingMode(1024);
                conn.setConnectTimeout(timeoutconnection);
                conn.setReadTimeout(timeoutconnection);
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("image", "my.jpg");

                conn.connect();

                DataOutputStream outputStream = new DataOutputStream(conn.getOutputStream());

//                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.hd1);
                if (image != null) {
                    image.compress(Bitmap.CompressFormat.JPEG, 75, outputStream);
                }


                outputStream.flush();
                outputStream.close();

                int code = conn.getResponseCode();

                if (code == HttpURLConnection.HTTP_OK) {
                    InputStream objInputStream = conn.getInputStream();

                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            objInputStream));

                    StringBuilder responseData = new StringBuilder();
                    try {
                        String line;
                        while ((line = reader.readLine()) != null) {
                            responseData.append(line).append("\n");
                        }
                        Log.e("Response Json", responseData.toString());
                        jsonObject = new JSONObject(responseData.toString());
                    } finally {

                        objInputStream.close();
                    }
                } else {
                    errorMessage = MyEnum.requestError.updateapplication;
                }
            } catch (OutOfMemoryError e) {
                Log.e("OutOfMemoryError", e.toString());
                errorMessage = MyEnum.requestError.connectionError;
            } catch (SocketTimeoutException e) {
                Log.e("exeption from socket", "" + e);
                errorMessage = MyEnum.requestError.connectionError;
            } catch (Exception e) {
                jsonObject = null;
                errorMessage = MyEnum.requestError.connectionError;
            }
            return errorMessage;
        }

        protected void onPostExecute(MyEnum.requestError result) {
            super.onPostExecute(result);

            if (objDisplayEnum == MyEnum.displayProgress.Show) {
                ShowProgressbar.dismissDialog();
            } else if (objDisplayEnum == MyEnum.displayProgress.Backshow) {
                Log.i("back show", "temp");
            }
            if (result == MyEnum.requestError.completeData && jsonObject != null) {
                if (onReplyDelegate != null)
                    onReplyDelegate.onPopulate(jsonObject, objParser);
            } else if (result == MyEnum.requestError.updateapplication) {
                onReplyDelegate.updateDetail();
            } else {
                if (onReplyDelegate != null)
                    onReplyDelegate.networkConnectionError(reqModel);
            }
        }
    }
}