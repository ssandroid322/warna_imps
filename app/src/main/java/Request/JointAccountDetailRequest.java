package Request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import Interface.onReply;
import Model.BaseModel;
import Model.JointAccountDetailsReqModel;
import Parser.JointAccountDetailsParser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class JointAccountDetailRequest extends BaseRequest {
    private static final String TAG = "JointAccountDetlRequest";
    public static final int SUCCESS = 0;
    public static final int NO_JOINT_ACCOUNT = 1;

    public JointAccountDetailRequest(Activity activity, MyEnum.displayProgress doShow){
        this.activityProgress = activity;
        this.objDisplayEnum = doShow;
        this.objIsMasking = MyEnum.isMasking.mask;
    }

    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model) {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new JointAccountDetailsParser();

        this.URL = URLGenerator.getJointAccountDetailRequest();

        Gson gson = new Gson();

        String json = gson.toJson(model, JointAccountDetailsReqModel.class);

        Log.i(TAG, "sendRequest: json is -> " + json);

        sendRequestToServer(json);
    }
}
