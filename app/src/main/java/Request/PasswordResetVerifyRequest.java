package Request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import Interface.onReply;
import Model.BaseModel;
import Model.PasswordResetVerifyReqModel;
import Parser.PasswordResetVerifyParser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class PasswordResetVerifyRequest extends BaseRequest {
    private static final String TAG = "PasswordResetVerRequest";
    public static final int SUCCESS = 0;
    public static final int DATA_NOT_VALID = 1;

    public PasswordResetVerifyRequest(Activity activity, MyEnum.displayProgress doShow){
        this.activityProgress = activity;
        this.objDisplayEnum = doShow;
        this.objIsMasking = MyEnum.isMasking.mask;
    }

    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model) {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new PasswordResetVerifyParser();

        this.URL = URLGenerator.getResetPasswordVerifyRequest();

        Gson gson = new Gson();

        String json = gson.toJson(model, PasswordResetVerifyReqModel.class);

        Log.i(TAG, "sendRequest: json is -> " + json);

        sendRequestToServer(json);
    }
}
