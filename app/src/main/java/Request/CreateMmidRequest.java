package Request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import Interface.onReply;
import Model.BaseModel;
import Model.CreateMmidReqModel;
import Parser.CreateMmidParser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by samvidmistry on 30/8/16.
 */

public class CreateMmidRequest extends BaseRequest {
    private static final String TAG = "CreateMmidRequest";
    public static final int SUCCESS = 0;
    public static final int INVALID_NUMBER = 2;
    public static final int PROCESSING_ERROR = 3;
    public static final int NCPI_REJECT = 5;

    public CreateMmidRequest(Activity activity, MyEnum.displayProgress doShow){
        this.activityProgress = activity;
        this.objDisplayEnum = doShow;
        this.objIsMasking = MyEnum.isMasking.mask;
    }

    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model) {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new CreateMmidParser();

        this.URL = URLGenerator.getCreateMmidRequest();

        Gson gson = new Gson();

        String json = gson.toJson(model, CreateMmidReqModel.class);

        Log.i(TAG, "sendRequest: json is -> " + json);

        sendRequestToServer(json);
    }
}
