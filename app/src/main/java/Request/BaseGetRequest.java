package Request;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.v4.util.SimpleArrayMap;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URI;

import Interface.onReply;
import Model.BaseModel;
import Parser.BaseParser;
import Utility.MyEnum;
import Utility.ShowProgressbar;

/**
 * Created by Samvid Mistry on 04/03/2016.
 */
public abstract class BaseGetRequest extends AsyncTask<Void, Void, MyEnum.requestError> {
    public static final String OUTPUT = "output";
    public static final int INVALID_CODE = 9;
    public static final String STATUS = "STATUS";

    public abstract void sendRequest(onReply replyInterface);

    public String URL;
    protected BaseParser objParser;
    protected onReply onReplyDelegate;
    protected BaseModel reqModel;
    protected JSONObject jsonObject;
    protected Activity activityProgress;
    protected MyEnum.displayProgress objDisplayEnum;
    protected MyEnum.isMasking objIsMasking;

    protected void sendRequestToServer(SimpleArrayMap<String, String> params){
        if(params != null && params.size() <= 0)
            for (int i = 0; i < params.size(); i++) {
                if(i == 0){
                    URL += "?";
                }
                URL += params.keyAt(i) + "=" + params.get(params.keyAt(i));
            }

        this.execute();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if(objDisplayEnum == MyEnum.displayProgress.Show){
            if(objIsMasking == MyEnum.isMasking.notmask){
                ShowProgressbar.showProgress(activityProgress, "Loading...");
            }else{
                ShowProgressbar.showMaskProgress(activityProgress, "Loading...");
            }
        }
    }

    @Override
    protected MyEnum.requestError doInBackground(Void... params) {
        MyEnum.requestError errorMessage = MyEnum.requestError.completeData;

        try
        {
            // Create Request
            int timeOutConnection = 20000;

            URI uri = new URI(URL);
            HttpURLConnection conn = (HttpURLConnection) uri.toURL().openConnection();

            conn.connect();

            int code = conn.getResponseCode();

            if (code == HttpURLConnection.HTTP_OK)
            {
                InputStream objInputStream = conn.getInputStream();

                BufferedReader reader = new BufferedReader(new InputStreamReader(objInputStream));

                StringBuilder responseData = new StringBuilder();
                try
                {
                    String line;
                    while ((line = reader.readLine()) != null)
                    {
                        responseData.append(line).append("\n");
                    }
                    Log.e("Response Json", responseData.toString());
                }
                finally
                {
                    objInputStream.close();
                }
                Log.i("Response", responseData.toString());
                jsonObject = new JSONObject();
                jsonObject.put(OUTPUT, new JSONArray(responseData.toString()));
            }
            else
            {
                errorMessage = MyEnum.requestError.connectionError;
            }
        }
        catch (SocketTimeoutException e)
        {
            Log.e("exception from socket", "" + e);
            errorMessage = MyEnum.requestError.connectionError;
        }
        catch (Exception e)
        {
            Log.e("exception from socket", "" + e);
            jsonObject = null;
            errorMessage = MyEnum.requestError.connectionError;
        }
        return errorMessage;
    }

    protected void onPostExecute(MyEnum.requestError result)
    {
        super.onPostExecute(result);
        if (objDisplayEnum == MyEnum.displayProgress.Show)
        {
            ShowProgressbar.dismissDialog();
        }
        else if (objDisplayEnum == MyEnum.displayProgress.Backshow)
        {
            Log.i("back show", "temp");
        }
        if (result == MyEnum.requestError.completeData && jsonObject != null)
        {
            try {
                if(jsonObject.getJSONArray(OUTPUT).getJSONObject(0).optInt(STATUS) == INVALID_CODE){
                    onReplyDelegate.onSessionExpired();
                    return;
                }

                if(onReplyDelegate != null) {
                    onReplyDelegate.onPopulate(jsonObject, objParser);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else if (result == MyEnum.requestError.updateapplication)
        {
            onReplyDelegate.updateDetail();
        }
        else
        {
            if(onReplyDelegate != null)
                onReplyDelegate.networkConnectionError(reqModel);
        }
    }
}
