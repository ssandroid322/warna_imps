package Request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import Interface.onReply;
import Model.BaseModel;
import Model.GetNoOfLeafsReqModel;
import Model.GetScreenMsg_ReqModel;
import Model.GetScreenMsg_ResModel;
import Parser.GetScreenMsg_Parser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by admin on 08/12/2016.
 */

public class GetScreenMsg_Request extends BaseRequest {

    private static final String TAG = "GetScreenMsg_Request";
    public static final String SUCCESS = "0";
//    public static final int NOT_FOUND = 1;


    public GetScreenMsg_Request(Activity activity) {
        this.activityProgress = activity;
        this.objDisplayEnum = MyEnum.displayProgress.NotShow;
    }

    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model) {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new GetScreenMsg_Parser();

        this.URL = URLGenerator.getScreenMsgUrl();

        Gson gson = new Gson();

        String json = gson.toJson(model, GetScreenMsg_ReqModel.class);

        Log.i(TAG, "sendRequest: json is -> " + json);

        sendRequestToServer(json);
    }
}
