package Request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import Interface.onReply;
import Model.BaseModel;
import Model.BlockCardVerifyReqModel;
import Model.FundTransferVerifyReqModel;
import Parser.BlockCardVerifyParser;
import Parser.FundTransferVerifyParser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class BlockCardVerifyRequest extends BaseRequest {
    private static final String TAG = "BlockCardVerifyRequest";
    public static final int SUCCESS = 0;
    public static final int REQ_NOT_FOUND = 1;

    public BlockCardVerifyRequest(Activity activity, MyEnum.displayProgress doShow){
        this.activityProgress = activity;
        this.objDisplayEnum = doShow;
        this.objIsMasking = MyEnum.isMasking.mask;
    }

    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model) {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new BlockCardVerifyParser();

        this.URL = URLGenerator.getBlockCardVerifyRequest();

        Gson gson = new Gson();

        String json = gson.toJson(model, BlockCardVerifyReqModel.class);

        Log.i(TAG, "sendRequest: json is -> " + json);

        sendRequestToServer(json);
    }
}