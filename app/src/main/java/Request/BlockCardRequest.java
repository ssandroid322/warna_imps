package Request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import Interface.onReply;
import Model.BaseModel;
import Model.BlockCardReqModel;
import Model.ChequeBookReqModel;
import Parser.BlockCardParser;
import Parser.ChequeBookParser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class BlockCardRequest extends BaseRequest {
    private static final String TAG = "BlockCardRequest";
    public static final int SUCCESS = 0;
    public static final int NOT_FOUND = 1;

    public BlockCardRequest(Activity activity, MyEnum.displayProgress doShow){
        this.activityProgress = activity;
        this.objDisplayEnum = doShow;
        this.objIsMasking = MyEnum.isMasking.mask;
    }

    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model) {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new BlockCardParser();

        this.URL = URLGenerator.getBlockCardRequest();

        Gson gson = new Gson();

        String json = gson.toJson(model, BlockCardReqModel.class);

        Log.i(TAG, "sendRequest: json is -> " + json);

        sendRequestToServer(json);
    }
}