package Request;

import android.app.Activity;

import Interface.onReply;
import Parser.BranchDetailParser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class BranchDetailRequest extends BaseGetRequest {
    private static final String TAG = "BranchDetailRequest";
    public static final int SUCCESS = 0;

    public BranchDetailRequest(Activity activity, MyEnum.displayProgress doShow){
        this.activityProgress = activity;
        this.objDisplayEnum = doShow;
        this.objIsMasking = MyEnum.isMasking.mask;
    }

    @Override
    public void sendRequest(onReply objReplyMethod) {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new BranchDetailParser();

        this.URL = URLGenerator.getBranchDetails();

        sendRequestToServer(null);
    }
}
