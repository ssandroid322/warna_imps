package Request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import Interface.onReply;
import Model.BaseModel;
import Model.SignUpS2ReqModel;
import Parser.SignUpS2Parser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by Samvid Mistry on 10/02/2016.
 */
public class SignUpS2Request extends BaseRequest {
    private static final String TAG = "SignUpS2Request";
    public static final int SUCCESSFUL = 0;

    public SignUpS2Request(Activity activity, MyEnum.displayProgress doShow){
        this.activityProgress = activity;
        this.objDisplayEnum = doShow;
        this.objIsMasking = MyEnum.isMasking.mask;
    }

    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model) {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new SignUpS2Parser();

        this.URL = URLGenerator.getUserActivationUrl();

        Gson gson = new Gson();

        String json = gson.toJson(model, SignUpS2ReqModel.class);

        Log.i(TAG, "sendRequest: json is -> " + json);

        sendRequestToServer(json);
    }
}
