package Request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import Interface.onReply;
import Model.BaseModel;
import Model.FundTransferP2PReqModel;
import Model.FundTransferReqModel;
import Parser.FundTransferP2PParser;
import Parser.FundTransferParser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class FundTransferP2PRequest extends BaseRequest {
    private static final String TAG = "FundTransferRequest";
    public static final int SUCCESSFUL = 0;
    public static final int INSUFFICIENT_BAL = 1;
    public static final int NOT_REG_FUND_TRANSFER = 2;

    public FundTransferP2PRequest(Activity activity, MyEnum.displayProgress doShow){
        this.activityProgress = activity;
        this.objDisplayEnum = doShow;
        this.objIsMasking = MyEnum.isMasking.mask;
    }

    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model) {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new FundTransferP2PParser();

        this.URL = URLGenerator.getFundTransfer_P2PRequest();

        Gson gson = new Gson();

        String json = gson.toJson(model, FundTransferP2PReqModel.class);

        Log.i(TAG, "sendRequest: json is -> " + json);

        sendRequestToServer(json);
    }
}
