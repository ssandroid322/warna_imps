package Request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import Interface.onReply;
import Model.AddBeneficiaryReqModel;
import Model.BaseModel;
import Model.ChangeUsernameReqModel;
import Parser.AddBeneficiaryParser;
import Parser.ChangeUsername_Parser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by admin on 06/12/2016.
 */

public class ChangeUsername_Request extends BaseRequest {
    private static final String TAG = "AddBeneficiaryRequest";
    public static final String SUCCESS = "0";
    public static final String ERROE = "99";

    public ChangeUsername_Request(Activity activity, MyEnum.displayProgress doShow){
        this.activityProgress = activity;
        this.objDisplayEnum = doShow;
        this.objIsMasking = MyEnum.isMasking.mask;
    }

    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model) {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new ChangeUsername_Parser();

        this.URL = URLGenerator.getUpdatesernameurl();

        Gson gson = new Gson();

        String json = gson.toJson(model, ChangeUsernameReqModel.class);

        Log.i(TAG, "sendRequest: json is -> " + json);

        sendRequestToServer(json);
    }
}
