package Request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import Interface.onReply;
import Model.BaseModel;
import Model.ChequeStopReqModel;
import Model.ChequeStopVerifyReqModel;
import Parser.ChequeStopParser;
import Parser.ChequeStopverifyParser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by admin on 26/10/2016.
 */
public class ChequeStopVerifyRequest extends BaseRequest {

    private static final String TAG = "ChequeStopRequest";
    public static final int REQUEST_ACCEPT = 0;
    public static final int PROCESSED = 1;
    public static final int STOP_PAYMENT = 2;
    public static final int CHEQUE_RETURN = 3;
    public static final int SURRENDER = 4;
    public static final int NOT_FOUND =  5;
    public static final int ERROR = 6;
    public static final int FACILITY_DISABLED = 7;

    public ChequeStopVerifyRequest(Activity activity, MyEnum.displayProgress doShow) {
        this.activityProgress = activity;
        this.objDisplayEnum = doShow;
        this.objIsMasking = MyEnum.isMasking.mask;
    }

    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model) {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new ChequeStopverifyParser();

        this.URL = URLGenerator.getsChequeStopVerifyRequest();

        Gson gson = new Gson();

        String json = gson.toJson(model, ChequeStopVerifyReqModel.class);

        Log.i(TAG, "sendRequest: json is -> " + json);

        sendRequestToServer(json);
    }
}
