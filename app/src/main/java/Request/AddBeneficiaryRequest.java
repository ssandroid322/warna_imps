package Request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import Interface.onReply;
import Model.AddBeneficiaryReqModel;
import Model.BaseModel;
import Parser.AddBeneficiaryParser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class AddBeneficiaryRequest extends BaseRequest {
    private static final String TAG = "AddBeneficiaryRequest";
    public static final int SUCCESS = 0;
    public static final int ALREADY_ADDED = 1;
    public static final int INVALID_ACCT_NO = 2;

    public AddBeneficiaryRequest(Activity activity, MyEnum.displayProgress doShow){
        this.activityProgress = activity;
        this.objDisplayEnum = doShow;
        this.objIsMasking = MyEnum.isMasking.mask;
    }

    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model) {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new AddBeneficiaryParser();

        this.URL = URLGenerator.getAddBeneficiaryRequest();

        Gson gson = new Gson();

        String json = gson.toJson(model, AddBeneficiaryReqModel.class);

        Log.i(TAG, "sendRequest: json is -> " + json);

        sendRequestToServer(json);
    }
}
