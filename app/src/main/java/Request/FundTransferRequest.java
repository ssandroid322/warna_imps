package Request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import Interface.onReply;
import Model.BaseModel;
import Model.FundTransferReqModel;
import Model.P2AFundTransferReqModel;
import Parser.FundTransferParser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class FundTransferRequest extends BaseRequest {
    private static final String TAG = "FundTransferRequest";
    public static final int SUCCESSFUL = 0;
    public static final int NOT_REG_FUND_TRANSFER = 1;
    public static final int INVALID_ACCT_NO = 2;
    public static final int INSUFFICIENT_BAL = 3;

    public FundTransferRequest(Activity activity, MyEnum.displayProgress doShow){
        this.activityProgress = activity;
        this.objDisplayEnum = doShow;
        this.objIsMasking = MyEnum.isMasking.mask;
    }

    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model) {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new FundTransferParser();

        this.URL = URLGenerator.getP2AFundTransferRequest();

        Gson gson = new Gson();

        String json = gson.toJson(model, P2AFundTransferReqModel.class);
//        String json = gson.toJson(model, FundTransferReqModel.class);
        Log.i(TAG, "sendRequest: json is -> " + json);

        sendRequestToServer(json);
    }
}
