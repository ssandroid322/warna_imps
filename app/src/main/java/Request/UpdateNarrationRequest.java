package Request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import Interface.onReply;
import Model.BaseModel;
import Model.FundTransferReqModel;
import Model.UpdateNarrationReqModel;
import Parser.FundTransferParser;
import Parser.UpdateNarrationParser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class UpdateNarrationRequest extends BaseRequest {
    private static final String TAG = "UpdateNarrationRequest";
    public static final int SUCCESSFUL = 0;
    public static final int UNSUCCESSFUL = 1;

    public UpdateNarrationRequest(Activity activity, MyEnum.displayProgress doShow){
        this.activityProgress = activity;
        this.objDisplayEnum = doShow;
        this.objIsMasking = MyEnum.isMasking.mask;
    }

    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model) {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new UpdateNarrationParser();
        this.URL = URLGenerator.getUpdateNarration();
        this.reqModel = new UpdateNarrationReqModel();

        Gson gson = new Gson();

        String json = gson.toJson(model, UpdateNarrationReqModel.class);

        Log.i(TAG, "sendRequest: json is -> " + json);

        sendRequestToServer(json);
    }
}