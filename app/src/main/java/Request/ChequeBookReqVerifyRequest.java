package Request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import Interface.onReply;
import Model.BaseModel;
import Model.ChequeBookReqModel;
import Model.ChequeBookReqVerifyReqModel;
import Parser.ChequeBookParser;
import Parser.ChequeBookReqVerifyParser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by admin on 26/10/2016.
 */
public class ChequeBookReqVerifyRequest extends BaseRequest {
    private static final String TAG = "ChequeBookReqVerifyReq";
    public static final int SUCCESS = 0;
    public static final int NOT_FOUND = 1;

    public ChequeBookReqVerifyRequest(Activity activity, MyEnum.displayProgress doShow) {
        this.activityProgress = activity;
        this.objDisplayEnum = doShow;
        this.objIsMasking = MyEnum.isMasking.mask;
    }

    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model) {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new ChequeBookReqVerifyParser();

        this.URL = URLGenerator.getsChequeBookReqVerify();

        this.reqModel = new ChequeBookReqVerifyReqModel();
        Gson gson = new Gson();

        String json = gson.toJson(model, ChequeBookReqVerifyReqModel.class);

        Log.i(TAG, "sendRequest: json is -> " + json);

        sendRequestToServer(json);
    }
}
