package Request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import Interface.onReply;
import Model.BaseModel;
import Model.GetBeneficiariesReqModel;
import Model.GetNoOfLeafsReqModel;
import Parser.GetBeneficiariesParser;
import Parser.GetNoOfLeafsParser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class GetNoOfLeafsRequest extends BaseRequest
{
    private static final String TAG = "GetNoOfLeafsRequest";
    public static final int SUCCESS = 0;
    public static final int NOT_FOUND = 1;

    public GetNoOfLeafsRequest(Activity activity, MyEnum.displayProgress doShow){
        this.activityProgress = activity;
        this.objDisplayEnum = doShow;
        this.objIsMasking = MyEnum.isMasking.mask;
    }

    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model)
    {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new GetNoOfLeafsParser();

        this.reqModel = new GetNoOfLeafsReqModel();
        this.URL = URLGenerator.getNoOfLeafsRequest();

        Gson gson = new Gson();

        String json = gson.toJson(model, GetNoOfLeafsReqModel.class);

        Log.i(TAG, "sendRequest: json is -> " + json);

        sendRequestToServer(json);
    }
}