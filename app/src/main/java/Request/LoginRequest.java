package Request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import Interface.onReply;
import Model.BaseModel;
import Model.LoginReqModel;
import Parser.LoginParser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by Samvid Mistry on 09/02/2016.
 */
public class LoginRequest extends BaseRequest {
    private static final String TAG = "LoginRequest";
    public static final int SUCCESSFUL = 0;
    public static final int UNSUCCESSFUL = 1;

    public LoginRequest(Activity activity, MyEnum.displayProgress doShow){
        this.activityProgress = activity;
        this.objDisplayEnum = doShow;
        this.objIsMasking = MyEnum.isMasking.mask;
    }

    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model) {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new LoginParser();

        this.URL = URLGenerator.getLoginUrl();

        Gson gson = new Gson();

        /*Type type = new TypeToken<ArrayList<LoginReqModel>>(){}.getType();
        String json = gson.toJson(model.getModelArray(), type);*/

        String json = gson.toJson(model, LoginReqModel.class);

        Log.i(TAG, "sendRequest: json is -> "+json);

        sendRequestToServer(json);
    }
}
