package Request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import Interface.onReply;
import Model.BaseModel;
import Model.ChangePasswordReqModel;
import Parser.ChangePasswordParser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by Samvid Mistry on 15/02/2016.
 */
public class ChangePasswordRequest extends BaseRequest {
    private static final String TAG = "LoginRequest";
    public static final int SUCCESSFUL = 0;
    public static final int PASSWORD_INVALID = 1;

    public ChangePasswordRequest(Activity activity, MyEnum.displayProgress doShow){
        this.activityProgress = activity;
        this.objDisplayEnum = doShow;
        this.objIsMasking = MyEnum.isMasking.mask;
    }

    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model) {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new ChangePasswordParser();

        this.URL = URLGenerator.getChangePassword();

        Gson gson = new Gson();

        String json = gson.toJson(model, ChangePasswordReqModel.class);

        Log.i(TAG, "sendRequest: json is -> " + json);

        sendRequestToServer(json);
    }
}
