package Request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import Interface.onReply;
import Model.BaseModel;
import Model.ChequeStatusInqReqModel;
import Parser.ChequeStatusInqParser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class ChequeStatusInqRequest extends BaseRequest {
    private static final String TAG = "ChequeStatusInqRequest";
    public static final int SUCCESSFUL = 0;
    public static final int NOT_FOUND = 1;

    public ChequeStatusInqRequest(Activity activity, MyEnum.displayProgress doShow){
        this.activityProgress = activity;
        this.objDisplayEnum = doShow;
        this.objIsMasking = MyEnum.isMasking.mask;
    }

    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model) {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new ChequeStatusInqParser();

        this.URL = URLGenerator.getChequeStatus();

        Gson gson = new Gson();

        String json = gson.toJson(model, ChequeStatusInqReqModel.class);

        Log.i(TAG, "sendRequest: json is -> " + json);

        sendRequestToServer(json);
    }
}
