package Request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import Interface.onReply;
import Model.BaseModel;
import Model.ChequeBookReqModel;
import Model.OperativeAccountReqModel;
import Parser.ChequeBookParser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class ChequeBookRequest extends BaseRequest {
    private static final String TAG = "ChequeBookRequest";
    public static final int SUCCESS = 0;
    public static final int NOT_FOUND = 1;

    public ChequeBookRequest(Activity activity, MyEnum.displayProgress doShow){
        this.activityProgress = activity;
        this.objDisplayEnum = doShow;
        this.objIsMasking = MyEnum.isMasking.mask;
    }

    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model) {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new ChequeBookParser();

        this.URL = URLGenerator.getChequeBookRequest();

        this.reqModel = new ChequeBookReqModel();
        Gson gson = new Gson();

        String json = gson.toJson(model, ChequeBookReqModel.class);

        Log.i(TAG, "sendRequest: json is -> " + json);

        sendRequestToServer(json);
    }
}
