package Request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import Interface.onReply;
import Model.BaseModel;
import Model.ChequeBookDetailReqModel;
import Parser.ChequeBookDetailParser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by Samvid Mistry on 25/02/2016.
 */
public class ChequeBookDetailRequest extends BaseRequest {
    private static final String TAG = "ChequeBookDetailRequest";
    public static final int SUCCESS = 0;
    public static final int CHEQUE_NOT_FOUND = 1;

    public ChequeBookDetailRequest(Activity activity, MyEnum.displayProgress doShow){
        this.activityProgress = activity;
        this.objDisplayEnum = doShow;
        this.objIsMasking = MyEnum.isMasking.mask;
    }

    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model) {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new ChequeBookDetailParser();

        this.URL = URLGenerator.getChequeBookDetailRequest();

        Gson gson = new Gson();

        String json = gson.toJson(model, ChequeBookDetailReqModel.class);

        Log.i(TAG, "sendRequest: json is -> " + json);

        sendRequestToServer(json);
    }
}
