package Request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import Interface.onReply;
import Model.AddBeneficiaryReqModel;
import Model.BaseModel;
import Model.CheckUsernameAvailability_ReqModel;
import Parser.AddBeneficiaryParser;
import Parser.CheckUsernameAvailabilityParser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by admin on 07/12/2016.
 */

public class CheckUsernameAvailability_Request extends BaseRequest {

    private static final String TAG = "ChaeckUsernameAva_Req";
    public static final String SUCCESS = "0";
    public static final String ERROR = "99";

    public CheckUsernameAvailability_Request(Activity activity, MyEnum.displayProgress doShow) {
        this.activityProgress = activity;
        this.objDisplayEnum = doShow;
        this.objIsMasking = MyEnum.isMasking.mask;
    }

    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model) {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new CheckUsernameAvailabilityParser();

        this.URL = URLGenerator.getUsernameAvailabilityUrl();

        Gson gson = new Gson();

        String json = gson.toJson(model, CheckUsernameAvailability_ReqModel.class);

        Log.i(TAG, "sendRequest: json is -> " + json);

        sendRequestToServer(json);
    }
}
