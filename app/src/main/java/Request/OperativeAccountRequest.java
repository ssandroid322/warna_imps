package Request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import Interface.onReply;
import Model.BaseModel;
import Model.OperativeAccountReqModel;
import Parser.OperativeAccountParser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by Samvid Mistry on 10/02/2016.
 */
public class OperativeAccountRequest extends BaseRequest {
    private static final String TAG = "OperativeAccountRequest";
    public static final int SUCCESS = 0;

    public OperativeAccountRequest(Activity activity, MyEnum.displayProgress doShow){
        this.activityProgress = activity;
        this.objDisplayEnum = doShow;
        this.objIsMasking = MyEnum.isMasking.mask;
    }
    
    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model) {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new OperativeAccountParser();

        this.reqModel = new OperativeAccountReqModel();
        this.URL = URLGenerator.getOperativeAccountUrl();

        Gson gson = new Gson();

        String json = gson.toJson(model, OperativeAccountReqModel.class);

        Log.i(TAG, "sendRequest: json is -> " + json);

        sendRequestToServer(json);
    }
}
