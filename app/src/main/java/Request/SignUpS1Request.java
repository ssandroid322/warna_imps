package Request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import Interface.onReply;
import Model.BaseModel;
import Model.SignUpS1ReqModel;
import Parser.SignUpS1Parser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by Samvid Mistry on 09/02/2016.
 */
public class SignUpS1Request extends BaseRequest {
    private static final String TAG = "SignUpS1Request";
    public static final int VALID_DATA = 0;
    public static final int ALREADY_REGISTERED = 1;
    public static final int INVALID_CUSTOMER_ID = 2;

    public SignUpS1Request(Activity activity, MyEnum.displayProgress doShow){
        this.activityProgress = activity;
        this.objDisplayEnum = doShow;
        this.objIsMasking = MyEnum.isMasking.mask;
    }

    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model) {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new SignUpS1Parser();

        this.URL = URLGenerator.getUserRegisterUrl();

        Gson gson = new Gson();

        String json = gson.toJson(model, SignUpS1ReqModel.class);

        Log.i(TAG, "sendRequest: json is -> " + json);

        sendRequestToServer(json);
    }
}
