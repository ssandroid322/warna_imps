package Request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import Interface.onReply;
import Model.BaseModel;
import Model.FundTransferP2PVerifyReqModel;
import Model.FundTransferVerifyReqModel;
import Parser.FundTransferP2PVerifyParser;
import Parser.FundTransferVerifyParser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class FundTransferVerifyP2PRequest extends BaseRequest {
    private static final String TAG = "FundTransferP2PVerRequest";
    public static final int SUCCESSFUL = 0;
    public static final int INSUFFICIENT_BAL = 1;
    public static final int NOT_REG_FUND_TRANSFER = 2;
    public static final int DUPLICATE = 4;
    public static final int REJECTED = 5;

    public FundTransferVerifyP2PRequest(Activity activity, MyEnum.displayProgress doShow) {
        this.activityProgress = activity;
        this.objDisplayEnum = doShow;
        this.objIsMasking = MyEnum.isMasking.mask;
    }

    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model) {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new FundTransferP2PVerifyParser();
        this.reqModel = model;
        this.URL = URLGenerator.getFundTransfer_P2PVerifyRequest();

        Gson gson = new Gson();

        String json = gson.toJson(model, FundTransferP2PVerifyReqModel.class);

        Log.i(TAG, "sendRe is -> " + json);

        sendRequestToServer(json);
    }
}