package Request;

import android.app.Activity;

import com.google.gson.Gson;

import Interface.onReply;
import Model.AddBenificiaryVerifyReqModel;
import Model.BaseModel;
import Parser.AddBenificiaryVerifyParser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by admin on 26/10/2016.
 */
public class AddBenificiaryVerifyRequest extends BaseRequest {
    private static final String TAG = "AddBenificiaryVerifyRequest";
    public static final int SUCCESSFUL = 0;
    public static final int ALREADY_ADDED = 1;
    public static final int INVALID_ACCT_NO = 2;

    public AddBenificiaryVerifyRequest(Activity activity, MyEnum.displayProgress doShow) {
        this.activityProgress = activity;
        this.objDisplayEnum = doShow;
        this.objIsMasking = MyEnum.isMasking.mask;
    }

    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model) {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new AddBenificiaryVerifyParser();

        this.URL = URLGenerator.getsBeneficiaryVerifyRequest();

        Gson gson = new Gson();

        String json = gson.toJson(model, AddBenificiaryVerifyReqModel.class);

//        Log.i(TAG, "sendRequest: json is -> " + json);

        sendRequestToServer(json);
    }
}
