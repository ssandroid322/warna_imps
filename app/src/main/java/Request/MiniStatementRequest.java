package Request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import Interface.onReply;
import Model.BaseModel;
import Model.MiniStatementReqModel;
import Parser.MiniStatementParser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by Samvid Mistry on 11/02/2016.
 */
public class MiniStatementRequest extends BaseRequest {
    private static final String TAG = "MiniStatementRequest";
    public static final int SUCCESS = 0;
    public static final int NO_TRANSACTIONS = 1;

    public MiniStatementRequest(Activity activity, MyEnum.displayProgress doShow){
        this.activityProgress = activity;
        this.objDisplayEnum = doShow;
        this.objIsMasking = MyEnum.isMasking.mask;
    }

    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model) {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new MiniStatementParser();

        this.URL = URLGenerator.getMiniStatement();

        String json = new Gson().toJson(model, MiniStatementReqModel.class);

        Log.i(TAG, "sendRequest: json is -> " + json);

        sendRequestToServer(json);
    }

}
