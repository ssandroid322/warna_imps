package Request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import Interface.onReply;
import Model.BaseModel;
import Model.P2AFundTransferReqModel;
import Parser.P2AFundTransferParser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by samvidmistry on 30/8/16.
 */

public class P2AFundTransferRequest extends BaseRequest {
    private static final String TAG = "P2AFundTransferRequest";
    public static final int SUCCESS = 0;
    public static final int INSUFFICIENT_BALANCE = 1;
    public static final int INVALID_NUMBER = 2;

    public P2AFundTransferRequest(Activity activity, MyEnum.displayProgress doShow) {
        this.activityProgress = activity;
        this.objDisplayEnum = doShow;
        this.objIsMasking = MyEnum.isMasking.mask;
    }

    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model) {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new P2AFundTransferParser();
        this.reqModel = model;
        this.URL = URLGenerator.getP2AFundTransferRequest();

        Gson gson = new Gson();

        String json = gson.toJson(model, P2AFundTransferReqModel.class);

        Log.i(TAG, "sendRequest: json is -> " + json);

        sendRequestToServer(json);
    }
}
