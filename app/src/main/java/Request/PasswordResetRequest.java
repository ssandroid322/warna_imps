package Request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import Interface.onReply;
import Model.BaseModel;
import Model.PasswordResetReqModel;
import Parser.PasswordResetParser;
import Utility.MyEnum;
import Utility.URLGenerator;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class PasswordResetRequest extends BaseRequest {
    private static final String TAG = "PasswordResetRequest";
    public static final int SUCCESS = 0;
    public static final int INVALID_CUSTOMER_ID = 1;
    public static final int NOT_REGISTERED = 2;

    public PasswordResetRequest(Activity activity, MyEnum.displayProgress doShow){
        this.activityProgress = activity;
        this.objDisplayEnum = doShow;
        this.objIsMasking = MyEnum.isMasking.mask;
    }

    @Override
    public void sendRequest(onReply objReplyMethod, BaseModel model) {
        this.onReplyDelegate = objReplyMethod;
        this.objParser = new PasswordResetParser();

        this.URL = URLGenerator.getResetPasswordRequest();

        Gson gson = new Gson();

        String json = gson.toJson(model, PasswordResetReqModel.class);

        Log.i(TAG, "sendRequest: json is -> " + json);

        sendRequestToServer(json);
    }
}
