package Buttons;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by Samvid Mistry on 08/02/2016.
 */
public class SegoeUiButton extends Button {
    public SegoeUiButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        //setFonts(context);
    }

    private void setFonts(Context context) {
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "Fonts/Helvetica-Normal.ttf"));
    }
}
