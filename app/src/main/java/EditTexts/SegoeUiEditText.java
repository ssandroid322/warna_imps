package EditTexts;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

/**
 * Created by Samvid Mistry on 08/02/2016.
 */
public class SegoeUiEditText extends AppCompatEditText {
    public SegoeUiEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        //setFonts(context);
    }

    private void setFonts(Context context) {
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "Fonts/Helvetica-Normal.ttf"));
    }
}
