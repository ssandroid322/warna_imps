package EditTexts;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

/**
 * Created by Samvid Mistry on 05/03/2016.
 */
public class SegoeUiBoldEditText extends AppCompatEditText {

    public SegoeUiBoldEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        //setFonts(context);
    }

    private void setFonts(Context context) {
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "Fonts/Helvetica-Bold.ttf"));
    }
}
