package Interface;

import android.support.v4.util.SimpleArrayMap;

/**
 * Created by Samvid Mistry on 23/02/2016.
 */
public interface OnBeneficiaryDeleteRequested {
    void onDeleteBeneficiary(SimpleArrayMap<String, String> map, int deletedPosition);
}
