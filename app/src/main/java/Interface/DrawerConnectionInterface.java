package Interface;

/**
 * Created by Samvid Mistry on 13/02/2016.
 */
public interface DrawerConnectionInterface {

    void onDrawerOpenRequested();

}
