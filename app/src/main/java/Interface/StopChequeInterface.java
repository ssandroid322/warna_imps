package Interface;

/**
 * Created by Samvid Mistry on 02/03/2016.
 */
public interface StopChequeInterface {
    void onStopChequeRequested(String chequeNo, int position);

    void onChequeRequested(int position);
}
