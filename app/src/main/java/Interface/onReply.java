package Interface;


import org.json.JSONObject;

import Model.BaseModel;
import Parser.BaseParser;

public interface onReply {
	/**
	 * This method is called when all the data is received in a request and it is ready to be populated.
	 * @param objJson json data to deserialize
	 * @param baseParser parser to parse JSON data and return the appropriate class
	 */
	void onPopulate(JSONObject objJson, BaseParser baseParser);

	void networkConnectionError(BaseModel reqModel);

    /**
     * This method is called when an app needs to be updated to call the request.
     */
	void updateDetail();

    /**
     * If an activity code is not used in 5 minutes, then the code gets expired, so whenever we get status:9 from web, we expire the session,
     * clear the task and take user to login screen again.
     */
    void onSessionExpired();

	void onRejectedService(String msg);

	void onRejectedServiceFinishActivity(String msg);
}
