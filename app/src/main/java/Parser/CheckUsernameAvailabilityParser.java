package Parser;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import Model.AddBeneficiaryResModel;
import Model.BaseModel;
import Model.CheckUsernameAvailability_ResModel;
import Request.BaseRequest;

/**
 * Created by admin on 07/12/2016.
 */

public class CheckUsernameAvailabilityParser extends BaseParser{

    private static String TAG = "CheckUsernameAvaParser";

    @Override
    public BaseModel doParsing(JSONObject objJSON) {

        try {
            Gson gson = new Gson();
            ArrayList<BaseModel> baseModels = new ArrayList<>();
            JSONArray jsonArray = objJSON.getJSONArray(BaseRequest.OUTPUT);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                baseModels.add(gson.fromJson(jsonObject.toString(), CheckUsernameAvailability_ResModel.class));
            }


            CheckUsernameAvailability_ResModel checkUsernameAvailability_resModel = new CheckUsernameAvailability_ResModel();
            checkUsernameAvailability_resModel.setModelArray(baseModels);
            return checkUsernameAvailability_resModel;
        }
        catch (Exception e){
            Log.e(TAG, "doParsing: Couldn't get data out of JSON");
            e.printStackTrace();
            return null;
        }
    }
}
