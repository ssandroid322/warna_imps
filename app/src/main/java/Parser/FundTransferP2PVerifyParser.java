package Parser;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Model.BaseModel;
import Model.FundTransferP2PVerifyResModel;
import Model.FundTransferVerifyResModel;
import Request.BaseRequest;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class FundTransferP2PVerifyParser extends BaseParser {
    private static final String TAG = "FundTransferP2PVerParser";

    @Override
    public BaseModel doParsing(JSONObject objJSON) {
        try {
            Gson gson = new Gson();
            ArrayList<BaseModel> baseModels = new ArrayList<>();
            JSONArray jsonArray = objJSON.getJSONArray(BaseRequest.OUTPUT);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                baseModels.add(gson.fromJson(jsonObject.toString(), FundTransferP2PVerifyResModel.class));
            }

            FundTransferP2PVerifyResModel fundTransferVerifyResModel = new FundTransferP2PVerifyResModel();
            fundTransferVerifyResModel.setModelArray(baseModels);
            return fundTransferVerifyResModel;
        } catch (JSONException e) {
            Log.e(TAG, "doParsing: Couldn't get");
            e.printStackTrace();
            return null;
        }
    }
}
