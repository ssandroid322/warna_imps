package Parser;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Model.BaseModel;
import Model.CancelMmidResModel;
import Request.BaseRequest;

/**
 * Created by samvidmistry on 30/8/16.
 */

public class CancelMmidParser extends BaseParser {
    private static final String TAG = "CancelMmidParser";

    @Override
    public BaseModel doParsing(JSONObject objJSON) {
        try {
            Gson gson = new Gson();
            ArrayList<BaseModel> baseModels = new ArrayList<>();
            JSONArray jsonArray = objJSON.getJSONArray(BaseRequest.OUTPUT);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                baseModels.add(gson.fromJson(jsonObject.toString(), CancelMmidResModel.class));
            }

            CancelMmidResModel cancelMmidResModel = new CancelMmidResModel();
            cancelMmidResModel.setModelArray(baseModels);
            return cancelMmidResModel;
        } catch (JSONException e) {
            Log.e(TAG, "doParsing: Couldn't get data out of JSON");
            e.printStackTrace();
            return null;
        }
    }
}
