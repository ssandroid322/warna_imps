package Parser;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Model.AddBeneficiaryResModel;
import Model.BaseModel;
import Model.ChangeUsernameReqModel;
import Model.ChangeUsername_ResModel;
import Model.CheckUsernameAvailability_ResModel;
import Request.BaseRequest;

/**
 * Created by admin on 06/12/2016.
 */

public class ChangeUsername_Parser extends BaseParser {
    private static final String TAG = "ChangeUsername_Parser";

    @Override
    public BaseModel doParsing(JSONObject objJSON) {
        try {
            Gson gson = new Gson();
            ArrayList<BaseModel> baseModels = new ArrayList<>();
            JSONArray jsonArray = objJSON.getJSONArray(BaseRequest.OUTPUT);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                baseModels.add(gson.fromJson(jsonObject.toString(), ChangeUsername_ResModel.class));
            }


            ChangeUsername_ResModel changeUsername_resModel = new ChangeUsername_ResModel();
            changeUsername_resModel.setModelArray(baseModels);
            return changeUsername_resModel;
        } catch (JSONException e) {
            Log.e(TAG, "doParsing: Couldn't get data out of JSON");
            e.printStackTrace();
            return null;
        }
    }
}
