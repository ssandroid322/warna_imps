package Parser;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Model.BaseModel;
import Model.P2AFundTransferVerifyResModel;
import Request.BaseRequest;

/**
 * Created by samvidmistry on 30/8/16.
 */

public class P2AFundTransferVerifyParser extends BaseParser {
    private static final String TAG = "P2AFundTransferVerifyPa";

    @Override
    public BaseModel doParsing(JSONObject objJSON) {
        try {
            Gson gson = new Gson();
            ArrayList<BaseModel> baseModels = new ArrayList<>();
            JSONArray jsonArray = objJSON.getJSONArray(BaseRequest.OUTPUT);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                baseModels.add(gson.fromJson(jsonObject.toString(),
                        P2AFundTransferVerifyResModel.class));
            }

            P2AFundTransferVerifyResModel fundTransferVerifyResModel =
                    new P2AFundTransferVerifyResModel();
            fundTransferVerifyResModel.setModelArray(baseModels);
            return fundTransferVerifyResModel;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
