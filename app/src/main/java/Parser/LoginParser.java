package Parser;

import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import Model.BaseModel;
import Model.LoginResModel;
import Request.BaseRequest;

/**
 * Created by Samvid Mistry on 09/02/2016.
 */
public class LoginParser extends BaseParser {
    private static final String TAG = "LoginParser";

    @Override
    public BaseModel doParsing(JSONObject objJSON) {
        try {
            Gson gson = new Gson();
            ArrayList<BaseModel> loginResModels = new ArrayList<>();
            JSONArray jsonArray = objJSON.getJSONArray(BaseRequest.OUTPUT);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                loginResModels.add(gson.fromJson(jsonObject.toString(), LoginResModel.class));
            }

            LoginResModel loginResModel = new LoginResModel();
            loginResModel.setModelArray(loginResModels);
            return loginResModel;
        } catch (JSONException e) {
            Log.e(TAG, "doParsing: Couldn't get data out of JSON");
            e.printStackTrace();
            return null;
        }
    }
}
