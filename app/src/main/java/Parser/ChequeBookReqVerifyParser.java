package Parser;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Model.BaseModel;
import Model.ChequeBookReqVerifyResModel;
import Model.ChequeBookResModel;
import Request.BaseRequest;

/**
 * Created by admin on 26/10/2016.
 */
public class ChequeBookReqVerifyParser extends BaseParser {
    private static final String TAG = "ChequeBookReqParser";
    @Override
    public BaseModel doParsing(JSONObject objJSON) {
        try{
            Gson gson = new Gson();
            ArrayList<BaseModel> baseModels = new ArrayList<>();
            JSONArray jsonArray = objJSON.getJSONArray(BaseRequest.OUTPUT);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                baseModels.add(gson.fromJson(jsonObject.toString(), ChequeBookReqVerifyResModel.class));
            }

            ChequeBookReqVerifyResModel chequeBookResModel = new ChequeBookReqVerifyResModel();
            chequeBookResModel.setModelArray(baseModels);
            return chequeBookResModel;
        }
        catch (JSONException e){
            Log.e(TAG, "doParsing: Couldn't get data out of JSON");
            e.printStackTrace();
            return null;
        }

    }
}
