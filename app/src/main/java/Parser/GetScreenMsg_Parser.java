package Parser;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Model.BaseModel;
import Model.GetNoOfLeafsResModel;
import Model.GetScreenMsg_ResModel;
import Request.BaseRequest;

/**
 * Created by admin on 08/12/2016.
 */

public class GetScreenMsg_Parser extends BaseParser {

    private static final String TAG = "GetScreenMsg_Parser";

    @Override
    public BaseModel doParsing(JSONObject objJSON) {
        try{
            Gson gson = new Gson();
            ArrayList<BaseModel> baseModels = new ArrayList<>();
            JSONArray jsonArray = objJSON.getJSONArray(BaseRequest.OUTPUT);

            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                baseModels.add(gson.fromJson(jsonObject.toString(), GetScreenMsg_ResModel.class));
            }

            GetScreenMsg_ResModel getScreenMsg_resModel = new GetScreenMsg_ResModel();
            getScreenMsg_resModel.setModelArray(baseModels);
            return getScreenMsg_resModel;
        } catch (JSONException e) {
            Log.e(TAG, "doParsing: Couldn't get data out of JSON");
            e.printStackTrace();
            return null;
        }
    }
}
