package Parser;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Model.BaseModel;
import Model.SignUpS1ResModel;
import Request.BaseRequest;

/**
 * Created by Samvid Mistry on 09/02/2016.
 */
public class SignUpS1Parser extends BaseParser {
    private static final String TAG = "SignUpS1Parser";

    @Override
    public BaseModel doParsing(JSONObject objJSON) {
        try {
            Gson gson = new Gson();
            ArrayList<BaseModel> baseModels = new ArrayList<>();
            JSONArray jsonArray = objJSON.getJSONArray(BaseRequest.OUTPUT);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                baseModels.add(gson.fromJson(jsonObject.toString(), SignUpS1ResModel.class));
            }

            SignUpS1ResModel signUpS1ResModel = new SignUpS1ResModel();
            signUpS1ResModel.setModelArray(baseModels);

            return signUpS1ResModel;
        } catch (JSONException e) {
            Log.e(TAG, "doParsing: Couldn't get data out of JSON");
            e.printStackTrace();
            return null;
        }
    }
}
