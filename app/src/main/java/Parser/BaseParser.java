package Parser;

import org.json.JSONObject;

import java.util.ArrayList;

import Model.BaseModel;

/**
 * Created by chitan on 10/17/2015.
 */
public abstract class BaseParser {
    protected ArrayList<BaseModel> modelList;

    public abstract BaseModel doParsing(JSONObject objJSON);
}
