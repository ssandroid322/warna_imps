package Parser;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Model.BaseModel;
import Model.SignUpS2ResModel;
import Request.BaseRequest;

/**
 * Created by Samvid Mistry on 10/02/2016.
 */
public class SignUpS2Parser extends BaseParser {
    private static final String TAG = "SignUpS2Parser";

    @Override
    public BaseModel doParsing(JSONObject objJSON) {
        try {
            Gson gson = new Gson();
            ArrayList<BaseModel> baseModels = new ArrayList<>();

            JSONArray jsonArray = objJSON.getJSONArray(BaseRequest.OUTPUT);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                SignUpS2ResModel signUpS2ResModel = gson.fromJson(jsonObject.toString(), SignUpS2ResModel.class);
                baseModels.add(signUpS2ResModel);
            }

            SignUpS2ResModel signUpS2ResModel = new SignUpS2ResModel();
            signUpS2ResModel.setModelArray(baseModels);

            return signUpS2ResModel;
        } catch (JSONException e) {
            Log.e(TAG, "doParsing: Couldn't get data out of JSON");
            e.printStackTrace();
            return null;
        }
    }
}
