package Parser;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Model.BaseModel;
import Model.ChequeStopResModel;
import Model.ChequeStopVerifyResModel;
import Request.BaseRequest;

/**
 * Created by admin on 26/10/2016.
 */
public class ChequeStopverifyParser extends BaseParser {
    private static final String TAG = "ChequeStopverifyParser";
    @Override
    public BaseModel doParsing(JSONObject objJSON) {
        try{
            Gson gson = new Gson();
            ArrayList<BaseModel> baseModels = new ArrayList<>();
            JSONArray jsonArray = objJSON.getJSONArray(BaseRequest.OUTPUT);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                baseModels.add(gson.fromJson(jsonObject.toString(), ChequeStopVerifyResModel.class));
            }

            ChequeStopVerifyResModel chequeStopVerifyResModel = new ChequeStopVerifyResModel();
            chequeStopVerifyResModel.setModelArray(baseModels);
            return chequeStopVerifyResModel;
        }
        catch (JSONException e) {
            Log.e(TAG, "doParsing: Couldn't get data out of JSON");
            e.printStackTrace();
            return null;
        }

    }
}
