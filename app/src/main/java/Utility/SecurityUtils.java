package Utility;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;


/**
 * This class is to provide easy access to various encryption and decryption algorithm implementations without the hassle of writing
 * all the boilerplate code for setting up classes to do actual encryption.
 * Created by Samvid Mistry on 11/02/2016.
 */


public class SecurityUtils {
    private static final String TAG = "SecurityUtils";
    private static  byte[] key;
    //private static final String ENC_DEC_KEY = "mobipro";
    /*private static final byte[] ENC_DEC_KEY = {
            'T', 'h', 'e', 'B', 'e', 's', 't',
            'S', 'e', 'c', 'r','e', 't', 'K', 'e', 'y'
    };*/
    private static final byte[] ENC_DEC_KEY = "ACUTEAMCBMOBIPRO".getBytes();

    private static final String ALGORITHM = "AES/ECB/PKCS5Padding";

    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();


    public static String encryptDataWithAes(String plainText) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {

//        SecretKeySpec secretKeySpec = new SecretKeySpec(ENC_DEC_KEY, ALGORITHM);
//        Cipher cipher = Cipher.getInstance(ALGORITHM);
//        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
//
//
//        byte[] b = cipher.doFinal(data.getBytes());
//        char[] hexArray = "0123456789ABCDEF".toCharArray();
//        char[] hexChars = new char[b.length * 2];
//        for (int j = 0; j < b.length; j++) {
//            int v = b[j] & 0xFF;
//            hexChars[j * 2] = hexArray[v >>> 4];
//            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
//        }
//        return new String(hexChars);


        try {
            key = "ACUTEAMCBMOBIPRO".getBytes("UTF8");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            SecretKeySpec secretKey = new SecretKeySpec(key, ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            byte[] cipherText = cipher.doFinal(plainText.getBytes("UTF8"));
            //String encryptedString = new String(Base64.getEncoder().encode(cipherText),"UTF-8");
            // String encryptedString = new String(Base64.getEncoder().encode(cipherText),"UTF-8");
            String encryptedString=bytesToHex(cipherText);
            return encryptedString;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public static String decryptDataWithAes(String encryptedText) throws
            NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException,
            BadPaddingException, IllegalBlockSizeException {
//        if ((data != null) &&(data.equals(""))){
//            return "";
//        }
//
//        SecretKeySpec secretKeySpec = new SecretKeySpec(ENC_DEC_KEY, ALGORITHM);
//        Cipher cipher = Cipher.getInstance(ALGORITHM);
//
//        int len = data.length();
//        byte[] data1 = new byte[len / 2];
//        for (int i = 0; i < len; i += 2) {
//            data1[i / 2] = (byte) ((Character.digit(data.charAt(i), 16) << 4)
//                    + Character.digit(data.charAt(i + 1), 16));
//        }
//        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
//        return new String(cipher.doFinal(data1));

        byte[] encryptedTextbyte = hexStringToByteArray(encryptedText);
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            SecretKeySpec secretKey = new SecretKeySpec(key, ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, secretKey);

            String decryptedString = new String(cipher.doFinal(encryptedTextbyte),"UTF-8");
            return decryptedString;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }

        return data;
    }

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
