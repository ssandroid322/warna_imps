package Utility;

import android.location.Address;
import android.support.v4.util.SimpleArrayMap;

import java.util.List;

/**
 * Created by Samvid Mistry on 09/01/2016.
 */
public class LocationUtils {
    public static final String ADDRESS_LINE_1 = "address_line_one";
    public static final String ADDRESS_LINE_2 = "address_line_two";
    public static final String CITY = "address_city";
    public static final String STATE = "address_state";
    public static final String COUNTRY = "address_country";
    public static final String ZIP = "address_zip";

    /**
     * A method for requesting the most accurate address from {@link List} of {@link Address}. This method
     * treats the address having maximum lines as the most accurate address. It returns following things as keys in {@link android.support.v4.util.SimpleArrayMap}
     * (1) ADDRESS_LINE_1
     * (2) ADDRESS_LINE_2
     * (3) CITY
     * (4) STATE
     * (5) COUNTRY
     * (6) ZIP
     * <strong>NOTE:</strong> Any data from the list above can be null or empty, so please check all the data before using.
     * @param addresses list of addresses to find the most accurate result
     * @return a map of data listed above
     */
    public static SimpleArrayMap<String, String> getMostAccurateAddress(List<Address> addresses){
        int maxStringAddress = 0, tempAddressLength = 0, maxStringText = 0;
        Address finalAddress;

        for (int i = 0; i < addresses.size(); i++) {
            Address address = addresses.get(i);

            for (int j = 0; j < 10; j++) {
                String addressLine = address.getAddressLine(j);
                if(addressLine == null || addressLine.trim().length() <= 0){
                    tempAddressLength = j-1;
                    break;
                }
            }

            if(maxStringText < tempAddressLength){
                maxStringText = tempAddressLength;
                maxStringAddress = i;
            }
        }

        finalAddress = addresses.get(maxStringAddress);

        SimpleArrayMap<String, String> locationMap = new SimpleArrayMap<>();
        locationMap.put(ADDRESS_LINE_1, finalAddress.getAddressLine(0));
        locationMap.put(ADDRESS_LINE_2, finalAddress.getAddressLine(1));
        locationMap.put(CITY, finalAddress.getLocality());
        locationMap.put(STATE, finalAddress.getAdminArea());
        locationMap.put(COUNTRY, finalAddress.getCountryName());

        if(finalAddress.getAddressLine(2) != null && finalAddress.getAddressLine(2).trim().length() > 0){
            String number = finalAddress.getAddressLine(2).replaceAll("[^0-9]", "");
            if(number.length() > 5){
                locationMap.put(ZIP, number);
            }
        }else if(finalAddress.getAddressLine(1) != null && finalAddress.getAddressLine(1).trim().length() > 0){
            String number = finalAddress.getAddressLine(1).replaceAll("[^0-9]", "");
            if(number.length() > 5){
                locationMap.put(ZIP, number);
            }
        }else if(finalAddress.getAddressLine(0) != null && finalAddress.getAddressLine(0).trim().length() > 0){
            String number = finalAddress.getAddressLine(0).replaceAll("[^0-9]", "");
            if(number.length() > 5){
                locationMap.put(ZIP, number);
            }
        }

        return locationMap;
    }

}
