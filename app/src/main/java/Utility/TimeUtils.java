package Utility;

import java.util.Calendar;
import java.util.Date;

/**
 * A simple class containing utility functions for comparison and management of Time using {@link Date} class.
 * Created by Samvid Mistry on 07/01/2016.
 */
public class TimeUtils {

    /**
     * A simple function returning whether source time is after the target time.
     * NOTE: This method only compares time and assumes that checking if the day is same or not is done before or not required.
     * @param source Date representing source time
     * @param target Date representing target time
     * @return true if source is after target or false if source is before target
     */
    public static boolean isSourceAfterTarget(Date source, Date target){
        Calendar sCalendar = Calendar.getInstance(), tCalendar = Calendar.getInstance();
        sCalendar.setTime(source);
        tCalendar.setTime(target);

        int sHour = sCalendar.get(Calendar.HOUR_OF_DAY);
        int sMinute = sCalendar.get(Calendar.MINUTE);
        int tHour = tCalendar.get(Calendar.HOUR_OF_DAY);
        int tMinute = tCalendar.get(Calendar.MINUTE);

        if(sHour < tHour){
            return false;
        }else if(sHour == tHour){
            if(sMinute < tMinute){
                return false;
            }else{
                return true;
            }
        }else {
            return true;
        }
    }
}
