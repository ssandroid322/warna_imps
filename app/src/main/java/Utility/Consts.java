package Utility;

/**
 * Created by admin on 08/12/2016.
 */

public class Consts {

    public static String NOTE_ADDBENF = "ADDBENF";
    public static String NOTE_GNMMID = "GNMMID";
    public static String NOTE_CNMMID = "CNMMID";
    public static String NOTE_IFT = "IFT";
    public static String NOTE_IMPSP2P = "IMPSP2P";
    public static String NOTE_IMPSP2A = "IMPSP2A";
    public static String NOTE_IMPSP2U = "IMPSP2U";
    public static String NOTE_RTGS = "RTGS";
    public static String NOTE_NEFT = "NEFT";
    public static String NOTE_ATMBLOCK = "ATMBLOCK";
    public static String NOTE_CHQSTOP = "CHQSTOP";
    public static String NOTE_CHQREQ = "CHQREQ";
    public static String ISFOR_TANDC = "ISFORTANDC";
    public static String ISFORFAQ = "ISFORFAQ";
}
