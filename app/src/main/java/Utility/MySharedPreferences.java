package Utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Model.OperativeAccountResModel;

/**
 * Created by chitan on 11/2/2015.
 */
public class MySharedPreferences {
    private static final String ACTIVITY_CODE = "activation_code";
    private static final String USER_ID = "user_id";
    private static final String MOBILE_NO = "mobile_no";
    private static final String CUSTOMER_NAME = "customer_name";
    private static final String ACCOUNT_NO = "account_no";
    private static final String STORED_ACCOUNT_COUNT = "stored_account_count";
    private static final String ACCOUNT_ = "account_";
    private static final String CUSTOMER_ID_ = "customer_id_";
    private static final String ACCT_NO_ = "acct_no_";
    private static final String BRANCH_CD_ = "branch_cd_";
    private static final String BRANCH_NM_ = "branch_nm_";
    private static final String ACCT_TYPE_ = "acct_type_";
    private static final String ACCT_TYPE_NM_ = "acct_type_nm_";
    private static final String PARENT_TYPE_ = "parent_type_";
    private static final String ACCT_CD_ = "acct_cd_";
    private static final String OP_DATE_ = "op_date_";
    private static final String LEAN_AMOUNT_ = "lean_amount_";
    private static final String UNCLEAR_BALANCE_ = "unclear_balance_";
    private static final String OPENING_BALANCE_ = "opening_balance_";
    private static final String BALANCE_ = "balance_";
    private static final String CD_DR_TYPE_ = "cr_dr_type_";
    private static final String LIMIT_AMOUNT_ = "limit_amount_";
    private static final String DRAWING_POWER_ = "drawing_power_";

    //Addded By Dan
    private static final String CHEQUEBOOK_REQUEST = "chequebook_request_";

    private static final String RTGS = "rtgs_";
    private static final String RTGSLimit = "rtgs_limit_";

    private static final String NEFT = "neft_";
    private static final String NEFTLimit = "neft_limit_";

    private static final String IFT = "ift_";
    private static final String IFTLimit = "ift_limit_";

    private static final String IMPS = "imps_";
    private static final String IMPSLimit = "imps_limit_";

    private static final String MMIDACCOUNT = "mmid";
    private static final String DEVICE_IMEI_NUMBER = "DEVICE_IMEI_NUMBER";

    public static void clearSharedPreference(Context context) {
        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = prefs.edit();
            editor.clear();
            editor.apply();
        } catch (Exception e) {
            Log.e("Myshared exception: ", e.toString());
        }
    }

    public static void putActivityCode(Context context, String activityCode) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(ACTIVITY_CODE, SecurityUtils.encryptDataWithAes(activityCode)).apply();
    }

    public static String getActivityCode(Context context) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        return SecurityUtils.decryptDataWithAes(PreferenceManager.getDefaultSharedPreferences(context).getString(ACTIVITY_CODE, ""));
    }

    public static void putUserId(Context context, String userId) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(USER_ID, SecurityUtils.encryptDataWithAes(userId)).apply();
    }

    public static String getUserId(Context context) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        return SecurityUtils.decryptDataWithAes(PreferenceManager.getDefaultSharedPreferences(context).getString(USER_ID, ""));
    }

    public static void putCustomerName(Context context, String customerName) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(CUSTOMER_NAME, SecurityUtils.encryptDataWithAes(customerName)).apply();
    }

    public static String getCustomerName(Context context) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        return SecurityUtils.decryptDataWithAes(PreferenceManager.getDefaultSharedPreferences(context).getString(CUSTOMER_NAME, ""));
    }

    public static void putCustomerID(Context context, String customerID) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(CUSTOMER_ID_, SecurityUtils.encryptDataWithAes(customerID)).apply();
    }

    public static String getCustomerID(Context context) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        return SecurityUtils.decryptDataWithAes(PreferenceManager.getDefaultSharedPreferences(context).getString(CUSTOMER_ID_, ""));
    }

    public static void putAccounts(Context context, ArrayList<String> accounts) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(ACCOUNT_NO, accounts.size());

        for (int i = 0; i < accounts.size(); i++) {
            editor.putString(ACCOUNT_ + i, SecurityUtils.encryptDataWithAes(accounts.get(i)));
        }

        editor.apply();
    }

    public static void putAccounts(Context context, OperativeAccountResModel.Response[] operativeAccountResModel) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putInt(STORED_ACCOUNT_COUNT, operativeAccountResModel.length);

        for (int i = 0; i < operativeAccountResModel.length; i++) {
            OperativeAccountResModel.Response response = operativeAccountResModel[i];
            edit.putString(CUSTOMER_ID_ + i, SecurityUtils.encryptDataWithAes(String.valueOf(response.getCustomerId())));
//            edit.putString(ACCT_NO_+i, SecurityUtils.encryptDataWithAes(response.getAccountNo()));
            edit.putString(ACCT_NO_ + i, response.getAccountNo());
            edit.putString(BRANCH_CD_ + i, SecurityUtils.encryptDataWithAes(response.getBranchCode()));
            edit.putString(BRANCH_NM_ + i, SecurityUtils.encryptDataWithAes(response.getBranchName()));
            edit.putString(ACCT_TYPE_ + i, SecurityUtils.encryptDataWithAes(response.getAccountType()));
            edit.putString(ACCT_TYPE_NM_ + i, SecurityUtils.encryptDataWithAes(String.valueOf(response.getAccountTypeName())));
            edit.putString(PARENT_TYPE_ + i, SecurityUtils.encryptDataWithAes(String.valueOf(response.getParentType())));
            edit.putString(ACCT_CD_ + i, SecurityUtils.encryptDataWithAes(String.valueOf(response.getAccountCode())));
            edit.putString(OP_DATE_ + i, SecurityUtils.encryptDataWithAes(String.valueOf(response.getOpeningDate())));
            edit.putString(LEAN_AMOUNT_ + i, SecurityUtils.encryptDataWithAes(String.valueOf(response.getLienAmount())));
            edit.putString(UNCLEAR_BALANCE_ + i, SecurityUtils.encryptDataWithAes(String.valueOf(response.getUnclearBalance())));
            edit.putString(OPENING_BALANCE_ + i, SecurityUtils.encryptDataWithAes(String.valueOf(response.getOpeningBalance())));
            edit.putString(BALANCE_ + i, SecurityUtils.encryptDataWithAes(String.valueOf(response.getBalance())));
            edit.putString(CD_DR_TYPE_ + i, SecurityUtils.encryptDataWithAes(String.valueOf(response.getCdDrType())));
            edit.putString(LIMIT_AMOUNT_ + i, SecurityUtils.encryptDataWithAes(String.valueOf(response.getLimitAmount())));
            edit.putString(DRAWING_POWER_ + i, SecurityUtils.encryptDataWithAes(String.valueOf(response.getDrawingPower())));

            //Addded By Dan
            edit.putString(CHEQUEBOOK_REQUEST + i, SecurityUtils.encryptDataWithAes(String.valueOf(response.getChequeBookRequest())));

            edit.putString(NEFT + i, SecurityUtils.encryptDataWithAes(String.valueOf(response.getNEFT())));
            edit.putString(NEFTLimit + i, SecurityUtils.encryptDataWithAes(String.valueOf(response.getNEFTLimit())));

            edit.putString(IMPS + i, SecurityUtils.encryptDataWithAes(String.valueOf(response.getIMPS())));
            edit.putString(IMPSLimit + i, SecurityUtils.encryptDataWithAes(String.valueOf(response.getIMPSLimit())));

            edit.putString(IFT + i, SecurityUtils.encryptDataWithAes(String.valueOf(response.getIFT())));
            edit.putString(IFTLimit + i, SecurityUtils.encryptDataWithAes(String.valueOf(response.getIFTLimit())));

            edit.putString(RTGS + i, SecurityUtils.encryptDataWithAes(String.valueOf(response.getRTGS())));
            edit.putString(RTGSLimit + i, SecurityUtils.encryptDataWithAes(String.valueOf(response.getRTGSLimit())));

            edit.putString(MMIDACCOUNT + i, SecurityUtils.encryptDataWithAes(String.valueOf(response.getMMID())));
        }


        edit.apply();
    }

    public static ArrayList<OperativeAccountResModel.Response> getAccounts(Context context, int dummy /*A dummy value just to differentiate the methods*/) throws InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, NoSuchPaddingException {
        ArrayList<OperativeAccountResModel.Response> arrayList = new ArrayList<>();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        int count = sharedPreferences.getInt(STORED_ACCOUNT_COUNT, 0);

        for (int i = 0; i < count; i++) {
            arrayList.add(getAccount(context, i));
        }

        return arrayList;
    }

    public static OperativeAccountResModel.Response getAccount(Context context, int position) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        return new OperativeAccountResModel.Response(SecurityUtils.decryptDataWithAes(preferences.getString(CUSTOMER_ID_ + position, "")),
//                SecurityUtils.decryptDataWithAes(preferences.getString(ACCT_NO_+position, "")),
                preferences.getString(ACCT_NO_ + position, ""),
                SecurityUtils.decryptDataWithAes(preferences.getString(BRANCH_CD_ + position, "")),
                SecurityUtils.decryptDataWithAes(preferences.getString(BRANCH_NM_ + position, "")),
                SecurityUtils.decryptDataWithAes(preferences.getString(ACCT_TYPE_ + position, "")),
                SecurityUtils.decryptDataWithAes(preferences.getString(ACCT_TYPE_NM_ + position, "")),
                SecurityUtils.decryptDataWithAes(preferences.getString(PARENT_TYPE_ + position, "")),
                SecurityUtils.decryptDataWithAes(preferences.getString(ACCT_CD_ + position, "")),
                SecurityUtils.decryptDataWithAes(preferences.getString(OP_DATE_ + position, "")),
                SecurityUtils.decryptDataWithAes(preferences.getString(LEAN_AMOUNT_ + position, "")),
                SecurityUtils.decryptDataWithAes(preferences.getString(UNCLEAR_BALANCE_ + position, "")),
                SecurityUtils.decryptDataWithAes(preferences.getString(OPENING_BALANCE_ + position, "")),
                SecurityUtils.decryptDataWithAes(preferences.getString(BALANCE_ + position, "")),
                SecurityUtils.decryptDataWithAes(preferences.getString(CD_DR_TYPE_ + position, "")),
                SecurityUtils.decryptDataWithAes(preferences.getString(LIMIT_AMOUNT_ + position, "")),
                SecurityUtils.decryptDataWithAes(preferences.getString(DRAWING_POWER_ + position, "")),
                SecurityUtils.decryptDataWithAes(preferences.getString(CHEQUEBOOK_REQUEST + position, "")),

                SecurityUtils.decryptDataWithAes(preferences.getString(RTGS + position, "")),
                SecurityUtils.decryptDataWithAes(preferences.getString(RTGSLimit + position, "")),

                SecurityUtils.decryptDataWithAes(preferences.getString(NEFT + position, "")),
                SecurityUtils.decryptDataWithAes(preferences.getString(NEFTLimit + position, "")),

                SecurityUtils.decryptDataWithAes(preferences.getString(IFT + position, "")),
                SecurityUtils.decryptDataWithAes(preferences.getString(IFTLimit + position, "")),

                SecurityUtils.decryptDataWithAes(preferences.getString(IMPS + position, "")),
                SecurityUtils.decryptDataWithAes(preferences.getString(IMPSLimit + position, "")),

                SecurityUtils.decryptDataWithAes(preferences.getString(MMIDACCOUNT + position, "")));
    }

    public static List<String> getAccounts(Context context) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        if (!preferences.contains(ACCOUNT_NO)) return null;

        int noOfAccounts = preferences.getInt(ACCOUNT_NO, 0);
        ArrayList<String> accounts = new ArrayList<>();

        for (int i = 0; i < noOfAccounts; i++) {
            accounts.add(SecurityUtils.decryptDataWithAes(preferences.getString(ACCOUNT_ + i, "")));
        }

        return accounts;
    }

    public static String getAccountTypeFromNumber(Context context, String accountno) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        String accountType = "";
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        if (!preferences.contains(ACCOUNT_NO))
            return null;

        int noOfAccounts = preferences.getInt(ACCOUNT_NO, 0);

        for (int i = 0; i < noOfAccounts; i++) {
            if (SecurityUtils.decryptDataWithAes(preferences.getString(ACCOUNT_ + i, "")).equals(accountno)) {
                accountType = SecurityUtils.decryptDataWithAes(preferences.getString(ACCT_TYPE_ + i, ""));
                break;
            }
        }
        return accountType;
    }

    public static List<String> getChequeBookAccounts(Context context) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        if (!preferences.contains(ACCOUNT_NO))
            return null;

        int noOfAccounts = preferences.getInt(ACCOUNT_NO, 0);
        ArrayList<String> accounts = new ArrayList<>();

        for (int i = 0; i < noOfAccounts; i++) {
            String isChequAccount = SecurityUtils.decryptDataWithAes(preferences.getString(CHEQUEBOOK_REQUEST + i, ""));

            if (isChequAccount.equalsIgnoreCase("Y"))
                accounts.add(SecurityUtils.decryptDataWithAes(preferences.getString(ACCOUNT_ + i, "")));
        }

        return accounts;
    }

    public static List<String> getIftAccounts(Context context) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        if (!preferences.contains(ACCOUNT_NO))
            return null;

        int noOfAccounts = preferences.getInt(ACCOUNT_NO, 0);
        ArrayList<String> accounts = new ArrayList<>();

        for (int i = 0; i < noOfAccounts; i++) {
            String isIFT = SecurityUtils.decryptDataWithAes(preferences.getString(IFT + i, ""));

            if (isIFT.equalsIgnoreCase("Y"))
                accounts.add(SecurityUtils.decryptDataWithAes(preferences.getString(ACCOUNT_ + i, "")));
        }

        return accounts;
    }

    public static List<String> getOtherAccounts(Context context) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        if (!preferences.contains(ACCOUNT_NO))
            return null;

        ArrayList<String> accounts = new ArrayList<>();
        int noOfAccounts = preferences.getInt(ACCOUNT_NO, 0);

        for (int i = 0; i < noOfAccounts; i++) {
            String isNeft = SecurityUtils.decryptDataWithAes(preferences.getString(NEFT + i, ""));
            String isRtgs = SecurityUtils.decryptDataWithAes(preferences.getString(RTGS + i, ""));
            String isImps = SecurityUtils.decryptDataWithAes(preferences.getString(IMPS + i, ""));

            if (isNeft.equalsIgnoreCase("Y") || isRtgs.equalsIgnoreCase("Y") || isImps.equalsIgnoreCase("Y"))
                accounts.add(SecurityUtils.decryptDataWithAes(preferences.getString(ACCOUNT_ + i, "")));
        }

        return accounts;
    }

    public static List<String> getMMIDAccounts(Context context) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        if (!preferences.contains(ACCOUNT_NO))
            return null;

        ArrayList<String> accounts = new ArrayList<>();
        int noOfAccounts = preferences.getInt(ACCOUNT_NO, 0);

        for (int i = 0; i < noOfAccounts; i++) {
            String isNeft = SecurityUtils.decryptDataWithAes(preferences.getString(MMIDACCOUNT + i, ""));

            if (isNeft.equalsIgnoreCase("Y"))
                accounts.add(SecurityUtils.decryptDataWithAes(preferences.getString(ACCOUNT_ + i, "")));
        }

        return accounts;
    }

    public static List<String> getNeftAccounts(Context context) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        if (!preferences.contains(ACCOUNT_NO))
            return null;

        int noOfAccounts = preferences.getInt(ACCOUNT_NO, 0);
        ArrayList<String> accounts = new ArrayList<>();

        for (int i = 0; i < noOfAccounts; i++) {
            String isNeft = SecurityUtils.decryptDataWithAes(preferences.getString(NEFT + i, ""));

            if (isNeft.equalsIgnoreCase("Y"))
                accounts.add(SecurityUtils.decryptDataWithAes(preferences.getString(ACCOUNT_ + i, "")));
        }

        return accounts;
    }

    public static List<String> getRtgsAccounts(Context context) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        if (!preferences.contains(ACCOUNT_NO))
            return null;

        int noOfAccounts = preferences.getInt(ACCOUNT_NO, 0);
        ArrayList<String> accounts = new ArrayList<>();

        for (int i = 0; i < noOfAccounts; i++) {
            String isRtgs = SecurityUtils.decryptDataWithAes(preferences.getString(RTGS + i, ""));

            if (isRtgs.equalsIgnoreCase("Y"))
                accounts.add(SecurityUtils.decryptDataWithAes(preferences.getString(ACCOUNT_ + i, "")));
        }

        return accounts;
    }

    public static List<String> getImpsAccounts(Context context) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        if (!preferences.contains(ACCOUNT_NO))
            return null;

        int noOfAccounts = preferences.getInt(ACCOUNT_NO, 0);
        ArrayList<String> accounts = new ArrayList<>();

        for (int i = 0; i < noOfAccounts; i++) {
            String isImps = SecurityUtils.decryptDataWithAes(preferences.getString(IMPS + i, ""));

            if (isImps.equalsIgnoreCase("Y"))
                accounts.add(SecurityUtils.decryptDataWithAes(preferences.getString(ACCOUNT_ + i, "")));
        }

        return accounts;
    }

    public static void putMobileNumber(Context context, String mobileNo) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        preferences.edit().putString(MOBILE_NO, SecurityUtils.encryptDataWithAes(mobileNo)).apply();
    }

    public static String getMobileNo(Context context) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        return SecurityUtils.decryptDataWithAes(PreferenceManager.getDefaultSharedPreferences(context).getString(MOBILE_NO, ""));
    }

    public static void setImeiNumber(Context context, String imeinumber) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putString(DEVICE_IMEI_NUMBER, imeinumber).commit();
    }

    public static String getImeiNumber(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(DEVICE_IMEI_NUMBER, "");
    }

}
