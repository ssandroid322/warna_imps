package Utility;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.Window;

import com.waranabank.mobipro.R;

import CircleProgressClasses.CircularProgressBar;


//import CircularPrgressclasses.CircularProgressBar;

public class ShowProgressbar
{
	private static ProgressDialog progressDialog;
    private static CircularProgressBar mPocketBar;
	private  static   Dialog d;
	private static CircularProgressBar mProgressBar;


	public static void showMaskProgress(Activity inwhichActivity,String message)
	{


			d = new Dialog(inwhichActivity);
		    d.requestWindowFeature(Window.FEATURE_NO_TITLE);
			d.setContentView(R.layout.circleprogress);
			d.setCancelable(false);
			d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		   // d.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		    d.getWindow().setGravity(Gravity.CENTER);
			mPocketBar = (CircularProgressBar) d.findViewById(R.id.circleview);
			d.show();


//		if(progressDialog!=null)
//		{
//			if (!progressDialog.isShowing())
//			{
//				progressDialog = ProgressDialog.show(inwhichActivity, "",message);
//				progressDialog.setCancelable(false);
//			}
//		}
//		else
//		{
//			progressDialog = ProgressDialog.show(inwhichActivity, "",message);
//		}


	}

	public static void showProgress(Activity inwhichActivity,String message)
	{



			d = new Dialog(inwhichActivity);
		    d.requestWindowFeature(Window.FEATURE_NO_TITLE);
			d.setContentView(R.layout.circleprogress);
			d.setCancelable(true);
			d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			mPocketBar = (CircularProgressBar) d.findViewById(R.id.circleview);
			d.show();


//		if(progressDialog!=null)
//		{
//			if (!progressDialog.isShowing())
//			{
//				progressDialog = ProgressDialog.show(inwhichActivity, "",message);
//				progressDialog.setCancelable(true);
//			}
//		}
//		else
//		{
//			progressDialog = ProgressDialog.show(inwhichActivity, "",message);
//		}
	//	}
	}

	public static void dismissDialog()
	{

		if(d == null){
			return;
		}
		if(d.isShowing()) {
			d.dismiss();
		}

//		if (progressDialog.isShowing())
//		{
//            progressDialog.dismiss();
//		}
	}
	public static boolean isProgressVisible()
	{
		boolean isShow;
		if (d== null)
		{
			isShow = false;
		}
		else
		{
			isShow = true;
		}
		return isShow;
	}
}