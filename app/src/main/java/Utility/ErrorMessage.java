package Utility;

/**
 * Created by Samvid Mistry on 08-Feb-16.
 */
public class ErrorMessage {
    public static String getPostNoResult(){
        return "Sorry, No results were found.";
    }

    public static String getUsernameError(){
        return "Username can contains only alphabatic and number and it must be atleast 6 character long.";
    }

    public static String getInvalidResponse(){
        return "An error has been occurred";
    }

    public static String getNetworkConnectionError(){
        return "Network Error, Please check your connection and retry";
    }

    public static String getNoTransactionsError(){
        return "No statement found for this account.";
    }

    public static String getNoTransactionsInPeriodError(){
        return "No statement found for this period";
    }

    public static String getMediaNotMountedError(){
        return "There was an error with external storage";
    }

    public static String getCouldNotGetListOfBeneficiariesError(){
        return "Unable to get list of beneficiaries.";
    }

    public static String getCouldNotDeleteBeneficiariesError(){
        return "Unable to delete beneficiary.";
    }

    public static String getGenericError(){
        return "There was an error. Please try again.";
    }

    public static String getNoChequeBooksError(){
        return "No cheque book found";
    }

    public static String getNoPendingChequebooksError(){
        return "No pending cheque book found.";
    }

    public static String getInvalidPasswordError(){
        return "Please enter a valid password";
    }


    public static String getInvalidNoOfLeafs(){
        return "Unable to get no of leafs for this account.";
    }

    public static String getConnectionTimeOutError(String refno){
        return "Your Transaction with Ref.no " +
                refno +
                " has been timed out at beneficiary bank. Please confirm with Bank before initiating again.";
    }

    public static String getRequiredPhoneStoragepermission() {
        return "Needs phone state permissions in order to use all functionality";
    }

    public static String getNeveraskagainforPhoneState() {
        return "User does not have permissions for phone state access please go to setting and provide the permission.";
    }
}
