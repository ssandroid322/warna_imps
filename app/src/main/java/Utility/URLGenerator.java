package Utility;

public class URLGenerator {

//  private static final String sBaseUrl = "http://122.170.119.180:6808/mobile_services/easy_integ/cbs_host/";
//  private static final String sBaseUrl = "https://netbanking.amco-bank.com:8443/mobile_services/easy_integ/cbs_host/";


    // LIVE URL
//    private static final String sBaseUrl = "http://117.247.84.138:8080/live_imps/easy_integ/cbs_host/";
//    private static final String sBaseImpsUrl = "http://117.247.84.138:8080/live_imps/easy_integ/imps/";


    private static final String sBaseUrl = "https://netbanking.waranabank.com/live_imps/easy_integ/cbs_host/";
    private static final String sBaseImpsUrl = "https://netbanking.waranabank.com/live_imps/easy_integ/imps/";


//    private static final String sBaseUrl = "https://netbanking.amco-bank.com:8443/live_imps/easy_integ/cbs_host/";
//    private static final String sBaseImpsUrl = "https://netbanking.amco-bank.com:8443/live_imps/easy_integ/imps/";
//    USERNAME : Riddhi
//    PASSWORD : 9725239999

    // UAT URL FOR TESTING
//    private static final String sBaseUrl = "https://netbanking.amco-bank.com:8443/uat_imps/easy_integ/cbs_host/";
//    private static final String sBaseImpsUrl = "https://netbanking.amco-bank.com:8443/uat_imps/easy_integ/imps/";
//    USERNAME : AMC0000206
//    PASSWORD : 97252399991


    private static final String sCheckUsernameAvailability = "check_user_nm";
    private static final String sUpdateUsername = "update_user_nm";

    private static final String sLoginUrl = "user_login";
    private static final String sUserRegisterUrl = "user_register";
    private static final String sUserActivationUrl = "user_activation";
    private static final String sOperativeAccountUrl = "operative_account";
    private static final String sMiniStatement = "mini_statement";
    private static final String sDetailedStatement = "full_statement";
    private static final String sChangePassword = "change_psw";
    private static final String sChequeStatus = "cheque_status";
    private static final String sChequeStop = "cheque_stop";
    private static final String sChequeBookRequest = "cheque_book_req";
    private static final String sFundTransferRequest = "fund_trn";
    private static final String sFundTransferVerifyRequest = "fund_trn_verify";
    private static final String sAddBeneficiaryRequest = "add_benf";
    private static final String sDelBeneficiaryRequest = "del_benf";
    private static final String sGetListOfBeneficiariesRequest = "get_benf";
    private static final String sBeneficiaryVerifyRequest = "add_benf_verify";
    private static final String sResetPasswordRequest = "reset_psw";
    private static final String sResetPasswordVerifyRequest = "reset_psw_verify";
    private static final String sJointAccountDetailRequest = "joint_dtl";
    private static final String sBranchDetails = "our_branches";
    private static final String sChequeBookDetailRequest = "cheque_book_dtl";
    private static final String sChequeBookRequestStatus = "cheque_book_req_status";
    private static final String sCreateMmid = "mmid_request";
    private static final String sCancelMmid = "mmid_cancel";
    private static final String sP2AFundTransfer = "p2a_trn_req";
    private static final String sP2AFundTransferVerify = "p2a_trn_verify";

    private static final String sGetScreenMsg = "get_screen_msg";

    private static final String sChequeStopVerifyRequest = "cheque_stop_verify";

    private static final String sBlockCardRequest = "atm_block_req";
    private static final String sChequeBookReqVerify = "cheque_book_req_verify";

    private static final String sblockCardVerifyRequest = "atm_block_req_verify";

    private static final String sGetNoOfLeafs = "cheque_leaf";

    private static final String sUpdateNarration = "update_remark";

    private static final String sFundTransfer_P2PRequest = "p2p_trn_req";
    private static final String sFundTransfer_P2PVerifyRequest = "p2p_trn_verify";


    public static String getScreenMsgUrl() {
        return sBaseUrl + sGetScreenMsg;
    }

    public static String getUpdatesernameurl() {
        return sBaseUrl + sUpdateUsername;
    }

    public static String getUsernameAvailabilityUrl() {
        return sBaseUrl + sCheckUsernameAvailability;
    }

    public static String getLoginUrl() {
        return sBaseUrl + sLoginUrl;
    }

    public static String getAppUrl() {
        return null;
    }

    public static String getUserRegisterUrl() {
        return sBaseUrl + sUserRegisterUrl;
    }

    public static String getUserActivationUrl() {
        return sBaseUrl + sUserActivationUrl;
    }

    public static String getOperativeAccountUrl() {
        return sBaseUrl + sOperativeAccountUrl;
    }

    public static String getMiniStatement() {
        return sBaseUrl + sMiniStatement;
    }

    public static String getDetailedStatement() {
        return sBaseUrl + sDetailedStatement;
    }

    public static String getChangePassword() {
        return sBaseUrl + sChangePassword;
    }

    public static String getChequeStatus() {
        return sBaseUrl + sChequeStatus;
    }

    public static String getChequeStop() {
        return sBaseUrl + sChequeStop;
    }

    public static String getChequeBookRequest() {
        return sBaseUrl + sChequeBookRequest;
    }

    public static String getBlockCardRequest() {
        return sBaseUrl + sBlockCardRequest;
    }

    public static String getBlockCardVerifyRequest() {
        return sBaseUrl + sblockCardVerifyRequest;
    }

    public static String getNoOfLeafsRequest() {
        return sBaseUrl + sGetNoOfLeafs;
    }

    public static String getUpdateNarration() {
        return sBaseUrl + sUpdateNarration;
    }

    public static String getFundTransferRequest() {
        return sBaseUrl + sFundTransferRequest;
    }

    public static String getFundTransferVerifyRequest() {
        return sBaseUrl + sFundTransferVerifyRequest;
    }

    public static String getAddBeneficiaryRequest() {
        return sBaseUrl + sAddBeneficiaryRequest;
    }

    public static String getDelBeneficiaryRequest() {
        return sBaseUrl + sDelBeneficiaryRequest;
    }

    public static String getListOfBeneficiariesRequest() {
        return sBaseUrl + sGetListOfBeneficiariesRequest;
    }

    public static String getResetPasswordRequest() {
        return sBaseUrl + sResetPasswordRequest;
    }

    public static String getResetPasswordVerifyRequest() {
        return sBaseUrl + sResetPasswordVerifyRequest;
    }

    public static String getJointAccountDetailRequest() {
        return sBaseUrl + sJointAccountDetailRequest;
    }

    public static String getBranchDetails() {
        return sBaseUrl + sBranchDetails;
    }

    public static String getChequeBookDetailRequest() {
        return sBaseUrl + sChequeBookDetailRequest;
    }

    public static String getChequeBookRequestStatus() {
        return sBaseUrl + sChequeBookRequestStatus;
    }

    public static String getFundTransfer_P2PRequest() {
        return sBaseImpsUrl + sFundTransfer_P2PRequest;
    }

    public static String getFundTransfer_P2PVerifyRequest() {
        return sBaseImpsUrl + sFundTransfer_P2PVerifyRequest;
    }

    public static String getCreateMmidRequest() {
        return sBaseImpsUrl + sCreateMmid;
    }

    public static String getCancelMmidRequest() {
        return sBaseImpsUrl + sCancelMmid;
    }

    public static String getP2AFundTransferRequest() {
        return sBaseImpsUrl + sP2AFundTransfer;
    }

    public static String getP2AFundTransferVerifyRequest() {
        return sBaseImpsUrl + sP2AFundTransferVerify;
    }

    public static String getsBeneficiaryVerifyRequest() {
        return sBaseUrl + sBeneficiaryVerifyRequest;
    }

    public static String getsChequeStopVerifyRequest() {
        return sBaseUrl + sChequeStopVerifyRequest;
    }

    public static String getsChequeBookReqVerify() {
        return sBaseUrl + sChequeBookReqVerify;
    }

    public static String getFAQURL() {
//        return "https://www.google.co.in/";
//        return "http://192.168.1.224/amco/faq.html";
        return "https://netbanking.waranabank.com/mobipro/faq.html";


//        return "http://sculptsoft.com/demo/faq.html";
    }

    public static String getTermsConditionURL() {
        return "https://netbanking.waranabank.com/mobipro/t&c.html";
//        return "http://sculptsoft.com/demo/tandc.html";
//        return "http://192.168.1.224/amco/tandc.html";
    }

    public static String getAdvertismentURL() {
//        return "http://192.168.1.224/amco/slider.html";
        return "https://netbanking.waranabank.com/mobipro/slider.html";
//        return "http://sculptsoft.com/demo/slider.html";
    }
}