package Utility;

/**
 * Created by Dan on 31-Oct-15.
 */
public class MyEnum
{
    public static enum requestError
    {
        connectionError,
        completeData,
        updateapplication
    }
    public static enum displayProgress
    {
        //Use for showing a ProgressDialog
        Show,
        //Use for not showing a ProgressDialog
        NotShow,
        //Not Currently used
        Backshow
    }

    public static enum isMasking
    {
        mask,
        notmask
    }

    public static enum shareDialogFragment
    {
        facebook,
        twitter,
        gmail,
        message
    }
    public static enum imagePickeroption
    {
        camera,
        gallary,
        cancle
    }
}
