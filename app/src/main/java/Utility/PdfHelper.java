package Utility;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;

import com.waranabank.mobipro.R;


import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * A helper class for doing operations against PDF files, currently, it contains operations related to writing tables only.
 * Other facilities will be added as required. If you want more customization than what this class provides, it is suggested that
 * you implement the functionality by yourself in your project code, rather than adding functionality to this class.
 * Created by Samvid Mistry on 16/02/2016.
 */
public class PdfHelper {
    private Context mContext;
    private Document mDocument;
    private PdfWriter mPdfWriter;
    private Paragraph mParagraph;
    private PdfPTable mPdfPTable;
    private int mLeftRightMargins = 30, mTopBottomMargins = 20;
    private int mPdfWidth = 595, mPdfHeight = 842;
    private String mAccountNo, mFromDate = "", mToDate = "";
    private BaseColor mSeparatorColor = new BaseColor(0, 0, 0);
    private Font mBigFont = new Font(Font.FontFamily.HELVETICA, 15, Font.BOLD, new BaseColor(0, 0, 0));
    private Font mNormalFont12 = new Font(Font.FontFamily.HELVETICA, 12, Font.NORMAL, new BaseColor(0, 0, 0));
    private Font mNormalFont12Bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD, new BaseColor(0, 0, 0));
    private Font mNormalFont13 = new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD, new BaseColor(0, 0, 0));
    private Font mSmallFont = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL, new BaseColor(0, 0, 0));

    public PdfHelper(String filePath, Context context, String accountNo, String fromDate, String toDate) throws FileNotFoundException, DocumentException {
        mContext = context;
        mAccountNo = accountNo;
        mFromDate = fromDate;
        mToDate = toDate;
        mDocument = new Document(PageSize.A4);
        mPdfWriter = PdfWriter.getInstance(mDocument, new FileOutputStream(filePath));
        mPdfWriter.setPageEvent(new DefaultPageEventHelper());

        mDocument.addCreationDate();
        mDocument.setMargins(mDocument.leftMargin() - 20, mDocument.rightMargin() - 20, mDocument.topMargin(), mDocument.bottomMargin() + 30);
        mDocument.open();

        mParagraph = new Paragraph();
    }

    public void createPdfTable(float[] columnWidths, float tableWidthPercentage){
        mPdfPTable = new PdfPTable(columnWidths);
        mPdfPTable.setWidthPercentage(tableWidthPercentage);
    }

    public static void insertCellToTable(PdfPTable table, String text, int alignment, int colspan, Font font, boolean shouldDrawBorder){
        PdfPCell pdfPCell = new PdfPCell(new Phrase(text, font));
        pdfPCell.setHorizontalAlignment(alignment);
        pdfPCell.setColspan(colspan);

        if(!shouldDrawBorder) pdfPCell.setBorder(Rectangle.NO_BORDER);

        if(text.trim().length() <= 0){
            pdfPCell.setMinimumHeight(10f);
        }

        table.addCell(pdfPCell);
    }

    public static void insertCellToTable(PdfPTable table, String text, int alignment, int colspan, Font font, boolean shouldDrawBorder, int paddingLeft, int paddingTopBottom){
        PdfPCell pdfPCell = new PdfPCell(new Phrase(text, font));
        pdfPCell.setHorizontalAlignment(alignment);
        pdfPCell.setColspan(colspan);
        pdfPCell.setPaddingLeft(paddingLeft);
        pdfPCell.setPaddingTop(paddingTopBottom);
        pdfPCell.setPaddingBottom(paddingTopBottom);

        if(!shouldDrawBorder) pdfPCell.setBorder(Rectangle.NO_BORDER);

        if(text.trim().length() <= 0){
            pdfPCell.setMinimumHeight(10f);
        }

        table.addCell(pdfPCell);
    }

    public static void insertCellToTable(PdfPTable table, String text, int alignment, int colspan, Font font, BaseColor borderColor, int paddingLeft, int paddingTopBottom){
        PdfPCell pdfPCell = new PdfPCell(new Phrase(text, font));
        pdfPCell.setHorizontalAlignment(alignment);
        pdfPCell.setColspan(colspan);
        pdfPCell.setPaddingLeft(paddingLeft);
        pdfPCell.setPaddingTop(paddingTopBottom);
        pdfPCell.setPaddingBottom(paddingTopBottom);

        pdfPCell.setBorderColor(borderColor);

        if(text.trim().length() <= 0){
            pdfPCell.setMinimumHeight(10f);
        }

        table.addCell(pdfPCell);
    }

    public PdfContentByte getContentByte(){
        return mPdfWriter.getDirectContent();
    }

    public PdfContentByte getContentByteUnder(){
        return mPdfWriter.getDirectContentUnder();
    }

    public void setPageEventHelper(PdfPageEventHelper helper){
        mPdfWriter.setPageEvent(helper);
    }

    public void addElement(Element element) throws DocumentException {
        mDocument.add(element);
    }

    public void addHeaderToPdf(Element element) throws DocumentException {
        mDocument.add(element);
    }

    public void addFooterToPdf(Element element) throws DocumentException {
        mDocument.add(element);
    }

    public void addTableToPdf(){
        mParagraph.add(mPdfPTable);
        mPdfPTable.setComplete(true);
    }

    public void finishWritingPdf() throws DocumentException {
        mDocument.add(mParagraph);
        mDocument.close();
        mPdfWriter.close();
    }

    /**
     * A method to add titles to the current table. If you have titles as part of your {@link ArrayList}, then just use {@code fillTableFromData} method
     * with {@code mIsFirstRowTitle} true. It will treat first item of your data as header or title of table.
     * @param count number of parameters passed in varargs.
     * @param font font to use when writing titles. Pass null if you want to use default fonts.
     * @param alignment alignment to use when writing titles.
     * @param titles titles to write to table.
     */
    public void addTitlesToTables(int count, Font font, int alignment, String... titles){
        Font fontToUse = font != null ? font : mNormalFont12Bold;
        for (int i = 0; i < count; i++) {
            insertCellToTable(mPdfPTable, titles[i], alignment, 1, fontToUse, true);
        }
        mPdfPTable.setHeaderRows(1);
    }

    public void addTitlesToTables(int count, Font font, int[] alignments, String... titles){
        Font fontToUse = font != null ? font : mNormalFont12Bold;
        for (int i = 0; i < count; i++) {
            insertCellToTable(mPdfPTable, titles[i], alignments[i], 1, fontToUse, true);
        }
        mPdfPTable.setHeaderRows(1);
    }

    public void addTitlesToTables(int count, Font font, int[] alignments, BaseColor borderColor, String... titles){
        Font fontToUse = font != null ? font : mNormalFont12Bold;
        for (int i = 0; i < count; i++) {
            insertCellToTable(mPdfPTable, titles[i], alignments[i], 1, fontToUse, borderColor, 2, 12);
        }
        mPdfPTable.setHeaderRows(1);
    }

    @SafeVarargs
    public final void fillTableFromData(int columns, int rows, boolean isFirstRowTitle, ArrayList<String>... data){
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if(isFirstRowTitle && i == 0){
                    insertCellToTable(mPdfPTable, data[j].get(i), Element.ALIGN_LEFT, 1, mNormalFont12Bold, true);
                    continue;
                }

                insertCellToTable(mPdfPTable, data[j].get(i), Element.ALIGN_LEFT, 1, mSmallFont, true);
            }
            if(isFirstRowTitle && i == 0) mPdfPTable.setHeaderRows(1);
        }
    }

    @SafeVarargs
    public final void fillTableFromData(int columns, int rows, boolean isFirstRowTitle, int[] alignments, ArrayList<String>... data){
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if(isFirstRowTitle && i == 0){
                    insertCellToTable(mPdfPTable, data[j].get(i), alignments[j], 1, mNormalFont12Bold, true);
                    continue;
                }

                insertCellToTable(mPdfPTable, data[j].get(i), alignments[j], 1, mSmallFont, true, 2, 4);
            }
            if(isFirstRowTitle && i == 0) mPdfPTable.setHeaderRows(1);
        }
    }

    @SafeVarargs
    public final void fillTableFromData(int columns, int rows, boolean isFirstRowTitle, int[] alignments, BaseColor baseColor, ArrayList<String>... data){
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if(isFirstRowTitle && i == 0){
                    insertCellToTable(mPdfPTable, data[j].get(i), alignments[j], 1, mNormalFont12Bold, baseColor, 2, 12);
                    continue;
                }

                insertCellToTable(mPdfPTable, data[j].get(i), alignments[j], 1, mSmallFont, baseColor, 2, 12);
            }
            if(isFirstRowTitle && i == 0) mPdfPTable.setHeaderRows(1);
        }
    }

    @SafeVarargs
    public final void fillTableFromData(int columns, int rows, boolean isFirstRowTitle, Font titleFont, Font normalFont, int[] alignments, ArrayList<String>... data){
        Font temp12Bold = mNormalFont12Bold;
        Font tempSmallFont = mSmallFont;
        mNormalFont12Bold = titleFont;
        mSmallFont = normalFont;
        fillTableFromData(columns, rows, isFirstRowTitle, alignments, data);
        mNormalFont12Bold = temp12Bold;
        mSmallFont = tempSmallFont;
    }

    @SafeVarargs
    public final void fillTableFromData(int columns, int rows, boolean isFirstRowTitle, Font titleFont, Font normalFont, int[] alignments, BaseColor borderColor, ArrayList<String>... data){
        Font temp12Bold = mNormalFont12Bold;
        Font tempSmallFont = mSmallFont;
        mNormalFont12Bold = titleFont;
        mSmallFont = normalFont;
        fillTableFromData(columns, rows, isFirstRowTitle, alignments, borderColor, data);
        mNormalFont12Bold = temp12Bold;
        mSmallFont = tempSmallFont;
    }

    public void showTextAtPosition(int alignment, String text, float x, float y, float rotation){
        ColumnText.showTextAligned(mPdfWriter.getDirectContent(), alignment, new Phrase(text), x, y, rotation);
    }

    public void showTextAtPosition(int alignment, String text, float x, float y, float rotation, BaseFont baseFont, float size){
        getContentByte().saveState();
        getContentByte().setFontAndSize(baseFont, size);
        getContentByte().showTextAligned(alignment, text, x, y, rotation);
        getContentByte().restoreState();
    }

    public Font getBigFont() {
        return mBigFont;
    }

    public Font getNormalFont12() {
        return mNormalFont12;
    }

    public Font getNormalFont13() {
        return mNormalFont13;
    }

    public Font getSmallFont() {
        return mSmallFont;
    }

    public Font getNormalFont12Bold() {
        return mNormalFont12Bold;
    }

    public BaseColor getSeparatorColor() {
        return mSeparatorColor;
    }

    private class DefaultPageEventHelper extends PdfPageEventHelper{
        private BaseFont helveticaBold;
        private BaseFont baseFont;

        public DefaultPageEventHelper(){
            try {
                helveticaBold = BaseFont.createFont("Helvetica-Bold", "Cp1252", false);
                baseFont = BaseFont.createFont("Helvetica", "Cp1252", false);
            } catch (DocumentException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onStartPage(PdfWriter writer, Document document) {
            /*ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_TOP, new Phrase(mContext.getString(R.string.bank_name)), mLeftRightMargins / 2, mPdfHeight - mTopBottomMargins, 0);
            ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_TOP, new Phrase("Bank Statement"), mPdfWidth - mLeftRightMargins - 70, mPdfHeight - mTopBottomMargins, 0);*/

            showTextAtPosition(Element.ALIGN_TOP, mContext.getString(R.string.bank_name), mLeftRightMargins / 2, mPdfHeight - mTopBottomMargins - 10 , 0, baseFont, 10);
            showTextAtPosition(Element.ALIGN_TOP, "Bank Statement", mPdfWidth - mLeftRightMargins - 60, mPdfHeight - mTopBottomMargins - 10, 0, baseFont, 10);

            if(document.getPageNumber() == 1){
                int imageWidth = 83;
                Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.bank_logo);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                Image image = null;
                try {
                    image = Image.getInstance(byteArrayOutputStream.toByteArray());
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (BadElementException e) {
                    e.printStackTrace();
                }

                image.scaleAbsolute(imageWidth, imageWidth);
                image.setAlignment(Element.ALIGN_CENTER);
                float x = (mPdfWidth / 2) - imageWidth / 2;
                float y = mPdfHeight - mTopBottomMargins * 2;
                image.setAbsolutePosition(x, y - 60);
                try {
                    writer.getDirectContent().addImage(image);
                } catch (DocumentException e) {
                    e.printStackTrace();
                }

                return;
            }

            Paragraph paragraph = new Paragraph();

            LineSeparator lineSeparator = new LineSeparator();
            lineSeparator.setLineColor(mSeparatorColor);
            lineSeparator.setLineWidth(1f);
            paragraph.add(lineSeparator);

            paragraph.add("\n");
            paragraph.add("\n");

            /*Paragraph accountNo = new Paragraph();
            accountNo.setAlignment(Element.ALIGN_LEFT);
            try {
                accountNo.add(MySharedPreferences.getCustomerName(mContext) + " - " + mAccountNo);
            } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException e) {
                e.printStackTrace();
            }
            paragraph.add(accountNo);

            if(mFromDate.trim().length() > 0 && mToDate.trim().length() > 0) {
                Paragraph dates = new Paragraph();
                dates.setAlignment(Element.ALIGN_LEFT);
                dates.add(mFromDate + " TO " + mToDate);
                paragraph.add(dates);
            }*/

            if(mFromDate != null && mToDate != null && mFromDate.length() > 0 && mToDate.length() > 0){
                paragraph.add("\n");
            }

            paragraph.add(lineSeparator);
            try {
                showTextAtPosition(Element.ALIGN_LEFT, mAccountNo + " - " + MySharedPreferences.getCustomerName(mContext), 50, 770, 0, helveticaBold, 12);
                if(mFromDate != null && mToDate != null && mFromDate.length() > 0 && mToDate.length() > 0){
                    showTextAtPosition(Element.ALIGN_LEFT, "Statement from "+mFromDate+" to "+mToDate, 50, 755, 0, helveticaBold, 12);
                }
            } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e) {
                e.printStackTrace();
            }

            paragraph.add("\n");
            paragraph.add("\n");
            paragraph.add("\n");

            try {
                //mPdfWriter.add(paragraph);
                mPdfWriter.getDirectContent().getPdfDocument().add(paragraph);
            } catch (DocumentException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onEndPage(PdfWriter writer, Document document) {
            /*LineSeparator lineSeparator = new LineSeparator();
            lineSeparator.setLineColor(new BaseColor(33, 33, 33));
            lineSeparator.setLineWidth(3);

            try {
                writer.getDirectContent().getPdfDocument().add(lineSeparator);
            } catch (DocumentException e) {
                e.printStackTrace();
            }*/

            PdfContentByte directContentUnder = writer.getDirectContentUnder();
            directContentUnder.moveTo(document.left(), 40);
            directContentUnder.setLineWidth(1f);
            directContentUnder.setColorStroke(mSeparatorColor);
            directContentUnder.lineTo(document.right(), 40);

            showTextAtPosition(Element.ALIGN_BOTTOM, "Page " + document.getPageNumber(), 535, 20, 0, baseFont, 10);
        }
    }
}
