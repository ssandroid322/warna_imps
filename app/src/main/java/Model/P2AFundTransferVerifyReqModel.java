package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by samvidmistry on 30/8/16.
 */

public class P2AFundTransferVerifyReqModel extends BaseModel {
    @SerializedName("USER_ID")
    private String mUserId;
    @SerializedName("ACTIVITY_CD")
    private String mActivityCode;
    @SerializedName("ACCT_NO")
    private String mAccountNo;
    @SerializedName("CHANNEL")
    private String mChannel = "Mobile";
    @SerializedName("BENF_ACCT_NO")
    private String mBenefAccountNo;
    @SerializedName("BENF_IFSC")
    private String mBenefIfsc;
    @SerializedName("REMARKS")
    private String mRemarks;
    @SerializedName("IMPS_REF_CD")
    private String mImpsRefCode;
    @SerializedName("OTP_NO")
    private String mOtpNo;
    @SerializedName("TRN_AMT")
    private String mTransactionAmount;
    @SerializedName("PIN_NO")
    private String mPinNo;
//    @SerializedName("MOBILE_NO")
//    private String mContact;

    public P2AFundTransferVerifyReqModel(String userId, String activityCode, String accountNo,
                                         String channel, String impsRefCode, String otpNo,
                                         String benefAccountNo, String benefIfsc,
                                         String transactionAmount, String pinNo, String remarks,
                                         String contact) {
        mUserId = userId;
        mActivityCode = activityCode;
        mAccountNo = accountNo;
        mChannel = channel;
        mImpsRefCode = impsRefCode;
        mOtpNo = otpNo;
        mBenefAccountNo = benefAccountNo;
        mBenefIfsc = benefIfsc;
        mTransactionAmount = transactionAmount;
        mPinNo = pinNo;
        mRemarks = remarks;
//        mContact = contact;
    }

    public String getmImpsRefCode() {
        return mImpsRefCode;
    }

    public void setmImpsRefCode(String mImpsRefCode) {
        this.mImpsRefCode = mImpsRefCode;
    }
}
