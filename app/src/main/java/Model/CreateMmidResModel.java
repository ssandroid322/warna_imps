package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by samvidmistry on 30/8/16.
 */

public class CreateMmidResModel extends BaseModel {
    @SerializedName("STATUS")
    private String mStatus;
    @SerializedName("RESPONSE")
    private Response[] mResponses;

    public int getStatus() {
        return Integer.parseInt(mStatus);
    }

    public Response[] getResponses() {
        return mResponses;
    }

    public static class Response {
        @SerializedName("MOBILE_NO")
        private String mMobileNo;
        @SerializedName("ACCT_NO")
        private String mAccountNo;
        @SerializedName("RESP_CD")
        private String mResCode;
        @SerializedName("RESP_DESC")
        private String mResDesc;
        @SerializedName("MMID")
        private String mMmid;

        public Response(String mobileNo, String accountNo, String resCode, String resDesc, String mmid) {
            mMobileNo = mobileNo;
            mAccountNo = accountNo;
            mResCode = resCode;
            mResDesc = resDesc;
            mMmid = mmid;
        }

        public String getMobileNo() {
            return mMobileNo;
        }

        public String getAccountNo() {
            return mAccountNo;
        }

        public String getResCode() {
            return mResCode;
        }

        public String getResDesc() {
            return mResDesc;
        }

        public String getMmid() {
            return mMmid;
        }
    }
}
