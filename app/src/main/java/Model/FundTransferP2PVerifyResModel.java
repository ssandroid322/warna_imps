package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class FundTransferP2PVerifyResModel extends BaseModel {
    @SerializedName("STATUS")
    private String mStaus;

    @SerializedName("RESPONSE")
    private Response[] mResponse;

    public Response[] getResponse() {
        return mResponse;
    }

    public int getStatus() {
        return Integer.parseInt(mStaus);
    }


    public static class Response {
        private String mUserId;
        private String mActivityCode;
        private String mRequestCode;

        @SerializedName("RESP_TRN_REF")
        private String mREFNO;

        @SerializedName("RESP_DESC")
        private String mResponseDESC;

        public Response(String userId, String activityCode, String requestCode) {
            mUserId = userId;
            mActivityCode = activityCode;
            mRequestCode = requestCode;
        }

        public String getUserId() {
            return mUserId;
        }

        public String getActivityCode() {
            return mActivityCode;
        }

        public String getRequestCode() {
            return mRequestCode;
        }

        public String getREFNO() {
            return mREFNO;
        }

        public String getResponseDESC() {
            return mResponseDESC;
        }
    }
}
