package Model;

import android.support.annotation.Nullable;

import com.waranabank.mobipro.AddBenificiaryVerify;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class AddBeneficiaryResModel extends BaseModel {
    @SerializedName("STATUS")
    private String mStatus;
    @Nullable
    @SerializedName("RESPONSE")
    Responce[] responce;


    public class Responce{
        @Nullable
        @SerializedName("USER_ID")
        String userid;
        @Nullable
        @SerializedName("ACTIVITY_CD")
        String activitycode;
        @Nullable
        @SerializedName("REQUEST_CD")
        String requestcode;

        public Responce(String userid, String activitycode, String requestcode) {
            this.userid = userid;
            this.activitycode = activitycode;
            this.requestcode = requestcode;
        }

        public String getUserid() {
            return userid;
        }

        public String getActivitycode() {
            return activitycode;
        }

        public String getRequestcode() {
            return requestcode;
        }
    }

    public Responce[] getResponce() {
        return responce;
    }

    public int getStatus() {
        return Integer.parseInt(mStatus);
    }
}
