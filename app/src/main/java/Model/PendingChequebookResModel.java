package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 25/02/2016.
 */
public class PendingChequebookResModel extends BaseModel {
    @SerializedName("STATUS")
    private String mStatus;
    @SerializedName("RESPONSE")
    private Response[] mResponses;

    public int getStatus() {
        return Integer.parseInt(mStatus);
    }

    public Response[] getResponses() {
        return mResponses;
    }

    public static class Response{
        @SerializedName("REQ_REF_NO")
        private String mReqRefNo;
        @SerializedName("REQ_DATE")
        private String mReqDate;
        @SerializedName("NO_OF_LEAF")
        private String mNoOfLeaf;
        @SerializedName("STATUS")
        private String mStatus;

        public Response(String reqRefNo, String reqDate, String noOfLeaf, String status) {
            mReqRefNo = reqRefNo;
            mReqDate = reqDate;
            mNoOfLeaf = noOfLeaf;
            mStatus = status;
        }

        public String getReqRefNo() {
            return mReqRefNo;
        }

        public String getReqDate() {
            return mReqDate;
        }

        public String getNoOfLeaf() {
            return mNoOfLeaf;
        }

        public String getStatus() {
            return mStatus;
        }
    }
}
