package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 26/10/2016.
 */
public class AddBenificiaryVerifyReqModel extends BaseModel {
    @SerializedName("USER_ID")
    private String mUserID ;
    @SerializedName("ACTIVITY_CD")
    private String mActivityCode ;
    @SerializedName("TRN_TYPE")
    private String mTrnType;
    @SerializedName("TO_IFSCCODE")
    private String mToIFSCCode;
    @SerializedName("TO_ACCT_NO")
    private String mToAcctNo;
    @SerializedName("TO_ACCT_NM")
    private String mToAcctName;
    @SerializedName("TO_ADD1")
    private String mToAdd1;
    @SerializedName("TO_CONTACT_NO")
    private String mToContactNo;
    @SerializedName("OTP_NO")
    private String mOTPNo;
    @SerializedName("REQUEST_CD")
    private String mRequestedCode;

    public AddBenificiaryVerifyReqModel(String mUserID, String mActivityCode, String mTrnType, String mToIFSCCode, String mToAcctNo, String mToAcctName, String mToAdd1, String mToContactNo, String mOTPNo, String mRequestedCode) {
        this.mUserID = mUserID;
        this.mActivityCode = mActivityCode;
        this.mTrnType = mTrnType;
        this.mToIFSCCode = mToIFSCCode;
        this.mToAcctNo = mToAcctNo;
        this.mToAcctName = mToAcctName;
        this.mToAdd1 = mToAdd1;
        this.mToContactNo = mToContactNo;
        this.mOTPNo = mOTPNo;
        this.mRequestedCode = mRequestedCode;
    }

    public String getmUserID() {
        return mUserID;
    }

    public String getmActivityCode() {
        return mActivityCode;
    }

    public String getmTrnType() {
        return mTrnType;
    }

    public String getmToIFSCCode() {
        return mToIFSCCode;
    }

    public String getmToAcctNo() {
        return mToAcctNo;
    }

    public String getmToAcctName() {
        return mToAcctName;
    }

    public String getmToAdd1() {
        return mToAdd1;
    }

    public String getmToContactNo() {
        return mToContactNo;
    }

    public String getmOTPNo() {
        return mOTPNo;
    }

    public String getmRequestedCode() {
        return mRequestedCode;
    }

    /*[{"USER_ID":"PRIME0000010","ACTIVITY_CD":"70379402","TRN_TYPE":"R","TO_IFSCCODE":
        "SBIN0000454","TO_ACCT_NO":"11101214215454","TO_ACCT_NM":"MAHENDRA SUTHAR",
        "TO_ADD1":"11101214215454","TO_CONTACT_NO":"9787454566",
        "OTP_NO":"00000000","REQUEST_CD":"33"}]    */
}


