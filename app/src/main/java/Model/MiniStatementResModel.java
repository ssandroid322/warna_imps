package Model;

import com.google.gson.annotations.SerializedName;

import Utility.StringUtils;

/**
 * Created by Samvid Mistry on 11/02/2016.
 */
public class MiniStatementResModel extends BaseModel {
    @SerializedName("STATUS")
    private String mStatus;
    @SerializedName("RESPONSE")
    private Response[] mResponses;

    public int getStatus() {
        return Integer.parseInt(mStatus);
    }

    public Response[] getResponses() {
        return mResponses;
    }

    public static class Response{
        @SerializedName("TRN_DATE")
        private String mTransactionDate;
        @SerializedName("NARRATION")
        private String mNarration;
        @SerializedName("TRN_AMOUNT")
        private String mTransactionAmount;
        @SerializedName("CD_DR_TYPE")
        private String mCdDrType;
        @SerializedName("TRAN_CD")
        private String mTrasnactionCode;

        public Response(String mTransactionDate, String mNarration, String mTransactionAmount, String mCdDrType, String mTrasnactionCode) {
            this.mTransactionDate = mTransactionDate;
            this.mNarration = mNarration;
            this.mTransactionAmount = mTransactionAmount;
            this.mCdDrType = mCdDrType;
            this.mTrasnactionCode = mTrasnactionCode;
        }

        public String getTrasnactionCode() {
            return mTrasnactionCode;
        }

        public String getTransactionDate() {
            return mTransactionDate;
        }

        public String getNarration() {
            return mNarration;
        }

        public String getTransactionAmount() {

//            return mTransactionAmount;
            return StringUtils.commaSeparated(mTransactionAmount);
        }

        public String getCdDrType() {
            return mCdDrType;
        }
    }
}
