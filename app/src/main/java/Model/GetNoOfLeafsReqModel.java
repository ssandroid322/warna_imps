package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class GetNoOfLeafsReqModel extends BaseModel {
    @SerializedName("USER_ID")
    private String mUserId;
    @SerializedName("ACTIVITY_CD")
    private String mActivityCode;
    @SerializedName("ACCT_TYPE")
    private String mAccountType;

    public GetNoOfLeafsReqModel(String mUserId, String mActivityCode, String mAccountType) {
        this.mUserId = mUserId;
        this.mActivityCode = mActivityCode;
        this.mAccountType = mAccountType;
    }
    public GetNoOfLeafsReqModel()
    {

    }
}
