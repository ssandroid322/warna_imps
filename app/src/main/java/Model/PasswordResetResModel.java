package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class PasswordResetResModel extends BaseModel {
    @SerializedName("STATUS")
    private String mStatus;
    @SerializedName("RESPONSE")
    private Response[] mResponse;

    public int getStatus() {
        return Integer.parseInt(mStatus);
    }

    public Response[] getResponse() {
        return mResponse;
    }

    public static class Response{
        @SerializedName("CUSTOMER_ID")
        private String mCustomerId;
        @SerializedName("MOBILE_NO")
        private String mMobileNo;
        @SerializedName("REQUEST_CD")
        private String mRequestCode;

        public Response(String customerId, String mobileNo, String requestCode) {
            mCustomerId = customerId;
            mMobileNo = mobileNo;
            mRequestCode = requestCode;
        }

        public String getCustomerId() {
            return mCustomerId;
        }

        public String getMobileNo() {
            return mMobileNo;
        }

        public String getRequestCode() {
            return mRequestCode;
        }
    }
}
