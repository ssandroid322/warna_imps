package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 13/02/2016.
 */
public class DetailedStatementReqModel extends BaseModel {
    @SerializedName("USER_ID")
    private String mUserId;
    @SerializedName("ACTIVITY_CD")
    private String mActivityCode;
    @SerializedName("ACCT_NO")
    private String mAccountNo;
    @SerializedName("FROM_DT")
    private String mFromDate;
    @SerializedName("TO_DT")
    private String mToDate;

    public DetailedStatementReqModel(String userId, String activityCode, String accountNo, String fromDate, String toDate) {
        mUserId = userId;
        mActivityCode = activityCode;
        mAccountNo = accountNo;
        mFromDate = fromDate;
        mToDate = toDate;
    }
}
