package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class FundTransferP2PVerifyReqModel extends BaseModel {
    @SerializedName("USER_ID")
    private String mUserId;

    @SerializedName("ACTIVITY_CD")
    private String mActivityCode;

    @SerializedName("OTP_NO")
    private String mOtpNo;

    @SerializedName("ACCT_NO")
    private String mAccountNo;

    @SerializedName("CHANNEL")
    private String mChannel;

    @SerializedName("BENF_MOB_NO")
    private String mBenificiaryMobileno;

    @SerializedName("BENF_MMID")
    private String mBenificiaryMMID;


    @SerializedName("TRN_AMT")
    private String mAmount;

    @SerializedName("PIN_NO")
    private String mPINNo;


    @SerializedName("REMARKS")
    private String mRemarks;


    @SerializedName("IMPS_REF_CD")
    private String mIMPSRefCode;

    public FundTransferP2PVerifyReqModel(String mUserId, String mActivityCode, String mOtpNo, String mAccountNo, String mBenificiaryMobileno, String mBenificiaryMMID, String mAmount, String mRemarks, String mIMPSRefCode) {
        this.mUserId = mUserId;
        this.mActivityCode = mActivityCode;
        this.mOtpNo = mOtpNo;
        this.mAccountNo = mAccountNo;
        this.mBenificiaryMobileno = mBenificiaryMobileno;
        this.mBenificiaryMMID = mBenificiaryMMID;
        this.mAmount = mAmount;
        this.mRemarks = mRemarks;
        this.mIMPSRefCode = mIMPSRefCode;
        this.mChannel = "Mobile";
        this.mPINNo = "00000";
    }

    public String getmIMPSRefCode() {
        return mIMPSRefCode;
    }

    public void setmIMPSRefCode(String mIMPSRefCode) {
        this.mIMPSRefCode = mIMPSRefCode;
    }
}