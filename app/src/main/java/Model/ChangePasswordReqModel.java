package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 15/02/2016.
 */
public class ChangePasswordReqModel extends BaseModel {
    @SerializedName("USER_ID")
    private String mUserId;
    @SerializedName("PASSWORD")
    private String mPassword;
    @SerializedName("NEW_PASSWORD")
    private String mNewPassword;

    public ChangePasswordReqModel(String userId, String password, String newPassword) {
        mUserId = userId;
        mPassword = password;
        mNewPassword = newPassword;
    }
}
