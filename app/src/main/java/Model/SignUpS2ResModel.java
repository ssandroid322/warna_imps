package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 10/02/2016.
 */
public class SignUpS2ResModel extends BaseModel {
    @SerializedName("STATUS")
    private String mStatus;
    @SerializedName("RESPONSE")
    private Response[] mResponses;

    public int getStatus() {
        return Integer.parseInt(mStatus);
    }

    public Response[] getResponses() {
        return mResponses;
    }

    public static class Response{
        @SerializedName("CUSTOMER_ID")
        private String mCustomerId;
        @SerializedName("USER_ID")
        private String mUserId;

        private Response(String customerId, String userId) {
            mCustomerId = customerId;
            mUserId = userId;
        }

        public String getCustomerId() {
            return mCustomerId;
        }

        public String getUserId() {
            return mUserId;
        }
    }
}
