package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class FundTransferVerifyReqModel extends BaseModel {
    @SerializedName("USER_ID")
    private String mUserId;
    @SerializedName("ACTIVITY_CD")
    private String mActivityCode;
    @SerializedName("REQUEST_CD")
    private String mRequestCode;
    @SerializedName("OTP_NO")
    private String mOtpNo;



    public FundTransferVerifyReqModel(String userId, String activityCode, String requestCode, String otpNo) {
        mUserId = userId;
        mActivityCode = activityCode;
        mRequestCode = requestCode;
        mOtpNo = otpNo;
    }
}
