package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by samvidmistry on 30/8/16.
 */

public class P2AFundTransferReqModel extends BaseModel {
    @SerializedName("USER_ID")
    private String mUserId;
    @SerializedName("ACTIVITY_CD")
    private String mActivityCode;
    @SerializedName("ACCT_NO")
    private String mAccountNo;
    @SerializedName("CHANNEL")
    private String mChannel = "Mobile";
    @SerializedName("BENF_ACCT_NO")
    private String mBenefAccountNo;
    @SerializedName("BENF_IFSC")
    private String mBenefIfsc;
    @SerializedName("TRN_AMT")
    private String mTransAmount;
    @SerializedName("PIN_NO")
    private String mPin;
    @SerializedName("REMARKS")
    private String mRemarks;
//    @SerializedName("MOBILE_NO")
//    private String mContactNo;

    public P2AFundTransferReqModel(String userId, String activityCode, String accountNo,
                                   String benefAccountNo, String benefIfsc, String transAmount,
                                   String pin, String remarks, String mobile) {
        mUserId = userId;
        mActivityCode = activityCode;
        mAccountNo = accountNo;
        mBenefAccountNo = benefAccountNo;
        mBenefIfsc = benefIfsc;
        mTransAmount = transAmount;
        mPin = pin;
        mRemarks = remarks;
//        mContactNo = mobile;
    }

    public String getUserId() {
        return mUserId;
    }

    public String getActivityCode() {
        return mActivityCode;
    }

    public String getAccountNo() {
        return mAccountNo;
    }

    public String getChannel() {
        return mChannel;
    }

    public String getBenefAccountNo() {
        return mBenefAccountNo;
    }

    public String getBenefIfsc() {
        return mBenefIfsc;
    }

    public String getTransAmount() {
        return mTransAmount;
    }

    public String getPin() {
        return mPin;
    }

    public String getRemarks() {
        return mRemarks;
    }

//    public String getContactNo() {
//        return mContactNo;
//    }
}
