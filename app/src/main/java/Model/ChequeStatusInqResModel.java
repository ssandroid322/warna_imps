package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class ChequeStatusInqResModel extends BaseModel {
    @SerializedName("STATUS")
    private String mStatus;
    @SerializedName("RESPONSE")
    private Response[] mResponse;

    public int getStatus() {
        return Integer.parseInt(mStatus);
    }

    public Response[] getResponses() {
        return mResponse;
    }

    public static class Response{
        @SerializedName("TRN_DATE")
        private String mTransactionDate;
        @SerializedName("CHQ_STATUS")
        private String mChequeStatus;
        @SerializedName("REMARKS")
        private String mRemarks;
        @SerializedName("AMOUNT")
        private String mAmount;

        public Response(String transactionDate, String chequeStatus, String remarks, String amount) {
            mTransactionDate = transactionDate;
            mChequeStatus = chequeStatus;
            mRemarks = remarks;
            mAmount = amount;
        }

        public String getTransactionDate() {
            return mTransactionDate;
        }

        public String getChequeStatus() {
            return mChequeStatus;
        }

        public String getRemarks() {
            return mRemarks;
        }

        public String getAmount() {
            return mAmount;
        }
    }
}
