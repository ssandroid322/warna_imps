package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class ChequeStatusInqReqModel extends BaseModel {
    @SerializedName("USER_ID")
    private String mUserId;
    @SerializedName("ACTIVITY_CD")
    private String mActivityCode;
    @SerializedName("ACCT_NO")
    private String mAccountNo;
    @SerializedName("CHQ_NO")
    private String mChequeNo;

    public ChequeStatusInqReqModel(String userId, String activityCode, String accountNo, String chequeNo) {
        mUserId = userId;
        mActivityCode = activityCode;
        mAccountNo = accountNo;
        mChequeNo = chequeNo;
    }
}
