package Model;

import com.google.gson.annotations.SerializedName;

import Utility.StringUtils;

/**
 * Created by Samvid Mistry on 10/02/2016.
 */
public class OperativeAccountResModel extends BaseModel {
    @SerializedName("STATUS")
    private String mStatus;
    @SerializedName("RESPONSE")
    private Response[] mResponses;

    public int getStatus() {
        return Integer.parseInt(mStatus);
    }

    public Response[] getResponses() {
        return mResponses;
    }

    public static class Response{
        @SerializedName("CUSTOMER_ID")
        private String mCustomerId;
        @SerializedName("ACCT_NO")
        private String mAccountNo;
        @SerializedName("BRANCH_CD")
        private String mBranchCode;
        @SerializedName("BRANCH_NM")
        private String mBranchName;
        @SerializedName("ACCT_TYPE")
        private String mAccountType;
        @SerializedName("ACCT_TYPE_NM")
        private String mAccountTypeName;
        @SerializedName("PARENT_TYPE")
        private String mParentType;
        @SerializedName("ACCT_CD")
        private String mAccountCode;
        @SerializedName("OP_DATE")
        private String mOpeningDate;
        @SerializedName("LEAN_AMOUNT")
        private String mLienAmount;
        @SerializedName("UNCLEAR_BALANCE")
        private String mUnclearBalance;
        @SerializedName("OPENING_BALANCE")
        private String mOpeningBalance;
        @SerializedName("BALANCE")
        private String mBalance;
        @SerializedName("CD_DR_TYPE")
        private String mCdDrType;
        @SerializedName("LIMIT_AMOUNT")
        private String mLimitAmount;
        @SerializedName("DRAWING_POWER")
        private String mDrawingPower;

        @SerializedName("CHQ_BOOK_REQ")
        private String mChequeBookRequest;

        @SerializedName("RTGS")
        private String mRTGS;
        @SerializedName("RTGS_LIMIT")
        private String mRTGSLimit;

        @SerializedName("NEFT")
        private String mNEFT;
        @SerializedName("NEFT_LIMIT")
        private String mNEFTLimit;

        @SerializedName("IFT")
        private String mIFT;
        @SerializedName("IFT_LIMIT")
        private String mIFTLimit;

        @SerializedName("IMPS")
        private String mIMPS;
        @SerializedName("IMPS_LIMIT")
        private String mIMPSLimit;

        @SerializedName("MMID")
        private String mMMID;

        public Response(String mCustomerId, String mAccountNo, String mBranchCode, String mBranchName, String mAccountType, String mAccountTypeName,
                        String mParentType, String mAccountCode, String mOpeningDate, String mLienAmount, String mUnclearBalance,
                        String mOpeningBalance, String mBalance, String mCdDrType, String mLimitAmount, String mDrawingPower,
                        String mChequeBookRequest, String mRTGS, String mRTGSLimit, String mNEFT, String mNEFTLimit,
                        String mIFT, String mIFTLimit, String mIMPS, String mIMPSLimit, String mMMID)
        {
            this.mCustomerId = mCustomerId;
            this.mAccountNo = mAccountNo;
            this.mBranchCode = mBranchCode;
            this.mBranchName = mBranchName;
            this.mAccountType = mAccountType;
            this.mAccountTypeName = mAccountTypeName;
            this.mParentType = mParentType;
            this.mAccountCode = mAccountCode;
            this.mOpeningDate = mOpeningDate;
            this.mLienAmount = mLienAmount;
            this.mUnclearBalance = mUnclearBalance;
            this.mOpeningBalance = mOpeningBalance;
            this.mBalance = mBalance;
            this.mCdDrType = mCdDrType;
            this.mLimitAmount = mLimitAmount;
            this.mDrawingPower = mDrawingPower;
            this.mChequeBookRequest = mChequeBookRequest;
            this.mRTGS = mRTGS;
            this.mRTGSLimit = mRTGSLimit;
            this.mNEFT = mNEFT;
            this.mNEFTLimit = mNEFTLimit;
            this.mIFT = mIFT;
            this.mIFTLimit = mIFTLimit;
            this.mIMPS = mIMPS;
            this.mIMPSLimit = mIMPSLimit;
            this.mMMID = mMMID;
        }

        public int getCustomerId() {
            return Integer.parseInt(mCustomerId);
        }

        public String getAccountNo() {
            return mAccountNo;
        }

        public String getBranchCode() {
            return mBranchCode;
        }

        public String getBranchName() {
            return mBranchName;
        }

        public String getAccountType() {
            return mAccountType;
        }

        public String getAccountTypeName() {
            return mAccountTypeName;
        }

        public String getParentType() {
            return mParentType;
        }

        public String getAccountCode() {
            return mAccountCode;
        }

        public String getOpeningDate() {
            return mOpeningDate;
        }

        public String getLienAmount() {
            return StringUtils.commaSeparated(mLienAmount);
        }

        public String getUnclearBalance() {
            return StringUtils.commaSeparated(mUnclearBalance);
        }

        public String getOpeningBalance() {
            String s = StringUtils.commaSeparated(mOpeningBalance);
            return s;
        }


        public String getBalance() {
            return StringUtils.commaSeparated(mBalance);
       }

        public String getCdDrType() {
            return mCdDrType;
        }

        public String getLimitAmount() {
            return mLimitAmount;
        }

        public String getDrawingPower() {
            return mDrawingPower;
        }

        public String getChequeBookRequest() {
            return mChequeBookRequest;
        }

        public String getRTGS() {
            return mRTGS;
        }

        public String getRTGSLimit() {
            return mRTGSLimit;
        }

        public String getNEFT() {
            return mNEFT;
        }

        public String getNEFTLimit() {
            return mNEFTLimit;
        }

        public String getIFT() {
            return mIFT;
        }

        public String getIFTLimit() {
            return mIFTLimit;
        }

        public String getMMID() {
            return mMMID;
        }

        public String getIMPS() {
            return mIMPS;
        }

        public String getIMPSLimit() {
            return mIMPSLimit;
        }
    }
}
