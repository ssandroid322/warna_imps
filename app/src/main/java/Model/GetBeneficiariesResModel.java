package Model;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class GetBeneficiariesResModel extends BaseModel {
    @SerializedName("STATUS")
    private String mStatus;
    @SerializedName("RESPONSE")
    private Response[] mResponse;

    public int getStatus() {
        return Integer.parseInt(mStatus);
    }

    public Response[] getResponse() {
        return mResponse;
    }

    public static class Response {
        @SerializedName("TRN_TYPE")
        private String mTransactionType;
        @SerializedName("TO_IFSCCODE")
        private String mToIfscCode;
        @SerializedName("TO_ACCT_NO")
        private String mToAccountNo;
        @SerializedName("TO_ACCT_NM")
        private String mToAccountName;
        @SerializedName("TO_ADD1")
        private String mToAddressOne;
        @SerializedName("TO_CONTACT_NO")
        private String mToContactNo;
        @Nullable
        @SerializedName("IFT")
        private String mIFT;
        @Nullable
        @SerializedName("RTGS")
        private String mRTGS;
        @SerializedName("NEFT")
        @Nullable
        private String mNEFT;
        @Nullable
        @SerializedName("IMPS")
        private String mIMPS;


        public Response(String transactionType, String toIfscCode, String toAccountNo, String toAccountName, String toAddressOne, String toContactNo, String mIFT, String mRTGS, String mNEFT, String mIMPS) {
            mTransactionType = transactionType;
            mToIfscCode = toIfscCode;
            mToAccountNo = toAccountNo;
            mToAccountName = toAccountName;
            mToAddressOne = toAddressOne;
            mToContactNo = toContactNo;
            this.mIFT = mIFT;
            this.mRTGS = mRTGS;
            this.mNEFT = mNEFT;
            this.mIMPS = mIMPS;
        }

        public String getmIFT() {
            return mIFT;
        }

        public String getmRTGS() {
            return mRTGS;
        }

        public String getmNEFT() {
            return mNEFT;
        }

        public String getmIMPS() {
            return mIMPS;
        }

        public String getTransactionType() {
            return mTransactionType;
        }

        public String getToIfscCode() {
            return mToIfscCode;
        }

        public String getToAccountNo() {
            return mToAccountNo;
        }

        public String getToAccountName() {
            return mToAccountName;
        }

        public String getToAddressOne() {
            return mToAddressOne;
        }

        public String getToContactNo() {
            return mToContactNo;
        }
    }
}
