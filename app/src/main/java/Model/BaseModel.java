package Model;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by chitan on 10/17/2015.
 */
public class BaseModel implements Serializable{
    protected  String name;
    private JSONObject objJson;
    protected ArrayList<BaseModel> modelArray;

    public ArrayList<BaseModel> getModelArray() {
        return modelArray;
    }

    public void setModelArray(ArrayList<BaseModel> modelArray) {
        this.modelArray = modelArray;
    }

    public JSONObject getObjJson() {
        return objJson;
    }

    public void setObjJson(JSONObject objJson) {
        this.objJson = objJson;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
