package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class BranchDetailsResModel extends BaseModel {
    @SerializedName("STATUS")
    private String mStatus;
    @SerializedName("RESPONSE")
    private Response[] mResponse;

    public int getStatus() {
        return Integer.parseInt(mStatus);
    }

    public Response[] getResponse() {
        return mResponse;
    }

    public static class Response{
        @SerializedName("BRANCH_CD")
        private String mBranchCode;
        @SerializedName("BRANCH_NM")
        private String mBranchName;
        @SerializedName("ADD1")
        private String mAddressOne;
        @SerializedName("ADD2")
        private String mAddressTwo;
        @SerializedName("AREA_NM")
        private String mAreaName;
        @SerializedName("PIN_CODE")
        private String mPinCode;
        @SerializedName("CITY_NM")
        private String mCityName;
        @SerializedName("STATE_NM")
        private String mStateName;
        @SerializedName("COUNTRY_NM")
        private String mCountryName;
        @SerializedName("CONTACT1")
        private String mContactOne;
        @SerializedName("CONTACT2")
        private String mContactTwo;
        @SerializedName("E_MAIL_ID")
        private String mEmailId;
        @SerializedName("IFSC_CODE")
        private String mIFSCode;
        @SerializedName("MICR_CODE")
        private String mMICR_CODE;

        public Response(String branchCode, String branchName, String addressOne, String addressTwo, String areaName, String pinCode, String cityName, String stateName, String countryName, String contactOne, String contactTwo, String emailId,String ifsCode,String micrCode) {
            mBranchCode = branchCode;
            mBranchName = branchName;
            mAddressOne = addressOne;
            mAddressTwo = addressTwo;
            mAreaName = areaName;
            mPinCode = pinCode;
            mCityName = cityName;
            mStateName = stateName;
            mCountryName = countryName;
            mContactOne = contactOne;
            mContactTwo = contactTwo;
            mEmailId = emailId;
            mIFSCode = ifsCode;
            mMICR_CODE= micrCode;
        }

        public String getBranchCode() {
            return mBranchCode;
        }

        public String getBranchName() {
            return mBranchName;
        }

        public String getAddressOne() {
            return mAddressOne;
        }

        public String getAddressTwo() {
            return mAddressTwo;
        }

        public String getAreaName() {
            return mAreaName;
        }

        public String getPinCode() {
            return mPinCode;
        }

        public String getCityName() {
            return mCityName;
        }

        public String getStateName() {
            return mStateName;
        }

        public String getCountryName() {
            return mCountryName;
        }

        public String getContactOne() {
            return mContactOne;
        }

        public String getContactTwo() {
            return mContactTwo;
        }

        public String getEmailId() {
            return mEmailId;
        }
        public String getIFSCode() {
            return mIFSCode;
        }
        public String getMICR_CODE() {
            return mMICR_CODE;
        }
    }
}
