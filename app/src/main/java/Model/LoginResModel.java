package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 09/02/2016.
 */
public class LoginResModel extends BaseModel {
    @SerializedName("STATUS")
    private String mStatus;
    @SerializedName("ACTIVITY_CD")
    private String mActivityCode;
    @SerializedName("RESPONSE")
    private Response[] mResponse;

    public static class Response{
        @SerializedName("CUSTOMER_ID")
        private String mCustomerId;
        @SerializedName("USER_ID")
        private String mUserId;
        @SerializedName("CUSTOMER_NM")
        private String mCustomerName;
        @SerializedName("LAST_LOGIN_DT")
        private String mLastLoginDate;
        @SerializedName("MOBILE_NO")
        private String mMobileNum;

        private Response(String customerId, String userId, String customerName, String lastLoginDate, String mobileNum) {
            mCustomerId = customerId;
            mUserId = userId;
            mCustomerName = customerName;
            mLastLoginDate = lastLoginDate;
            mMobileNum = mobileNum;
        }

        public int getCustomerId() {
            return Integer.parseInt(mCustomerId);
        }

        public String getUserId() {
            return mUserId;
        }

        public String getCustomerName() {
            return mCustomerName;
        }

        public String getLastLoginDate() {
            return mLastLoginDate;
        }

        public String getMobileNum() {
            return mMobileNum;
        }

    }

    public int getStatus() {
        return Integer.parseInt(mStatus);
    }

    public Response[] getResponse() {
        return mResponse;
    }

    public String getActivityCode() {
        return mActivityCode;
    }
}
