package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class GetBeneficiariesReqModel extends BaseModel {
    @SerializedName("USER_ID")
    private String mUserId;
    @SerializedName("ACTIVITY_CD")
    private String mActivityCode;

    public GetBeneficiariesReqModel(String userId, String activityCode) {
        mUserId = userId;
        mActivityCode = activityCode;
    }
}
