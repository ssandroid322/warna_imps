package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 09/02/2016.
 */
public class SignUpS1ReqModel extends BaseModel {
    @SerializedName("CUSTOMER_ID")
    private String mCustomerId;
    @SerializedName("MOBILE_NO")
    private String mMobileNo;

    public SignUpS1ReqModel(String customerId, String mobileNo) {
        mCustomerId = customerId;
        mMobileNo = mobileNo;
    }
}
