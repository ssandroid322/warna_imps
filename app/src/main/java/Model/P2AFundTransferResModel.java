package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by samvidmistry on 30/8/16.
 */

public class P2AFundTransferResModel extends BaseModel {
    @SerializedName("STATUS")
    private String mStatus;
    @SerializedName("RESPONSE")
    private Response[] mResponse;

    public int getStatus() {
        return Integer.parseInt(mStatus);
    }

    public Response[] getResponse() {
        return mResponse;
    }

    public static class Response {
        @SerializedName("USER_ID")
        private String mUserId;
        @SerializedName("MOBILE_NO")
        private String mMobileNo;
        @SerializedName("ACCT_NO")
        private String mAccountNo;
        @SerializedName("ACTIVITY_CD")
        private String mActivityCode;
        @SerializedName("IMPS_REF_CD")
        private String mImpsRefCode;

        public Response(String userId, String mobileNo, String accountNo, String activityCode,
                        String impsRefCode) {
            mUserId = userId;
            mMobileNo = mobileNo;
            mAccountNo = accountNo;
            mActivityCode = activityCode;
            mImpsRefCode = impsRefCode;
        }

        public String getUserId() {
            return mUserId;
        }

        public String getMobileNo() {
            return mMobileNo;
        }

        public String getAccountNo() {
            return mAccountNo;
        }

        public String getActivityCode() {
            return mActivityCode;
        }

        public String getImpsRefCode() {
            return mImpsRefCode;
        }
    }
}
