package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 26/10/2016.
 */
public class ChequeBookReqVerifyReqModel extends BaseModel {
    @SerializedName("USER_ID")
    private String mUserId;
    @SerializedName("ACTIVITY_CD")
    private String mActivityCode;
    @SerializedName("ACCT_NO")
    private String mAccountNo;
    @SerializedName("NO_OF_LEAF")
    private String mNoOfLeaf;
    @SerializedName("REQUEST_CD")
    private String requestcode;
    @SerializedName("OTP_NO")
    private String otpno;

    public ChequeBookReqVerifyReqModel(String mUserId, String mActivityCode, String mAccountNo, String mNoOfLeaf, String requestcode, String otpno) {
        this.mUserId = mUserId;
        this.mActivityCode = mActivityCode;
        this.mAccountNo = mAccountNo;
        this.mNoOfLeaf = mNoOfLeaf;
        this.requestcode = requestcode;
        this.otpno = otpno;
    }

    public ChequeBookReqVerifyReqModel() {

    }
}
