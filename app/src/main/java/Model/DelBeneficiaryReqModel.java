package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class DelBeneficiaryReqModel extends BaseModel {
    @SerializedName("USER_ID")
    private String mUserId;
    @SerializedName("ACTIVITY_CD")
    private String mActivityCode;
    @SerializedName("TRN_TYPE")
    private String mTransactionType;
    @SerializedName("TO_IFSCCODE")
    private String mToIfscCode;
    @SerializedName("TO_ACCT_NO")
    private String mToAccountNo;

    public DelBeneficiaryReqModel(String userId, String activityCode, String transactionType, String toIfscCode, String toAccountNo) {
        mUserId = userId;
        mActivityCode = activityCode;
        mTransactionType = transactionType;
        mToIfscCode = toIfscCode;
        mToAccountNo = toAccountNo;
    }
}
