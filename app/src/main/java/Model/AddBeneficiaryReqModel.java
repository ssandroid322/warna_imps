package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class AddBeneficiaryReqModel extends BaseModel {
    @SerializedName("USER_ID")
    private String mUserId;
    @SerializedName("ACTIVITY_CD")
    private String mActivityCode;
    @SerializedName("TRN_TYPE")
    private String mTransactionType;
    @SerializedName("TO_IFSCCODE")
    private String mToIfscCode;
    @SerializedName("TO_ACCT_NO")
    private String mToAccountNo;
    @SerializedName("TO_ACCT_NM")
    private String mToAccountName;
    @SerializedName("TO_ADD1")
    private String mToAddressOne;
    @SerializedName("TO_CONTACT_NO")
    private String mToContactNo;

    public AddBeneficiaryReqModel(String userId, String activityCode, String transactionType, String toIfscCode, String toAccountNo, String toAccountName, String toAddressOne, String toContactNo) {
        mUserId = userId;
        mActivityCode = activityCode;
        mTransactionType = transactionType;
        mToIfscCode = toIfscCode;
        mToAccountNo = toAccountNo;
        mToAccountName = toAccountName;
        mToAddressOne = toAddressOne;
        mToContactNo = toContactNo;
    }

    public String getToIfscCode() {
        return mToIfscCode;
    }

    public void setToIfscCode(String toIfscCode) {
        mToIfscCode = toIfscCode;
    }
}
