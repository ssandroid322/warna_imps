package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class FundTransferReqModel extends BaseModel {
    @SerializedName("USER_ID")
    private String mUserId;
    @SerializedName("ACTIVITY_CD")
    private String mActivityCode;
    @SerializedName("TRN_TYPE")
    private String mTransactionType;
    @SerializedName("FROM_ACCT_NO")
    private String mFromAccountNo;
    @SerializedName("BENF_IFSC")
    private String mToIfscCode;
    @SerializedName("BENF_ACCT_NO")
    private String mToAccountNo;
    @SerializedName("TRN_AMT")
    private String mAmount;


    public FundTransferReqModel(String userId, String activityCode, String transactionType, String fromAccountNo, String toIfscCode, String toAccountNo, String amount) {
        mUserId = userId;
        mActivityCode = activityCode;
        mTransactionType = transactionType;
        mFromAccountNo = fromAccountNo;
        mToIfscCode = toIfscCode;
        mToAccountNo = toAccountNo;
        mAmount = amount;
    }
}
//[{"USER_ID":"132001","ACTIVITY_CD":"1211",,"TRN_TYPE":"I",,
//        "FROM_ACCT_NO":"5100212122","TO_IFSCCODE":"SBIN12121","TO_ACCT_NO":"111001012322","AMOUNT":"500.00"}]

//[{"USER_ID":"132001","ACTIVITY_CD":"1211",,"TRN_TYPE":"I",,
//        "FROM_ACCT_NO":"5100212122","BENF_IFSC":"SBIN12121","BENF_ACCT_NO":"111001012322","TRN_AMT":"500.00"}]