package Model;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class ChequeStopResModel extends BaseModel {
    @SerializedName("STATUS")
    private String mStatus;
    @Nullable
    @SerializedName("RESPONSE")
    private Responce[] responce;

    public class Responce{
        @Nullable
        @SerializedName("USER_ID")
        private String userid;
        @Nullable
        @SerializedName("ACTIVITY_CD")
        private String activitycode;
        @Nullable
        @SerializedName("REQUEST_CD")
        private String requestcode;

        public Responce(String userid, String activitycode, String requestcode) {
            this.userid = userid;
            this.activitycode = activitycode;
            this.requestcode = requestcode;
        }

        public String getUserid() {
            return userid;
        }

        public String getActivitycode() {
            return activitycode;
        }

        public String getRequestcode() {
            return requestcode;
        }
    }
    public int getStatus() {
        return Integer.parseInt(mStatus);
    }

    public Responce[] getResponce() {
        return responce;
    }
}
