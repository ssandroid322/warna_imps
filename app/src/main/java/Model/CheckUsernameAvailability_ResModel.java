package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 07/12/2016.
 */

public class CheckUsernameAvailability_ResModel extends BaseModel{
    @SerializedName("STATUS")
    private String status;

    @SerializedName("MESSAGES")
    private String message;

   /* public CheckUsernameAvailability_ResModel(String status, String message) {
        this.status = status;
        this.message = message;
    }*/

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
