package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by samvidmistry on 30/8/16.
 */

public class CreateMmidReqModel extends BaseModel {
    @SerializedName("USER_ID")
    private String mUserId;
    @SerializedName("ACTIVITY_CD")
    private String mActivityCode;
    @SerializedName("ACCT_NO")
    private String mAccountNo;
    @SerializedName("CHANNEL")
    private String mChannel = "Mobile";

    public CreateMmidReqModel(String userId, String activityCode, String accountNo) {
        mUserId = userId;
        mActivityCode = activityCode;
        mAccountNo = accountNo;
    }
}
