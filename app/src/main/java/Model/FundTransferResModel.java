package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class FundTransferResModel extends BaseModel {
    @SerializedName("STATUS")
    private String mStatus;
    @SerializedName("RESPONSE")
    private Response[] mResponse;

    public int getStatus() {
        return Integer.parseInt(mStatus);
    }

    public Response[] getResponse() {
        return mResponse;
    }

    public static class Response{
        @SerializedName("USER_ID")
        private String mUserId;
        @SerializedName("ACTIVITY_CD")
        private String mActivityCode;
        @SerializedName("REQUEST_CD")
        private String mRequestCode;

        public Response(String userId, String activityCode, String requestCode) {
            mUserId = userId;
            mActivityCode = activityCode;
            mRequestCode = requestCode;
        }

        public String getUserId() {
            return mUserId;
        }

        public String getActivityCode() {
            return mActivityCode;
        }

        public String getRequestCode() {
            return mRequestCode;
        }
    }
}
