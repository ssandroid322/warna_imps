package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 09/02/2016.
 */
public class SignUpS1ResModel extends BaseModel {
    @SerializedName("STATUS")
    private String mStatus;
    @SerializedName("RESPONSE")
    private Response[] mResponses;

    public int getStatus() {
        return Integer.parseInt(mStatus);
    }

    public Response[] getResponses() {
        return mResponses;
    }

    public static class Response{
        @SerializedName("CUSTOMER_ID")
        private String mCustomerId;
        @SerializedName("MOBILE_NO")
        private String mMobileNo;
        @SerializedName("ACTIVATION_CD")
        private String mActivationCode;

        private Response(String customerId, String mobileNo, String activationCode) {
            mCustomerId = customerId;
            mMobileNo = mobileNo;
            mActivationCode = activationCode;
        }

        public String getCustomerId() {
            return mCustomerId;
        }

        public void setCustomerId(int customerId) {
            mCustomerId = String.valueOf(customerId);
        }

        public String getMobileNo() {
            return mMobileNo;
        }

        public void setMobileNo(String mobileNo) {
            mMobileNo = mobileNo;
        }

        public String getActivationCode() {
            return mActivationCode;
        }

        public void setActivationCode(String activationCode) {
            mActivationCode = activationCode;
        }
    }
}
