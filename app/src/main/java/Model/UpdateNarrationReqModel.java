package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class UpdateNarrationReqModel extends BaseModel {
    @SerializedName("USER_ID")
    private String mUserId;
    @SerializedName("ACTIVITY_CD")
    private String mActivityCode;
    @SerializedName("TRAN_CD")
    private String mTransactionCode;
    @SerializedName("REMARKS")
    private String mNarration;

    public UpdateNarrationReqModel( String mUserId, String mActivityCode, String mTransactionCode, String mNarration)
    {
        this.mUserId = mUserId;
        this.mActivityCode = mActivityCode;
        this.mTransactionCode = mTransactionCode;
        this.mNarration = mNarration;
    }
    public UpdateNarrationReqModel()
    {

    }
}