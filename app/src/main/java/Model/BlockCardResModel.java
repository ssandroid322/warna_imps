package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class BlockCardResModel extends BaseModel {
    @SerializedName("STATUS")
    private String mStatus;
    @SerializedName("RESPONSE")
    private Response[] mResponses;

    public int getStatus() {
        return Integer.parseInt(mStatus);
    }

    public Response[] getResponses() {
        return mResponses;
    }

    public static class Response{
        @SerializedName("REQUEST_CD")
        private String mREQUEST_CD;

        public Response(String requestCode)
        {
            mREQUEST_CD = requestCode;
        }

        public String getREQUEST_CD() {
            return mREQUEST_CD;
        }
    }
}
