package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class FundTransferVerifyResModel extends BaseModel {
    @SerializedName("STATUS")
    private String mStaus;
    @SerializedName("RESPONSE")
    private Response[] mResponse;

    public int getStaus() {
        return Integer.parseInt(mStaus);
    }

    public Response[] getResponse() {
        return mResponse;
    }

    public static class Response{
        private String mUserId;
        private String mActivityCode;
        private String mRequestCode;

        public Response(String userId, String activityCode, String requestCode) {
            mUserId = userId;
            mActivityCode = activityCode;
            mRequestCode = requestCode;
        }

        public String getUserId() {
            return mUserId;
        }

        public String getActivityCode() {
            return mActivityCode;
        }

        public String getRequestCode() {
            return mRequestCode;
        }
    }
}
