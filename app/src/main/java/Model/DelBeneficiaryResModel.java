package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class DelBeneficiaryResModel extends BaseModel {
    @SerializedName("STATUS")
    private String mStatus;

    public int getStatus() {
        return Integer.parseInt(mStatus);
    }
}
