package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 09/02/2016.
 */
public class LoginReqModel extends BaseModel {
    @SerializedName("USER_ID")
    private String userId;
    @SerializedName("PASSWORD")
    private String password;
    @SerializedName("VERSION")
    private String mVersion;
    @SerializedName("IMEI")
    private String mImei;
    @SerializedName("DEVICE")
    private String device;

    public LoginReqModel(String userId, String password, String mVersion, String mImei) {
        this.userId = userId;
        this.password = password;
        this.mVersion = mVersion;
        this.device = "ANDROID";
        this.mImei = mImei;
    }
}
