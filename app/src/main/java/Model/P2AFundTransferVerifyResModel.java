package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by samvidmistry on 30/8/16.
 */

public class P2AFundTransferVerifyResModel extends BaseModel {
    @SerializedName("STATUS")
    private String mStatus;
    @SerializedName("RESPONSE")
    private Response[] mResponse;

    public int getStatus() {
        return Integer.parseInt(mStatus);
    }

    public Response[] getResponse() {
        return mResponse;
    }

    public static class Response {
        @SerializedName("RESP_CD")
        private String mRespCode;
        @SerializedName("RESP_DESC")
        private String mRespDesc;
        @SerializedName("RESP_TRN_REF")
        private String mRespTrnRef;

        public Response(String respCode, String respDesc, String respTrnRef) {
            mRespCode = respCode;
            mRespDesc = respDesc;
            mRespTrnRef = respTrnRef;
        }

        public String getRespCode() {
            return mRespCode;
        }

        public String getRespDesc() {
            return mRespDesc;
        }

        public String getRespTrnRef() {
            return mRespTrnRef;
        }
    }
}
