package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 08/12/2016.
 */

public class GetScreenMsg_ResModel extends BaseModel {
    @SerializedName("STATUS")
    private String status;

    @SerializedName("MESSAGE")
    private String message;


    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
