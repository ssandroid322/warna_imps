package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class PasswordResetReqModel extends BaseModel {
    @SerializedName("CUSTOMER_ID")
    private String mCustomerId;
    @SerializedName("MOBILE_NO")
    private String mMobileNo;

    public PasswordResetReqModel(String customerId, String mobileNo) {
        mCustomerId = customerId;
        mMobileNo = mobileNo;
    }
}
