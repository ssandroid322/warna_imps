package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class PasswordResetVerifyResModel extends BaseModel {
    @SerializedName("STATUS")
    private String mStatus;
    @SerializedName("RESPONSE")
    private Response[] mResponse;

    public int getStatus() {
        return Integer.parseInt(mStatus);
    }

    public Response[] getResponse() {
        return mResponse;
    }

    public static class Response{
        @SerializedName("CUSTOMER_ID")
        private String mCustomerId;
        @SerializedName("USER_ID")
        private String mUserId;

        public Response(String customerId, String userId) {
            mCustomerId = customerId;
            mUserId = userId;
        }

        public String getCustomerId() {
            return mCustomerId;
        }

        public String getUserId() {
            return mUserId;
        }
    }
}
