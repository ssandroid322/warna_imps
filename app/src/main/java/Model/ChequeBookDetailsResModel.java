package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 25/02/2016.
 */
public class ChequeBookDetailsResModel extends BaseModel {
    @SerializedName("STATUS")
    private String mStatus;
    @SerializedName("RESPONSE")
    private Response[] mResponse;

    public int getStatus() {
        return Integer.parseInt(mStatus);
    }

    public Response[] getResponse() {
        return mResponse;
    }

    public static class Response{
        @SerializedName("ISSUE_DATE")
        private String mIssueDate;
        @SerializedName("FROM_CHEQUE")
        private String mFromCheque;
        @SerializedName("TO_CHEQUE")
        private String mToCheque;
        @SerializedName("CHEQUE_TOTAL")
        private String mChequeTotal;
        @SerializedName("NO_OF_USED")
        private String mNumberOfUsedCheques;
        @SerializedName("CHARACTERISTICS")
        private String mCharacteristics;
        @SerializedName("PAYABLE_AT_PAR")
        private String mPayableAtPar;

        public Response(String issueDate, String fromCheque, String toCheque, String chequeTotal, String numberOfUsedCheques, String characteristics, String payableAtPar) {
            mIssueDate = issueDate;
            mFromCheque = fromCheque;
            mToCheque = toCheque;
            mChequeTotal = chequeTotal;
            mNumberOfUsedCheques = numberOfUsedCheques;
            mCharacteristics = characteristics;
            mPayableAtPar = payableAtPar;
        }

        public String getIssueDate() {
            return mIssueDate;
        }

        public String getFromCheque() {
            return mFromCheque;
        }

        public String getToCheque() {
            return mToCheque;
        }

        public String getChequeTotal() {
            return mChequeTotal;
        }

        public String getNumberOfUsedCheques() {
            return mNumberOfUsedCheques;
        }

        public String getCharacteristics() {
            return mCharacteristics;
        }

        public String getPayableAtPar() {
            return mPayableAtPar;
        }
    }
}
