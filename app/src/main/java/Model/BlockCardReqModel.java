package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class BlockCardReqModel extends BaseModel {
    @SerializedName("USER_ID")
    private String mUserId;
    @SerializedName("ACTIVITY_CD")
    private String mActivityCode;
    @SerializedName("CUSTOMER_ID")
    private String mCustomerId;
    @SerializedName("MOBILE_NO")
    private String mMOBILE_NO;
    @SerializedName("ACCT_NO")
    private String mAccountNumber;
    @SerializedName("REMARKS")
    private String mREMARKS;

    public BlockCardReqModel(String mUserId, String mActivityCode, String mCustomerId, String mMOBILE_NO, String mAccountNumber, String mREMARKS) {
        this.mUserId = mUserId;
        this.mActivityCode = mActivityCode;
        this.mCustomerId = mCustomerId;
        this.mMOBILE_NO = mMOBILE_NO;
        this.mAccountNumber = mAccountNumber;
        this.mREMARKS = mREMARKS;
    }
}
