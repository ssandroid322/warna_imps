package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 08/12/2016.
 */

public class GetScreenMsg_ReqModel extends BaseModel{
    @SerializedName("SCREEN")
    private String screen;

    public GetScreenMsg_ReqModel(String screen) {
        this.screen = screen;
    }

    public String getScreen() {
        return screen;
    }
}
