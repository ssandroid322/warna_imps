package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class BlockCardVerifyReqModel extends BaseModel {
    @SerializedName("USER_ID")
    private String mUserId;
    @SerializedName("ACTIVITY_CD")
    private String mActivityCode;
    @SerializedName("REQUEST_CD")
    private String mRequestCode;
    @SerializedName("CUSTOMER_ID")
    private String mCustomerId;
    @SerializedName("OTP_NO")
    private String mOtpNo;

    public BlockCardVerifyReqModel(String mUserId, String mActivityCode, String mRequestCode, String mCustomerId, String mOtpNo) {
        this.mUserId = mUserId;
        this.mActivityCode = mActivityCode;
        this.mRequestCode = mRequestCode;
        this.mCustomerId = mCustomerId;
        this.mOtpNo = mOtpNo;
    }
}