package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 10/02/2016.
 */
public class OperativeAccountReqModel extends BaseModel {
    @SerializedName("USER_ID")
    private String mUserId;
    @SerializedName("ACTIVITY_CD")
    private String mActivityCode;

    public OperativeAccountReqModel(String userId, String activityCode) {
        mUserId = userId;
        mActivityCode = activityCode;
    }
    public OperativeAccountReqModel()
    {

    }
}
