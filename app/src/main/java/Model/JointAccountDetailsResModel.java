package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class JointAccountDetailsResModel extends BaseModel {
    @SerializedName("STATUS")
    private String mStatus;
    @SerializedName("RESPONSE")
    private Response[] mResponses;

    public int getStatus() {
        return Integer.parseInt(mStatus);
    }

    public Response[] getResponses() {
        return mResponses;
    }

    public static class Response{
        @SerializedName("JOINT_TYPE")
        private String mJointType;
        @SerializedName("PERSON_NAME")
        private String mPersonName;
        @SerializedName("REF_ACCT_NO")
        private String mRefAccountNo;
        @SerializedName("MEM_ACCT_NO")
        private String mMemAccountNo;

        public Response(String jointType, String personName, String refAccountNo, String memAccountNo) {
            mJointType = jointType;
            mPersonName = personName;
            mRefAccountNo = refAccountNo;
            mMemAccountNo = memAccountNo;
        }

        public String getJointType() {
            return mJointType;
        }

        public String getPersonName() {
            return mPersonName;
        }

        public String getRefAccountNo() {
            return mRefAccountNo;
        }

        public String getMemAccountNo() {
            return mMemAccountNo;
        }
    }
}
