package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 26/10/2016.
 */
public class ChequeStopVerifyReqModel extends BaseModel {

    @SerializedName("USER_ID")
    private String mUserId;
    @SerializedName("ACTIVITY_CD")
    private String mActivityCode;
    @SerializedName("ACCT_NO")
    private String mAccountNo;
    @SerializedName("CHQ_NO")
    private String mChequeNo;
    @SerializedName("OTP_NO")
    private String otpno;
    @SerializedName("REQUEST_CD")
    private String requestcode;

    public ChequeStopVerifyReqModel(String mUserId, String mActivityCode, String mAccountNo, String mChequeNo, String otpno, String requestcode) {
        this.mUserId = mUserId;
        this.mActivityCode = mActivityCode;
        this.mAccountNo = mAccountNo;
        this.mChequeNo = mChequeNo;
        this.otpno = otpno;
        this.requestcode = requestcode;
    }
}
