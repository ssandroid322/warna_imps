package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class UpdateNarrationResModel extends BaseModel {
    @SerializedName("STATUS")
    private String mStatus;
    @SerializedName("RESPONSE")
    private Response[] mResponse;

    public int getStatus() {
        return Integer.parseInt(mStatus);
    }

    public Response[] getResponse() {
        return mResponse;
    }

    public static class Response{
        @SerializedName("TRAN_CD")
        private String mTransactionCode;
        @SerializedName("ACTIVITY_CD")
        private String mActivityCode;

        public Response(String mTransactionCode, String mActivityCode) {
            this.mTransactionCode = mTransactionCode;
            this.mActivityCode = mActivityCode;
        }

        public String getActivityCode() {
            return mActivityCode;
        }

        public String getTransactionCode() {
            return mTransactionCode;
        }
    }
}