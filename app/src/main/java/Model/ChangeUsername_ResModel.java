package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 06/12/2016.
 */

public class ChangeUsername_ResModel extends BaseModel {
    @SerializedName("STATUS")
    private String status;

    @SerializedName("MESSAGES")
    private String message;

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
