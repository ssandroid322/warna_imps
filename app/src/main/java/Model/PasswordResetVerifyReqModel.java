package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class PasswordResetVerifyReqModel extends BaseModel {
    @SerializedName("CUSTOMER_ID")
    private String mCustomerId;
    @SerializedName("REQUEST_CD")
    private String mRequestCd;
    @SerializedName("OTP_NO")
    private String mOtpNo;
    @SerializedName("NEW_PASSWORD")
    private String mNewPassword;

    public PasswordResetVerifyReqModel(String customerId, String requestCd, String otpNo, String newPassword) {
        mCustomerId = customerId;
        mRequestCd = requestCd;
        mOtpNo = otpNo;
        mNewPassword = newPassword;
    }
}
