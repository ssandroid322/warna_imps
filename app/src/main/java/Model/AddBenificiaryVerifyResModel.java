package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 26/10/2016.
 */
public class AddBenificiaryVerifyResModel extends BaseModel {

    @SerializedName("STATUS")
    private String mStatus;

    public int getmStatus() {
        return Integer.parseInt(mStatus);
    }
}
