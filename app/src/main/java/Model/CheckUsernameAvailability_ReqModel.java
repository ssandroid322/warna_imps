package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 07/12/2016.
 */

public class CheckUsernameAvailability_ReqModel extends BaseModel{
    @SerializedName("USER_ID")
    private String userid;
    @SerializedName("ACTIVITY_CD")
    private String activitycode;
    @SerializedName("CUSTOM_USER_ID")
    private String customuserId;

    public CheckUsernameAvailability_ReqModel(String userid, String activitycode, String customuserId) {
        this.userid = userid;
        this.activitycode = activitycode;
        this.customuserId = customuserId;
    }
}
