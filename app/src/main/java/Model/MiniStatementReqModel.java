package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 11/02/2016.
 */
public class MiniStatementReqModel extends BaseModel {
    @SerializedName("USER_ID")
    private String mUserId;
    @SerializedName("ACTIVITY_CD")
    private String mActivityCode;
    @SerializedName("ACCT_NO")
    private String mAccountNo;

    public MiniStatementReqModel(String userId, String activityCode, String accountNo) {
        mUserId = userId;
        mActivityCode = activityCode;
        mAccountNo = accountNo;
    }
}
