package Model;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class ChequeBookResModel extends BaseModel {
    @SerializedName("STATUS")
    private String mStatus;
    @Nullable
    @SerializedName("RESPONSE")
    private Response[] mResponses;

    public int getStatus() {
        return Integer.parseInt(mStatus);
    }

    public Response[] getResponses() {
        return mResponses;
    }

    public static class Response{
       /* @SerializedName("REQ_REF_NO")
        private String mRequestReferenceNo;

        public Response(String requestReferenceNo) {
            mRequestReferenceNo = requestReferenceNo;
        }

        public String getRequestReferenceNo() {
            return mRequestReferenceNo;
        }*/

        @Nullable
        @SerializedName("USER_ID")
        private String userid;
        @Nullable
        @SerializedName("ACTIVITY_CD")
        private String activitycode;
        @Nullable
        @SerializedName("REQUEST_CD")
        private String requestcode;

        public String getUserid() {
            return userid;
        }

        public String getActivitycode() {
            return activitycode;
        }

        public String getRequestcode() {
            return requestcode;
        }
    }
}
