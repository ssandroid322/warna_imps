package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 10/02/2016.
 */
public class SignUpS2ReqModel extends BaseModel {
    @SerializedName("CUSTOMER_ID")
    private String mCustomerId;
    @SerializedName("ACTIVATION_CD")
    private String mActivationCode;
    @SerializedName("OTP_NO")
    private String mOtpNo;
    @SerializedName("PASSWORD")
    private String mPassword;

    public SignUpS2ReqModel(String customerId, String activationCode, String otpNo, String password) {
        mCustomerId = customerId;
        mActivationCode = activationCode;
        mOtpNo = otpNo;
        mPassword = password;
    }
}
