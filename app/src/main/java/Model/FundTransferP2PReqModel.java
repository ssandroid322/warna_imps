package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class FundTransferP2PReqModel extends BaseModel {
    @SerializedName("USER_ID")
    private String mUserId;
    @SerializedName("ACTIVITY_CD")
    private String mActivityCode;

    @SerializedName("ACCT_NO")
    private String mAccountNo;

    @SerializedName("CHANNEL")
    private String mChannel;

    @SerializedName("BENF_MOB_NO")
    private String mBenificiaryMobileno;

    @SerializedName("BENF_MMID")
    private String mBenificiaryMMID;


    @SerializedName("TRN_AMT")
    private String mAmount;

    @SerializedName("PIN_NO")
    private String mPINNo;


    @SerializedName("REMARKS")
    private String mRemarks;

    public FundTransferP2PReqModel(String mUserId, String mActivityCode, String mAccountNo, String mBenificiaryMobileno, String mBenificiaryMMID, String mAmount, String mRemarks) {
        this.mUserId = mUserId;
        this.mActivityCode = mActivityCode;
        this.mAccountNo = mAccountNo;
        this.mBenificiaryMobileno = mBenificiaryMobileno;
        this.mBenificiaryMMID = mBenificiaryMMID;
        this.mAmount = mAmount;
        this.mRemarks = mRemarks;

        mChannel = "Mobile";
        mPINNo = "00000";
    }


    //    public FundTransferP2PReqModel(String userId, String activityCode, String transactionType, String fromAccountNo, String toIfscCode, String toAccountNo, String amount) {
//        mUserId = userId;
//        mActivityCode = activityCode;
//        mTransactionType = transactionType;
//        mFromAccountNo = fromAccountNo;
//        mToIfscCode = toIfscCode;
//        mToAccountNo = toAccountNo;
//        mAmount = amount;
//        mChannel = "Mobile";
//        mPINNo = "00000";
//    }
}
