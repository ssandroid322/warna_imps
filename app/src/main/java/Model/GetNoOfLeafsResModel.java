package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class GetNoOfLeafsResModel extends BaseModel {
    @SerializedName("STATUS")
    private String mStatus;
    @SerializedName("RESPONSE")
    private Response[] mResponse;

    public int getStatus() {
        return Integer.parseInt(mStatus);
    }

    public Response[] getResponse() {
        return mResponse;
    }

    public static class Response{
        @SerializedName("CHEQUE_LEAF")
        private String mChequeLeaf;

        public String getmChequeLeaf()
        {
            return mChequeLeaf;
        }

        public Response(String mChequeLeaf)
        {

            this.mChequeLeaf = mChequeLeaf;
        }
    }
}