package Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samvid Mistry on 17/02/2016.
 */
public class ChequeBookReqModel extends BaseModel {
    @SerializedName("USER_ID")
    private String mUserId;
    @SerializedName("ACTIVITY_CD")
    private String mActivityCode;
    @SerializedName("ACCT_NO")
    private String mAccountNo;
    @SerializedName("NO_OF_LEAF")
    private String mNoOfLeaf;

    public ChequeBookReqModel(String userId, String activityCode, String accountNo, String noOfLeaf) {
        mUserId = userId;
        mActivityCode = activityCode;
        mAccountNo = accountNo;
        mNoOfLeaf = noOfLeaf;
    }
    public ChequeBookReqModel()
    {

    }
}