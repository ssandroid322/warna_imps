package ItemDecorators;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Samvid Mistry on 10/02/2016.
 */
public class OperativeAccountItemDecoration extends RecyclerView.ItemDecoration {
    private int mSpace;

    public OperativeAccountItemDecoration(int space) {
        mSpace = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.bottom = mSpace / 2;

        if(!(parent.getChildAdapterPosition(view) == 0)){
            outRect.top = mSpace / 2;
        }
    }
}
