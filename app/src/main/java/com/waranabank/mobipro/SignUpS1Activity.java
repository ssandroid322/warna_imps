package com.waranabank.mobipro;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONObject;

import Model.BaseModel;
import Model.SignUpS1ReqModel;
import Model.SignUpS1ResModel;
import Parser.BaseParser;
import Request.SignUpS1Request;
import Utility.ErrorMessage;
import Utility.MyEnum;

public class SignUpS1Activity extends com.waranabank.mobipro.BaseActivity {
    private static final int SIGN_UP_S2 = 385;
    public static final String SHOULD_FINISH = "shouldfinish";

    private Toolbar mToolbar;
    private EditText mCustomerId, mMobileNum;
    private ImageView mBackButton;
    private TextView mWelcomeString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_s1);

        bindViews();

        mWelcomeString.setText(getString(R.string.welcome_string, getString(R.string.bank_name_welcome)));

        setSupportActionBar(mToolbar);

        getSupportActionBar().setTitle("");
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    @Override
    protected void bindViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar_signUpS1);
        mCustomerId = (EditText) findViewById(R.id.customerId_signUpS1);
        mMobileNum = (EditText) findViewById(R.id.mobileNo_signUpS1);
        mBackButton = (ImageView) findViewById(android.R.id.home);
        mWelcomeString = (TextView) findViewById(R.id.welcomeString_signUpS1);
    }

    public void submitData(View view){
        hideKeybord(mCustomerId);

        if(mCustomerId.getText().toString().trim().length() <= 0){
            mCustomerId.setError("Please enter a valid customer id.");
            return;
        }

        if(mMobileNum.getText().toString().trim().length() <= 0){
            mMobileNum.setError("Please enter a valid mobile number.");
            return;
        }

//        if(!ResetPasswordS1.validatePhoneNumber(mMobileNum.getText().toString())){
//            mMobileNum.setError("Please enter a valid mobile number.");
//            return;
//        }

        String customerId = mCustomerId.getText().toString();
        String mobileNo = mMobileNum.getText().toString();

        SignUpS1Request signUpS1Request = new SignUpS1Request(this, MyEnum.displayProgress.Show);
        signUpS1Request.sendRequest(this, new SignUpS1ReqModel(customerId, mobileNo));
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {
        SignUpS1ResModel superModel = (SignUpS1ResModel) baseParser.doParsing(objJson);

        SignUpS1ResModel signUpS1ResModel = (SignUpS1ResModel) superModel.getModelArray().get(0);

        if(signUpS1ResModel.getStatus() == SignUpS1Request.VALID_DATA){
            Intent intent = new Intent(this, com.waranabank.mobipro.SignUpS2Activity.class);
            SignUpS1ResModel.Response[] responses = signUpS1ResModel.getResponses();
            intent.putExtra(com.waranabank.mobipro.SignUpS2Activity.CUSTOMER_ID, responses[0].getCustomerId());
            intent.putExtra(com.waranabank.mobipro.SignUpS2Activity.ACTIVATION_CODE, responses[0].getActivationCode());
            intent.putExtra(com.waranabank.mobipro.SignUpS2Activity.MOBILE_NO, responses[0].getMobileNo());
            startActivityForResult(intent, SIGN_UP_S2);
        }else if(signUpS1ResModel.getStatus() == SignUpS1Request.ALREADY_REGISTERED){
            displayErrorMessage(mToolbar, "This user already exists, please try another id.");
        }else if(signUpS1ResModel.getStatus() == SignUpS1Request.INVALID_CUSTOMER_ID){
            displayErrorMessage(mToolbar, "Please enter a valid customer id.");
        }
    }

    @Override
    public void networkConnectionError(BaseModel reqModel) {
        displayErrorMessage(mToolbar, ErrorMessage.getNetworkConnectionError());
    }

    public void goBack(View view){
        onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == SIGN_UP_S2 && resultCode == RESULT_OK && data.getBooleanExtra(SHOULD_FINISH, false)){
            finish();
        }
    }
}
