package com.waranabank.mobipro;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class DashBoardGridTwoFragment extends Fragment implements View.OnClickListener {
    private ViewClickHandler mViewClickHandler;

    public DashBoardGridTwoFragment() {
        // Required empty public constructor
    }

    public static DashBoardGridTwoFragment newInstance() {
        return new DashBoardGridTwoFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dash_board_grid_two, container, false);

        view.findViewById(R.id.stopCheque_dashBoard).setOnClickListener(this);
        view.findViewById(R.id.blockAtm_dashBoard).setOnClickListener(this);
        view.findViewById(R.id.ourBranches_dashBoard).setOnClickListener(this);

        return view;
    }

    public void setViewClickHandler(ViewClickHandler viewClickHandler) {
        mViewClickHandler = viewClickHandler;
    }

    @Override
    public void onClick(View v) {
        if(mViewClickHandler == null) return;

        mViewClickHandler.onViewClick(v);
    }
}
