package com.waranabank.mobipro;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

public class DemoActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo2);
        Toast.makeText(DemoActivity2.this, "This is an example of a sample toast", Toast.LENGTH_SHORT).show();

        Toast.makeText(DemoActivity2.this, "A new Toast", Toast.LENGTH_SHORT).show();

        Toast.makeText(DemoActivity2.this, "A new Toas From Dant", Toast.LENGTH_SHORT).show();

        Toast.makeText(DemoActivity2.this, "Another Toast for testing purpose", Toast.LENGTH_SHORT).show();
    }
}
