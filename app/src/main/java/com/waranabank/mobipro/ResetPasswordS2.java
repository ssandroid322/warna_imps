package com.waranabank.mobipro;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import org.json.JSONObject;

import Model.BaseModel;
import Model.PasswordResetVerifyReqModel;
import Model.PasswordResetVerifyResModel;
import Parser.BaseParser;
import Request.PasswordResetVerifyRequest;
import Utility.ErrorMessage;
import Utility.MyEnum;

public class ResetPasswordS2 extends BaseActivity {
    public static final String CUSTOMER_ID = "customer_id";
    public static final String REQUEST_CD = "request_cd";

    private EditText mOtp, mPassword, mConfirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password_s2);

        bindViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    @Override
    protected void bindViews() {
        mOtp = (EditText) findViewById(R.id.otp_resetPasswordS2);
        mPassword = (EditText) findViewById(R.id.newPassword_resetPasswordS2);
        mConfirmPassword = (EditText) findViewById(R.id.confirmPassword_resetPasswordS2);
    }

    public void resetPassword(View view){
        hideKeybord(mConfirmPassword);

        if(getTextFromView(mOtp).length() == 0){
            mOtp.setError("Please enter a valid OTP.");
            return;
        }

        if(getTextFromView(mPassword).length() == 0){
            mPassword.setError("Please enter a valid password.");
            return;
        }

        if(getTextFromView(mConfirmPassword).length() == 0){
            mConfirmPassword.setError("Please enter confirm password same as password.");
            return;
        }

        if(getTextFromView(mPassword).length() < 8){
            mPassword.setError("The password must be at least 8 characters long.");
            return;
        }

        if(!getTextFromView(mPassword).equals(getTextFromView(mConfirmPassword))){
            displayErrorMessage(mOtp, "Please enter confirm password same as password.");
            return;
        }

        PasswordResetVerifyReqModel passwordResetVerifyReqModel = new PasswordResetVerifyReqModel(getIntent().getStringExtra(CUSTOMER_ID), getIntent().getStringExtra(REQUEST_CD), getTextFromView(mOtp), getTextFromView(mPassword));
        PasswordResetVerifyRequest passwordResetVerifyRequest = new PasswordResetVerifyRequest(this, MyEnum.displayProgress.Show);
        passwordResetVerifyRequest.sendRequest(this, passwordResetVerifyReqModel);
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {
        PasswordResetVerifyResModel superModel = (PasswordResetVerifyResModel) baseParser.doParsing(objJson);

        PasswordResetVerifyResModel passwordResetVerifyResModel = (PasswordResetVerifyResModel) superModel.getModelArray().get(0);

        if(passwordResetVerifyResModel.getStatus() == PasswordResetVerifyRequest.DATA_NOT_VALID){
            displayErrorMessage(mOtp, "Please enter a valid OTP.");
        }else if(passwordResetVerifyResModel.getStatus() == PasswordResetVerifyRequest.SUCCESS){
            setResult(RESULT_OK);
            showLongToast("Password changed successfully.");
            finish();
        }
    }

    public void goBack(View view){
        super.onBackPressed();
    }

    @Override
    public void networkConnectionError(BaseModel reqModel) {
        displayErrorMessage(mOtp, ErrorMessage.getNetworkConnectionError());
    }
}
