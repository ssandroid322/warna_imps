package com.waranabank.mobipro;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Model.BaseModel;
import Model.FundTransferReqModel;
import Model.FundTransferResModel;
import Model.P2AFundTransferReqModel;
import Model.P2AFundTransferResModel;
import Parser.BaseParser;
import Parser.FundTransferParser;
import Parser.P2AFundTransferParser;
import Request.FundTransferRequest;
import Request.P2AFundTransferRequest;
import Utility.ErrorMessage;
import Utility.MyEnum;
import Utility.MySharedPreferences;
import Utility.SecurityUtils;

public class FundTransferVerifyActivity extends BaseActivity {
    private static final int REQUEST_VERIFY = 760;
    public static final String FROM_ACCOUNT = "from_account";
    public static final String BENEFICIARY = "beneficiary";
    public static final String TRANSACTION_TYPE = "transaction_type";
    public static final String IFSC_CODE = "ifsc_code";
    public static final String ACCOUNT_NO = "account_no";
    public static final String AMOUNT = "amount";
    public static final String TITLE = "title";
    public static final String IS_IMPS = "is_imps";
    public static final String PIN = "pin";
    public static final String CONTACT_NO = "contact_no";
    public static final String REMARKS = "remarks";

    private ScrollView mScrollView;
    private P2AFundTransferReqModel p2AFundTransferReqModel;
    private TextView mFromAccount, mFromAccountName, mBeneficiary, mTransactionType, mAccountNo, mAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fund_transfer_verify);

        bindViews();
        syncDataWithIntent();

    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    private void syncDataWithIntent() {
        Intent intent = getIntent();
        mFromAccount.setText(intent.getStringExtra(FROM_ACCOUNT));
        mBeneficiary.setText(intent.getStringExtra(BENEFICIARY));
        if (intent.getStringExtra(TRANSACTION_TYPE).equalsIgnoreCase("I")) {
            mTransactionType.setText("IFT");
        } else {
            mTransactionType.setText(intent.getStringExtra(TRANSACTION_TYPE));
        }
        try {
            mFromAccountName.setText(MySharedPreferences.getCustomerName(this));
        } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e) {
            e.printStackTrace();
        }
        mAccountNo.setText(intent.getStringExtra(ACCOUNT_NO));
        mAmount.setText("₹" + intent.getStringExtra(AMOUNT));
    }

    @Override
    protected void bindViews() {
        mFromAccount = (TextView) findViewById(R.id.fromAccount_fundTransferVerify);
        mBeneficiary = (TextView) findViewById(R.id.beneficiary_fundTransferVerify);
        mTransactionType = (TextView) findViewById(R.id.transactionType_fundTransferVerify);
        mAccountNo = (TextView) findViewById(R.id.toAccountNo_fundTransferVerify);
        mAmount = (TextView) findViewById(R.id.amount_fundTransferVerify);
        mScrollView = (ScrollView) findViewById(R.id.scrollView_fundTransferVerify);
        mFromAccountName = (TextView) findViewById(R.id.fromAccountName_fundTransferVerify);
    }

    public void goBack(View v) {
        super.onBackPressed();
    }

    public void fundTransfer(View view) {
        /*try {
            FundTransferVerifyReqModel fundTransferVerifyReqModel = new FundTransferVerifyReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this), getIntent().getStringExtra(REQUEST_CD), getTextFromView(mOtp));
            FundTransferVerifyRequest fundTransferVerifyRequest = new FundTransferVerifyRequest(this, MyEnum.displayProgress.Show);
            fundTransferVerifyRequest.sendRequest(this, fundTransferVerifyReqModel);
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }*/

        try {
            String trnType = null;
            if (getIntent().getStringExtra(TRANSACTION_TYPE).equalsIgnoreCase("I")) {
                trnType = "I";
            } else {
                switch (getIntent().getStringExtra(TRANSACTION_TYPE).toUpperCase()) {
                    case "R":
                    case "N":
                    case "IMPS":
                    case "O":
                        trnType = "O";
                        break;
                }
            }

            if (trnType == null || trnType.trim().length() <= 0) {
                showShortToast("Error completing transaction, please try again later.");
                return;
            }

            Intent intent = getIntent();
            String eBENF_ACCT_NO = SecurityUtils.encryptDataWithAes(getIntent().getStringExtra(ACCOUNT_NO));
//            String eBENF_ACCT_NO = getIntent().getStringExtra(ACCOUNT_NO);
            String eBENF_IFSC = SecurityUtils.encryptDataWithAes(getIntent().getStringExtra(IFSC_CODE));
            String eTRN_AMT = SecurityUtils.encryptDataWithAes(getIntent().getStringExtra(AMOUNT));
            if (!intent.hasExtra(IS_IMPS) || !intent.getBooleanExtra(IS_IMPS, false)) {

//                FundTransferReqModel fundTransferReqModel = new FundTransferReqModel(
//                        MySharedPreferences.getUserId(this),
//                        MySharedPreferences.getActivityCode(this),
//                        trnType,
//                        intent.getStringExtra(FROM_ACCOUNT),
//                        eBENF_ACCT_NO,
//                        eBENF_IFSC,
//                        eTRN_AMT
//                );
                p2AFundTransferReqModel =
                        new P2AFundTransferReqModel(
                                MySharedPreferences.getUserId(this),
                                MySharedPreferences.getActivityCode(this),
                                intent.getStringExtra(FROM_ACCOUNT),
//                                intent.getStringExtra(ACCOUNT_NO),
//                                intent.getStringExtra(IFSC_CODE),
//                                intent.getStringExtra(AMOUNT),
                                eBENF_ACCT_NO,
                                eBENF_IFSC,
                                eTRN_AMT,
                                intent.getStringExtra(PIN),
                                intent.getStringExtra(REMARKS),
                                intent.getStringExtra(CONTACT_NO)
                        );
//                FundTransferRequest fundTransferRequest = new FundTransferRequest(this, MyEnum.displayProgress.Show);
//                fundTransferRequest.sendRequest(this, p2AFundTransferReqModel);
                P2AFundTransferRequest p2AFundTransferRequest = new P2AFundTransferRequest(
                        this, MyEnum.displayProgress.Show);
                p2AFundTransferRequest.sendRequest(this, p2AFundTransferReqModel);
            } else if (intent.hasExtra(IS_IMPS) || intent.getBooleanExtra(IS_IMPS, false)) {

                p2AFundTransferReqModel =
                        new P2AFundTransferReqModel(
                                MySharedPreferences.getUserId(this),
                                MySharedPreferences.getActivityCode(this),
                                intent.getStringExtra(FROM_ACCOUNT),
//                                intent.getStringExtra(ACCOUNT_NO),
//                                intent.getStringExtra(IFSC_CODE),
//                                intent.getStringExtra(AMOUNT),
                                eBENF_ACCT_NO,
                                eBENF_IFSC,
                                eTRN_AMT,
                                intent.getStringExtra(PIN),
                                intent.getStringExtra(REMARKS),
                                intent.getStringExtra(CONTACT_NO)
                        );
                P2AFundTransferRequest p2AFundTransferRequest = new P2AFundTransferRequest(
                        this, MyEnum.displayProgress.Show);
                p2AFundTransferRequest.sendRequest(this, p2AFundTransferReqModel);
            }
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {
        if (baseParser instanceof FundTransferParser) {
            FundTransferResModel superModel = (FundTransferResModel) baseParser.doParsing(objJson);

            FundTransferResModel fundTransferResModel = (FundTransferResModel) superModel.getModelArray().get(0);

            if (fundTransferResModel.getStatus() == FundTransferRequest.NOT_REG_FUND_TRANSFER) {
                displayErrorMessage(mBeneficiary, "This account is not registered for fund transfer.");
            } else if (fundTransferResModel.getStatus() == FundTransferRequest.INSUFFICIENT_BAL) {
                displayErrorMessage(mBeneficiary, "Not sufficient balance available to transfer.");
            } else if (fundTransferResModel.getStatus() == FundTransferRequest.INVALID_ACCT_NO) {
                displayErrorMessage(mBeneficiary, "Invalid account number.");
            } else if (fundTransferResModel.getStatus() == FundTransferRequest.SUCCESSFUL) {
                Intent intent = new Intent(this, FundTransferOtpActivity.class);
                intent.putExtra(FundTransferOtpActivity.REQUEST_CD, fundTransferResModel.getResponse()[0].getRequestCode());
                intent.putExtra(FundTransferOtpActivity.TITLE, getIntent().getStringExtra(TITLE));
                startActivityForResult(intent, REQUEST_VERIFY);
            }
        } else if (baseParser instanceof P2AFundTransferParser) {
            P2AFundTransferResModel superModel =
                    (P2AFundTransferResModel) baseParser.doParsing(objJson);

            P2AFundTransferResModel fundTransferResModel =
                    (P2AFundTransferResModel) superModel.getModelArray().get(0);

            switch (fundTransferResModel.getStatus()) {
                case P2AFundTransferRequest.SUCCESS:
                    Intent intent = new Intent(this, FundTransferOtpActivity.class);
                    try {
                        if (p2AFundTransferReqModel == null) {
                            p2AFundTransferReqModel = new P2AFundTransferReqModel(
                                    MySharedPreferences.getUserId(this),
                                    MySharedPreferences.getActivityCode(this),
                                    intent.getStringExtra(FROM_ACCOUNT),
                                    intent.getStringExtra(ACCOUNT_NO),
                                    intent.getStringExtra(IFSC_CODE),
                                    intent.getStringExtra(AMOUNT),
                                    intent.getStringExtra(PIN),
                                    intent.getStringExtra(REMARKS),
                                    intent.getStringExtra(CONTACT_NO)
                            );
                        }
                        FundTransferOtpActivity.createImpsVerifyIntent(this, intent,
                                p2AFundTransferReqModel, fundTransferResModel.getResponse()[0]
                                        .getImpsRefCode());
                        startActivityForResult(intent, REQUEST_VERIFY);
                    } catch (InvalidKeyException e) {
                        e.printStackTrace();
                    } catch (BadPaddingException e) {
                        e.printStackTrace();
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (IllegalBlockSizeException e) {
                        e.printStackTrace();
                    } catch (NoSuchPaddingException e) {
                        e.printStackTrace();
                    }
                    break;
                case P2AFundTransferRequest.INVALID_NUMBER:
                    new AlertDialog.Builder(this)
                            .setTitle("Error")
                            .setMessage("The number is not valid or it is not registered with " +
                                    "any account.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).create().show();
                    break;
                case P2AFundTransferRequest.INSUFFICIENT_BALANCE:
                    new AlertDialog.Builder(this)
                            .setTitle("Error")
                            .setMessage("The account you are trying to transfer from has " +
                                    "insufficient balance.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).create().show();
                    break;
                default:
                    new AlertDialog.Builder(this)
                            .setTitle("Error")
                            .setMessage("There was an error processing your request, please try " +
                                    "again.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).create().show();
                    break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_VERIFY && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void networkConnectionError(BaseModel reqModel) {
        displayErrorMessage(mBeneficiary, ErrorMessage.getNetworkConnectionError());
    }
}
