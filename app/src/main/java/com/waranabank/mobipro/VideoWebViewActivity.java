package com.waranabank.mobipro;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.lang.reflect.InvocationTargetException;

import Utility.Consts;

import static com.waranabank.mobipro.R.id.webView;

public class VideoWebViewActivity extends ActionBarActivity
{
    public final static String EXTRAS_URL = "URL";
    public final static String EXTRAS_ISFROM = "ISLOCAL";
    public final static String TAG = "VideoPalyerActivity";

    private VideoEnabledWebView webView;
    private VideoEnabledWebChromeClient webChromeClient;
//    String ISFROM = Consts.ISFROM_INQCHAT;
    String videoSource = "http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4";
    Dialog mDialog;
    View loadingView;

    public void getIntentData()
    {

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        /*getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videowebview);

        mDialog = new Dialog(this);

        // Save the web view
        webView = (VideoEnabledWebView) findViewById(R.id.webView);

        // Initialize the VideoEnabledWebChromeClient and set event handlers
        View nonVideoLayout = findViewById(R.id.nonVideoLayout); // Your own view, read class comments
        ViewGroup videoLayout = (ViewGroup) findViewById(R.id.videoLayout); // Your own view, read class comments
        //noinspection all
//        loadingView = getLayoutInflater().inflate(R.layout.view_loading_video, null); // Your own view, read class comments
        webChromeClient = new VideoEnabledWebChromeClient(VideoWebViewActivity.this,mDialog,nonVideoLayout, videoLayout, loadingView, webView) // See all available constructors...
        {
            // Subscribe to standard events, such as onProgressChanged()...
            @Override
            public void onProgressChanged(WebView view, int progress) {
//                ShowProgressbar.showProgress(VideoWebViewActivity.this,"Loading...",mDialog);
                // Your code...
            }
        };
        webChromeClient.setOnToggledFullscreen(new VideoEnabledWebChromeClient.ToggledFullscreenCallback() {
            @Override
            public void toggledFullscreen(boolean fullscreen) {
                // Your code to handle the full-screen change, for example showing and hiding the title bar. Example:
                if (fullscreen) {
                    WindowManager.LayoutParams attrs = getWindow().getAttributes();
                    attrs.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
                    attrs.flags |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
                    getWindow().setAttributes(attrs);
                    if (android.os.Build.VERSION.SDK_INT >= 14) {
                        //noinspection all
                        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
                    }
                } else {
                    WindowManager.LayoutParams attrs = getWindow().getAttributes();
                    attrs.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
                    attrs.flags &= ~WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
                    getWindow().setAttributes(attrs);
                    if (android.os.Build.VERSION.SDK_INT >= 14) {
                        //noinspection all
                        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
                    }
                }

            }
        });
        webView.setWebChromeClient(webChromeClient);
        // Call private class InsideWebViewClient
        webView.setWebViewClient(new InsideWebViewClient());

        // Navigate anywhere you want, but consider that this classes have only been tested on YouTube's mobile site

//        webView.loadUrl("http://www.youtube.com");

//        File extFile = Environment.getExternalStorageDirectory();

//        File file = new File(extFile,"IMG_0011.MOV");
//        webView.loadUrl("file:///android_asset/videiview.html");

        getIntentData();

        String filename = "test.MOV";
        String htmlString = "<div class=\"video-container\">\n" +
                "    <p>local</p>\n" +
                "    <video poster=\"video/star.png\" controls>" +
                "        <source src=\"" + videoSource + "\" />" +
                "    </video>" +
                "</div>";

//        webView.loadData(htmlString, "text/html; charset=utf-8", "UTF-8");
      webView.loadUrl(videoSource);
    }


    private class InsideWebViewClient extends WebViewClient {
        @Override
        // Force links to be opened inside WebView and not in Default Browser
        // Thanks http://stackoverflow.com/a/33681975/1815624
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
         //   super.onPageFinished(view, url);

            loadingView.setVisibility(View.GONE);
//            view.loadUrl("javascript:alert('Hello World!')");
//            ShowProgressbar.dismissDialog(mDialog);
            view.loadUrl("javascript:(function(){document.getElementsByTagName('video')[0].play();})()");

        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            loadingView.setVisibility(View.VISIBLE);
//            ShowProgressbar.showProgress(VideoWebViewActivity.this,"Loading...",mDialog);
        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();

    }


    @Override
    protected void onPause() {
        super.onPause();
        webView.onPause();
        try {
            Class.forName("android.webkit.WebView")
                    .getMethod("onPause", (Class[]) null)
                    .invoke(webView, (Object[]) null);

        } catch(ClassNotFoundException cnfe) {

        } catch(NoSuchMethodException nsme) {

        } catch(InvocationTargetException ite) {

        } catch (IllegalAccessException iae) {

        }
    }
}
