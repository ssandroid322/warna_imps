package com.waranabank.mobipro;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Model.AddBenificiaryVerifyReqModel;
import Model.AddBenificiaryVerifyResModel;
import Model.ChequeStopVerifyReqModel;
import Model.ChequeStopVerifyResModel;
import Parser.BaseParser;
import Request.AddBenificiaryVerifyRequest;
import Request.ChequeStopRequest;
import Request.ChequeStopVerifyRequest;
import Utility.MyEnum;
import Utility.MySharedPreferences;

public class ChequeStopVerify extends BaseActivity implements View.OnClickListener {

    ImageView backButton;
    EditText et_otp;
    Button btn_submit;

    public static final String EXTRA_USER_ID = "USER_ID";
    public static final String EXTRA_ACTIVITY_CD = "ACTIVITY_CD";
    public static final String EXTRA_ACCT_NO = "ACCT_NO";
    public static final String EXTRA_CHQ_NO = "CHQ_NO";
    public static final String EXTRA_REQUEST_CD = "USER_ID";

    private String acct_no,chq_no,request_cd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheque_stop_verify);

        init();
        getIntentData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    private void init() {
        backButton = (ImageView) findViewById(R.id.benificiarybackbutton);
        et_otp = (EditText) findViewById(R.id.Addbeneficiary_otp);
        btn_submit = (Button) findViewById(R.id.Addbeneficiary_submit);
        backButton.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
    }

    public void getIntentData(){
        acct_no = getIntent().getStringExtra(EXTRA_ACCT_NO);
        chq_no = getIntent().getStringExtra(EXTRA_CHQ_NO);
        request_cd = getIntent().getStringExtra(EXTRA_REQUEST_CD);
    }

    @Override
    public void onClick(View view) {
        if (view == backButton){
            onBackPressed();
        }
        else if (view == btn_submit){
            sendRequestToVerify();
        }
    }

    private void sendRequestToVerify() {
        String otp = et_otp.getText().toString();
        ChequeStopVerifyRequest chequeStopVerifyRequest = new ChequeStopVerifyRequest(this, MyEnum.displayProgress.Show);
        ChequeStopVerifyReqModel chequeStopVerifyReqModel = null;
        try {
            chequeStopVerifyReqModel = new ChequeStopVerifyReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this),acct_no,chq_no,otp,request_cd);
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        chequeStopVerifyRequest.sendRequest(this,chequeStopVerifyReqModel);
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {
        ChequeStopVerifyResModel superModel = (ChequeStopVerifyResModel) baseParser.doParsing(objJson);
        ChequeStopVerifyResModel chequeStopVerifyResModel = (ChequeStopVerifyResModel) superModel.getModelArray().get(0);

        if(chequeStopVerifyResModel.getStatus() == ChequeStopRequest.CHEQUE_RETURN){
            showLongToast("Cheque with number "+chq_no+" is returned.");
//            builder.setMessage("Cheque with number "+chq_no+" is returned.");
        }else if(chequeStopVerifyResModel.getStatus() == ChequeStopRequest.ERROR){
            showLongToast("Unable to stop the cheque with number "+chq_no+".");
//            builder.setMessage("Unable to stop the cheque with number "+chq_no+".");
        }else if(chequeStopVerifyResModel.getStatus() == ChequeStopRequest.NOT_FOUND){
            showLongToast("Cheque with number "+chq_no+" is not found, please check cheque number.");
//            builder.setMessage("Cheque with number "+chq_no+" is not found, please check cheque number.");
        }else if(chequeStopVerifyResModel.getStatus() == ChequeStopRequest.PROCESSED){
            showLongToast("Cheque with number "+chq_no+" is processed.");
//            builder.setMessage("Cheque with number "+chq_no+" is processed.");
        }else if(chequeStopVerifyResModel.getStatus() == ChequeStopRequest.REQUEST_ACCEPT){
            showAlert("Request to stop cheque accepted successfully.");
//            showLongToast("Request to stop cheque accepted successfully.");
//                builder.setMessage("Request to stop cheque accepted successfully.");


        }else if(chequeStopVerifyResModel.getStatus() == ChequeStopRequest.STOP_PAYMENT){
            showLongToast("Payment of Cheque with number "+chq_no+" is stopped.");
//            builder.setMessage("Payment of Cheque with number "+chq_no+" is stopped.");
        }else if(chequeStopVerifyResModel.getStatus() == ChequeStopRequest.SURRENDER){
            showLongToast("Cheque with number "+chq_no+" is surrendered.");
//            builder.setMessage("Cheque with number "+chq_no+" is surrendered.");
        }else if(chequeStopVerifyResModel.getStatus() == ChequeStopRequest.FACILITY_DISABLED){
            showLongToast("Temporarily, this service has been disabled by the bank. Please try again after sometime.");
//            builder.setMessage("Temporarily, this service has been disabled by the bank. Please try again after sometime.");
        }
    }

    public void showAlert(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        setResult(RESULT_OK);
                        finish();
                    }
                })
                .create().show();
    }
}
