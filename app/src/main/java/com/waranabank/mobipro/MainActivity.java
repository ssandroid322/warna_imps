package com.waranabank.mobipro;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDelegate;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import Interface.DrawerConnectionInterface;
import Utility.Consts;

public class MainActivity extends BaseActivity implements DrawerConnectionInterface, NavigationView.OnNavigationItemSelectedListener, DrawerLayout.DrawerListener {
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private Runnable mPendingRunnable;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_main, new DashBoardFragment()).commit();

        bindViews();

        mNavigationView.setNavigationItemSelectedListener(this);
        mDrawerLayout.setDrawerListener(this);
        mNavigationView.setCheckedItem(R.id.home_drawer);
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    @Override
    protected void bindViews() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_main);
        mNavigationView = (NavigationView) findViewById(R.id.navigationView_home);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Log out?").setMessage("Are you sure you want to logout?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                logOut();
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.create().show();
    }

    @Override
    public void onDrawerOpenRequested() {
        mDrawerLayout.openDrawer(GravityCompat.START);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.home_drawer:
                mPendingRunnable = null;
                mDrawerLayout.closeDrawers();
                return true;

            case R.id.contactUs_drawer:
                mPendingRunnable = null;
//                Toast.makeText(this, "Coming Soon", Toast.LENGTH_SHORT).show();
                mPendingRunnable = new Runnable() {
                    @Override
                    public void run() {
                        startActivityWithAnimation(MainActivity.this, new Intent(MainActivity.this, ContactUsActivity.class));
                    }
                };
                mDrawerLayout.closeDrawers();
                return false;

            case R.id.locateUs_drawer:
                mPendingRunnable = null;
//                Toast.makeText(this, "Coming Soon", Toast.LENGTH_SHORT).show();
                mDrawerLayout.closeDrawers();
                return false;

            case R.id.changePassword_drawer:
                mPendingRunnable = new Runnable() {
                    @Override
                    public void run() {
                        startActivityWithAnimation(MainActivity.this, new Intent(MainActivity.this, ChangePasswordActivity.class));
                    }
                };
                mDrawerLayout.closeDrawers();
                return false;

            case R.id.feedback_drawer:
                mPendingRunnable = null;
                Toast.makeText(this, "Coming Soon", Toast.LENGTH_SHORT).show();
                mDrawerLayout.closeDrawers();
                return false;

            case R.id.logout_drawer:
                new AlertDialog.Builder(this)
                        .setTitle("Log Out?")
                        .setMessage("Are you sure you want to log out?")
                        .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                logOut();
                            }
                        })
                        .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                mDrawerLayout.closeDrawers();
                            }
                        })
                        .create().show();
                return false;

            case R.id.ourBranches_drawer:
                mPendingRunnable = new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(MainActivity.this, OurBranchesActivity.class));
                    }
                };
                mDrawerLayout.closeDrawers();
                return false;

            case R.id.changeUsername_drawer:
                mPendingRunnable = new Runnable() {
                    @Override
                    public void run() {
                        startActivityWithAnimation(MainActivity.this, new Intent(MainActivity.this, ChangeUsernameActivity.class));
                    }
                };
                mDrawerLayout.closeDrawers();
                return false;
            case R.id.FAQ:
                mPendingRunnable = new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(MainActivity.this,FAQWebView.class);
                        intent.putExtra(FAQWebView.ISFOR, Consts.ISFORFAQ);
                        startActivityWithAnimation(MainActivity.this,intent);
                    }
                };
                mDrawerLayout.closeDrawers();
                return false;

            case R.id.terms_and_condition_drawer:
                mPendingRunnable = new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(MainActivity.this,FAQWebView.class);
                        intent.putExtra(FAQWebView.ISFOR, Consts.ISFOR_TANDC);
                        startActivityWithAnimation(MainActivity.this,intent);
                    }
                };
                mDrawerLayout.closeDrawers();
                return false;
        }

        return false;
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(View drawerView) {

    }

    @Override
    public void onDrawerClosed(View drawerView) {
        if(mPendingRunnable == null) return;

        new Handler().post(mPendingRunnable);
        mPendingRunnable = null;
    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }
}
