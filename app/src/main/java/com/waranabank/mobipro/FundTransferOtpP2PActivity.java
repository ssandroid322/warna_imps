package com.waranabank.mobipro;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Model.BaseModel;
import Model.FundTransferP2PVerifyReqModel;
import Model.FundTransferP2PVerifyResModel;
import Model.P2AFundTransferVerifyReqModel;
import Parser.BaseParser;
import Request.FundTransferVerifyP2PRequest;
import Utility.ErrorMessage;
import Utility.MyEnum;
import Utility.MySharedPreferences;
import Utility.SecurityUtils;

public class FundTransferOtpP2PActivity extends BaseActivity {
    public static final String ACCOUNTNO = "ACCOUNTNO";
    public static final String AMOUNT = "AMOUNT";
    public static final String MMID = "MMID";
    public static final String MOBILENO = "MOBILENO";
    public static final String IMPS_REF_CD = "IMPS_REF_CD";
    public static final String ACTIVATION_CD = "ACTIVATION_CD";
    public static final String REMARKS = "REMARKS";

    public static final String REQUEST_CD = "request_cd";
    public static final String TITLE = "title";
    EditText mOTP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fund_transfer_otp);

        mOTP = (EditText) findViewById(R.id.otp_fundTransferOtp);
        if (getIntent().hasExtra(TITLE)) {
            ((TextView) findViewById(R.id.title_otp)).setText(getIntent().getStringExtra(TITLE));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    public void fundTransfer(View view) {
        if (getTextFromView((TextView) findViewById(R.id.otp_fundTransferOtp)).length() <= 4) {
            ((TextView) findViewById(R.id.otp_fundTransferOtp)).setError("Please enter a valid OTP.");
            return;
        }

        try {
            Intent intent = getIntent();
            String eBENF_MOBILE_NO = SecurityUtils.encryptDataWithAes(intent.getStringExtra(MOBILENO));
            String eMMID = SecurityUtils.encryptDataWithAes(intent.getStringExtra(MMID));
            String eAMOUNT = SecurityUtils.encryptDataWithAes(intent.getStringExtra(AMOUNT));
            FundTransferP2PVerifyReqModel fundTransferVerifyReqModel = new FundTransferP2PVerifyReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this),

                    getTextFromView(((TextView) findViewById(R.id.otp_fundTransferOtp))),
                    intent.getStringExtra(ACCOUNTNO),
                    eBENF_MOBILE_NO,
                    eMMID,
                    eAMOUNT,
                    intent.getStringExtra(REMARKS),
                    intent.getStringExtra(IMPS_REF_CD));


            FundTransferVerifyP2PRequest fundTransferVerifyRequest = new FundTransferVerifyP2PRequest(this, MyEnum.displayProgress.Show);
            fundTransferVerifyRequest.sendRequest(this, fundTransferVerifyReqModel);

        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    public void goBack(View view) {
        super.onBackPressed();
    }

    @Override
    public void networkConnectionError(BaseModel reqModel) {
        // CHANGE BY RAKESH TO SHOW TIME OUT CONNECTION IN FUND TRANSFER :: 19-09-2017
        FundTransferP2PVerifyReqModel fundTransferP2PVerifyReqModel = (FundTransferP2PVerifyReqModel) reqModel;
        new AlertDialog.Builder(this)
                .setTitle("Connection Timeout")
                .setMessage(ErrorMessage.getConnectionTimeOutError(fundTransferP2PVerifyReqModel.getmIMPSRefCode()))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                }).create().show();
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {
        FundTransferP2PVerifyResModel superModel = (FundTransferP2PVerifyResModel) baseParser.doParsing(objJson);

        FundTransferP2PVerifyResModel fundTransferVerifyResModel = (FundTransferP2PVerifyResModel) superModel.getModelArray().get(0);

        if (fundTransferVerifyResModel.getStatus() == FundTransferVerifyP2PRequest.NOT_REG_FUND_TRANSFER) {
            new AlertDialog.Builder(this)
                    .setTitle("Error")
                    .setMessage("The number is not valid or it is not registered with " +
                            "any account.")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).create().show();
        } else if (fundTransferVerifyResModel.getStatus() == FundTransferVerifyP2PRequest.INSUFFICIENT_BAL) {
            new AlertDialog.Builder(this)
                    .setTitle("Error")
                    .setMessage("The account you are trying to transfer from has " +
                            "insufficient balance.")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).create().show();
        } else if (fundTransferVerifyResModel.getStatus() == FundTransferVerifyP2PRequest.REJECTED) {
            new AlertDialog.Builder(this)
                    .setTitle("Error")
                    .setMessage(fundTransferVerifyResModel.getResponse()[0].getResponseDESC())
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).create().show();
        } else if (fundTransferVerifyResModel.getStatus() == FundTransferVerifyP2PRequest.DUPLICATE) {
            new AlertDialog.Builder(this)
                    .setTitle("Error")
                    .setMessage("Your request for verify is duplicate, Please try again.")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).create().show();
        } else if (fundTransferVerifyResModel.getStatus() == FundTransferVerifyP2PRequest.SUCCESSFUL) {
            new AlertDialog.Builder(this)
                    .setTitle("Fund transfer successful")
                    .setMessage("Your MMID Fund transfer was successful. Your reference number is " + fundTransferVerifyResModel.getResponse()[0].getREFNO())
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            setResult(RESULT_OK);
                            finish();
                        }
                    }).create().show();
        }
    }
}