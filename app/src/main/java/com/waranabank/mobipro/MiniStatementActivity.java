package com.waranabank.mobipro;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.draw.LineSeparator;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Adapters.MiniStatementAdapter;
import Model.BaseModel;
import Model.MiniStatementReqModel;
import Model.MiniStatementResModel;
import Model.OperativeAccountReqModel;
import Model.OperativeAccountResModel;
import Model.UpdateNarrationReqModel;
import Model.UpdateNarrationResModel;
import Parser.BaseParser;
import Parser.MiniStatementParser;
import Parser.OperativeAccountParser;
import Parser.UpdateNarrationParser;
import Request.MiniStatementRequest;
import Request.OperativeAccountRequest;
import Request.UpdateNarrationRequest;
import Utility.ErrorMessage;
import Utility.MyEnum;
import Utility.MySharedPreferences;
import Utility.PdfHelper;
import Utility.ShowProgressbar;

public class MiniStatementActivity extends com.waranabank.mobipro.BaseActivity implements AdapterView.OnItemSelectedListener {
    private static final String TAG = "MiniStatementActivity";
    private static final String BANK_FOLDER = "mobipro";
    private static final String PDF_DIR = "statements";
    public static final String ACCOUNT_NO = "account_no";
    private static final int REQUEST_WRITE = 106;

    private Spinner mSpinner;
    private View mTransactionTable, mExportButton;
    private TextView mResultMessage;
    private ListView mTransactionList;
    private ArrayList<String> mAccounts;
    private MiniStatementAdapter mMiniStatementAdapter;
    private int mSpinnerPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mini_statement);

        bindViews();

        try {
            if(MySharedPreferences.getAccounts(this) == null || MySharedPreferences.getAccounts(this).size() <= 0){
                OperativeAccountRequest operativeAccountRequest = new OperativeAccountRequest(this, MyEnum.displayProgress.Show);
                OperativeAccountReqModel operativeAccountReqModel = null;
                try {
                    operativeAccountReqModel = new OperativeAccountReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this));
                } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e) {
                    e.printStackTrace();
                }
                operativeAccountRequest.sendRequest(this, operativeAccountReqModel);
            }else{
                fillAdapterData();
            }
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    private void getMiniStatement(String accountNo){
        try {
            MiniStatementReqModel miniStatementReqModel = new MiniStatementReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this), accountNo);
            MiniStatementRequest miniStatementRequest = new MiniStatementRequest(this, MyEnum.displayProgress.Show);
            miniStatementRequest.sendRequest(this, miniStatementReqModel);
        } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException e) {
            e.printStackTrace();
        }
    }

    private ArrayList<String> getCreditArrayList()
    {
        ArrayList<String> credits = new ArrayList<>();
        int count = mMiniStatementAdapter.mAmount.size();
        ArrayList<String> amount = mMiniStatementAdapter.mAmount;
        ArrayList<Boolean> isCredit = mMiniStatementAdapter.mIsCredit;
        for (int i = 0; i < count; i++)
        {
            credits.add(isCredit.get(i) ? amount.get(i) : "");
        }

        return credits;
    }

    private ArrayList<String> getDebitArrayList(){
        ArrayList<String> debits = new ArrayList<>();
        int count = mMiniStatementAdapter.mAmount.size();
        ArrayList<String> amount = mMiniStatementAdapter.mAmount;
        ArrayList<Boolean> isCredit = mMiniStatementAdapter.mIsCredit;
        for (int i = 0; i < count; i++) {
            debits.add(!isCredit.get(i) ? amount.get(i) : "");
        }

        return debits;
    }

    public void exportToPdf(View view)
    {
        if(mMiniStatementAdapter == null) return;

        if(mMiniStatementAdapter.getCount() == 0) {
            displayErrorMessage(mSpinner, "No data to export");
            return;
        }

        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{ Manifest.permission.WRITE_EXTERNAL_STORAGE }, REQUEST_WRITE);
        }else{
            writeDataToPdf();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == REQUEST_WRITE){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                writeDataToPdf();
            }else{
                displayErrorMessage(mSpinner, "This app needs storage permission for storing PDF");
            }
        }
    }

    private void writeDataToPdf(){
        if(!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
            showLongToast(ErrorMessage.getMediaNotMountedError());
            return;
        }

        new WritePdfTask(mSpinner.getSelectedItemPosition()).execute();
    }

    @Override
    protected void bindViews() {
        mSpinner = (Spinner) findViewById(R.id.accountList_miniStatement);
        mTransactionTable = findViewById(R.id.transactionLayout_miniStatement);
        mExportButton = findViewById(R.id.exportPdf_miniStatement);
        mTransactionList = (ListView) findViewById(R.id.transactionList_miniStatement);
        mResultMessage = (TextView) findViewById(R.id.message_miniStatement);
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser)
    {
        if(baseParser instanceof OperativeAccountParser)
        {
            OperativeAccountResModel superModel = (OperativeAccountResModel) baseParser.doParsing(objJson);
            OperativeAccountResModel operativeAccountResModel = (OperativeAccountResModel) superModel.getModelArray().get(0);

            if(operativeAccountResModel.getStatus() != OperativeAccountRequest.SUCCESS)
            {
                displayErrorMessage(mSpinner, ErrorMessage.getGenericError());
                mResultMessage.setText("Accounts not found.");
                return;
            }

            new CacheAccountsTask(this, operativeAccountResModel).execute();
        }
        else if(baseParser instanceof MiniStatementParser)
        {
            MiniStatementResModel superModel = (MiniStatementResModel) baseParser.doParsing(objJson);

            MiniStatementResModel miniStatementResModel = (MiniStatementResModel) superModel.getModelArray().get(0);

            mSpinnerPosition = mSpinner.getSelectedItemPosition();

            if(miniStatementResModel.getStatus() == MiniStatementRequest.NO_TRANSACTIONS){
                mTransactionTable.setVisibility(View.INVISIBLE);
                mExportButton.setVisibility(View.INVISIBLE);
                mResultMessage.setText(ErrorMessage.getNoTransactionsError());
            }else{
                mTransactionTable.setVisibility(View.VISIBLE);
                mExportButton.setVisibility(View.VISIBLE);
                mMiniStatementAdapter = new MiniStatementAdapter(this, miniStatementResModel);
                mTransactionList.setAdapter(mMiniStatementAdapter);
            }
        }
        else if(baseParser instanceof UpdateNarrationParser)
        {
            UpdateNarrationResModel superModel = (UpdateNarrationResModel) baseParser.doParsing(objJson);

            UpdateNarrationResModel updateNarrationResModel = (UpdateNarrationResModel) superModel.getModelArray().get(0);

            if (updateNarrationResModel.getStatus() == UpdateNarrationRequest.SUCCESSFUL)
            {
                displayErrorMessage(mTransactionList, "Narration updated successfully.");
            }
            else
            {
                displayErrorMessage(mTransactionList, "Unable to update narration, please try again.");
            }
        }
    }

    public void requestUpdateNarration(String transactionCode,String narration)
    {
        try
        {
            UpdateNarrationReqModel updateNarrationReqModel = new UpdateNarrationReqModel(MySharedPreferences.getUserId(this),
                    MySharedPreferences.getActivityCode(this),
                    transactionCode,
                    narration);

            UpdateNarrationRequest updateNarrationRequest= new UpdateNarrationRequest(this , MyEnum.displayProgress.Show);
            updateNarrationRequest.sendRequest(this, updateNarrationReqModel);
        }
        catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException e) {
            e.printStackTrace();
        }

    }
    @Override
    public void networkConnectionError(BaseModel reqModel) {
        displayErrorMessage(mSpinner, ErrorMessage.getNetworkConnectionError());
    }

    private void fillAdapterData() {
        try {
            mAccounts = (ArrayList<String>) MySharedPreferences.getAccounts(this);
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        if (mAccounts == null) return;

        mAccounts.add(0, "Select Account Number");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mAccounts);
        mSpinner.setAdapter(arrayAdapter);
        mSpinner.setOnItemSelectedListener(this);

        if(getIntent().hasExtra(ACCOUNT_NO)){
            mSpinner.setSelection(mAccounts.indexOf(getIntent().getStringExtra(ACCOUNT_NO)));
            //getMiniStatement(mAccounts.get(mSpinner.getSelectedItemPosition()));
        }
    }

    public void goBack(View view){
        onBackPressed();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        if(position == 0)
        {
            mResultMessage.setText("Account not selected");
            return;
        }

        getMiniStatement(mAccounts.get(position));
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private class CacheAccountsTask extends AsyncTask<Void, Void, Void> {
        private Activity mActivity;
        private OperativeAccountResModel mOperativeAccountResModel;

        private CacheAccountsTask(Activity activity, OperativeAccountResModel operativeAccountResModel) {
            mActivity = activity;
            mOperativeAccountResModel = operativeAccountResModel;
        }

        @Override
        protected void onPreExecute() {
            ShowProgressbar.showMaskProgress(mActivity, "Processing data");
        }

        @Override
        protected Void doInBackground(Void... params) {
            OperativeAccountResModel.Response[] responses = mOperativeAccountResModel.getResponses();

            ArrayList<String> accounts = new ArrayList<>();

            for (int i = 0; i < responses.length; i++) {
                accounts.add(responses[i].getAccountNo());
            }

            try {
                MySharedPreferences.putAccounts(MiniStatementActivity.this, accounts);
                MySharedPreferences.putAccounts(MiniStatementActivity.this, responses);
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (BadPaddingException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            ShowProgressbar.dismissDialog();
            fillAdapterData();
        }
    }

    private class WritePdfTask extends AsyncTask<Void, Void, Boolean>{
        private int mPosition;
        private File mFilePath;

        WritePdfTask(int position){
            mPosition = position;
        }

        @Override
        protected void onPreExecute() {
            ShowProgressbar.showMaskProgress(MiniStatementActivity.this, "Loading...");
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Date date = new Date();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy-hh-mm-ss-a");
            String pdfName = "Mini" + simpleDateFormat.format(date) + ".pdf";

            File extDir = new File(Environment.getExternalStorageDirectory(), BANK_FOLDER+"/"+PDF_DIR);

            if(!extDir.exists()){
                extDir.mkdirs();
            }

            extDir = new File(extDir, pdfName);
            mFilePath = extDir;
            try {
                PdfHelper pdfHelper = new PdfHelper(extDir.getAbsolutePath(), MiniStatementActivity.this, mAccounts.get(mPosition), "", "");

                //Writing header with Image and date info
                Paragraph paragraph = new Paragraph();

                /*Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bank_logo);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                Image image = null;
                try {
                    image = Image.getInstance(byteArrayOutputStream.toByteArray());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                image.scaleAbsolute(100, 100);
                image.setAlignment(Element.ALIGN_CENTER);
                paragraph.add(image);*/
                paragraph.add("\n");
                paragraph.add("\n");
                paragraph.add("\n");
                paragraph.add("\n");

                Paragraph bankName = new Paragraph();
                bankName.setAlignment(Element.ALIGN_CENTER);
                bankName.setFont(pdfHelper.getBigFont());
                bankName.add(MiniStatementActivity.this.getString(R.string.banking_name));
                paragraph.add(bankName);

                LineSeparator lineSeparator = new LineSeparator();
                lineSeparator.setLineColor(pdfHelper.getSeparatorColor());
                lineSeparator.setLineWidth(0.5f);
                paragraph.add(lineSeparator);

                paragraph.add("\n");
                paragraph.add("\n");


                /*Paragraph accountNo = new Paragraph();
                accountNo.setAlignment(Element.ALIGN_LEFT);
                try {
                    accountNo.add(MySharedPreferences.getCustomerName(MiniStatementActivity.this) + " - " + mAccounts.get(mSpinnerPosition));
                } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException e) {
                    e.printStackTrace();
                }
                paragraph.add(accountNo);*/

                PdfContentByte contentByte = pdfHelper.getContentByte();
                contentByte.saveState();
                try {
                    contentByte.setFontAndSize(BaseFont.createFont("Helvetica-Bold", "Cp1252", false), 15);
                    contentByte.showTextAligned(Element.ALIGN_LEFT, "Account Details", 50, 689, 0);
                } catch (IOException e) {
                    e.printStackTrace();
                }finally {
                    contentByte.restoreState();
                }

                paragraph.add(lineSeparator);
                paragraph.add("\n");

                PdfPTable pdfPTable = new PdfPTable(new float[]{1f, 2f});
                pdfPTable.setWidthPercentage(100);
                pdfPTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
                pdfPTable.setHorizontalAlignment(Element.ALIGN_LEFT);

                String[][] strings = null;
                try {
                    OperativeAccountResModel.Response account = MySharedPreferences.getAccount(MiniStatementActivity.this, mSpinnerPosition - 1);
                    strings = new String[9][];
                    strings[0] = new String[]{
                            "Account Name",
                            ": " + MySharedPreferences.getCustomerName(MiniStatementActivity.this)
                    };
                    strings[1] = new String[]{
                            "Mobile Number",
                            ": " + MySharedPreferences.getMobileNo(MiniStatementActivity.this)
                    };
                    strings[2] = new String[]{
                            "Account Number",
                            ": " + account.getAccountNo()
                    };
                    strings[3] = new String[]{
                            "Account Type",
                            ": " + account.getAccountTypeName()
                    };

                    strings[4] = new String[]{
                            "Branch",
                            ": " + account.getBranchName()
                    };

                    strings[5] = new String[]{
                            "Opening Date",
                            ": " + account.getOpeningDate()
                    };

                    strings[6] = new String[]{
                            "Opening Balance",
                            ": " + account.getOpeningBalance()
                    };

                    strings[7] = new String[]{
                            "Balance",
                            ": " + account.getBalance()
                    };

                    Date currentDate = new Date(System.currentTimeMillis());
                    SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a");

                    strings[8] = new String[]{
                            "Date",
                            ": " + simpleDateFormat2.format(currentDate)
                    };
                } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException e) {
                    e.printStackTrace();
                }

                for (int i = 0; i < strings.length; i++) {
                    for (int j = 0; j < strings[i].length; j++) {
                        PdfHelper.insertCellToTable(pdfPTable, strings[i][j], Element.ALIGN_LEFT, 1, pdfHelper.getNormalFont12(), false, 35, 3);
                    }
                }

                Paragraph table = new Paragraph();
                table.setAlignment(Element.ALIGN_LEFT);
                table.add(pdfPTable);
                pdfPTable.setComplete(true);

                paragraph.add(table);

                paragraph.add("\n");
                paragraph.add("\n");

                /*Paragraph miniStatement = new Paragraph(new Phrase("Mini Statement"));
                miniStatement.setAlignment(Element.ALIGN_LEFT);
                paragraph.add(miniStatement);*/
                contentByte.saveState();
                try {
                    contentByte.setFontAndSize(BaseFont.createFont("Helvetica-Bold", "Cp1252", false), 13);
                    contentByte.showTextAligned(Element.ALIGN_LEFT, "Mini Statement", 50, 481, 0);
                } catch (IOException e) {
                    e.printStackTrace();
                }finally {
                    contentByte.restoreState();
                }

                int[] alignments = new int[] {Element.ALIGN_CENTER, Element.ALIGN_LEFT, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT};

                pdfHelper.addHeaderToPdf(paragraph);
                pdfHelper.createPdfTable(new float[]{1f, 2f, 0.8f, 0.8f}, 88);
                //pdfHelper.addTitlesToTables(3, null, Element.ALIGN_LEFT, "Date", "Narration", "Amount");;
                pdfHelper.addTitlesToTables(4, null, alignments, pdfHelper.getSeparatorColor(), "Date", "Narration", "Debit", "Credit");//₹
                ArrayList<String> credits = getCreditArrayList();
                ArrayList<String> debits = getDebitArrayList();
                pdfHelper.fillTableFromData(4, mMiniStatementAdapter.getCount(), false, pdfHelper.getNormalFont12Bold(), pdfHelper.getSmallFont(), alignments, pdfHelper.getSeparatorColor(), mMiniStatementAdapter.mDate, mMiniStatementAdapter.mNarration, debits, credits);

                pdfHelper.addTableToPdf();
                pdfHelper.finishWritingPdf();
                return true;
            } catch (FileNotFoundException | DocumentException e){//| InvalidKeyException | BadPaddingException | NoSuchAlgorithmException | IllegalBlockSizeException | NoSuchPaddingException e) {
                e.printStackTrace();
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean isOk) {
            ShowProgressbar.dismissDialog();
            if(isOk) {
                showLongToast("Successfully written file to SDCard/" + BANK_FOLDER + "/" + PDF_DIR);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(mFilePath), "application/pdf");
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

                Intent chooser = Intent.createChooser(intent, "Open PDF");

                startActivity(chooser);
            }
            else {
                showShortToast("There was an error saving file");
            }
        }
    }
}
