package com.waranabank.mobipro;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import Utility.ShowProgressbar;
import Utility.URLGenerator;

public class TermsAndCondition extends BaseActivity {
    WebView tandc;

//    ProgressDialog prDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_condition);
        bindView();

        loadWebview();
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    private void bindView() {
        tandc = (WebView) findViewById(R.id.tAndC_webview);
//        prDialog = ProgressDialog.show(this, "Loading", "Please wait..", true);

//        tandc.loadUrl(URLGenerator.getTermsConditionURL());
    }

    public void loadWebview() {
        tandc.getSettings().setJavaScriptEnabled(true);
        tandc.getSettings().setLoadWithOverviewMode(true);
        tandc.getSettings().setUseWideViewPort(true);
        tandc.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                prDialog.show();
                ShowProgressbar.showProgress(TermsAndCondition.this,"Loading....");
                view.loadUrl(url);

                return true;
            }

            @Override
            public void onPageFinished(WebView view, final String url) {
                ShowProgressbar.dismissDialog();
//                prDialog.dismiss();
            }
        });

        tandc.loadUrl(URLGenerator.getTermsConditionURL());

    }

    public void goBack(View view) {
        super.onBackPressed();
    }
}
