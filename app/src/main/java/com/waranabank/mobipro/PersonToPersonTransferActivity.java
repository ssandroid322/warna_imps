package com.waranabank.mobipro;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Model.BaseModel;
import Model.FundTransferP2PReqModel;
import Model.FundTransferP2PResModel;
import Model.GetScreenMsg_ReqModel;
import Model.GetScreenMsg_ResModel;
import Model.OperativeAccountReqModel;
import Model.OperativeAccountResModel;
import Parser.BaseParser;
import Parser.FundTransferP2PParser;
import Parser.GetScreenMsg_Parser;
import Parser.OperativeAccountParser;
import Request.FundTransferP2PRequest;
import Request.FundTransferRequest;
import Request.GetScreenMsg_Request;
import Request.OperativeAccountRequest;
import Utility.Consts;
import Utility.ErrorMessage;
import Utility.MyEnum;
import Utility.MySharedPreferences;
import Utility.SecurityUtils;
import Utility.ShowProgressbar;

public class PersonToPersonTransferActivity extends BaseActivity implements AdapterView.OnItemSelectedListener {

    private static final int FUND_TRANSFER = 12;
    private Spinner mSpinner;
    private TextView mNote;
    private EditText mMobileno, mMMID, mAmount, mRemarks;
    private ArrayList<String> mAccounts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_to_person_transfer);

        bindViews();

        sendRequestForNote(Consts.NOTE_IMPSP2P);

        findViewById(R.id.submit_p2pTransfer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mSpinner.getSelectedItemPosition() == 0) {
                    displayErrorMessage(mSpinner, "Please select an account");
                    return;
                }

                if(((TextView) findViewById(R.id.mmid_p2pTransfer)).getText().toString().trim().length() <= 0) {
                    displayErrorMessage(findViewById(R.id.submit_p2pTransfer), "Please enter a valid MMID");
                    return;
                }

                mAmount = (EditText) findViewById(R.id.amount_p2pTransfer);

                if(mAmount.getText().toString().trim().length() <= 0) {
                    displayErrorMessage(findViewById(R.id.submit_p2pTransfer), "Please enter amount to transfer");
                    return;
                }

                String phone = ((TextView) findViewById(R.id.phoneNo_p2pTransfer)).getText().toString().trim();
                if(phone.length() <= 0 || !ResetPasswordS1.validatePhoneNumber(phone)) {
                    displayErrorMessage(findViewById(R.id.submit_p2pTransfer), "Please enter a valid phone number");
                    return;
                }


//                Intent intent = new Intent(PersonToPersonTransferActivity.this, FundTransferOtpP2PActivity.class);
//                intent.putExtra(FundTransferOtpActivity.REQUEST_CD, "123456");
//                intent.putExtra(FundTransferOtpActivity.TITLE, "IMPS Transfer");
//                startActivityForResult(intent, FUND_TRANSFER);
                /*showLongToast("Fund Transfer successful");
                ActivityCompat.finishAfterTransition(PersonToPersonTransferActivity.this);*/

                fundTransfer();
            }
        });

        try {
            if(MySharedPreferences.getAccounts(this) == null || MySharedPreferences.getAccounts(this).size() <= 0){
                OperativeAccountRequest operativeAccountRequest = new OperativeAccountRequest(this, MyEnum.displayProgress.Show);
                OperativeAccountReqModel operativeAccountReqModel = null;
                try {
                    operativeAccountReqModel = new OperativeAccountReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this));
                } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e) {
                    e.printStackTrace();
                }
                operativeAccountRequest.sendRequest(this, operativeAccountReqModel);
            }else{
                fillAdapterData();
            }

        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == FUND_TRANSFER && resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void networkConnectionError(BaseModel reqModel) {
        super.networkConnectionError(reqModel);
        displayErrorMessage(mNote, ErrorMessage.getNetworkConnectionError());
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {
        if(baseParser instanceof OperativeAccountParser)
        {
            OperativeAccountResModel superModel = (OperativeAccountResModel) baseParser.doParsing(objJson);
            OperativeAccountResModel operativeAccountResModel = (OperativeAccountResModel) superModel.getModelArray().get(0);

            if(operativeAccountResModel.getStatus() != OperativeAccountRequest.SUCCESS)
            {
                displayErrorMessage(mSpinner, ErrorMessage.getGenericError());
                return;
            }

            new CacheAccountsTask(this, operativeAccountResModel).execute();
        }
        else if(baseParser instanceof FundTransferP2PParser)
        {
            FundTransferP2PResModel superModel = (FundTransferP2PResModel) baseParser.doParsing(objJson);
            FundTransferP2PResModel fundTransferResModel = (FundTransferP2PResModel) superModel.getModelArray().get(0);

            if(fundTransferResModel.getStatus() == FundTransferP2PRequest.NOT_REG_FUND_TRANSFER){
                displayErrorMessage(mAmount, "Invalid mobile number OR not registered with account number, Contact to near branch");
            }else if(fundTransferResModel.getStatus() == FundTransferP2PRequest.INSUFFICIENT_BAL){
                displayErrorMessage(mAmount, "Not sufficient balance available to transfer.");
            }
            else if(fundTransferResModel.getStatus() == FundTransferRequest.SUCCESSFUL)
            {
                Intent intent = new Intent(this, FundTransferOtpP2PActivity.class);
                intent.putExtra(FundTransferOtpP2PActivity.REQUEST_CD, fundTransferResModel.getResponse()[0].getRequestCode());
                intent.putExtra(FundTransferOtpP2PActivity.TITLE, "MMID Transfer");

                intent.putExtra(FundTransferOtpP2PActivity.ACCOUNTNO, mAccounts.get(mSpinner.getSelectedItemPosition()));
                intent.putExtra(FundTransferOtpP2PActivity.AMOUNT, getTextFromView(mAmount));
                intent.putExtra(FundTransferOtpP2PActivity.ACTIVATION_CD, fundTransferResModel.getResponse()[0].getActiviationCode());
                intent.putExtra(FundTransferOtpP2PActivity.IMPS_REF_CD, fundTransferResModel.getResponse()[0].getIMPSRefCode());
                intent.putExtra(FundTransferOtpP2PActivity.MOBILENO,getTextFromView(mMobileno));
                intent.putExtra(FundTransferOtpP2PActivity.MMID, getTextFromView(mMMID));
                intent.putExtra(FundTransferOtpP2PActivity.REMARKS, getTextFromView(mRemarks));

                startActivityForResult(intent, FUND_TRANSFER);
            }
        }
        else if(baseParser instanceof GetScreenMsg_Parser){
            GetScreenMsg_ResModel superModel = (GetScreenMsg_ResModel) baseParser.doParsing(objJson);
            GetScreenMsg_ResModel getScreenMsg_resModel = (GetScreenMsg_ResModel) superModel.getModelArray().get(0);
            if((getScreenMsg_resModel.getStatus().equals(GetScreenMsg_Request.SUCCESS)) && (getScreenMsg_resModel.getMessage() != null)){
                mNote.setText(getScreenMsg_resModel.getMessage());
            }
        }
    }

    private void fillAdapterData() {
        try {
            mAccounts = (ArrayList<String>) MySharedPreferences.getImpsAccounts(this);
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        if (mAccounts == null) return;

        mAccounts.add(0, "Select Account Number");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mAccounts);
        mSpinner.setAdapter(arrayAdapter);
        mSpinner.setOnItemSelectedListener(this);
    }

    @Override
    protected void bindViews() {
        mSpinner = (Spinner) findViewById(R.id.accountList_p2pTransfer);
        mNote = (TextView) findViewById(R.id.note_p2pTransfer);
        mRemarks = (EditText) findViewById(R.id.remarks_p2pTransfer);
        mMMID = (EditText) findViewById(R.id.mmid_p2pTransfer);
        mMobileno = (EditText) findViewById(R.id.phoneNo_p2pTransfer);
        mAmount = (EditText) findViewById(R.id.amount_p2pTransfer);
    }

    public void goBack(View view) {
        ActivityCompat.finishAfterTransition(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(position == 0) {
            mNote.setVisibility(View.GONE);
        } else {
            mNote.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private class CacheAccountsTask extends AsyncTask<Void, Void, Void> {
        private Activity mActivity;
        private OperativeAccountResModel mOperativeAccountResModel;

        private CacheAccountsTask(Activity activity, OperativeAccountResModel operativeAccountResModel) {
            mActivity = activity;
            mOperativeAccountResModel = operativeAccountResModel;
        }

        @Override
        protected void onPreExecute() {
            ShowProgressbar.showMaskProgress(mActivity, "Processing data");
        }

        @Override
        protected Void doInBackground(Void... params) {
            OperativeAccountResModel.Response[] responses = mOperativeAccountResModel.getResponses();

            ArrayList<String> accounts = new ArrayList<>();

            for (int i = 0; i < responses.length; i++) {
                accounts.add(responses[i].getAccountNo());
            }

            try {
                MySharedPreferences.putAccounts(mActivity, accounts);
                MySharedPreferences.putAccounts(mActivity, responses);
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (BadPaddingException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            ShowProgressbar.dismissDialog();
            fillAdapterData();
        }
    }
    public void fundTransfer()
    {
        try
        {
            String eMobileNo = SecurityUtils.encryptDataWithAes(getTextFromView(mMobileno));
            String eMMID = SecurityUtils.encryptDataWithAes(getTextFromView(mMMID));
            String eAmount = SecurityUtils.encryptDataWithAes(getTextFromView(mAmount));
            FundTransferP2PReqModel fundTransferReqModel = new FundTransferP2PReqModel( MySharedPreferences.getUserId(this),
                    MySharedPreferences.getActivityCode(this),
                    mAccounts.get(mSpinner.getSelectedItemPosition()),eMobileNo,eMMID,eAmount,getTextFromView(mRemarks));
            FundTransferP2PRequest fundTransferRequest = new FundTransferP2PRequest(this, MyEnum.displayProgress.Show);
            fundTransferRequest.sendRequest(this, fundTransferReqModel);
        }
        catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }


    private void sendRequestForNote(String screen){
        GetScreenMsg_ReqModel getScreenMsg_reqModel = new GetScreenMsg_ReqModel(screen);
        GetScreenMsg_Request getScreenMsg_request = new GetScreenMsg_Request(PersonToPersonTransferActivity.this);
        getScreenMsg_request.sendRequest(PersonToPersonTransferActivity.this,getScreenMsg_reqModel);
    }
}