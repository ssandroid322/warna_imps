package com.waranabank.mobipro;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Model.AddBenificiaryVerifyReqModel;
import Model.ChequeBookReqVerifyReqModel;
import Model.ChequeBookReqVerifyResModel;
import Model.ChequeBookResModel;
import Parser.BaseParser;
import Request.AddBenificiaryVerifyRequest;
import Request.ChequeBookReqVerifyRequest;
import Request.ChequeBookRequest;
import Utility.MyEnum;
import Utility.MySharedPreferences;

public class ChequeBookReqVerify extends BaseActivity implements View.OnClickListener {
    ImageView backButton;
    EditText et_otp;
    Button btn_submit;

    public static final String EXTRA_ACCT_NO = "ACCT_NO";
    public static final String EXTRA_NO_OF_LEAF = "NO_OF_LEAF";
    public static final String EXTRA_REQUEST_CD = "REQUEST_CD";
    private String acct_no,no_of_leaf,req_cd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheque_book_req_verify);

        init();
        getIntentData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    public void init(){
        backButton = (ImageView) findViewById(R.id.benificiarybackbutton);
        et_otp = (EditText) findViewById(R.id.Addbeneficiary_otp);
        btn_submit = (Button) findViewById(R.id.Addbeneficiary_submit);
        backButton.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == backButton){
            onBackPressed();
        }
        else if (view == btn_submit){
            sendRequestToVerify();
        }
    }

    private void sendRequestToVerify() {
        String otp = et_otp.getText().toString();
        ChequeBookReqVerifyRequest chequeBookReqVerifyRequest = new ChequeBookReqVerifyRequest(this, MyEnum.displayProgress.Show);
        ChequeBookReqVerifyReqModel chequeBookReqVerifyReqModel = null;
        try {
            chequeBookReqVerifyReqModel = new ChequeBookReqVerifyReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this),acct_no,no_of_leaf,req_cd,otp);
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        chequeBookReqVerifyRequest.sendRequest(this,chequeBookReqVerifyReqModel);
    }

    private void getIntentData(){
        acct_no = getIntent().getStringExtra(EXTRA_ACCT_NO);
        no_of_leaf = getIntent().getStringExtra(EXTRA_NO_OF_LEAF);
        req_cd = getIntent().getStringExtra(EXTRA_REQUEST_CD);
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {
        ChequeBookReqVerifyResModel superModel = (ChequeBookReqVerifyResModel) baseParser.doParsing(objJson);

        ChequeBookReqVerifyResModel chequeBookReqVerifyResModel = (ChequeBookReqVerifyResModel) superModel.getModelArray().get(0);

        if(chequeBookReqVerifyResModel.getmStatus() == ChequeBookRequest.NOT_FOUND)
        {
            showLongToast("Unable to process request for this account.");
//            displayErrorMessage(mSpinner, "Unable to process request for this account.");
        }
        else if(chequeBookReqVerifyResModel.getmStatus() == ChequeBookRequest.SUCCESS)
        {
            showAlert("Cheque book request sent successfully.");
//            showLongToast("Cheque book request sent successfully.");

        }
    }

    public void showAlert(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        setResult(RESULT_OK);
                        finish();
                    }
                })
                .create().show();
    }
}
