package com.waranabank.mobipro;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Model.BaseModel;
import Model.FundTransferReqModel;
import Model.FundTransferResModel;
import Parser.BaseParser;
import Parser.FundTransferParser;
import Request.FundTransferRequest;
import Utility.ErrorMessage;
import Utility.MyEnum;
import Utility.MySharedPreferences;

public class FundTransferVerifyP2PActivity extends BaseActivity {
    private static final int REQUEST_VERIFY = 760;
    public static final String FROM_ACCOUNT = "from_account";
    public static final String BENEFICIARY = "beneficiary";
    public static final String TRANSACTION_TYPE = "transaction_type";
    public static final String IFSC_CODE = "ifsc_code";
    public static final String ACCOUNT_NO = "account_no";
    public static final String AMOUNT = "amount";
    public static final String TITLE = "title";

    private ScrollView mScrollView;
    private TextView mFromAccount, mFromAccountName, mBeneficiary, mTransactionType, mAccountNo, mAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fund_transfer_verify);

        bindViews();
        syncDataWithIntent();

    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    private void syncDataWithIntent() {
        Intent intent = getIntent();
        mFromAccount.setText(intent.getStringExtra(FROM_ACCOUNT));
        mBeneficiary.setText(intent.getStringExtra(BENEFICIARY));
        if(intent.getStringExtra(TRANSACTION_TYPE).equalsIgnoreCase("I")){
            mTransactionType.setText("IFT");
        }else{
            mTransactionType.setText(intent.getStringExtra(TRANSACTION_TYPE));
        }
        try {
            mFromAccountName.setText(MySharedPreferences.getCustomerName(this));
        } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e) {
            e.printStackTrace();
        }
        mAccountNo.setText(intent.getStringExtra(ACCOUNT_NO));
        mAmount.setText("₹ "+intent.getStringExtra(AMOUNT));
    }

    @Override
    protected void bindViews() {
        mFromAccount = (TextView) findViewById(R.id.fromAccount_fundTransferVerify);
        mBeneficiary = (TextView) findViewById(R.id.beneficiary_fundTransferVerify);
        mTransactionType = (TextView) findViewById(R.id.transactionType_fundTransferVerify);
        mAccountNo = (TextView) findViewById(R.id.toAccountNo_fundTransferVerify);
        mAmount = (TextView) findViewById(R.id.amount_fundTransferVerify);
        mScrollView = (ScrollView) findViewById(R.id.scrollView_fundTransferVerify);
        mFromAccountName = (TextView) findViewById(R.id.fromAccountName_fundTransferVerify);
    }

    public void goBack(View v){
        super.onBackPressed();
    }

    public void fundTransfer(View view){
        /*try {
            FundTransferVerifyReqModel fundTransferVerifyReqModel = new FundTransferVerifyReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this), getIntent().getStringExtra(REQUEST_CD), getTextFromView(mOtp));
            FundTransferVerifyRequest fundTransferVerifyRequest = new FundTransferVerifyRequest(this, MyEnum.displayProgress.Show);
            fundTransferVerifyRequest.sendRequest(this, fundTransferVerifyReqModel);
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }*/

        try {
            String
                    trnType = null;
            if(getIntent().getStringExtra(TRANSACTION_TYPE).equalsIgnoreCase("I")){
                trnType = "I";
            } else {
                switch (getIntent().getStringExtra(TRANSACTION_TYPE).toUpperCase()) {
                    case "R":
                    case "N":
                    case "IMPS":
                    case "O":
                        trnType = "O";
                        break;
                }
            }

            if(trnType == null || trnType.trim().length() <= 0) {
                showShortToast("Error completing transaction, please try again later.");
                return;
            }

            Intent intent = getIntent();
            FundTransferReqModel fundTransferReqModel = new FundTransferReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this), trnType, intent.getStringExtra(FROM_ACCOUNT), intent.getStringExtra(IFSC_CODE), intent.getStringExtra(ACCOUNT_NO), intent.getStringExtra(AMOUNT));
            FundTransferRequest fundTransferRequest = new FundTransferRequest(this, MyEnum.displayProgress.Show);
            fundTransferRequest.sendRequest(this, fundTransferReqModel);
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {
        /*FundTransferVerifyResModel superModel = (FundTransferVerifyResModel) baseParser.doParsing(objJson);

        FundTransferVerifyResModel fundTransferVerifyResModel = (FundTransferVerifyResModel) superModel.getModelArray().get(0);

        if(fundTransferVerifyResModel.getStaus() == FundTransferVerifyRequest.REQ_NOT_FOUND){
            displayErrorMessage(mAccountNo, "Request not found, please try again.");
        }else if(fundTransferVerifyResModel.getStaus() == FundTransferVerifyRequest.SUCCESS){
            showLongToast("Fund transfer successful.");
            setResult(RESULT_OK);
            finish();
        }else */
        if(baseParser instanceof FundTransferParser){
            FundTransferResModel superModel = (FundTransferResModel) baseParser.doParsing(objJson);

            FundTransferResModel fundTransferResModel = (FundTransferResModel) superModel.getModelArray().get(0);

            /*if(fundTransferResModel.getStatus() == FundTransferRequest.NOT_REG_FUND_TRANSFER){
                displayErrorMessage(mBeneficiary, "This account is not registered for fund transfer.");
            }else if(fundTransferResModel.getStatus() == FundTransferRequest.INSUFFICIENT_BAL){
                displayErrorMessage(mBeneficiary, "Not sufficient balance available to transfer.");
            }else if(fundTransferResModel.getStatus() == FundTransferRequest.INVALID_ACCT_NO){
                displayErrorMessage(mBeneficiary, "Invalid account number.");
            }else if(fundTransferResModel.getStatus() == FundTransferRequest.SUCCESSFUL){
                Intent intent = new Intent(this, FundTransferOtpActivity.class);
                intent.putExtra(FundTransferOtpActivity.REQUEST_CD, fundTransferResModel.getResponse()[0].getRequestCode());
                intent.putExtra(FundTransferOtpActivity.TITLE, getIntent().getStringExtra(TITLE));
                startActivityForResult(intent, REQUEST_VERIFY);
            }*/

            Intent intent = new Intent(this, FundTransferOtpActivity.class);
            intent.putExtra(FundTransferOtpActivity.REQUEST_CD, fundTransferResModel.getResponse()[0].getRequestCode());
            intent.putExtra(FundTransferOtpActivity.TITLE, getIntent().getStringExtra(TITLE));
            startActivityForResult(intent, REQUEST_VERIFY);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_VERIFY && resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void networkConnectionError(BaseModel reqModel) {
        displayErrorMessage(mBeneficiary, ErrorMessage.getNetworkConnectionError());
    }
}
