package com.waranabank.mobipro;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.json.JSONObject;

import Model.BaseModel;
import Model.PasswordResetReqModel;
import Model.PasswordResetResModel;
import Parser.BaseParser;
import Request.PasswordResetRequest;
import Utility.ErrorMessage;
import Utility.MyEnum;

public class ResetPasswordS1 extends BaseActivity {
    private static final int REQUEST_VERIFY = 71;

    private TextView mCustomerId, mMobileNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password_s1);

        bindViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    @Override
    protected void bindViews() {
        mCustomerId = (TextView) findViewById(R.id.customerId_resetPasswordS1);
        mMobileNo = (TextView) findViewById(R.id.mobileNo_resetPasswordS1);
    }

    public void submitData(View view){
        hideKeybord(mCustomerId);
        if(getTextFromView(mCustomerId).length() == 0){
            mCustomerId.setError("Please enter a valid customer id.");
            return;
        }

        if(getTextFromView(mMobileNo).length() == 0){
            mMobileNo.setError("Please enter a valid mobile number.");
            return;
        }

        if(!validatePhoneNumber(getTextFromView(mMobileNo))){
            mMobileNo.setError("Please enter a valid mobile number.");
            return;
        }

        PasswordResetReqModel passwordResetReqModel = new PasswordResetReqModel(getTextFromView(mCustomerId), getTextFromView(mMobileNo));
        PasswordResetRequest passwordResetRequest = new PasswordResetRequest(this, MyEnum.displayProgress.Show);
        passwordResetRequest.sendRequest(this, passwordResetReqModel);
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {
        PasswordResetResModel superModel = (PasswordResetResModel) baseParser.doParsing(objJson);

        PasswordResetResModel passwordResetResModel = (PasswordResetResModel) superModel.getModelArray().get(0);

        if(passwordResetResModel.getStatus() == PasswordResetRequest.NOT_REGISTERED){
            displayErrorMessage(mCustomerId, "User id is not registered.");
            return;
        }

        if(passwordResetResModel.getStatus() == PasswordResetRequest.INVALID_CUSTOMER_ID){
            displayErrorMessage(mCustomerId, "Invalid customer id or mobile number.");
            return;
        }

        if(passwordResetResModel.getStatus() == PasswordResetRequest.SUCCESS){
            Intent intent = new Intent(this, com.waranabank.mobipro.ResetPasswordS2.class);
            PasswordResetResModel.Response response = passwordResetResModel.getResponse()[0];
            intent.putExtra(com.waranabank.mobipro.ResetPasswordS2.REQUEST_CD, response.getRequestCode());
            intent.putExtra(ResetPasswordS2.CUSTOMER_ID, response.getCustomerId());
            startActivityForResult(intent, REQUEST_VERIFY);
        }
    }

    public void goBack(View view){
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_VERIFY && resultCode == Activity.RESULT_OK){
            finish();
        }
    }

    @Override
    public void networkConnectionError(BaseModel reqModel) {
        displayErrorMessage(mCustomerId, ErrorMessage.getNetworkConnectionError());
    }

    public static boolean validatePhoneNumber(String phoneNo) {
        //validate phone numbers of format "1234567890"
        if (phoneNo.matches("\\d{10}")) return true;
            //validating phone number with -, . or spaces
        else if(phoneNo.matches("\\d{3}[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}")) return true;
            //validating phone number with extension length from 3 to 5
        else if(phoneNo.matches("\\d{3}-\\d{3}-\\d{4}\\s(x|(ext))\\d{3,5}")) return true;
            //validating phone number where area code is in braces ()
        else if(phoneNo.matches("\\(\\d{3}\\)-\\d{3}-\\d{4}")) return true;
            //phone no with country code
        else if(phoneNo.matches("\\+\\d{12}")) return true;
            //return false if nothing matches the input
        else return false;

    }
}
