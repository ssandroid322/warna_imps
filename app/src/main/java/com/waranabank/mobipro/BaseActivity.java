package com.waranabank.mobipro;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.Serializable;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Interface.onReply;
import Model.BaseModel;
import Parser.BaseParser;
import Utility.MySharedPreferences;
import Utility.URLGenerator;

/**
 * Created by Dan on 31-Oct-15.
 */
public class BaseActivity extends AppCompatActivity implements Serializable, onReply {

    static BaseActivity baseActivity;
    public String noInternetMessage =  "Please check your internet connection or try again later";

    protected void showComingSoon(View view,String message) {
        Snackbar.make(view, message + " - Coming Soon !", Snackbar.LENGTH_LONG).show();
    }

    public void hideKeybord(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);

        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(),
                InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

    protected void showKeyboard(View view){
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInputFromWindow(view.getWindowToken(), InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_NOT_ALWAYS);
    }

    protected void displayErrorMessage(View view,String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
    }

    /**
     * A method for initializing and binding all the views in hierarchy with their XML representations. This method should be used
     * to separate all the pieces in activity and to follow the architecture of this framework.
     */
    protected void bindViews(){}

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)this
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Network[] networks = connectivityManager.getAllNetworks();

            if(networks == null || networks.length <= 0) return false;

            for(Network network : networks){
                if(connectivityManager.getNetworkInfo(network).isConnected()){
                    return true;
                }
            }

            return false;
        }else{
            //noinspection deprecation
            NetworkInfo wifi = connectivityManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            //noinspection deprecation
            NetworkInfo mobile = connectivityManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            return (wifi.isConnected() || mobile.isConnected());
        }
    }

    protected boolean validateString(String values) {
        boolean isValid = false;

        if (values.trim().length() > 0) {
            isValid = true;
        }

        return isValid;
    }

    public void startActivityWithAnimation(Activity activity, Intent intent){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            startActivity(intent, ActivityOptionsCompat.makeSceneTransitionAnimation(activity).toBundle());
        }else{
            startActivity(intent);
        }
    }

    public void startActivityWithSharedElement(Activity activity, Intent intent, View sharedElement){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            startActivity(intent, ActivityOptionsCompat.makeSceneTransitionAnimation(activity, sharedElement, sharedElement.getTransitionName()).toBundle());
        }else{
            startActivity(intent);
        }
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {

    }

    @Override
    public void networkConnectionError(BaseModel reqModel) {

    }

    protected boolean isEditTextEmpty(EditText editText){
        return editText.getText().toString().trim().length() <= 0;
    }

    protected EditText getEmptyEditText(EditText... editTexts){
        int count = editTexts.length;
        for (int i = 0; i < count; i++) {
            if(isEditTextEmpty(editTexts[i])){
                return editTexts[i];
            }
        }

        return null;
    }

    protected String getTextFromView(TextView textView){
        return  textView.getText().toString().trim();
    }

    @Override
    public void onSessionExpired() {
        new android.support.v7.app.AlertDialog.Builder(this)
                .setTitle("Session Expired")
                .setMessage("Your session has been expired. You need to log in again.")
                .setCancelable(false)
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        logOut();
                    }
                })
                .setPositiveButton("OK", null)
                .create().show();
    }

    @Override
    public void onRejectedService(String msg) {
        new android.support.v7.app.AlertDialog.Builder(this)
                .setTitle("Alert")
                .setMessage(msg)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create().show();
    }

    @Override
    public void onRejectedServiceFinishActivity(String msg) {
        new android.support.v7.app.AlertDialog.Builder(this)
                .setTitle("Alert")
                .setMessage(msg)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
//                        baseActivity.finish();

//                        Intent mIntent = new Intent(BaseActivity.this,MainActivity.class);
//                        mIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                        mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(mIntent);
//                        finish();


                        Intent mIntent = null;
                        try {
                            if ((MySharedPreferences.getCustomerID(getApplicationContext()) != null)&&!MySharedPreferences.getCustomerID(getApplicationContext()).equals("")) {
                                mIntent = new Intent(BaseActivity.this, MainActivity.class);
                            }else{
                                mIntent  = new Intent(BaseActivity.this,LogInActivity.class);
                            }
                        } catch (IllegalBlockSizeException e) {
                            e.printStackTrace();
                        } catch (InvalidKeyException e) {
                            e.printStackTrace();
                        } catch (BadPaddingException e) {
                            e.printStackTrace();
                        } catch (NoSuchAlgorithmException e) {
                            e.printStackTrace();
                        } catch (NoSuchPaddingException e) {
                            e.printStackTrace();
                        }

                        if (mIntent!= null) {
                            mIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(mIntent);
                            finish();
                        }



                    }
                })
                .create().show();
    }

    public void showShortToast(String message){
        Toast.makeText(BaseActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    public void showLongToast(String message){
        Toast.makeText(BaseActivity.this, message, Toast.LENGTH_LONG).show();
    }

    protected void logOut(){
        MySharedPreferences.clearSharedPreference(this);
        Intent intent = new Intent(this, com.waranabank.mobipro.LogInActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void updateDetail() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("");

        // set dialog message
        alertDialogBuilder.setMessage("A new version of Car Trader is available, please update it.")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, close
                        // current activity
                        stopAppUsage();
                    }
                });

        alertDialogBuilder.show();

    }

    private void stopAppUsage() {
        MySharedPreferences.clearSharedPreference(this);
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(URLGenerator.getAppUrl())));

        this.finish();
    }
}