package com.waranabank.mobipro;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Model.AddBeneficiaryReqModel;
import Model.AddBeneficiaryResModel;
import Model.BaseModel;
import Model.GetScreenMsg_ReqModel;
import Model.GetScreenMsg_ResModel;
import Parser.AddBeneficiaryParser;
import Parser.BaseParser;
import Parser.GetScreenMsg_Parser;
import Request.AddBeneficiaryRequest;
import Request.GetScreenMsg_Request;
import Utility.Consts;
import Utility.ErrorMessage;
import Utility.MyEnum;
import Utility.MySharedPreferences;

public class AddBeneficiaryActivity extends com.waranabank.mobipro.BaseActivity implements AdapterView.OnItemSelectedListener {
    private String[] mAccountTypes = {
            "Within bank",
            "NEFT/RTGS/IMPS"
    };
    private Spinner mSpinner;
    private EditText mIfscCode, mAccountNo, mAccountHolderName, mAddressOne, mContact;
    AddBeneficiaryReqModel addBeneficiaryReqModel;
    String transactionType = "";
    String requestcode;
    AddBeneficiaryResModel.Responce responce;
    private static String TAG = "AddBeneficiaryActivity";
    TextView mNote;
    public static final int REQUEST_CODE = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_beneficiary);

        bindViews();
        try {
            sendRequestForNote(Consts.NOTE_ADDBENF);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

        mIfscCode.setVisibility(View.GONE);

        mSpinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mAccountTypes));
        mSpinner.setOnItemSelectedListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    @Override
    protected void bindViews() {
        mSpinner = (Spinner) findViewById(R.id.types_addBeneficiary);
        mIfscCode = (EditText) findViewById(R.id.ifscCode_addBeneficiary);
        mIfscCode.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mAccountNo = (EditText) findViewById(R.id.accountNo_addBeneficiary);
//        mAccountNo.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        mAccountNo.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20), new InputFilter.AllCaps()});
        mAccountHolderName = (EditText) findViewById(R.id.accHolderName_addBeneficiary);
        mAccountHolderName.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mAddressOne = (EditText) findViewById(R.id.addressOne_addBeneficiary);
        mAddressOne.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        mContact = (EditText) findViewById(R.id.contact_addBeneficiary);
        mNote = (TextView) findViewById(R.id.note);
    }

    public void goBack(View view) {
        super.onBackPressed();
    }

    public void addBeneficiary(View view) {
        hideKeybord(mAccountHolderName);

        /*if(mSpinner.getSelectedItemPosition() == 0){
            displayErrorMessage(mSpinner, "Please select an account type.");
        }*/

        try {
            EditText editText = null;
            if (mIfscCode.getVisibility() == View.VISIBLE) {
                editText = getEmptyEditText(mIfscCode, mAccountNo, mAccountHolderName, mAddressOne, mContact);
            } else if (mIfscCode.getVisibility() == View.GONE) {
                editText = getEmptyEditText(mAccountNo, mAccountHolderName, mAddressOne, mContact);
            }
            if (editText != null) {
                if (editText == mContact) {
                    editText.setError("Please enter a valid contact number.");
                    return;
                } else if (editText == mAddressOne) {
                    editText.setError("Please enter a valid address.");
                    return;
                } else if (editText == mAccountHolderName) {
                    editText.setError("Please enter a valid account holder's name.");
                    return;
                } else if (editText == mAccountNo) {
                    editText.setError("Please enter a valid account number.");
                    return;
                } else if (mSpinner.getSelectedItemPosition() != 0 && editText == mIfscCode) {
                    editText.setError("Please enter a valid IFSC code.");
                    return;
                }
            } else if (mContact.getText().toString().trim().length() < 10) {
                mContact.setError("Pleasse enter valid contact number.");
            } else {

                transactionType = "";
                if (mSpinner.getSelectedItemPosition() == 0) {
                    transactionType = "I";
                } else {
                    transactionType = "O";
                }

                addBeneficiaryReqModel = new AddBeneficiaryReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this), transactionType, getTextFromView(mIfscCode), getTextFromView(mAccountNo), getTextFromView(mAccountHolderName), getTextFromView(mAddressOne), getTextFromView(mContact));
                AddBeneficiaryRequest addBeneficiaryRequest = new AddBeneficiaryRequest(this, MyEnum.displayProgress.Show);
                addBeneficiaryReqModel.setToIfscCode(mIfscCode.getText().toString());
                addBeneficiaryRequest.sendRequest(this, addBeneficiaryReqModel);
            }
        } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {
        if (baseParser instanceof AddBeneficiaryParser) {
            AddBeneficiaryResModel superModel = (AddBeneficiaryResModel) baseParser.doParsing(objJson);

            AddBeneficiaryResModel addBeneficiaryResModel = (AddBeneficiaryResModel) superModel.getModelArray().get(0);

            if (addBeneficiaryResModel.getStatus() == AddBeneficiaryRequest.ALREADY_ADDED) {
                displayErrorMessage(mSpinner, "Beneficiary is already added.");
            } else if (addBeneficiaryResModel.getStatus() == AddBeneficiaryRequest.INVALID_ACCT_NO) {
                displayErrorMessage(mSpinner, "Please enter a valid account number.");
            } else if (addBeneficiaryResModel.getStatus() == AddBeneficiaryRequest.SUCCESS) {

                if (addBeneficiaryResModel.getResponce() != null) {
                    for (int i = 0; i < addBeneficiaryResModel.getResponce().length; i++) {
                        responce = addBeneficiaryResModel.getResponce()[i];
                    }
                    requestcode = responce.getRequestcode();
                }
                if (responce != null) {
                    try {
                        new MySharedPreferences().putActivityCode(this, responce.getActivitycode());
                        new MySharedPreferences().putUserId(this, responce.getUserid());
                    } catch (IllegalBlockSizeException e) {
                        e.printStackTrace();
                    } catch (InvalidKeyException e) {
                        e.printStackTrace();
                    } catch (BadPaddingException e) {
                        e.printStackTrace();
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (NoSuchPaddingException e) {
                        e.printStackTrace();
                    }
                }
                // changes by Rakesh to add Benificiary Verify
//            showLongToast("Beneficiary is added successfully.");
//            setResult(RESULT_OK);
                if (addBeneficiaryReqModel != null) {
                    Intent intent = new Intent(this, AddBenificiaryVerify.class);
                    intent.putExtra(AddBenificiaryVerify.EXTRA_TO_ACCT_NM, getTextFromView(mAccountHolderName));
                    intent.putExtra(AddBenificiaryVerify.EXTRA_TO_ACCT_NO, getTextFromView(mAccountNo));
                    intent.putExtra(AddBenificiaryVerify.EXTRA_TO_ADD1, getTextFromView(mAddressOne));
                    intent.putExtra(AddBenificiaryVerify.EXTRA_TO_CONTACT_NO, getTextFromView(mContact));
                    intent.putExtra(AddBenificiaryVerify.EXTRA_TO_IFSCCODE, getTextFromView(mIfscCode));
                    intent.putExtra(AddBenificiaryVerify.EXTRA_TRN_TYPE, transactionType);
                    intent.putExtra(AddBenificiaryVerify.EXTRA_REQUEST_CD, requestcode);
                    startActivityForResult(intent, REQUEST_CODE);
                }

                finish();
            }
        }
        else if (baseParser instanceof GetScreenMsg_Parser) {
            GetScreenMsg_ResModel superModel1 = (GetScreenMsg_ResModel) baseParser.doParsing(objJson);
            GetScreenMsg_ResModel getScreenMsg_resModel = (GetScreenMsg_ResModel) superModel1.getModelArray().get(0);
            if ((getScreenMsg_resModel.getStatus().equals(GetScreenMsg_Request.SUCCESS)) && (getScreenMsg_resModel.getMessage() != null)) {
                mNote.setText(getScreenMsg_resModel.getMessage());
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == REQUEST_CODE) && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
        }
    }

    @Override
    public void networkConnectionError(BaseModel reqModel) {
        displayErrorMessage(mSpinner, ErrorMessage.getNetworkConnectionError());
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position == 0) {
            mIfscCode.setVisibility(View.GONE);
        } else {
            mIfscCode.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void sendRequestForNote(String screen) {
        GetScreenMsg_ReqModel getScreenMsg_reqModel = new GetScreenMsg_ReqModel(screen);
        GetScreenMsg_Request getScreenMsg_request = new GetScreenMsg_Request(AddBeneficiaryActivity.this);
        getScreenMsg_request.sendRequest(AddBeneficiaryActivity.this, getScreenMsg_reqModel);
    }
}
