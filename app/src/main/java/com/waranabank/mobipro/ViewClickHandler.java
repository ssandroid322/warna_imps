package com.waranabank.mobipro;

import android.support.v4.view.ViewPager;
import android.view.View;

/**
 * Created by Samvid Mistry on 21/07/2016.
 */

public interface ViewClickHandler {
    void onViewClick(View view);

    void setViewPager(ViewPager viewPager);
}
