package com.waranabank.mobipro;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.util.SimpleArrayMap;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Adapters.BeneficiariesAdapter;
import Interface.OnBeneficiaryDeleteRequested;
import ItemDecorators.OperativeAccountItemDecoration;
import Model.BaseModel;
import Model.DelBeneficiaryReqModel;
import Model.DelBeneficiaryResModel;
import Model.GetBeneficiariesReqModel;
import Model.GetBeneficiariesResModel;
import Parser.BaseParser;
import Parser.DelBeneficiaryParser;
import Parser.GetBeneficiariesParser;
import Request.DelBeneficiaryRequest;
import Request.GetBeneficiariesRequest;
import Utility.ErrorMessage;
import Utility.MyEnum;
import Utility.MySharedPreferences;
import Utility.ShowProgressbar;

public class BeneficiariesActivity extends BaseActivity implements OnBeneficiaryDeleteRequested {
    private static final int ADD_BENEFICIARY = 90;

    private Toolbar mToolbar;
    private RecyclerView mBeneficiariesList;
    private BeneficiariesAdapter mBeneficiariesAdapter;
    private int mDeletedPosition = -1;
    private TextView mErrorTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beneficiaries);

        bindViews();

        getBeneficiaries();
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    @Override
    protected void bindViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar_beneficiaries);
        mBeneficiariesList = (RecyclerView) findViewById(R.id.beneficiariesList_beneficiaries);
        mErrorTextView = (TextView) findViewById(R.id.errorText_beneficiaries);
    }

    public void goBack(View view) {
        super.onBackPressed();
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {
        if (baseParser instanceof GetBeneficiariesParser) {
            GetBeneficiariesResModel superModel = (GetBeneficiariesResModel) baseParser.doParsing(objJson);

            GetBeneficiariesResModel getBeneficiariesResModel = (GetBeneficiariesResModel) superModel.getModelArray().get(0);

            if (getBeneficiariesResModel.getStatus() == GetBeneficiariesRequest.NOT_FOUND) {
                mErrorTextView.setVisibility(View.VISIBLE);
                mBeneficiariesList.setVisibility(View.GONE);
                showGetListError();
            } else if (getBeneficiariesResModel.getStatus() == GetBeneficiariesRequest.SUCCESS) {
                if (getBeneficiariesResModel.getResponse().length <= 0) {
                    mErrorTextView.setVisibility(View.VISIBLE);
                    mBeneficiariesList.setVisibility(View.GONE);
                    mErrorTextView.setText("No beneficiaries available.");
                    return;
                }
                mErrorTextView.setVisibility(View.GONE);
                mBeneficiariesList.setVisibility(View.VISIBLE);
                new PopulateBeneficiariesTask(getBeneficiariesResModel).execute();
            }
        } else if (baseParser instanceof DelBeneficiaryParser) {
            if (mDeletedPosition == -1) return;

            DelBeneficiaryResModel superModel = (DelBeneficiaryResModel) baseParser.doParsing(objJson);

            DelBeneficiaryResModel delBeneficiaryResModel = (DelBeneficiaryResModel) superModel.getModelArray().get(0);

            if (delBeneficiaryResModel.getStatus() == DelBeneficiaryRequest.NOT_FOUND) {
                displayErrorMessage(mToolbar, ErrorMessage.getCouldNotDeleteBeneficiariesError());
            } else if (delBeneficiaryResModel.getStatus() == DelBeneficiaryRequest.SUCCESS) {
                displayErrorMessage(mToolbar, "Beneficiary is deleted successfully.");
                mBeneficiariesAdapter.removeItem(mDeletedPosition);
                if (mBeneficiariesAdapter.getItemCount() == 0) {
                    mErrorTextView.setVisibility(View.VISIBLE);
                    mBeneficiariesList.setVisibility(View.GONE);
                    mErrorTextView.setText("No beneficiaries found.");
                }
                mDeletedPosition = -1;
            }
        }
    }

    @Override
    public void networkConnectionError(BaseModel reqModel) {
        if (reqModel instanceof DelBeneficiaryResModel) {
            displayErrorMessage(mToolbar, ErrorMessage.getNetworkConnectionError());
            return;
        }
        mErrorTextView.setVisibility(View.VISIBLE);
        mBeneficiariesList.setVisibility(View.GONE);
        displayErrorMessage(mToolbar, ErrorMessage.getNetworkConnectionError());
    }

    public void addBeneficiary(View view) {
        startActivityForResult(new Intent(this, AddBeneficiaryActivity.class), ADD_BENEFICIARY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ADD_BENEFICIARY && resultCode == RESULT_OK) {
            getBeneficiaries();
        }
    }

    private void showGetListError() {
        final Snackbar snackbar = Snackbar.make(mToolbar, ErrorMessage.getCouldNotGetListOfBeneficiariesError(), Snackbar.LENGTH_INDEFINITE);
        snackbar.show();
    }

    private void getBeneficiaries() {
        try {
            GetBeneficiariesReqModel getBeneficiariesReqModel = new GetBeneficiariesReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this));
            GetBeneficiariesRequest getBeneficiariesRequest = new GetBeneficiariesRequest(this, MyEnum.displayProgress.Show);
            getBeneficiariesRequest.sendRequest(this, getBeneficiariesReqModel);
        } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e) {
            showGetListError();
        }
    }

    @Override
    public void onDeleteBeneficiary(SimpleArrayMap<String, String> map, int deletedPosition) {
        mDeletedPosition = deletedPosition;
        DelBeneficiaryRequest delBeneficiaryRequest = new DelBeneficiaryRequest(this, MyEnum.displayProgress.Show);
        try {
            DelBeneficiaryReqModel delBeneficiaryReqModel = new DelBeneficiaryReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this), map.get(BeneficiariesAdapter.TRN_TYPE), map.get(BeneficiariesAdapter.TO_IFSCCODE), map.get(BeneficiariesAdapter.TO_ACCT_NO));
            delBeneficiaryRequest.sendRequest(this, delBeneficiaryReqModel);
        } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    private class PopulateBeneficiariesTask extends AsyncTask<Void, Void, ArrayList<SimpleArrayMap<String, String>>> {
        private GetBeneficiariesResModel mGetBeneficiariesResModel;
        private ArrayList<SimpleArrayMap<String, String>> mArrayMaps;

        private PopulateBeneficiariesTask(GetBeneficiariesResModel getBeneficiariesResModel) {
            mGetBeneficiariesResModel = getBeneficiariesResModel;
            mArrayMaps = new ArrayList<>();
        }

        @Override
        protected void onPreExecute() {
            ShowProgressbar.showMaskProgress(BeneficiariesActivity.this, "Populating the list.");
        }

        @Override
        protected ArrayList<SimpleArrayMap<String, String>> doInBackground(Void... params) {
            GetBeneficiariesResModel.Response[] response = mGetBeneficiariesResModel.getResponse();
            int count = response.length;
            for (int i = 0; i < count; i++) {
                GetBeneficiariesResModel.Response responseData = response[i];
                SimpleArrayMap<String, String> map = new SimpleArrayMap<>();

                map.put(BeneficiariesAdapter.TRN_TYPE, responseData.getTransactionType());
                map.put(BeneficiariesAdapter.TO_IFSCCODE, responseData.getToIfscCode());
                map.put(BeneficiariesAdapter.TO_ACCT_NO, responseData.getToAccountNo());
                map.put(BeneficiariesAdapter.TO_ACCT_NM, responseData.getToAccountName());
                map.put(BeneficiariesAdapter.TO_ADD1, responseData.getToAddressOne());
                map.put(BeneficiariesAdapter.TO_CONTACT_NO, responseData.getToContactNo());
                mArrayMaps.add(map);
            }

            return mArrayMaps;
        }

        @Override
        protected void onPostExecute(ArrayList<SimpleArrayMap<String, String>> arrayMaps) {
            ShowProgressbar.dismissDialog();
            mBeneficiariesAdapter = new BeneficiariesAdapter(BeneficiariesActivity.this, arrayMaps, BeneficiariesActivity.this);
            mBeneficiariesList.setHasFixedSize(true);
            mBeneficiariesList.setLayoutManager(new LinearLayoutManager(BeneficiariesActivity.this));
            mBeneficiariesList.addItemDecoration(new OperativeAccountItemDecoration(20));
            mBeneficiariesList.setAdapter(mBeneficiariesAdapter);
        }
    }
}
