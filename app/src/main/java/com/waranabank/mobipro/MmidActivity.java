package com.waranabank.mobipro;

import android.app.Activity;
import android.content.DialogInterface;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Model.CancelMmidReqModel;
import Model.CancelMmidResModel;
import Model.CreateMmidReqModel;
import Model.CreateMmidResModel;
import Model.GetScreenMsg_ReqModel;
import Model.GetScreenMsg_ResModel;
import Model.OperativeAccountReqModel;
import Model.OperativeAccountResModel;
import Parser.BaseParser;
import Parser.CancelMmidParser;
import Parser.CreateMmidParser;
import Parser.GetScreenMsg_Parser;
import Parser.OperativeAccountParser;
import Request.CancelMmidRequest;
import Request.CreateMmidRequest;
import Request.GetScreenMsg_Request;
import Request.OperativeAccountRequest;
import Utility.Consts;
import Utility.ErrorMessage;
import Utility.MyEnum;
import Utility.MySharedPreferences;
import Utility.ShowProgressbar;

public class MmidActivity extends BaseActivity implements AdapterView.OnItemSelectedListener {
    public static final String ACTION = "action";
    public static final int GENERATE = 1;
    public static final int CANCEL = 2;
    private static final int FUND_TRANSFER = 12;
    public static String TAG = "MmidActivity";
    private Spinner mSpinner;
    private Button mButton;
    private TextView mCustomerName, mMobileNo;
    private int mAction;
    private TextView mTextView;
    private Toolbar mToolbar;
    private ArrayList<String> mAccounts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mmid);

        bindViews();

//        sendRequestForNote(Consts.NOTE_GNMMID);

        mAction = getIntent().getIntExtra(ACTION, 1);
        mTextView.setText(mAction == GENERATE ? "Generate MMID" : "Cancel MMID");
        String type = (mAction == GENERATE ? Consts.NOTE_GNMMID : Consts.NOTE_CNMMID);
        Log.e(TAG, type);
        sendRequestForNote(type);
        try {
            if (MySharedPreferences.getMMIDAccounts(this) == null || MySharedPreferences.getMMIDAccounts(this).size() <= 0) {
                OperativeAccountRequest operativeAccountRequest = new OperativeAccountRequest(this, MyEnum.displayProgress.Show);
                OperativeAccountReqModel operativeAccountReqModel = null;
                try {
                    operativeAccountReqModel = new OperativeAccountReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this));
                } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e) {
                    e.printStackTrace();
                }
                operativeAccountRequest.sendRequest(this, operativeAccountReqModel);
            } else {
                fillAdapterData();
            }
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }

        try {
            mCustomerName.setText(mCustomerName.getText() + MySharedPreferences.getCustomerName(this));
            //mMobileNo.setText(mMobileNo.getText() + MySharedPreferences.getMobileNo(this));
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSpinner.getSelectedItemPosition() == 0) {
                    displayErrorMessage(mToolbar, "Please select an account");
                    return;
                }

                if (mAction == GENERATE) {
                    CreateMmidRequest createMmidRequest = new CreateMmidRequest(MmidActivity.this,
                            MyEnum.displayProgress.Show);
                    try {
                        createMmidRequest.sendRequest(MmidActivity.this,
                                new CreateMmidReqModel(
                                        MySharedPreferences.getUserId(MmidActivity.this),
                                        MySharedPreferences.getActivityCode(MmidActivity.this),
                                        mAccounts.get(mSpinner.getSelectedItemPosition())
                                ));
                    } catch (IllegalBlockSizeException e) {
                        e.printStackTrace();
                    } catch (InvalidKeyException e) {
                        e.printStackTrace();
                    } catch (BadPaddingException e) {
                        e.printStackTrace();
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (NoSuchPaddingException e) {
                        e.printStackTrace();
                    }
                } else {
                    CancelMmidRequest cancelMmidRequest = new CancelMmidRequest(MmidActivity.this,
                            MyEnum.displayProgress.Show);
                    try {
                        cancelMmidRequest.sendRequest(MmidActivity.this,
                                new CancelMmidReqModel(
                                        MySharedPreferences.getUserId(MmidActivity.this),
                                        MySharedPreferences.getActivityCode(MmidActivity.this),
                                        mAccounts.get(mSpinner.getSelectedItemPosition())
                                ));
                    } catch (IllegalBlockSizeException e) {
                        e.printStackTrace();
                    } catch (InvalidKeyException e) {
                        e.printStackTrace();
                    } catch (BadPaddingException e) {
                        e.printStackTrace();
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (NoSuchPaddingException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {
        if (baseParser instanceof OperativeAccountParser) {
            OperativeAccountResModel superModel = (OperativeAccountResModel) baseParser.doParsing(objJson);
            OperativeAccountResModel operativeAccountResModel = (OperativeAccountResModel) superModel.getModelArray().get(0);

            if (operativeAccountResModel.getStatus() != OperativeAccountRequest.SUCCESS) {
                displayErrorMessage(mSpinner, ErrorMessage.getGenericError());
                return;
            }

            new CacheAccountsTask(this, operativeAccountResModel).execute();
        } else if (baseParser instanceof CreateMmidParser) {
            CreateMmidResModel superModel = (CreateMmidResModel) baseParser.doParsing(objJson);
            CreateMmidResModel createMmidResModel =
                    (CreateMmidResModel) superModel.getModelArray().get(0);
            switch (createMmidResModel.getStatus()) {
                case CreateMmidRequest.SUCCESS:
                    new AlertDialog.Builder(MmidActivity.this)
                            .setTitle("MMID generated")
                            .setMessage("The generated MMID is " +
                                    createMmidResModel.getResponses()[0].getMmid())
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            ActivityCompat.finishAfterTransition(MmidActivity.this);
                        }
                    }).create().show();
                    break;
                case CreateMmidRequest.INVALID_NUMBER:
                    new AlertDialog.Builder(MmidActivity.this)
                            .setTitle("Error")
                            .setMessage("The mobile number is invalid or it is not associated " +
                                    "with any account number")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).create().show();
                    break;
                case CreateMmidRequest.NCPI_REJECT:
                    new AlertDialog.Builder(MmidActivity.this)
                            .setTitle("Error")
                            .setMessage(createMmidResModel.getResponses()[0].getResDesc())
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).create().show();
                    break;
                case CreateMmidRequest.PROCESSING_ERROR:
                default:
                    new AlertDialog.Builder(MmidActivity.this)
                            .setTitle("Error")
                            .setMessage("There was an error processing your request, please try " +
                                    "again later.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).create().show();
            }
        } else if (baseParser instanceof CancelMmidParser) {
            CancelMmidResModel superModel = (CancelMmidResModel) baseParser.doParsing(objJson);
            CancelMmidResModel cancelMmidResModel =
                    (CancelMmidResModel) superModel.getModelArray().get(0);
            switch (cancelMmidResModel.getStatus()) {
                case CancelMmidRequest.SUCCESS:
                    new AlertDialog.Builder(MmidActivity.this)
                            .setTitle("MMID cancelled")
                            .setMessage("The mmid is successfully cancelled.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            ActivityCompat.finishAfterTransition(MmidActivity.this);
                        }
                    }).create().show();
                    break;
                case CancelMmidRequest.INVALID_NUMBER:
                    new AlertDialog.Builder(MmidActivity.this)
                            .setTitle("Error")
                            .setMessage("The mobile number is invalid or it is not associated " +
                                    "with any account number")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).create().show();
                    break;
                case CancelMmidRequest.NCPI_REJECT:
                    new AlertDialog.Builder(MmidActivity.this)
                            .setTitle("Error")
                            .setMessage(cancelMmidResModel.getResponses()[0].getResDesc())
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).create().show();
                    break;
                case CancelMmidRequest.PROCESSING_ERROR:
                default:
                    new AlertDialog.Builder(MmidActivity.this)
                            .setTitle("Error")
                            .setMessage("There was an error processing your request, please try " +
                                    "again later.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).create().show();
            }
        } else if (baseParser instanceof GetScreenMsg_Parser) {
            GetScreenMsg_ResModel superModel = (GetScreenMsg_ResModel) baseParser.doParsing(objJson);
            GetScreenMsg_ResModel getScreenMsg_resModel = (GetScreenMsg_ResModel) superModel.getModelArray().get(0);
            if ((getScreenMsg_resModel.getStatus().equals(GetScreenMsg_Request.SUCCESS)) && (getScreenMsg_resModel.getMessage() != null)) {
                ((TextView) findViewById(R.id.noteDesc_mmid)).setText(getScreenMsg_resModel.getMessage());
            }
        }
    }

    private void fillAdapterData() {
        try {
            mAccounts = (ArrayList<String>) MySharedPreferences.getMMIDAccounts(this);
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        if (mAccounts == null) return;

        mAccounts.add(0, "Select Account Number");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mAccounts);
        mSpinner.setAdapter(arrayAdapter);
        mSpinner.setOnItemSelectedListener(this);
    }

    public void goBack(View view) {
        ActivityCompat.finishAfterTransition(this);
    }

    @Override
    protected void bindViews() {
        mSpinner = (Spinner) findViewById(R.id.accountList_mmid);
        mButton = (Button) findViewById(R.id.submit_mmid);
        mToolbar = (Toolbar) findViewById(R.id.toolbar_mmid);
        mTextView = (TextView) findViewById(R.id.toolbarTitle_mmid);
        mCustomerName = (TextView) findViewById(R.id.customerName_mmid);
        mMobileNo = (TextView) findViewById(R.id.mobileNo_mmid);
        try {
//            String mobileNo1 =  "09879464649";
            String mobileNo = MySharedPreferences.getMobileNo(this);
            if ((mobileNo != null) && mobileNo.length() >= 10) {
                String mNo = mobileNo.substring(mobileNo.length() - 4);
                mMobileNo.setText("Mobile No: " + "XXXXXX" + mNo);
            } else {
                mMobileNo.setText("");
            }
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position == 0) {
            findViewById(R.id.note_mmid).setVisibility(View.GONE);
            findViewById(R.id.noteDesc_mmid).setVisibility(View.GONE);
        } else {
            findViewById(R.id.note_mmid).setVisibility(View.VISIBLE);
            findViewById(R.id.noteDesc_mmid).setVisibility(View.VISIBLE);
        }
    }


    private void sendRequestForNote(String screen) {
        GetScreenMsg_ReqModel getScreenMsg_reqModel = new GetScreenMsg_ReqModel(screen);
        GetScreenMsg_Request getScreenMsg_request = new GetScreenMsg_Request(MmidActivity.this);
        getScreenMsg_request.sendRequest(MmidActivity.this, getScreenMsg_reqModel);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private class CacheAccountsTask extends AsyncTask<Void, Void, Void> {
        private Activity mActivity;
        private OperativeAccountResModel mOperativeAccountResModel;

        private CacheAccountsTask(Activity activity, OperativeAccountResModel operativeAccountResModel) {
            mActivity = activity;
            mOperativeAccountResModel = operativeAccountResModel;
        }

        @Override
        protected void onPreExecute() {
            ShowProgressbar.showMaskProgress(mActivity, "Processing data");
        }

        @Override
        protected Void doInBackground(Void... params) {
            OperativeAccountResModel.Response[] responses = mOperativeAccountResModel.getResponses();

            ArrayList<String> accounts = new ArrayList<>();

            for (int i = 0; i < responses.length; i++) {
                accounts.add(responses[i].getAccountNo());
            }

            try {
                MySharedPreferences.putAccounts(mActivity, accounts);
                MySharedPreferences.putAccounts(mActivity, responses);
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (BadPaddingException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            ShowProgressbar.dismissDialog();
            fillAdapterData();
        }
    }
}
