package com.waranabank.mobipro;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Model.ChangeUsernameReqModel;
import Model.ChangeUsername_ResModel;
import Model.CheckUsernameAvailability_ReqModel;
import Model.CheckUsernameAvailability_ResModel;
import Model.LoginResModel;
import Parser.BaseParser;
import Parser.ChangeUsername_Parser;
import Parser.CheckUsernameAvailabilityParser;
import Request.ChangeUsername_Request;
import Request.CheckUsernameAvailability_Request;
import Utility.ErrorMessage;
import Utility.MyEnum;
import Utility.MySharedPreferences;

public class ChangeUsernameActivity extends com.waranabank.mobipro.BaseActivity implements View.OnClickListener {
    EditText et_oldUsername, et_newUsername;
    TextView tv_checkAvalibility;
    Button btn_changeUsername;
    String userName, verifiedUsername;
    Context mContext;
    boolean isDigitorLetter = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_username);

        bindViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    public void bindViews() {
        mContext = this;
        et_oldUsername = (EditText) findViewById(R.id.changeusername_CurrentUsername);
        try {
            et_oldUsername.setText(MySharedPreferences.getUserId(mContext));
            et_oldUsername.setEnabled(false);
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        et_newUsername = (EditText) findViewById(R.id.changeusername_NewUsername);
        tv_checkAvalibility = (TextView) findViewById(R.id.changeusername_checkAvability);
        btn_changeUsername = (Button) findViewById(R.id.changeusername_btn_ChangeUsername);
        et_newUsername.addTextChangedListener(textWatcher);
        tv_checkAvalibility.setOnClickListener(this);
        btn_changeUsername.setOnClickListener(this);
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (charSequence.length() > 0) {
                userName = charSequence.toString();
                tv_checkAvalibility.setVisibility(View.VISIBLE);
                tv_checkAvalibility.setClickable(true);
            } else {
                tv_checkAvalibility.setVisibility(View.INVISIBLE);
            }

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if ((verifiedUsername != null) && !(verifiedUsername.equals("")) && !(editable.toString().equals(verifiedUsername))) {
                tv_checkAvalibility.setText("Check Availability");
                tv_checkAvalibility.setClickable(true);
                tv_checkAvalibility.setTextColor(getResources().getColor(R.color.colorPrimary));
                btn_changeUsername.setVisibility(View.INVISIBLE);
            }
        }
    };

    public void goBack(View view) {
        super.onBackPressed();
    }

    private boolean validateName(String name) {
        if (name == null || name.equals("") || name.length()<6) {
            return false;
        }
        Pattern pattern = Pattern.compile("^[a-zA-Z0-9]+$");
        Matcher matcher = pattern.matcher(name);
        if(matcher.matches()){
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public void onClick(View view) {
        if (view == tv_checkAvalibility) {
//            checkAvability();
            verifiedUsername = et_newUsername.getText().toString();
            if((validateName(verifiedUsername)) == true){
                checkAvability();
            }
            else {
                et_newUsername.setError(ErrorMessage.getUsernameError());
            }
//            tv_checkAvalibility.setText(et_newUsername.getText().toString() + " is available");
//            tv_checkAvalibility.setTextColor(getResources().getColor(R.color.gplus_color_1));
//            btn_changeUsername.setVisibility(View.VISIBLE);
        } else if (view == btn_changeUsername) {
            updateUserName();
        }
    }
    private void updateUserName() {
        try {
            if ((et_newUsername.getText().toString().trim().length()) >= 6) {
                ChangeUsernameReqModel changeUsernameReqModel = new ChangeUsernameReqModel(new MySharedPreferences().getUserId(ChangeUsernameActivity.this),new MySharedPreferences().getActivityCode(ChangeUsernameActivity.this),et_newUsername.getText().toString());
                ChangeUsername_Request changeUsername_request = new ChangeUsername_Request(ChangeUsernameActivity.this, MyEnum.displayProgress.Show);
                changeUsername_request.sendRequest(ChangeUsernameActivity.this,changeUsernameReqModel);
            } else {
                showLongToast("New user name minimum length should be 6 characters..!");
            }
        }
        catch (Exception e){
            Log.e("UpdateUsername",e.toString());
        }
    }
    private void checkAvability() {
        try {
            if ((et_newUsername.getText().toString().trim().length()) >= 6) {
                CheckUsernameAvailability_ReqModel checkUsernameAvailability_reqModel = new CheckUsernameAvailability_ReqModel(new MySharedPreferences().getUserId(ChangeUsernameActivity.this),new MySharedPreferences().getActivityCode(ChangeUsernameActivity.this),et_newUsername.getText().toString().toUpperCase());
                CheckUsernameAvailability_Request checkUsernameAvailability_request = new CheckUsernameAvailability_Request(ChangeUsernameActivity.this, MyEnum.displayProgress.Show);
                checkUsernameAvailability_request.sendRequest(ChangeUsernameActivity.this,checkUsernameAvailability_reqModel);
            }
            else {
                showLongToast("New user name minimum length should be 6 characters..!");
            }
        }
        catch (Exception e){
            Log.e("CheckUserAvailability",e.toString());
        }
    }


    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {

        if(baseParser instanceof CheckUsernameAvailabilityParser)
        {
            CheckUsernameAvailability_ResModel superModel = (CheckUsernameAvailability_ResModel) baseParser.doParsing(objJson);

            CheckUsernameAvailability_ResModel checkUsernameAvailability_resModel = (CheckUsernameAvailability_ResModel) superModel.getModelArray().get(0);

            if(checkUsernameAvailability_resModel.getStatus().equals(CheckUsernameAvailability_Request.SUCCESS)){
                tv_checkAvalibility.setText(et_newUsername.getText().toString() + " is available");
                tv_checkAvalibility.setTextColor(getResources().getColor(R.color.gplus_color_1));
                tv_checkAvalibility.setClickable(false);
                btn_changeUsername.setVisibility(View.VISIBLE);
            }
            else if(checkUsernameAvailability_resModel.getStatus().equals(CheckUsernameAvailability_Request.ERROR)){
                showLongToast(checkUsernameAvailability_resModel.getMessage());
            }
        }

        if(baseParser instanceof ChangeUsername_Parser){
            ChangeUsername_ResModel superModel = (ChangeUsername_ResModel) baseParser.doParsing(objJson);

            ChangeUsername_ResModel changeUsername_resModel = (ChangeUsername_ResModel) superModel.getModelArray().get(0);

            if(changeUsername_resModel.getStatus().equals(ChangeUsername_Request.SUCCESS)){
//                startActivityWithAnimation(ChangeUsernameActivity.this,new Intent(ChangeUsernameActivity.this,MainActivity.class));
                finish();
            }
            else if(changeUsername_resModel.getStatus().equals(ChangeUsername_Request.ERROE)){
                showLongToast(changeUsername_resModel.getMessage());
            }

        }

    }
}
