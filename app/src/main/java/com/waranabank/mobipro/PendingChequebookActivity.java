package com.waranabank.mobipro;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Adapters.PendingChequebookAdapter;
import Model.BaseModel;
import Model.PendingChequebookReqModel;
import Model.PendingChequebookResModel;
import Parser.BaseParser;
import Request.PendingChequebookRequest;
import Utility.ErrorMessage;
import Utility.MyEnum;
import Utility.MySharedPreferences;

public class PendingChequebookActivity extends com.waranabank.mobipro.BaseActivity {
    public static final String ACCOUNT_NO = "account_no";

    private TextView mMessage , accountNumber;
    private View mChequeLayout;
    private ListView mChequeBookList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_chequebooks);

        bindViews();

        try {
            PendingChequebookRequest pendingChequebookRequest = new PendingChequebookRequest(this, MyEnum.displayProgress.Show);
            PendingChequebookReqModel pendingChequebookReqModel = new PendingChequebookReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this), getIntent().getStringExtra(ACCOUNT_NO));
            pendingChequebookRequest.sendRequest(this, pendingChequebookReqModel);
        } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    @Override
    protected void bindViews()
    {
        mMessage = (TextView) findViewById(R.id.message_pendingChequebooks);
        accountNumber = (TextView) findViewById(R.id.tv_accountnumber);
        mChequeLayout = findViewById(R.id.chequesLayout_pendingChequebooks);
        mChequeBookList = (ListView) findViewById(R.id.chequeBookList_pendingChequebooks);

        accountNumber.setText(getIntent().getStringExtra(ACCOUNT_NO));
    }

    public void goBack(View view){
        super.onBackPressed();
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {
        PendingChequebookResModel superModel = (PendingChequebookResModel) baseParser.doParsing(objJson);

        PendingChequebookResModel pendingChequebookResModel = (PendingChequebookResModel) superModel.getModelArray().get(0);

        if(pendingChequebookResModel.getStatus() == PendingChequebookRequest.NOT_FOUND){
            mMessage.setVisibility(View.VISIBLE);
            mChequeLayout.setVisibility(View.GONE);
            mMessage.setText(ErrorMessage.getNoPendingChequebooksError());
        }else if(pendingChequebookResModel.getStatus() == PendingChequebookRequest.SUCCESS){
            mMessage.setVisibility(View.GONE);
            mChequeLayout.setVisibility(View.VISIBLE);
            mChequeBookList.setAdapter(new PendingChequebookAdapter(this, pendingChequebookResModel));
        }
    }

    @Override
    public void networkConnectionError(BaseModel reqModel) {
        displayErrorMessage(mMessage, ErrorMessage.getNetworkConnectionError());
    }
}
