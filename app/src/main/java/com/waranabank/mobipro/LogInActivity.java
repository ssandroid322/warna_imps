package com.waranabank.mobipro;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.codec.binary.Hex;
import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Model.BaseModel;
import Model.LoginReqModel;
import Model.LoginResModel;
import Parser.BaseParser;
import Request.LoginRequest;
import Utility.AppStrings;
import Utility.Consts;
import Utility.ErrorMessage;
import Utility.MyEnum;
import Utility.MySharedPreferences;
import Utility.SecurityUtils;
import Utility.ShowProgressbar;
import Utility.URLGenerator;

public class LogInActivity extends BaseActivity implements View.OnClickListener {
    private TextView mBankTextView, mForgotPassword, mTandC, mFAQ;
    private EditText mUsername, mPassword;
//    WebView adv_webView;

    // 13-12-2016
    Dialog mDialog;
    private VideoEnabledWebView webView;
    private VideoEnabledWebChromeClient webChromeClient;
    View nonVideoLayout;
    ViewGroup videoLayout;

    private static final int PERMISSIONS_REQUEST_READ_PHONE_STATE = 999;
    private TelephonyManager mTelephonyManager;

    ArrayList<String> permissionList;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        Log.e("SplashScreen", "attachBaseContext() Called");
        try {
            MultiDex.install(newBase);
        } catch (Exception e) {
            Log.e("SplashScreen", e.toString());
            Toast.makeText(newBase, "Unable to install MultiDex", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        bindViews();
        mDialog = new Dialog(this);
        mBankTextView.setText(AppStrings.getWelcomeString(getResources().getString(R.string.bank_name_welcome)));
        givePermission();

        //mUsername.setText("13200450");
        // mPassword.setText("123456");

//        mUsername.setText("AMC0000206");
//        mPassword.setText("97252399991");

//        mUsername.setText("");
//        mPassword.setText("");
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    @Override
    protected void bindViews() {
        MySharedPreferences.clearSharedPreference(this);
        mBankTextView = (TextView) findViewById(R.id.welcomeString_logIn);
        mUsername = (EditText) findViewById(R.id.username_logIn);
        mPassword = (EditText) findViewById(R.id.password_logIn);
        mForgotPassword = (TextView) findViewById(R.id.loginactivity_forgotpassword);
        mForgotPassword.setOnClickListener(this);
        mTandC = (TextView) findViewById(R.id.login_tandc);
        mTandC.setPaintFlags(mTandC.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mTandC.setOnClickListener(this);
        mFAQ = (TextView) findViewById(R.id.login_faq);
        mFAQ.setPaintFlags(mFAQ.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mFAQ.setOnClickListener(this);

        webView = (VideoEnabledWebView) findViewById(R.id.login_adv_webview);
//        adv_webView.loadUrl(URLGenerator.getAdvertismentURL());
        nonVideoLayout = findViewById(R.id.nonVideoLayout); // Your own view, read class comments
        videoLayout = (ViewGroup) findViewById(R.id.videoLayout); // Your own view, read class comments

        loadChromeWebView();
//        loadWebview();
    }

    public void forgotPassword() {
        Intent intent = new Intent(this, com.waranabank.mobipro.ResetPasswordS1.class);
        startActivity(intent);
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {


        LoginResModel superModel = (LoginResModel) baseParser.doParsing(objJson);

        LoginResModel loginResModel = (LoginResModel) superModel.getModelArray().get(0);

        if (loginResModel.getStatus() == LoginRequest.SUCCESSFUL) {
            try {
                MySharedPreferences.putActivityCode(this, loginResModel.getActivityCode());
                LoginResModel.Response[] response = loginResModel.getResponse();
                MySharedPreferences.putUserId(this, response[0].getUserId());
                MySharedPreferences.putCustomerName(this, response[0].getCustomerName());
                MySharedPreferences.putCustomerID(this, response[0].getCustomerId() + "");
                MySharedPreferences.putMobileNumber(this, response[0].getMobileNum());
            } catch (Exception e) {
                e.printStackTrace();
            }
            Intent intent = new Intent(this, com.waranabank.mobipro.MainActivity.class);

            mUsername.setText("");
            mPassword.setText("");

            startActivityWithAnimation(this, intent);
        } else if (loginResModel.getStatus() == LoginRequest.UNSUCCESSFUL) {
            displayErrorMessage(mBankTextView, "Invalid username or password, please try again later.");
        }
    }

    @Override
    public void networkConnectionError(BaseModel reqModel) {
        displayErrorMessage(mBankTextView, ErrorMessage.getNetworkConnectionError());
    }

    public void doLogin(View view) {
        hideKeybord(mBankTextView);

        String userId = mUsername.getText().toString();
        String password = mPassword.getText().toString();

        if (userId.trim().length() <= 0) {
            mUsername.setError("Please enter a valid username.");
            return;
        }

        if (password.trim().length() <= 0) {
            mPassword.setError("Please enter a valid password.");
            return;
        }
        String version = BuildConfig.VERSION_NAME;
        LoginReqModel loginReqModel = new LoginReqModel(userId, password,version,MySharedPreferences.getImeiNumber(this));
        LoginRequest loginRequest = new LoginRequest(this, MyEnum.displayProgress.Show);
        loginRequest.sendRequest(this, loginReqModel);
    }

    private void getDeviceImei() {

        String myAndroidDeviceId = "";

        mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);


        if (mTelephonyManager.getDeviceId() != null){
            myAndroidDeviceId = mTelephonyManager.getDeviceId();
        }else{
            myAndroidDeviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        }
        MySharedPreferences.setImeiNumber(this,myAndroidDeviceId);
        Log.d("msg", "DeviceImei " + myAndroidDeviceId);
    }

    private void givePermission() {
        if (ContextCompat.checkSelfPermission(LogInActivity.this, android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            getDeviceImei();
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(LogInActivity.this, android.Manifest.permission.READ_PHONE_STATE)) {
            new AlertDialog.Builder(LogInActivity.this)
                    .setMessage(ErrorMessage.getRequiredPhoneStoragepermission())
                    .setCancelable(false)
                    .setPositiveButton("Give Permission", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            requestReadStoragePermission();
                        }
                    })
                    .create().show();
        }

        else {
            requestReadStoragePermission();
        }
    }

    private void requestReadStoragePermission() {
        permissionList = new ArrayList<>();
        if ((ContextCompat.checkSelfPermission(LogInActivity.this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED)) {
            permissionList.add(android.Manifest.permission.READ_PHONE_STATE);
        }

        if ((permissionList != null) && (permissionList.size() > 0)) {
            String[] permissionData = permissionList.toArray(new String[0]);
            ActivityCompat.requestPermissions(LogInActivity.this, permissionData, PERMISSIONS_REQUEST_READ_PHONE_STATE);
        } else if (ContextCompat.checkSelfPermission(LogInActivity.this, android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            getDeviceImei();
        } else {
            new AlertDialog.Builder(LogInActivity.this)
                    .setMessage(ErrorMessage.getNeveraskagainforPhoneState())
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create().show();
        }
        /*ActivityCompat.requestPermissions(CreateAccount.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE , Manifest.permission.CAMERA},
                REQUEST_STORAGE_PERMISSION);*/
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_READ_PHONE_STATE: {
                boolean showrational = false;
                boolean isHavingallPermission = true;
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if ((grantResults[i] != PackageManager.PERMISSION_GRANTED) && (ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[i]))) {
                            isHavingallPermission = false;
                            showrational = true;
                            break;
                        } else if ((grantResults[i] != PackageManager.PERMISSION_GRANTED) && (!ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[i]))) {
                            isHavingallPermission = false;
                            showrational = false;
                        }
                    }
                    if (isHavingallPermission == true) {
                        getDeviceImei();
                    }
                    if (showrational) {
                        showrational = false;
                        showAlert();
                    } else {
                        for (int i = 0; i < grantResults.length; i++) {
                            if ((grantResults[i] != PackageManager.PERMISSION_GRANTED) && (!ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[i]) == true)) {
                                new android.support.v7.app.AlertDialog.Builder(LogInActivity.this)
                                        .setMessage(ErrorMessage.getNeveraskagainforPhoneState())
                                        .setCancelable(false)
                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        })
                                        .create().show();
                            }
                        }
                    }
                }
            }
        }
    }


    public void showAlert() {
        new AlertDialog.Builder(this)
                .setMessage(ErrorMessage.getRequiredPhoneStoragepermission())
                .setCancelable(false)
                .setPositiveButton("Give Permission", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestReadStoragePermission();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create().show();
    }

    public void doSignUp(View view) {
        startActivityWithAnimation(this, new Intent(this, com.waranabank.mobipro.SignUpS1Activity.class));
    }

    public void loadChromeWebView() {
        webChromeClient = new VideoEnabledWebChromeClient(LogInActivity.this, mDialog, nonVideoLayout, videoLayout, webView) // See all available constructors...
        {
            // Subscribe to standard events, such as onProgressChanged()...
            @Override
            public void onProgressChanged(WebView view, int progress) {
//                ShowProgressbar.showProgress(VideoWebViewActivity.this,"Loading...",mDialog);
                // Your code...
            }
        };
        webChromeClient.setOnToggledFullscreen(new VideoEnabledWebChromeClient.ToggledFullscreenCallback() {
            @Override
            public void toggledFullscreen(boolean fullscreen) {
                // Your code to handle the full-screen change, for example showing and hiding the title bar. Example:
                if (fullscreen) {
                    WindowManager.LayoutParams attrs = getWindow().getAttributes();
                    attrs.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
                    attrs.flags |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
                    getWindow().setAttributes(attrs);
                    if (android.os.Build.VERSION.SDK_INT >= 14) {
                        //noinspection all
                        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
                    }
                } else {
                    WindowManager.LayoutParams attrs = getWindow().getAttributes();
                    attrs.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
                    attrs.flags &= ~WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
                    getWindow().setAttributes(attrs);
                    if (android.os.Build.VERSION.SDK_INT >= 14) {
                        //noinspection all
                        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
                    }
                }

            }
        });
        webView.setWebChromeClient(webChromeClient);
        webView.setWebViewClient(new InsideWebViewClient());
        webView.getSettings().setDomStorageEnabled(true);
        webView.loadUrl(URLGenerator.getAdvertismentURL());
    }

    public void loadWebview() {
//        adv_webView.getSettings().setJavaScriptEnabled(true);
//        adv_webView.getSettings().setLoadWithOverviewMode(true);
//        adv_webView.getSettings().setUseWideViewPort(true);
//        adv_webView.setWebViewClient(new InsideWebViewClient());
//        adv_webView.setWebViewClient(new WebViewClient(){
//
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
////                ShowProgressbar.showProgress(LogInActivity.this,"Loading.....");
//                view.loadUrl(url);
//
//                return true;
//            }
//            @Override
//            public void onPageFinished(WebView view, final String url) {
////                ShowProgressbar.dismissDialog();
//            }
//        });

//        adv_webView.loadUrl(URLGenerator.getAdvertismentURL());
    }

    private class InsideWebViewClient extends WebViewClient {
        @Override
        // Force links to be opened inside WebView and not in Default Browser
        // Thanks http://stackoverflow.com/a/33681975/1815624
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            //   super.onPageFinished(view, url);

//            loadingView.setVisibility(View.GONE);
//            view.loadUrl("javascript:alert('Hello World!')");
//            ShowProgressbar.dismissDialog(mDialog);
            view.loadUrl("javascript:(function(){document.getElementsByTagName('video')[0].play();})()");

        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
//            loadingView.setVisibility(View.VISIBLE);
//            ShowProgressbar.showProgress(VideoWebViewActivity.this,"Loading...",mDialog);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == mForgotPassword) {
            forgotPassword();
        } else if (v == mTandC) {
//            startActivity(new Intent(this, TermsAndCondition.class));
            Intent intent = new Intent(this, FAQWebView.class);
            intent.putExtra(FAQWebView.ISFOR, Consts.ISFOR_TANDC);
            startActivity(intent);
        } else if (v == mFAQ) {
            Intent intent = new Intent(this, FAQWebView.class);
            intent.putExtra(FAQWebView.ISFOR, Consts.ISFORFAQ);
            startActivity(intent);
//            startActivity(new Intent(this, FAQWebView.class));
        }
    }
}
