package com.waranabank.mobipro;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Model.BaseModel;
import Model.ChequeStopReqModel;
import Model.ChequeStopResModel;
import Model.GetScreenMsg_ReqModel;
import Model.GetScreenMsg_ResModel;
import Model.OperativeAccountReqModel;
import Model.OperativeAccountResModel;
import Parser.BaseParser;
import Parser.ChequeStopParser;
import Parser.GetScreenMsg_Parser;
import Parser.OperativeAccountParser;
import Request.ChequeStopRequest;
import Request.GetScreenMsg_Request;
import Request.OperativeAccountRequest;
import Utility.Consts;
import Utility.ErrorMessage;
import Utility.MyEnum;
import Utility.MySharedPreferences;
import Utility.ShowProgressbar;
public class StopChequeRequestActivity extends BaseActivity implements AdapterView.OnItemSelectedListener {
    private ArrayList<String> mAccounts;
    private Toolbar mToolbar;
    private Spinner mSpinner;
    private View mBottomLayout;
    private TextView mErrorMessage,mNote;
    private EditText mChequeNo;
    private String mChequeNumber;
    private int REQUEST_CODE = 101;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stop_cheque_request);

        bindViews();

        sendRequestForNote(Consts.NOTE_CHQSTOP);
        try {
            if(MySharedPreferences.getAccounts(this) == null || MySharedPreferences.getAccounts(this).size() <= 0){
                OperativeAccountRequest operativeAccountRequest = new OperativeAccountRequest(this, MyEnum.displayProgress.Show);
                OperativeAccountReqModel operativeAccountReqModel = null;
                try {
                    operativeAccountReqModel = new OperativeAccountReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this));
                } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e) {
                    e.printStackTrace();
                }
                operativeAccountRequest.sendRequest(this, operativeAccountReqModel);
            }else{
                fillAdapterData();
            }
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    public void goBack(View view){
        super.onBackPressed();
    }

    private void fillAdapterData() {
        try {
//            mAccounts = (ArrayList<String>) MySharedPreferences.getAccounts(this);
            mAccounts = (ArrayList<String>) MySharedPreferences.getChequeBookAccounts(this);
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        if (mAccounts == null) return;

        mAccounts.add(0, "Select Account Number");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mAccounts);
        mSpinner.setAdapter(adapter);
        mSpinner.setOnItemSelectedListener(this);
        mErrorMessage.setText("Account not selected");
        //initializeRecyclerView();
        //addViewsFromData();
    }

    /*private void initializeRecyclerView() {
        mChequesList.setVisibility(View.VISIBLE);
        mStopChequeAdapter = new StopChequeAdapter(this, mAccounts, this);
        mChequesList.setLayoutManager(new LinearLayoutManager(this));
        mChequesList.setAdapter(mStopChequeAdapter);
    }*/

    private void addViewsFromData() {
        /*mChequeNumbers = new ArrayList<>();
        mChequesList.removeAllViews();

        for (int i = 0; i < 20; i++) {
            final View view = LayoutInflater.from(this).inflate(R.layout.view_stop_cheque, mChequesList, false);

            TextView chequeNo = (TextView) view.findViewById(R.id.chequeNo_stopCheque);
            TextView issuedDate = (TextView) view.findViewById(R.id.issuedDate_stopCheque);
            TextView issuedTo = (TextView) view.findViewById(R.id.issuedTo_stopCheque);
            TextView amount = (TextView) view.findViewById(R.id.amount_stopCheque);
            Button stopButton = (Button) view.findViewById(R.id.stopCheque_stopCheque);

            chequeNo.setText("132456");
            issuedDate.setText("10/2/2015");
            issuedTo.setText("Mahendra Suthar");
            amount.setText("₹ " + StringUtils.commaSeparated("540000"));

            mChequeNumbers.add("123456");
            ChequeInfo chequeInfo = new ChequeInfo(i);

            stopButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onViewClick(View v) {

                }
            });

            view.setTag(chequeInfo);

            mChequesList.addView(view);
        }*/
    }

    @Override
    protected void bindViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar_stopChequeRequest);
        mChequeNo = (EditText) findViewById(R.id.chequeNo_stopChequeRequest);
        mSpinner = (Spinner) findViewById(R.id.accountList_stopChequeRequest);
        mBottomLayout = findViewById(R.id.bottomLayout_stopChequeRequest);
        mErrorMessage = (TextView) findViewById(R.id.errorText_stopChequeRequest);

        mNote = (TextView) findViewById(R.id.note);
        mNote.setVisibility(View.GONE);
    }

    public void stopCheque(View v){
        if(mChequeNo.getText().toString().trim().length() <= 0){
            mChequeNo.setError("Please enter a cheque number");
            return;
        }

        mChequeNumber = getTextFromView(mChequeNo);

        new AlertDialog.Builder(this).setTitle("Stop Cheque?").setMessage("Are you sure you want to stop cheque?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        stopCheque();
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create().show();
    }

    public void stopCheque(){
        hideKeybord(mBottomLayout);

        if(getEmptyEditText(mChequeNo) != null){
            mChequeNo.setError("Please enter a cheque number.");
            return;
        }

        try {
            ChequeStopReqModel chequeStopReqModel = new ChequeStopReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this), mAccounts.get(mSpinner.getSelectedItemPosition()), mChequeNo.getText().toString());
            ChequeStopRequest chequeStopRequest = new ChequeStopRequest(this, MyEnum.displayProgress.Show);
            chequeStopRequest.sendRequest(this, chequeStopReqModel);
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {
        if(baseParser instanceof OperativeAccountParser) {
            OperativeAccountResModel superModel = (OperativeAccountResModel) baseParser.doParsing(objJson);
            OperativeAccountResModel operativeAccountResModel = (OperativeAccountResModel) superModel.getModelArray().get(0);

            if(operativeAccountResModel.getStatus() != OperativeAccountRequest.SUCCESS) {
                displayErrorMessage(mToolbar, ErrorMessage.getGenericError());
                return;
            }

            mBottomLayout.setVisibility(View.VISIBLE);
            mErrorMessage.setVisibility(View.GONE);

            new CacheAccountsTask(this, operativeAccountResModel).execute();
        }else if(baseParser instanceof ChequeStopParser){
            ChequeStopResModel superModel = (ChequeStopResModel) baseParser.doParsing(objJson);

            ChequeStopResModel chequeStopResModel = (ChequeStopResModel) superModel.getModelArray().get(0);
            ChequeStopResModel.Responce responce = null;

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Cheque Stop");
            builder.setPositiveButton("OKAY", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            if(chequeStopResModel.getStatus() == ChequeStopRequest.CHEQUE_RETURN){
               builder.setMessage("Cheque with number "+mChequeNumber+" is returned.");
            }else if(chequeStopResModel.getStatus() == ChequeStopRequest.ERROR){
                builder.setMessage("Unable to stop the cheque with number "+mChequeNumber+".");
            }else if(chequeStopResModel.getStatus() == ChequeStopRequest.NOT_FOUND){
                builder.setMessage("Cheque with number "+mChequeNumber+" is not found, please check cheque number.");
            }else if(chequeStopResModel.getStatus() == ChequeStopRequest.PROCESSED){
                builder.setMessage("Cheque with number "+mChequeNumber+" is processed.");
            }else if(chequeStopResModel.getStatus() == ChequeStopRequest.REQUEST_ACCEPT){
                if(chequeStopResModel.getResponce() != null) {
                    for (int i = 0; i < chequeStopResModel.getResponce().length; i++) {
                        responce = chequeStopResModel.getResponce()[i];
                    }
                }

                if(responce != null){
                    try {
                        new MySharedPreferences().putActivityCode(this,responce.getActivitycode());
                        new MySharedPreferences().putUserId(this,responce.getUserid());
                    } catch (IllegalBlockSizeException e) {
                        e.printStackTrace();
                    } catch (InvalidKeyException e) {
                        e.printStackTrace();
                    } catch (BadPaddingException e) {
                        e.printStackTrace();
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (NoSuchPaddingException e) {
                        e.printStackTrace();
                    }
                }
//                builder.setMessage("Request to stop cheque accepted successfully.");
                if(responce != null) {
                    Intent intent = new Intent(this, ChequeStopVerify.class);
                    intent.putExtra(ChequeStopVerify.EXTRA_REQUEST_CD,responce.getRequestcode());
                    intent.putExtra(ChequeStopVerify.EXTRA_ACCT_NO,mAccounts.get(mSpinner.getSelectedItemPosition()));
                    intent.putExtra(ChequeStopVerify.EXTRA_CHQ_NO,mChequeNo.getText().toString());
                    startActivityForResult(intent,REQUEST_CODE);
                }

            }else if(chequeStopResModel.getStatus() == ChequeStopRequest.STOP_PAYMENT){
                builder.setMessage("Payment of Cheque with number "+mChequeNumber+" is stopped.");
            }else if(chequeStopResModel.getStatus() == ChequeStopRequest.SURRENDER){
                builder.setMessage("Cheque with number "+mChequeNumber+" is surrendered.");
            }else if(chequeStopResModel.getStatus() == ChequeStopRequest.FACILITY_DISABLED){
                builder.setMessage("Temporarily, this service has been disabled by the bank. Please try again after sometime.");
            }

            mChequeNo.setText("");

            builder.create().show();
        }

        else if(baseParser instanceof GetScreenMsg_Parser){
            GetScreenMsg_ResModel superModel = (GetScreenMsg_ResModel) baseParser.doParsing(objJson);
            GetScreenMsg_ResModel getScreenMsg_resModel = (GetScreenMsg_ResModel) superModel.getModelArray().get(0);
            if((getScreenMsg_resModel.getStatus().equals(GetScreenMsg_Request.SUCCESS)) && (getScreenMsg_resModel.getMessage() != null)){
                mNote.setText(getScreenMsg_resModel.getMessage());
                mNote.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void networkConnectionError(BaseModel reqModel) {
        displayErrorMessage(mToolbar, ErrorMessage.getNetworkConnectionError());
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(position == 0){
            mBottomLayout.setVisibility(View.GONE);
            mNote.setVisibility(View.GONE);
            mErrorMessage.setVisibility(View.VISIBLE);
        }else{
            mBottomLayout.setVisibility(View.VISIBLE);
            mErrorMessage.setVisibility(View.GONE);
            mNote.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private class CacheAccountsTask extends AsyncTask<Void, Void, Void> {
        private Activity mActivity;
        private OperativeAccountResModel mOperativeAccountResModel;

        private CacheAccountsTask(Activity activity, OperativeAccountResModel operativeAccountResModel) {
            mActivity = activity;
            mOperativeAccountResModel = operativeAccountResModel;
        }

        @Override
        protected void onPreExecute() {
            ShowProgressbar.showMaskProgress(mActivity, "Processing data");
        }

        @Override
        protected Void doInBackground(Void... params) {
            OperativeAccountResModel.Response[] responses = mOperativeAccountResModel.getResponses();

            ArrayList<String> accounts = new ArrayList<>();

            for (int i = 0; i < responses.length; i++) {
                accounts.add(responses[i].getAccountNo());
            }

            try {
                MySharedPreferences.putAccounts(StopChequeRequestActivity.this, accounts);
                MySharedPreferences.putAccounts(StopChequeRequestActivity.this, responses);
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (BadPaddingException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            ShowProgressbar.dismissDialog();
            fillAdapterData();
        }
    }

    private void sendRequestForNote(String screen){
        GetScreenMsg_ReqModel getScreenMsg_reqModel = new GetScreenMsg_ReqModel(screen);
        GetScreenMsg_Request getScreenMsg_request = new GetScreenMsg_Request(StopChequeRequestActivity.this);
        getScreenMsg_request.sendRequest(StopChequeRequestActivity.this,getScreenMsg_reqModel);
    }
}
