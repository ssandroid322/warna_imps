package com.waranabank.mobipro;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class SignUpFinishActivity extends BaseActivity {
    public static final String USER_ID = "user_id";

    private TextView mUserIdTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_finish);

        bindViews();
        syncViewsWithIntent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    private void syncViewsWithIntent() {
        mUserIdTextView.setText(getResources().getString(R.string.user_id_sign_up_finish, getIntent().getStringExtra(USER_ID)));
    }

    @Override
    protected void bindViews() {
        mUserIdTextView = (TextView) findViewById(R.id.userId_signUpFinish);
    }

    public void goBack(View view){
        finishSignUp();
    }

    public void finishSignUp(View view){
        finishSignUp();
    }

    private void finishSignUp() {
        Intent intent = new Intent();
        intent.putExtra(com.waranabank.mobipro.SignUpS2Activity.SHOULD_FINISH, true);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        finishSignUp();
    }
}
