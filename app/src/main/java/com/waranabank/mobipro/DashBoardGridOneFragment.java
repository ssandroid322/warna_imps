package com.waranabank.mobipro;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class DashBoardGridOneFragment extends BaseFragment implements View.OnClickListener {
    private ViewClickHandler mViewClickHandler;

    public DashBoardGridOneFragment() {
        // Required empty public constructor
    }

    public static DashBoardGridOneFragment newInstance() {
        return new DashBoardGridOneFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dash_board_grid_one, container, false);

        view.findViewById(R.id.operativeAc_dashBoard).setOnClickListener(this);
        view.findViewById(R.id.jointAc_dashBoard).setOnClickListener(this);
        view.findViewById(R.id.miniStatement_dashBoard).setOnClickListener(this);
        view.findViewById(R.id.detailedStatement_dashBoard).setOnClickListener(this);
        view.findViewById(R.id.chequeBook_dashBoard).setOnClickListener(this);
        view.findViewById(R.id.chequeSearch_dashBoard).setOnClickListener(this);
        view.findViewById(R.id.mmid_dashBoard).setOnClickListener(this);
        view.findViewById(R.id.fundTransfer_dashBoard).setOnClickListener(this);
        view.findViewById(R.id.beneficiary_dashBoard).setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        if(mViewClickHandler == null) return;

        mViewClickHandler.onViewClick(v);
    }

    public void setViewClickHandler(ViewClickHandler viewClickHandler) {
        mViewClickHandler = viewClickHandler;
    }
}
