package com.waranabank.mobipro;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Model.BaseModel;
import Model.BlockCardReqModel;
import Model.BlockCardResModel;
import Model.GetScreenMsg_ReqModel;
import Model.GetScreenMsg_ResModel;
import Model.OperativeAccountReqModel;
import Model.OperativeAccountResModel;
import Parser.BaseParser;
import Parser.BlockCardParser;
import Parser.GetScreenMsg_Parser;
import Parser.OperativeAccountParser;
import Request.BlockCardRequest;
import Request.GetScreenMsg_Request;
import Request.OperativeAccountRequest;
import Utility.Consts;
import Utility.ErrorMessage;
import Utility.MyEnum;
import Utility.MySharedPreferences;
import Utility.ShowProgressbar;

public class BlockATMActivity extends BaseActivity
{
    private Spinner mSpinner;
    private ArrayList<String> mAccounts ;

    private View mBottomLayout;
    private TextView mErrorMessage,mNote;
    private Button mConfirm;

    private static final int REQUEST_VERIFY = 760;
    EditText editText;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_block_atm);

        bindViews();

        try
        {
            if(MySharedPreferences.getAccounts(this) == null || MySharedPreferences.getAccounts(this).size() <= 0)
            {
                OperativeAccountRequest operativeAccountRequest = new OperativeAccountRequest(this, MyEnum.displayProgress.Show);
                OperativeAccountReqModel operativeAccountReqModel = new OperativeAccountReqModel(MySharedPreferences.getUserId(this),
                        MySharedPreferences.getActivityCode(this));
                operativeAccountRequest.sendRequest(this, operativeAccountReqModel);
            }
            else
            {
                fillAdapterData();
            }
        }
        catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e)
        {
            e.printStackTrace();
        }

        sendRequestForNote(Consts.NOTE_ATMBLOCK);
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    @Override
    protected void bindViews()
    {
        mSpinner = (Spinner) findViewById(R.id.accountList_blockCard);

        mBottomLayout = findViewById(R.id.bottomLayout_blockCard);
        mErrorMessage = (TextView) findViewById(R.id.errorText_blockCard);
        mNote = (TextView) findViewById(R.id.note);
        mNote.setVisibility(View.GONE);
        mConfirm = (Button) findViewById(R.id.confirm_blockCard);
    }

    public void goBack(View view)
    {
        super.onBackPressed();
    }

    private void fillAdapterData()
    {
        try
        {
            mAccounts = (ArrayList<String>) MySharedPreferences.getAccounts(this);

            if(mAccounts == null || mAccounts.size() <= 0)
                return;
            mAccounts.add(0, "Select an account");
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mAccounts);
            mSpinner.setAdapter(adapter);

            mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
            {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
                {
                    if(position == 0)
                    {
                        mErrorMessage.setVisibility(View.VISIBLE);
                        mErrorMessage.setText("Account not selected");
                        mBottomLayout.setVisibility(View.GONE);
                        mConfirm.setVisibility(View.INVISIBLE);
                        mNote.setVisibility(View.GONE);
                        return;
                    }

                    mErrorMessage.setVisibility(View.GONE);
                    mBottomLayout.setVisibility(View.VISIBLE);
                    mConfirm.setVisibility(View.VISIBLE);
                    mNote.setVisibility(View.VISIBLE);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent)
                {

                }
            });

        }
        catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e)
        {
            e.printStackTrace();
        }
    }
    public void confirm(View view)
    {
        editText = (EditText) findViewById(R.id.remarks_blockATM);

        if(isEditTextEmpty(editText))
        {
            editText.setError("Please enter a valid remarks.");
            return;
        }
        try
        {
            BlockCardRequest blockCardRequest = new BlockCardRequest(this,MyEnum.displayProgress.Show);

            BlockCardReqModel blockCardReqModel = new BlockCardReqModel(
                    MySharedPreferences.getUserId(this),
                    MySharedPreferences.getActivityCode(this),
                    MySharedPreferences.getCustomerID(this),
                    MySharedPreferences.getMobileNo(this),
                    mAccounts.get(mSpinner.getSelectedItemPosition()),
                    getTextFromView(editText));

            blockCardRequest.sendRequest(this, blockCardReqModel);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    private void sendRequestForNote(String screen){
        GetScreenMsg_ReqModel getScreenMsg_reqModel = new GetScreenMsg_ReqModel(screen);
        GetScreenMsg_Request getScreenMsg_request = new GetScreenMsg_Request(BlockATMActivity.this);
        getScreenMsg_request.sendRequest(BlockATMActivity.this,getScreenMsg_reqModel);
    }
    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser)
    {
        super.onPopulate(objJson, baseParser);

        if(baseParser instanceof OperativeAccountParser)
        {
            OperativeAccountResModel superModel = (OperativeAccountResModel) baseParser.doParsing(objJson);

            OperativeAccountResModel operativeAccountResModel = (OperativeAccountResModel) superModel.getModelArray().get(0);

            if(operativeAccountResModel.getStatus() != OperativeAccountRequest.SUCCESS)
            {
                displayErrorMessage(mSpinner, ErrorMessage.getGenericError());
                return;
            }

            new CacheAccountsTask(this, operativeAccountResModel).execute();
        }
        else if(baseParser instanceof BlockCardParser)
        {
            BlockCardResModel superModel = (BlockCardResModel) baseParser.doParsing(objJson);

            BlockCardResModel blockCardResModel = (BlockCardResModel) superModel.getModelArray().get(0);

            if(blockCardResModel.getStatus() == BlockCardRequest.NOT_FOUND)
            {
                displayErrorMessage(editText, "Sorry, No active ATM card found with your account.");
            }
            else if (blockCardResModel.getStatus() == BlockCardRequest.SUCCESS)
            {
                Intent intent = new Intent(this, BlockATMOTPActivity.class);
                intent.putExtra(BlockATMOTPActivity.REQUEST_CD, blockCardResModel.getResponses()[0].getREQUEST_CD());
                startActivityForResult(intent, REQUEST_VERIFY);
            }
        }

        else if(baseParser instanceof GetScreenMsg_Parser){
            GetScreenMsg_ResModel superModel = (GetScreenMsg_ResModel) baseParser.doParsing(objJson);
            GetScreenMsg_ResModel getScreenMsg_resModel = (GetScreenMsg_ResModel) superModel.getModelArray().get(0);
            if((getScreenMsg_resModel.getStatus().equals(GetScreenMsg_Request.SUCCESS)) && (getScreenMsg_resModel.getMessage() != null)){
                mNote.setText(getScreenMsg_resModel.getMessage());
            }
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode == REQUEST_VERIFY && resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void networkConnectionError(BaseModel reqModel)
    {
        if(reqModel instanceof OperativeAccountReqModel)
        {
            mErrorMessage.setVisibility(View.VISIBLE);
            mBottomLayout.setVisibility(View.GONE);
            mConfirm.setVisibility(View.INVISIBLE);
            mErrorMessage.setText("Accounts not found.");
        }
        displayErrorMessage(mSpinner, ErrorMessage.getNetworkConnectionError());
    }
    private class CacheAccountsTask extends AsyncTask<Void, Void, Void>
    {
        private Activity mActivity;
        private OperativeAccountResModel mOperativeAccountResModel;

        private CacheAccountsTask(Activity activity, OperativeAccountResModel operativeAccountResModel)
        {
            mActivity = activity;
            mOperativeAccountResModel = operativeAccountResModel;
        }

        @Override
        protected void onPreExecute()
        {
            ShowProgressbar.showMaskProgress(mActivity, "Processing data");
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            OperativeAccountResModel.Response[] responses = mOperativeAccountResModel.getResponses();

            ArrayList<String> accounts = new ArrayList<>();

            for (int i = 0; i < responses.length; i++)
            {
                accounts.add(responses[i].getAccountNo());
            }

            try
            {
                MySharedPreferences.putAccounts(mActivity, accounts);
                MySharedPreferences.putAccounts(mActivity, responses);
            }
            catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e)
            {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid)
        {
            ShowProgressbar.dismissDialog();
            fillAdapterData();
        }


    }
}