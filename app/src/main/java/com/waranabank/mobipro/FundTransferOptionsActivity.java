package com.waranabank.mobipro;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;

public class FundTransferOptionsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fund_transfer_options);

        findViewById(R.id.ift_fundTransferOptions).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FundTransferOptionsActivity.this, FundTransferActivity.class);
                intent.putExtra(FundTransferActivity.TRANSACTION_TYPE, "I");
                startActivity(intent);
            }
        });
//        findViewById(R.id.obNeft_fundTransferOptions).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent =
//                        new Intent(FundTransferOptionsActivity.this, FundTransferActivity.class);
//                intent.putExtra(FundTransferActivity.TRANSACTION_TYPE, "N");
//                startActivity(intent);
//            }
//        });
//        findViewById(R.id.obRtgs_fundTransferOptions).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent =
//                        new Intent(FundTransferOptionsActivity.this, FundTransferActivity.class);
//                intent.putExtra(FundTransferActivity.TRANSACTION_TYPE, "R");
//                startActivity(intent);
//            }
//        });
        findViewById(R.id.p2a_fundTransferOptions).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =
                        new Intent(FundTransferOptionsActivity.this, FundTransferActivity.class);
                intent.putExtra(FundTransferActivity.TRANSACTION_TYPE, "IMPS");
                startActivity(intent);
            }
        });
        findViewById(R.id.p2p_fundTransferOptions).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FundTransferOptionsActivity.this, PersonToPersonTransferActivity.class);
                startActivity(intent);
            }
        });
//        findViewById(R.id.p2ac_fundTransferOptions).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(
//                        new Intent(FundTransferOptionsActivity.this, PersonToAdharCardActivity.class)
//                );
//            }
//        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    public void goBack(View view) {
        ActivityCompat.finishAfterTransition(this);
    }
}
