package com.waranabank.mobipro;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import Utility.AppStrings;
import Utility.Consts;
import Utility.ShowProgressbar;
import Utility.URLGenerator;

public class FAQWebView extends BaseActivity {
//    WebView faqWebview;
    TextView tv_title;
     String url = "url";
    public static String ISFOR = "isfrom";
    String title;
    String from;
    Dialog mDialog;
    private VideoEnabledWebView webView;
    private VideoEnabledWebChromeClient webChromeClient;
    View nonVideoLayout;
    ViewGroup videoLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faqweb_view);
        getIntentData();
        webView = (VideoEnabledWebView) findViewById(R.id.FAQ_webview);
        nonVideoLayout = findViewById(R.id.nonVideoLayout); // Your own view, read class comments
        videoLayout = (ViewGroup) findViewById(R.id.videoLayout); // Your own view, read class comments
        bindView();
        loadChromeWebView();
        //loadWebview();
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    private void getIntentData() {
        from = getIntent().getStringExtra(ISFOR);
        Log.e("From",from);
    }
    private void bindView() {

        if (from.equals(Consts.ISFOR_TANDC)){
            url = URLGenerator.getTermsConditionURL();

            title = "Terms & Conditions";
        }
        else if (from.equals(Consts.ISFORFAQ)){
            url = URLGenerator.getFAQURL();
            title = "FAQ";
        }
        Log.e("URL",url);

//        faqWebview = (WebView) findViewById(R.id.FAQ_webview);
        tv_title = (TextView) findViewById(R.id.webView_title);
        tv_title.setText(title);
//        faqWebview.loadUrl(URLGenerator.getFAQURL());
    }
    public void loadChromeWebView() {
        webChromeClient = new VideoEnabledWebChromeClient(FAQWebView.this, mDialog, nonVideoLayout, videoLayout, webView) // See all available constructors...
        {
            // Subscribe to standard events, such as onProgressChanged()...
            @Override
            public void onProgressChanged(WebView view, int progress) {
//                ShowProgressbar.showProgress(VideoWebViewActivity.this,"Loading...",mDialog);
                // Your code...
            }
        };
        webChromeClient.setOnToggledFullscreen(new VideoEnabledWebChromeClient.ToggledFullscreenCallback() {
            @Override
            public void toggledFullscreen(boolean fullscreen) {
                // Your code to handle the full-screen change, for example showing and hiding the title bar. Example:
                if (fullscreen) {
                    WindowManager.LayoutParams attrs = getWindow().getAttributes();
                    attrs.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
                    attrs.flags |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
                    getWindow().setAttributes(attrs);
                    if (android.os.Build.VERSION.SDK_INT >= 14) {
                        //noinspection all
                        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
                    }
                } else {
                    WindowManager.LayoutParams attrs = getWindow().getAttributes();
                    attrs.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
                    attrs.flags &= ~WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
                    getWindow().setAttributes(attrs);
                    if (android.os.Build.VERSION.SDK_INT >= 14) {
                        //noinspection all
                        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
                    }
                }

            }
        });
        webView.setWebChromeClient(webChromeClient);
        webView.setWebViewClient(new InsideWebViewClient());
        webView.loadUrl(url);
    }
    private class InsideWebViewClient extends WebViewClient {
        @Override
        // Force links to be opened inside WebView and not in Default Browser
        // Thanks http://stackoverflow.com/a/33681975/1815624
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            //   super.onPageFinished(view, url);

//            loadingView.setVisibility(View.GONE);
//            view.loadUrl("javascript:alert('Hello World!')");
//            ShowProgressbar.dismissDialog(mDialog);
            view.loadUrl("javascript:(function(){document.getElementsByTagName('video')[0].play();})()");

        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
//            loadingView.setVisibility(View.VISIBLE);
//            ShowProgressbar.showProgress(VideoWebViewActivity.this,"Loading...",mDialog);
        }
    }
    public void goBack(View view){
        super.onBackPressed();
    }

    public void loadWebview(){
//        faqWebview.getSettings().setJavaScriptEnabled(true);
//        faqWebview.getSettings().setLoadWithOverviewMode(true);
//        faqWebview.getSettings().setUseWideViewPort(true);
//        faqWebview.setWebViewClient(new WebViewClient(){
//
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
////                ShowProgressbar.showProgress(FAQWebView.this,"Loading.....");
//                view.loadUrl(url);
//
//                return true;
//            }
//            @Override
//            public void onPageFinished(WebView view, final String url) {
////                ShowProgressbar.dismissDialog();
//            }
//        });
//
//        faqWebview.loadUrl(url);

    }
}
