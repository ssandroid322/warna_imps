package com.waranabank.mobipro;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.draw.LineSeparator;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Adapters.DetailedStatementAdapter;
import Model.BaseModel;
import Model.DetailedStatementReqModel;
import Model.DetailedStatementResModel;
import Model.OperativeAccountReqModel;
import Model.OperativeAccountResModel;
import Model.UpdateNarrationReqModel;
import Model.UpdateNarrationResModel;
import Parser.BaseParser;
import Parser.DetailedStatementParser;
import Parser.OperativeAccountParser;
import Parser.UpdateNarrationParser;
import Request.DetailedStatementRequest;
import Request.OperativeAccountRequest;
import Request.UpdateNarrationRequest;
import Utility.ErrorMessage;
import Utility.MyEnum;
import Utility.MySharedPreferences;
import Utility.PdfHelper;
import Utility.ShowProgressbar;

public class DetailedStatementActivity extends com.waranabank.mobipro.BaseActivity implements View.OnClickListener {
    private static final String TAG = "DetailedStatementActivity";
    private static final String BANK_FOLDER = "mobipro";
    private static final String PDF_DIR = "statements";
    public static final String ACCOUNT_NO = "account_no";
    private static final int REQUEST_WRITE = 928;

    private Spinner mSpinner;
    private View mTransactionTable, mExportButton, mGetTransactions;
    private ListView mTransactionList;
    private ArrayList<String> mAccounts;
    private TextView mFromDateTextView, mToDateTextView, mResultMessage;
    private String mFromDateString, mToDateString;
    private int mSpinnerPosition;
    private Date mFromDate, mToDate;
    private DatePickerDialog mFromDateDialog, mToDateDialog;
    private DetailedStatementAdapter mDetailedStatementAdapter;    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_statement);

        bindViews();

        try {
            if(MySharedPreferences.getAccounts(this) == null || MySharedPreferences.getAccounts(this).size() <= 0){
                OperativeAccountRequest operativeAccountRequest = new OperativeAccountRequest(this, MyEnum.displayProgress.Show);
                OperativeAccountReqModel operativeAccountReqModel = null;
                try {
                    operativeAccountReqModel = new OperativeAccountReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this));
                } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e) {
                    e.printStackTrace();
                }
                operativeAccountRequest.sendRequest(this, operativeAccountReqModel);
            }else{
                fillAdapterData();
            }
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }

        mFromDateTextView.setOnClickListener(this);
        mToDateTextView.setOnClickListener(this);
        mGetTransactions.setOnClickListener(this);
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mSpinnerPosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    @Override
    protected void bindViews() {
        mSpinner = (Spinner) findViewById(R.id.accountList_detailedStatement);
        mTransactionTable = findViewById(R.id.transactionLayout_detailedStatement);
        mExportButton = findViewById(R.id.exportPdf_detailedStatement);
        mTransactionList = (ListView) findViewById(R.id.transactionList_detailedStatement);
        mFromDateTextView = (TextView) findViewById(R.id.fromDate_detailedStatement);
        mToDateTextView = (TextView) findViewById(R.id.toDate_detailedStatement);
        mGetTransactions = findViewById(R.id.getTransactions_detailedStatement);
        mResultMessage = (TextView) findViewById(R.id.message_detailedStatement);
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {
        if(baseParser instanceof OperativeAccountParser)
        {
            OperativeAccountResModel superModel = (OperativeAccountResModel) baseParser.doParsing(objJson);
            OperativeAccountResModel operativeAccountResModel = (OperativeAccountResModel) superModel.getModelArray().get(0);

            new CacheAccountsTask(this, operativeAccountResModel).execute();
        }
        else if(baseParser instanceof DetailedStatementParser)
        {
            DetailedStatementResModel superModel = (DetailedStatementResModel) baseParser.doParsing(objJson);

            DetailedStatementResModel detailedStatementResModel = (DetailedStatementResModel) superModel.getModelArray().get(0);

            mSpinnerPosition = mSpinner.getSelectedItemPosition();

            if(detailedStatementResModel.getStatus() == DetailedStatementRequest.NO_TRANSACTIONS){
                mTransactionTable.setVisibility(View.INVISIBLE);
                mExportButton.setVisibility(View.INVISIBLE);
                mResultMessage.setText(ErrorMessage.getNoTransactionsInPeriodError());
            }else{
                mTransactionTable.setVisibility(View.VISIBLE);
                mExportButton.setVisibility(View.VISIBLE);
                mDetailedStatementAdapter = new DetailedStatementAdapter(this, detailedStatementResModel);
                mTransactionList.setAdapter(mDetailedStatementAdapter);
            }

            mFromDate = null;
            mToDate = null;
        }
        else if(baseParser instanceof UpdateNarrationParser)
        {
            UpdateNarrationResModel superModel = (UpdateNarrationResModel) baseParser.doParsing(objJson);

            UpdateNarrationResModel updateNarrationResModel = (UpdateNarrationResModel) superModel.getModelArray().get(0);

            if (updateNarrationResModel.getStatus() == UpdateNarrationRequest.SUCCESSFUL)
            {
                displayErrorMessage(mTransactionList, "Narration updated successfully.");
            }
            else
            {
                displayErrorMessage(mTransactionList, "Unable to update narration, please try again.");
            }
        }
    }
    @Override
    public void networkConnectionError(BaseModel reqModel) {
        displayErrorMessage(mFromDateTextView, ErrorMessage.getNetworkConnectionError());
    }

    public void requestUpdateNarration(String transactionCode,String narration)
    {
        try
        {
            UpdateNarrationReqModel updateNarrationReqModel = new UpdateNarrationReqModel(MySharedPreferences.getUserId(this),
                    MySharedPreferences.getActivityCode(this),
                    transactionCode,
                    narration);

            UpdateNarrationRequest updateNarrationRequest= new UpdateNarrationRequest(this , MyEnum.displayProgress.Show);
            updateNarrationRequest.sendRequest(this, updateNarrationReqModel);
        }
        catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException e) {
            e.printStackTrace();
        }

    }
    private void fillAdapterData() {
        try {
            mAccounts = (ArrayList<String>) MySharedPreferences.getAccounts(this);
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        if (mAccounts == null) return;

        mAccounts.add(0, "Select Account Number");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mAccounts);
        mSpinner.setAdapter(arrayAdapter);

        if(getIntent().hasExtra(ACCOUNT_NO)){
            mSpinner.setSelection(mAccounts.indexOf(getIntent().getStringExtra(ACCOUNT_NO)));
        }
    }

    public void exportToPdf(View view){
        if(mDetailedStatementAdapter == null) return;

        if(mDetailedStatementAdapter.getCount() == 0){
            displayErrorMessage(mSpinner, "No data to export");
            return;
        }

        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{ Manifest.permission.WRITE_EXTERNAL_STORAGE }, REQUEST_WRITE);
        }else{
            writeDataToPdf();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == REQUEST_WRITE){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                writeDataToPdf();
            }else{
                displayErrorMessage(mSpinner, "This app needs storage permission for storing PDF");
            }
        }
    }

    private void writeDataToPdf(){
        if(!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
            showLongToast(ErrorMessage.getMediaNotMountedError());
            return;
        }

        new WritePdfTask(mSpinner.getSelectedItemPosition()).execute();
    }

    public void goBack(View view){
        onBackPressed();
    }

    private ArrayList<String> getCreditArrayList(){
        ArrayList<String> credits = new ArrayList<>();
        int count = mDetailedStatementAdapter.mAmount.size();
        ArrayList<String> amount = mDetailedStatementAdapter.mAmount;
        ArrayList<Boolean> isCredit = mDetailedStatementAdapter.mIsCredit;
        for (int i = 0; i < count; i++) {
            credits.add(isCredit.get(i) ? amount.get(i) : "");
        }

        return credits;
    }

    private ArrayList<String> getDebitArrayList(){
        ArrayList<String> debits = new ArrayList<>();
        int count = mDetailedStatementAdapter.mAmount.size();
        ArrayList<String> amount = mDetailedStatementAdapter.mAmount;
        ArrayList<Boolean> isCredit = mDetailedStatementAdapter.mIsCredit;
        for (int i = 0; i < count; i++) {
            debits.add(!isCredit.get(i) ? amount.get(i) : "");
        }

        return debits;
    }

    @Override
    public void onClick(View v) {
        if(v == mFromDateTextView){
            final Calendar calendar = Calendar.getInstance();

            if(mFromDate != null){
                calendar.setTime(mFromDate);
            }

            mFromDateDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar calendar1 = new GregorianCalendar(year, monthOfYear, dayOfMonth);
                    mFromDate = calendar1.getTime();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
                    mFromDateString = simpleDateFormat.format(mFromDate);
                    mFromDateTextView.setText(mFromDateString);
                }
            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

            mToDate = null;
            mToDateString = "";
            mToDateTextView.setText(mToDateString);

            /*if(mToDate != null){
                calendar.setTime(mToDate);
                calendar.set(Calendar.HOUR_OF_DAY, 23);
                calendar.set(Calendar.MINUTE, 59);
                calendar.set(Calendar.SECOND, 59);
                mFromDateDialog.getDatePicker().setMaxDate(mToDate.getTime());
                mFromDateDialog.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                calendar.add(Calendar.DAY_OF_MONTH, -90);
                mFromDateDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
            }else{*/
                calendar.setTime(new Date());
                calendar.set(Calendar.HOUR_OF_DAY, 23);
                calendar.set(Calendar.MINUTE, 59);
                calendar.set(Calendar.SECOND, 59);
                mFromDateDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
                calendar.add(Calendar.YEAR, -2);
                mToDateTextView.setText("");
                Calendar calendar1 = new GregorianCalendar(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                mFromDateDialog.getDatePicker().setMinDate(calendar1.getTimeInMillis());
            //}

            /*final Calendar calendar = Calendar.getInstance();
            Calendar prevTime = Calendar.getInstance();*/

            /*mFromDateDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar calendar1 = new GregorianCalendar(year, monthOfYear, dayOfMonth);
                    if(calendar1.compareTo(calendar) == 1){
                        displayErrorMessage(mFromDateTextView, "Please select date before present date");
                        mFromDateDialog.dismiss();
                        return;
                    }

                    calendar.add(Calendar.YEAR, -2);
                    if(calendar1.compareTo(calendar) == -1){
                        displayErrorMessage(mFromDateTextView, "Only 2 years transactions are available");
                        mFromDateDialog.dismiss();
                        return;
                    }

                    mFromDate = calendar1.getTime();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
                    mFromDateString = simpleDateFormat.format(mFromDate);
                    mFromDateTextView.setText(mFromDateString);
                }
            }, prevTime.get(Calendar.YEAR), prevTime.get(Calendar.MONTH), prevTime.get(Calendar.DAY_OF_MONTH));*/

            mFromDateDialog.setCanceledOnTouchOutside(false);
            mFromDateDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mFromDateDialog.show();
        }else if(v == mToDateTextView){
            if(mFromDate == null){
                displayErrorMessage(mFromDateTextView, "First select 'From Date'.");
                return;
            }

            final Calendar calendar = Calendar.getInstance();

            if(mToDate != null){
                calendar.setTime(mToDate);
            }

            mToDateDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar calendar1 = new GregorianCalendar(year, monthOfYear, dayOfMonth);
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(mFromDate);
                    cal.add(Calendar.DAY_OF_MONTH, 90);
                    if(calendar1.compareTo(cal) == 1){
                        displayErrorMessage(mFromDateTextView, "Only up to 90 days transaction after from date can be requested");
                        return;
                    }
                    mToDate = calendar1.getTime();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
                    mToDateString = simpleDateFormat.format(mToDate);
                    mToDateTextView.setText(mToDateString);
                }
            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

            if(mFromDate != null){
                calendar.setTime(mFromDate);
                mToDateDialog.getDatePicker().setMinDate(mFromDate.getTime());
                Date currentDate = new Date();
                calendar.add(Calendar.DAY_OF_MONTH, 90);
                mToDateDialog.getDatePicker().setMaxDate(currentDate.getTime());
                /*if(currentDate.compareTo(calendar.getTime()) < 0){
                    mToDateDialog.getDatePicker().setMaxDate(currentDate.getTime());
                }else{
                    mToDateDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
                }*/

            }/*else{
                mToDateDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
                calendar.add(Calendar.YEAR, -2);
                mFromDateTextView.setText("");
                calendar.set(Calendar.HOUR_OF_DAY, calendar.getMaximum(Calendar.HOUR_OF_DAY));
                calendar.set(Calendar.MINUTE, calendar.getMaximum(Calendar.MINUTE));
                calendar.set(Calendar.MILLISECOND, calendar.getMaximum(Calendar.MILLISECOND));
                mToDateDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
            }*/

            /*if(mFromDate == null){
                displayErrorMessage(mFromDateTextView, "First select \"From Date\".");
                return;
            }

            final Calendar calendar = Calendar.getInstance();
            Calendar prevTime = Calendar.getInstance();
            prevTime.setTime(mFromDate);

            mToDateDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar calendar1 = new GregorianCalendar(year, monthOfYear, dayOfMonth);
                    calendar.setTime(mFromDate);

                    if(calendar1.compareTo(calendar) == -1){
                        displayErrorMessage(mFromDateTextView, "Please select a date after from date");
                        return;
                    }

                    calendar.add(Calendar.DAY_OF_MONTH, 90);

                    Calendar current = Calendar.getInstance();
                    if(calendar1.compareTo(current) == 1){
                        displayErrorMessage(mFromDateTextView, "Please select a date up to present date");
                        return;
                    }

                    if(calendar1.compareTo(calendar) == 1){
                        displayErrorMessage(mFromDateTextView, "Only up to 90 days transaction after from date can be requested");
                        return;
                    }

                    mToDate = calendar1.getTime();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
                    mToDateString = simpleDateFormat.format(mToDate);
                    mToDateTextView.setText(mToDateString);
                }
            }, prevTime.get(Calendar.YEAR), prevTime.get(Calendar.MONTH), prevTime.get(Calendar.DAY_OF_MONTH));*/

            mToDateDialog.setCanceledOnTouchOutside(false);
            mToDateDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mToDateDialog.show();
        }else if(v == mGetTransactions){
            if(mSpinner.getSelectedItemPosition() == 0){
               displayErrorMessage(mFromDateTextView, "Please select a valid account number.");
                return;
            }

            if(mFromDateString == null || mFromDateString.length() <= 0){
                displayErrorMessage(mFromDateTextView, "Please select a valid 'From Date'.");
                return;
            }

            if(mToDateString == null || mToDateString.length() <= 0){
                displayErrorMessage(mToDateTextView, "Please select a valid 'To Date'.");
                return;
            }

            DetailedStatementRequest detailedStatementRequest = new DetailedStatementRequest(this, MyEnum.displayProgress.Show);
            try {
                DetailedStatementReqModel detailedStatementReqModel = new DetailedStatementReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this), mAccounts.get(mSpinner.getSelectedItemPosition()), mFromDateString, mToDateString);
                detailedStatementRequest.sendRequest(this, detailedStatementReqModel);
            } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e) {
                e.printStackTrace();
            }
        }
    }

    private class CacheAccountsTask extends AsyncTask<Void, Void, Void> {
        private Activity mActivity;
        private OperativeAccountResModel mOperativeAccountResModel;

        private CacheAccountsTask(Activity activity, OperativeAccountResModel operativeAccountResModel) {
            mActivity = activity;
            mOperativeAccountResModel = operativeAccountResModel;
        }

        @Override
        protected void onPreExecute() {
            ShowProgressbar.showMaskProgress(mActivity, "Processing data");
        }

        @Override
        protected Void doInBackground(Void... params) {
            OperativeAccountResModel.Response[] responses = mOperativeAccountResModel.getResponses();

            ArrayList<String> accounts = new ArrayList<>();

            for (int i = 0; i < responses.length; i++) {
                accounts.add(responses[i].getAccountNo());
            }

            try {
                MySharedPreferences.putAccounts(DetailedStatementActivity.this, accounts);
                MySharedPreferences.putAccounts(DetailedStatementActivity.this, responses);
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (BadPaddingException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            ShowProgressbar.dismissDialog();
            fillAdapterData();
        }
    }

    private class WritePdfTask extends AsyncTask<Void, Void, Boolean>{
        private int mPosition;
        private File mFilePath;

        WritePdfTask(int position){
            mPosition = position;
        }

        @Override
        protected void onPreExecute() {
            ShowProgressbar.showMaskProgress(DetailedStatementActivity.this, "Loading...");
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Date date = new Date();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy-hh-mm-ss-a");
            String pdfName = "Detail" + "_" +simpleDateFormat.format(date) + ".pdf";

            File extDir = new File(Environment.getExternalStorageDirectory(), BANK_FOLDER+"/"+PDF_DIR);

            if(!extDir.exists()){
                extDir.mkdirs();
            }

            extDir = new File(extDir, pdfName);
            mFilePath = extDir;
            try {
                PdfHelper pdfHelper = new PdfHelper(extDir.getAbsolutePath(), DetailedStatementActivity.this, mAccounts.get(mPosition), mFromDateString, mToDateString);

                //Writing header with Image and date info
                Paragraph paragraph = new Paragraph();

                /*Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bank_logo);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                Image image = null;
                try {
                    image = Image.getInstance(byteArrayOutputStream.toByteArray());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                image.scaleAbsolute(100, 100);
                image.setAlignment(Element.ALIGN_CENTER);
                paragraph.add(image);*/
                paragraph.add("\n");
                paragraph.add("\n");
                paragraph.add("\n");
                paragraph.add("\n");

                Paragraph bankName = new Paragraph();
                bankName.setAlignment(Element.ALIGN_CENTER);
                bankName.setFont(pdfHelper.getBigFont());
                bankName.add(DetailedStatementActivity.this.getString(R.string.banking_name));
                paragraph.add(bankName);

                LineSeparator lineSeparator = new LineSeparator();
                lineSeparator.setLineColor(pdfHelper.getSeparatorColor());
                lineSeparator.setLineWidth(1f);
                paragraph.add(lineSeparator);

                paragraph.add("\n");
                paragraph.add("\n");


                /*Paragraph accountNo = new Paragraph();
                accountNo.setAlignment(Element.ALIGN_LEFT);
                try {
                    accountNo.add(MySharedPreferences.getCustomerName(MiniStatementActivity.this) + " - " + mAccounts.get(mSpinnerPosition));
                } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException e) {
                    e.printStackTrace();
                }
                paragraph.add(accountNo);*/

                PdfContentByte contentByte = pdfHelper.getContentByte();
                contentByte.saveState();
                try {
                    contentByte.setFontAndSize(BaseFont.createFont("Helvetica-Bold", "Cp1252", false), 15);
                    contentByte.showTextAligned(Element.ALIGN_LEFT, "Account Details", 50, 689, 0);
                } catch (IOException e) {
                    e.printStackTrace();
                }finally {
                    contentByte.restoreState();
                }

                paragraph.add(lineSeparator);
                paragraph.add("\n");

                PdfPTable pdfPTable = new PdfPTable(new float[]{1f, 2f});
                pdfPTable.setWidthPercentage(100);
                pdfPTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
                pdfPTable.setHorizontalAlignment(Element.ALIGN_LEFT);

                String[][] strings = null;
                try {
                    OperativeAccountResModel.Response account = MySharedPreferences.getAccount(DetailedStatementActivity.this, mSpinnerPosition - 1);
                    strings = new String[9][];
                    strings[0] = new String[]{
                            "Account Name",
                            ": " + MySharedPreferences.getCustomerName(DetailedStatementActivity.this)
                    };
                    strings[1] = new String[]{
                            "Mobile Number",
                            ": " + MySharedPreferences.getMobileNo(DetailedStatementActivity.this)
                    };
                    strings[2] = new String[]{
                            "Account Number",
                            ": " + account.getAccountNo()
                    };
                    strings[3] = new String[]{
                            "Account Type",
                            ": " + account.getAccountTypeName()
                    };

                    strings[4] = new String[]{
                            "Branch",
                            ": " + account.getBranchName()
                    };

                    strings[5] = new String[]{
                            "Opening Date",
                            ": " + account.getOpeningDate()
                    };

                    strings[6] = new String[]{
                            "Opening Balance",
                            ": " + account.getOpeningBalance()
                    };

                    strings[7] = new String[]{
                            "Balance",
                            ": " + account.getBalance()
                    };

                    Date currentDate = new Date(System.currentTimeMillis());
                    SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a");

                    strings[8] = new String[]{
                            "Date",
                            ": " + simpleDateFormat2.format(currentDate)
                    };
                } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException e) {
                    e.printStackTrace();
                }

                for (int i = 0; i < strings.length; i++) {
                    for (int j = 0; j < strings[i].length; j++) {
                        PdfHelper.insertCellToTable(pdfPTable, strings[i][j], Element.ALIGN_LEFT, 1, pdfHelper.getNormalFont12(), false, 35, 3);
                    }
                }

                Paragraph table = new Paragraph();
                table.setAlignment(Element.ALIGN_LEFT);
                table.add(pdfPTable);
                pdfPTable.setComplete(true);

                paragraph.add(table);

                paragraph.add("\n");
                paragraph.add("\n");

                /*Paragraph miniStatement = new Paragraph(new Phrase("Mini Statement"));
                miniStatement.setAlignment(Element.ALIGN_LEFT);
                paragraph.add(miniStatement);*/
                contentByte.saveState();
                try {
                    contentByte.setFontAndSize(BaseFont.createFont("Helvetica-Bold", "Cp1252", false), 13);
                    contentByte.showTextAligned(Element.ALIGN_LEFT, "Statement from "+mFromDateString+" to "+mToDateString, 50, 481, 0);
                } catch (IOException e) {
                    e.printStackTrace();
                }finally {
                    contentByte.restoreState();
                }

                int[] alignments = new int[] {Element.ALIGN_CENTER, Element.ALIGN_LEFT, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT};

                pdfHelper.addHeaderToPdf(paragraph);
                pdfHelper.createPdfTable(new float[]{1f, 2f, 0.8f, 0.8f}, 88);
                //pdfHelper.addTitlesToTables(3, null, Element.ALIGN_LEFT, "Date", "Narration", "Amount");;
                pdfHelper.addTitlesToTables(4, null, alignments, pdfHelper.getSeparatorColor(), "Date", "Narration", "Debit", "Credit");//₹
                ArrayList<String> credits = getCreditArrayList();
                ArrayList<String> debits = getDebitArrayList();
                pdfHelper.fillTableFromData(4, mDetailedStatementAdapter.getCount(), false, pdfHelper.getNormalFont12Bold(), pdfHelper.getSmallFont(), alignments, pdfHelper.getSeparatorColor(), mDetailedStatementAdapter.mDate, mDetailedStatementAdapter.mNarration, debits, credits);

                pdfHelper.addTableToPdf();
                pdfHelper.finishWritingPdf();
                return true;
            } catch (FileNotFoundException | DocumentException e){//| InvalidKeyException | BadPaddingException | NoSuchAlgorithmException | IllegalBlockSizeException | NoSuchPaddingException e) {
                e.printStackTrace();
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean isOk) {
            ShowProgressbar.dismissDialog();
            if(isOk) {
                showLongToast("Successfully written file to SDCard/" + BANK_FOLDER + "/" + PDF_DIR);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(mFilePath), "application/pdf");
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

                startActivity(intent);
            }
            else {
                showShortToast("There was an error saving file");
            }
        }
    }

}
