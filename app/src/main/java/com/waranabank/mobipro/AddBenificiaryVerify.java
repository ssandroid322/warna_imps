package com.waranabank.mobipro;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Model.AddBenificiaryVerifyReqModel;
import Model.AddBenificiaryVerifyResModel;
import Parser.BaseParser;
import Request.AddBenificiaryVerifyRequest;
import Utility.MyEnum;
import Utility.MySharedPreferences;

public class AddBenificiaryVerify extends BaseActivity implements View.OnClickListener {

    ImageView backButton;
    EditText et_otp;
    Button btn_submit;
    public static final String EXTRA_USER_ID = "USER_ID";
    public static final String EXTRA_ACTIVITY_CD = "ACTIVITY_CD";
    public static final String EXTRA_REQUEST_CD = "REQUEST_CD";
    public static final String EXTRA_TRN_TYPE = "TRN_TYPE";
    public static final String EXTRA_TO_IFSCCODE = "TO_IFSCCODE";
    public static final String EXTRA_TO_ACCT_NO = "TO_ACCT_NO";
    public static final String EXTRA_TO_ACCT_NM = "TO_ACCT_NM";
    public static final String EXTRA_TO_ADD1 = "TO_ADD1";
    public static final String EXTRA_TO_CONTACT_NO = "TO_CONTACT_NO";

    private String tran_type,to_ifscode,to_acc_no,to_acc_name,to_add1,to_contact_no,user_id;
    private String activity_cd,request_cd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_benificiary_verify);
        init();
        getIntentData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    private void init() {
        backButton = (ImageView) findViewById(R.id.benificiarybackbutton);
        et_otp = (EditText) findViewById(R.id.Addbeneficiary_otp);
        btn_submit = (Button) findViewById(R.id.Addbeneficiary_submit);
        backButton.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == backButton){
            onBackPressed();
        }
        else if (view == btn_submit){
            sendRequestToVerify();
        }
    }

    private void sendRequestToVerify() {
        String otp = et_otp.getText().toString();
        AddBenificiaryVerifyRequest addBenificiaryVerifyRequest = new AddBenificiaryVerifyRequest(this, MyEnum.displayProgress.Show);
        AddBenificiaryVerifyReqModel addBenificiaryVerifyReqModel = null;
        try {
            addBenificiaryVerifyReqModel = new AddBenificiaryVerifyReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this),tran_type,to_ifscode,to_acc_no,to_acc_name,to_add1,to_contact_no,otp,request_cd);
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        addBenificiaryVerifyRequest.sendRequest(this,addBenificiaryVerifyReqModel);
    }

    public void getIntentData(){
        tran_type = getIntent().getStringExtra(EXTRA_TRN_TYPE);
        to_acc_name = getIntent().getStringExtra(EXTRA_TO_ACCT_NM);
        to_acc_no = getIntent().getStringExtra(EXTRA_TO_ACCT_NO);
        to_ifscode = getIntent().getStringExtra(EXTRA_TO_IFSCCODE);
        to_add1 = getIntent().getStringExtra(EXTRA_TO_ADD1);
        to_contact_no = getIntent().getStringExtra(EXTRA_TO_CONTACT_NO);
        user_id = getIntent().getStringExtra(EXTRA_USER_ID);
        activity_cd = getIntent().getStringExtra(EXTRA_ACTIVITY_CD);
        request_cd = getIntent().getStringExtra(EXTRA_REQUEST_CD);
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {
        AddBenificiaryVerifyResModel superModel = (AddBenificiaryVerifyResModel) baseParser.doParsing(objJson);
        AddBenificiaryVerifyResModel addBenificiaryVerifyResModel = (AddBenificiaryVerifyResModel) superModel.getModelArray().get(0);

        if(addBenificiaryVerifyResModel.getmStatus() == AddBenificiaryVerifyRequest.ALREADY_ADDED){
            showShortToast("Beneficiary is already added.");
        }
        else if(addBenificiaryVerifyResModel.getmStatus() == AddBenificiaryVerifyRequest.INVALID_ACCT_NO){
            showShortToast("Please enter a valid account number.");
        }
        else if(addBenificiaryVerifyResModel.getmStatus() == AddBenificiaryVerifyRequest.SUCCESSFUL){
//            showLongToast("Beneficiary is added successfully.");
            showAlert("Beneficiary is added successfully.");

        }
    }


    public void showAlert(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        setResult(RESULT_OK);
                        finish();
                    }
                })
                .create().show();
    }
}
