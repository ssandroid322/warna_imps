package com.waranabank.mobipro;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Adapters.OperativeAccountsAdapter;
import ItemDecorators.OperativeAccountItemDecoration;
import Model.BaseModel;
import Model.OperativeAccountReqModel;
import Model.OperativeAccountResModel;
import Parser.BaseParser;
import Request.OperativeAccountRequest;
import Utility.ErrorMessage;
import Utility.MyEnum;
import Utility.MySharedPreferences;
import Utility.ShowProgressbar;

public class OperativeAccountActivity extends BaseActivity {
    private RecyclerView mAccountList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operative_account);

        bindViews();

        OperativeAccountRequest operativeAccountRequest = new OperativeAccountRequest(this, MyEnum.displayProgress.Show);
        OperativeAccountReqModel operativeAccountReqModel = null;
        try {
            operativeAccountReqModel = new OperativeAccountReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this));
        } catch (Exception e) {
            e.printStackTrace();
        }
        operativeAccountRequest.sendRequest(this, operativeAccountReqModel);
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    @Override
    protected void bindViews() {
        mAccountList = (RecyclerView) findViewById(R.id.accountList_operativeAccount);
    }

    public void goBack(View view){
        onBackPressed();
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {
        OperativeAccountResModel superModel = (OperativeAccountResModel) baseParser.doParsing(objJson);

        OperativeAccountResModel operativeAccountResModel = (OperativeAccountResModel) superModel.getModelArray().get(0);

        if(operativeAccountResModel.getStatus() == OperativeAccountRequest.SUCCESS) {
            if(operativeAccountResModel.getResponses().length <= 0) {
                ((TextView) findViewById(R.id.errorText_operativeAccount)).setText("No Operative Account found.");
                return;
            }

            new SortAccountTask(this, operativeAccountResModel).execute();
            findViewById(R.id.errorText_operativeAccount).setVisibility(View.GONE);
            mAccountList.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void networkConnectionError(BaseModel reqModel) {
        displayErrorMessage(mAccountList, ErrorMessage.getNetworkConnectionError());
        mAccountList.setVisibility(View.GONE);
//        findViewById(R.id.errorText_operativeAccount).setVisibility(View.VISIBLE);
    }

    private class SortAccountTask extends AsyncTask<Void, Void, Void>{
        private Activity mActivity;
        private OperativeAccountResModel mOperativeAccountResModel;
        private ArrayList<OperativeAccountResModel.Response> mResponses;
        private ArrayList<Integer> mSections;

        private SortAccountTask(Activity activity, OperativeAccountResModel operativeAccountResModel) {
            mActivity = activity;
            mOperativeAccountResModel = operativeAccountResModel;
            mResponses = new ArrayList<>();
        }

        @Override
        protected void onPreExecute() {
            ShowProgressbar.showMaskProgress(mActivity, "Processing data");
        }

        @Override
        protected Void doInBackground(Void... params) {
            ArrayList<String> accountTypes = new ArrayList<>();

            OperativeAccountResModel.Response[] responses = mOperativeAccountResModel.getResponses();

            for (int i = 0; i < responses.length; i++) {
                OperativeAccountResModel.Response response = responses[i];
                if(!accountTypes.contains(response.getAccountTypeName())){
                    accountTypes.add(response.getAccountTypeName());
                }
            }

            mSections = new ArrayList<>(accountTypes.size());

            for (int j = 0; j < accountTypes.size(); j++) {
                boolean isSectionAdded = false;
                for (int k = 0; k < responses.length; k++) {
                    if(responses[k].getAccountTypeName().equals(accountTypes.get(j))){
                        if(!isSectionAdded){
                            mSections.add(k);
                            isSectionAdded = true;
                        }
                        mResponses.add(responses[k]);
                    }
                }
            }

            ArrayList<String> accounts = new ArrayList<>();

            for (int i = 0; i < responses.length; i++) {
                accounts.add(responses[i].getAccountNo());
            }

            try {
                MySharedPreferences.putAccounts(OperativeAccountActivity.this, accounts);
                MySharedPreferences.putAccounts(OperativeAccountActivity.this, responses);
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (BadPaddingException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            ShowProgressbar.dismissDialog();
            mAccountList.setLayoutManager(new LinearLayoutManager(OperativeAccountActivity.this));
            mAccountList.setHasFixedSize(true);
            mAccountList.setAdapter(new OperativeAccountsAdapter(OperativeAccountActivity.this, mResponses, mSections));
            mAccountList.addItemDecoration(new OperativeAccountItemDecoration(20));
            mAccountList.setItemAnimator(new DefaultItemAnimator());
        }
    }
}
