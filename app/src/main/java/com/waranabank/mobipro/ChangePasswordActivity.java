package com.waranabank.mobipro;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Model.BaseModel;
import Model.ChangePasswordReqModel;
import Model.ChangePasswordResModel;
import Parser.BaseParser;
import Request.ChangePasswordRequest;
import Utility.ErrorMessage;
import Utility.MyEnum;
import Utility.MySharedPreferences;

public class ChangePasswordActivity extends com.waranabank.mobipro.BaseActivity {
    private EditText mCurrentPassword, mNewPassword, mAgainNewPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        bindViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    @Override
    protected void bindViews() {
        mCurrentPassword = (EditText) findViewById(R.id.originalPassword_changePassword);
        mNewPassword = (EditText) findViewById(R.id.newPassword_changePassword);
        mAgainNewPassword = (EditText) findViewById(R.id.confirmPassword_changePassword);
    }

    public void goBack(View view){
        super.onBackPressed();
    }

    public void changePassword(View view){
        hideKeybord(mAgainNewPassword);

        if(mCurrentPassword.getText().toString().trim().length() <= 0){
            mCurrentPassword.setError("Please enter a valid original password.");
            mCurrentPassword.requestFocus();
            return;
        }

        if(mNewPassword.getText().toString().trim().length() <= 0){
            mNewPassword.setError("Please enter a valid new password.");
            mNewPassword.requestFocus();
            return;
        }

        if(mAgainNewPassword.getText().toString().trim().length() <= 0){
            mAgainNewPassword.setError("Please enter confirm password same as new password.");
            mAgainNewPassword.requestFocus();
            return;
        }

        if(getTextFromView(mNewPassword).length() <= 0){
            mNewPassword.setError("The password must be at least 8 characters long.");
            mNewPassword.requestFocus();
            return;
        }

        if(!mNewPassword.getText().toString().equals(mAgainNewPassword.getText().toString())){
            displayErrorMessage(mNewPassword, "Please enter confirm password same as new password.");
            return;
        }

        try {
            ChangePasswordReqModel changePasswordReqModel = new ChangePasswordReqModel(MySharedPreferences.getUserId(this), mCurrentPassword.getText().toString(), mNewPassword.getText().toString());
            ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest(this, MyEnum.displayProgress.Show);
            changePasswordRequest.sendRequest(this, changePasswordReqModel);
        } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {
        ChangePasswordResModel superModel = (ChangePasswordResModel) baseParser.doParsing(objJson);

        ChangePasswordResModel changePasswordResModel = (ChangePasswordResModel) superModel.getModelArray().get(0);

        if(changePasswordResModel.getStatus() == ChangePasswordRequest.SUCCESSFUL){
            showLongToast("Your password has been changed successfully.");
            finish();
        }else if(changePasswordResModel.getStatus() == ChangePasswordRequest.PASSWORD_INVALID){
            displayErrorMessage(mNewPassword, "Invalid current password.");
        }
    }

    @Override
    public void networkConnectionError(BaseModel reqModel) {
        displayErrorMessage(mNewPassword, ErrorMessage.getNetworkConnectionError());
    }
}
