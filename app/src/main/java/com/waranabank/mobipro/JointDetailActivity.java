package com.waranabank.mobipro;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Model.BaseModel;
import Model.JointAccountDetailsReqModel;
import Model.JointAccountDetailsResModel;
import Model.OperativeAccountReqModel;
import Model.OperativeAccountResModel;
import Parser.BaseParser;
import Parser.JointAccountDetailsParser;
import Parser.OperativeAccountParser;
import Request.JointAccountDetailRequest;
import Request.OperativeAccountRequest;
import Utility.ErrorMessage;
import Utility.MyEnum;
import Utility.MySharedPreferences;
import Utility.ShowProgressbar;

public class JointDetailActivity extends BaseActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {
    private Spinner mAccountList;
    private View mPersonsLayout, mDetailLayout;
    private TextView mOpeningDate, mOpeningBal, mUnClearBal, mLienBalance, mAvailableBalance, mMiniStatement, mDetailedStatement, mErrorMessage;
    private ArrayList<String> mAccounts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joint_detail);

        bindViews();

        try {
            if(MySharedPreferences.getAccounts(this) == null || MySharedPreferences.getAccounts(this).size() <= 0){
                OperativeAccountRequest operativeAccountRequest = new OperativeAccountRequest(this, MyEnum.displayProgress.Show);
                OperativeAccountReqModel operativeAccountReqModel = null;
                try {
                    operativeAccountReqModel = new OperativeAccountReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this));
                } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e) {
                    e.printStackTrace();
                }
                operativeAccountRequest.sendRequest(this, operativeAccountReqModel);
            }else{
                fillAdapterData();
            }
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    public void goBack(View v){
        super.onBackPressed();
    }

    private void fillAdapterData() {
        try {
            mAccounts = (ArrayList<String>) MySharedPreferences.getAccounts(this);
        } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException e) {
            e.printStackTrace();
        }
        if (mAccounts == null) return;

        mAccounts.add(0, "Select Account Number");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mAccounts);
        mAccountList.setAdapter(arrayAdapter);
        mAccountList.setOnItemSelectedListener(this);

        mMiniStatement.setOnClickListener(this);
        mDetailedStatement.setOnClickListener(this);
    }

    @Override
    protected void bindViews() {
        mAccountList = (Spinner) findViewById(R.id.accountList_jointDetail);
        mPersonsLayout = findViewById(R.id.personsLayout_jointDetail);
        mOpeningDate = (TextView) findViewById(R.id.openingDate_jointDetail);
        mOpeningBal = (TextView) findViewById(R.id.openingBal_jointDetail);
        mUnClearBal = (TextView) findViewById(R.id.unclearBal_jointDetail);
        mLienBalance = (TextView) findViewById(R.id.lienBal_jointDetail);
        mAvailableBalance = (TextView) findViewById(R.id.availableBal_jointDetail);
        mMiniStatement = (TextView) findViewById(R.id.miniStatement_jointDetail);
        mDetailedStatement = (TextView) findViewById(R.id.detailedStatement_jointDetail);
        mDetailLayout = findViewById(R.id.detailLayout_jointDetail);
        mErrorMessage = (TextView) findViewById(R.id.errorText_jointDetail);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(position == 0) {
            mErrorMessage.setText("Account not selected");
            return;
        }

        getJointAccountDetail(mAccounts.get(position));
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {
        if(baseParser instanceof OperativeAccountParser) {
            OperativeAccountResModel superModel = (OperativeAccountResModel) baseParser.doParsing(objJson);
            OperativeAccountResModel operativeAccountResModel = (OperativeAccountResModel) superModel.getModelArray().get(0);

            if(operativeAccountResModel.getStatus() != OperativeAccountRequest.SUCCESS) {
                displayErrorMessage(mAccountList, ErrorMessage.getGenericError());
                return;
            }

            new CacheAccountsTask(this, operativeAccountResModel).execute();
        }else if(baseParser instanceof JointAccountDetailsParser){
            JointAccountDetailsResModel superModel = (JointAccountDetailsResModel) baseParser.doParsing(objJson);

            JointAccountDetailsResModel jointAccountDetailsResModel = (JointAccountDetailsResModel) superModel.getModelArray().get(0);

            if(jointAccountDetailsResModel.getStatus() == JointAccountDetailRequest.NO_JOINT_ACCOUNT){
                mDetailLayout.setVisibility(View.INVISIBLE);
                mErrorMessage.setVisibility(View.VISIBLE);
                mErrorMessage.setText("Joint account's detail not found.");
            }else if(jointAccountDetailsResModel.getStatus() == JointAccountDetailRequest.SUCCESS){
                mDetailLayout.setVisibility(View.VISIBLE);
                fillDataFromModel(jointAccountDetailsResModel);
            }
        }
    }

    private void fillDataFromModel(JointAccountDetailsResModel jointAccountDetailsResModel) {
        JointAccountDetailsResModel.Response[] responses = jointAccountDetailsResModel.getResponses();

        /*JointAccountDetailsResModel.Response[] r = new JointAccountDetailsResModel.Response[responses.length + 3];

        r[0] = responses[0];
        JointAccountDetailsResModel.Response res = new JointAccountDetailsResModel.Response("Jointer", "Samvid Mistry", "12345678", "12345678");
        r[1] = res;
        r[2] = res;
        r[3] = res;
        responses = r;*/

        ((ViewGroup) mPersonsLayout).removeAllViews();

        for (int i = 0; i < responses.length; i++) {
            JointAccountDetailsResModel.Response response = responses[i];
            View view = LayoutInflater.from(this).inflate(R.layout.view_person_detail, (ViewGroup) mPersonsLayout, false);
            if(i % 2 == 1){
                view.setBackgroundColor(Color.parseColor("#F5F5F5"));
            }
            TextView position = (TextView) view.findViewById(R.id.serialNo_personDetail);
            TextView personName = (TextView) view.findViewById(R.id.personName_personDetail);
            TextView jointType = (TextView) view.findViewById(R.id.jointType_personDetail);
            position.setText(String.valueOf(i+1));
            personName.setText(response.getPersonName());
            jointType.setText(response.getJointType());

            ((ViewGroup) mPersonsLayout).addView(view);
        }

        try {
            OperativeAccountResModel.Response account = MySharedPreferences.getAccount(this, mAccountList.getSelectedItemPosition() - 1);
            mOpeningDate.setText(account.getOpeningDate());
            mOpeningBal.setText("₹ "+account.getOpeningBalance());
            mUnClearBal.setText("₹ "+account.getUnclearBalance());
            mLienBalance.setText("₹ "+account.getLienAmount());
            mAvailableBalance.setText("₹ "+account.getBalance());
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        if(v == mMiniStatement){
            Intent intent = new Intent(this,MiniStatementActivity.class);
            intent.putExtra(MiniStatementActivity.ACCOUNT_NO, mAccounts.get(mAccountList.getSelectedItemPosition()));
            startActivity(intent);
        }else if(v == mDetailedStatement){
            Intent intent = new Intent(this, DetailedStatementActivity.class);
            intent.putExtra(DetailedStatementActivity.ACCOUNT_NO, mAccounts.get(mAccountList.getSelectedItemPosition()));
            startActivity(intent);
        }
    }

    private class CacheAccountsTask extends AsyncTask<Void, Void, Void> {
        private Activity mActivity;
        private OperativeAccountResModel mOperativeAccountResModel;

        private CacheAccountsTask(Activity activity, OperativeAccountResModel operativeAccountResModel) {
            mActivity = activity;
            mOperativeAccountResModel = operativeAccountResModel;
        }

        @Override
        protected void onPreExecute() {
            ShowProgressbar.showMaskProgress(mActivity, "Processing data");
        }

        @Override
        protected Void doInBackground(Void... params) {
            OperativeAccountResModel.Response[] responses = mOperativeAccountResModel.getResponses();

            ArrayList<String> accounts = new ArrayList<>();

            for (int i = 0; i < responses.length; i++) {
                accounts.add(responses[i].getAccountNo());
            }

            try {
                MySharedPreferences.putAccounts(JointDetailActivity.this, accounts);
                MySharedPreferences.putAccounts(JointDetailActivity.this, responses);
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (BadPaddingException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            ShowProgressbar.dismissDialog();
            fillAdapterData();
        }
    }

    @Override
    public void networkConnectionError(BaseModel reqModel) {
        displayErrorMessage(mAccountList, ErrorMessage.getNetworkConnectionError());
    }

    private void getJointAccountDetail(String s) {
        try {
            JointAccountDetailsReqModel jointAccountDetailsReqModel = new JointAccountDetailsReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this), s);
            JointAccountDetailRequest jointAccountDetailRequest = new JointAccountDetailRequest(this, MyEnum.displayProgress.Show);
            jointAccountDetailRequest.sendRequest(this, jointAccountDetailsReqModel);
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
