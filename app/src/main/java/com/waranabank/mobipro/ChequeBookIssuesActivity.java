package com.waranabank.mobipro;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Adapters.ChequeBookIssuesAdapter;
import ItemDecorators.OperativeAccountItemDecoration;
import Model.BaseModel;
import Model.ChequeBookDetailReqModel;
import Model.ChequeBookDetailsResModel;
import Model.OperativeAccountReqModel;
import Model.OperativeAccountResModel;
import Parser.BaseParser;
import Parser.ChequeBookDetailParser;
import Parser.OperativeAccountParser;
import Request.ChequeBookDetailRequest;
import Request.OperativeAccountRequest;
import Utility.ErrorMessage;
import Utility.MyEnum;
import Utility.MySharedPreferences;
import Utility.ShowProgressbar;

public class ChequeBookIssuesActivity extends BaseActivity implements AdapterView.OnItemSelectedListener {
    private Spinner mSpinner;
    private TextView mMessage;
    private ChequeBookIssuesAdapter mChequeBookIssuesAdapter;
    private ArrayList<String> mAccounts;
    private TextView mSeeChequebooks;
    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheque_book_issues);

        bindViews();

        try
        {
            if(MySharedPreferences.getAccounts(this) == null || MySharedPreferences.getAccounts(this).size() <= 0)
            {
                OperativeAccountRequest operativeAccountRequest = new OperativeAccountRequest(this, MyEnum.displayProgress.Show);
                OperativeAccountReqModel operativeAccountReqModel = new OperativeAccountReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this));
                operativeAccountRequest.sendRequest(this, operativeAccountReqModel);
            }
            else
            {
                fillAdapterData();
            }
        } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    private void fillAdapterData()
    {
        try
        {
            mAccounts = (ArrayList<String>) MySharedPreferences.getChequeBookAccounts(this);

            if(mAccounts == null || mAccounts.size() <= 0) return;

            mAccounts.add(0, "Select Account Number");
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mAccounts);
            mSpinner.setAdapter(adapter);
            mSpinner.setOnItemSelectedListener(this);
        }
        catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void bindViews()
    {
        mSpinner = (Spinner) findViewById(R.id.accountList_chequeBookIssues);
        mMessage = (TextView) findViewById(R.id.errorText_chequeBookIssues);
        mSeeChequebooks = (TextView) findViewById(R.id.seeChequebooks_chequeBookIssues);
        mRecyclerView = (RecyclerView) findViewById(R.id.chequeList_chequeBookIssues);
    }

    public void goBack(View view){
        super.onBackPressed();
    }

    public void requestChequebook(View view){
        startActivity(new Intent(this, ChequeBookRequestActivity.class));
    }

    public void seePendingChequebooks(View view)
    {
        if(mSpinner.getSelectedItemPosition() == 0){
            displayErrorMessage(mSpinner, "Please Select Account Number");
            return;
        }

        Intent intent = new Intent(this, PendingChequebookActivity.class);
        intent.putExtra(PendingChequebookActivity.ACCOUNT_NO, mAccounts.get(mSpinner.getSelectedItemPosition()));
        startActivity(intent);
    }

    private void getCheques(String s) {
        try {
            ChequeBookDetailRequest chequeBookDetailRequest = new ChequeBookDetailRequest(this, MyEnum.displayProgress.Show);
            ChequeBookDetailReqModel chequeBookDetailReqModel = new ChequeBookDetailReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this), mAccounts.get(mSpinner.getSelectedItemPosition()));
            chequeBookDetailRequest.sendRequest(this, chequeBookDetailReqModel);
        } catch (IllegalBlockSizeException | InvalidKeyException | BadPaddingException | NoSuchAlgorithmException | NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser)
    {
        if(baseParser instanceof OperativeAccountParser)
        {
            OperativeAccountResModel superModel = (OperativeAccountResModel) baseParser.doParsing(objJson);

            OperativeAccountResModel operativeAccountResModel = (OperativeAccountResModel) superModel.getModelArray().get(0);

            if(operativeAccountResModel.getStatus() != OperativeAccountRequest.SUCCESS){
                displayErrorMessage(mSpinner, ErrorMessage.getGenericError());
                return;
            }

            new CacheAccountsTask(this, operativeAccountResModel).execute();
        }else if(baseParser instanceof ChequeBookDetailParser){
            ChequeBookDetailsResModel superModel = (ChequeBookDetailsResModel) baseParser.doParsing(objJson);

            ChequeBookDetailsResModel chequeBookDetailsResModel = (ChequeBookDetailsResModel) superModel.getModelArray().get(0);

            /*mMessage.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            mChequeBookIssuesAdapter = new ChequeBookIssuesAdapter(this, chequeBookDetailsResModel);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            mRecyclerView.addItemDecoration(new OperativeAccountItemDecoration(10));
            mRecyclerView.setAdapter(mChequeBookIssuesAdapter);*/

            if(chequeBookDetailsResModel.getStatus() == ChequeBookDetailRequest.CHEQUE_NOT_FOUND){
                mRecyclerView.setVisibility(View.GONE);
                mMessage.setVisibility(View.VISIBLE);
                mMessage.setText(ErrorMessage.getNoChequeBooksError());
            }else if(chequeBookDetailsResModel.getStatus() == ChequeBookDetailRequest.SUCCESS){
                mMessage.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);
                mChequeBookIssuesAdapter = new ChequeBookIssuesAdapter(this, chequeBookDetailsResModel);
                mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
                mRecyclerView.addItemDecoration(new OperativeAccountItemDecoration(10));
                mRecyclerView.setAdapter(mChequeBookIssuesAdapter);
            }
        }
    }

    @Override
    public void networkConnectionError(BaseModel reqModel) {
        if(reqModel instanceof OperativeAccountReqModel){
            mMessage.setText("Accounts not found.");
        }else if(reqModel instanceof ChequeBookDetailsResModel){
            mMessage.setText("Chequebooks not found.");
        }
        displayErrorMessage(mSpinner, ErrorMessage.getNetworkConnectionError());
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(position == 0) {
            mSeeChequebooks.setVisibility(View.INVISIBLE);
            mMessage.setText("Account not selected");
            return;
        }

        mSeeChequebooks.setVisibility(View.VISIBLE);
        getCheques(mAccounts.get(position));
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private class CacheAccountsTask extends AsyncTask<Void, Void, Void> {
        private Activity mActivity;
        private OperativeAccountResModel mOperativeAccountResModel;

        private CacheAccountsTask(Activity activity, OperativeAccountResModel operativeAccountResModel) {
            mActivity = activity;
            mOperativeAccountResModel = operativeAccountResModel;
        }

        @Override
        protected void onPreExecute() {
            ShowProgressbar.showMaskProgress(mActivity, "Processing data");
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            OperativeAccountResModel.Response[] responses = mOperativeAccountResModel.getResponses();

            ArrayList<String> accounts = new ArrayList<>();

            for (int i = 0; i < responses.length; i++)
            {
                accounts.add(responses[i].getAccountNo());
            }

            try
            {
                MySharedPreferences.putAccounts(mActivity, accounts);
                MySharedPreferences.putAccounts(mActivity, responses);
            }
            catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e)
            {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            ShowProgressbar.dismissDialog();
            fillAdapterData();
        }
    }
}
