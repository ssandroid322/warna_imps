package com.waranabank.mobipro;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class ContactUsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }
    public void goBack(View view){
        super.onBackPressed();
    }
}
