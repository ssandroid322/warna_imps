package com.waranabank.mobipro;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import static Adapters.BranchesAdapter.*;

public class BranchDetailActivity extends BaseActivity {
    private TextView mBranchName, mAddressOne, mAddressTwo, mAreaName, mPinCode, mCity, mState, mCountry, mContactOne, mContactTwo, mEmailId, mIfsCode,mMICR_CODE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_branch_detail);

        bindViews();

        syncDataWithIntent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    private void syncDataWithIntent() {
        Intent intent = getIntent();
        mBranchName.setText(intent.getStringExtra(BRANCH_NM));
        mAddressOne.setText(intent.getStringExtra(ADD1));
        mAddressTwo.setText(intent.getStringExtra(ADD2));
        mAreaName.setText(intent.getStringExtra(AREA_NM));
        mPinCode.setText(intent.getStringExtra(PIN_CODE));
        mCity.setText(intent.getStringExtra(CITY_NM));
        mState.setText(intent.getStringExtra(STATE_NM));
        mCountry.setText(intent.getStringExtra(COUNTRY_NM));
        mContactOne.setText(intent.getStringExtra(CONTACT1));
        mContactTwo.setText(intent.getStringExtra(CONTACT2));
        mEmailId.setText(intent.getStringExtra(EMAIL_ID));
        mIfsCode.setText(intent.getStringExtra(IFS_CODE));
        mMICR_CODE.setText(intent.getStringExtra(MICR_CODE));

    }

    public void goBack(View view){
        super.onBackPressed();
    }

    @Override
    protected void bindViews() {
        mBranchName = (TextView) findViewById(R.id.branchName_branchDetail);
        mAddressOne = (TextView) findViewById(R.id.addressOne_branchDetail);
        mAddressTwo = (TextView) findViewById(R.id.addressTwo_branchDetail);
        mAreaName = (TextView) findViewById(R.id.areaName_branchDetail);
        mPinCode = (TextView) findViewById(R.id.pinCode_branchDetail);
        mCity = (TextView) findViewById(R.id.city_branchDetail);
        mState = (TextView) findViewById(R.id.state_branchDetail);
        mCountry = (TextView) findViewById(R.id.country_branchDetail);
        mContactOne = (TextView) findViewById(R.id.contactOne_branchDetail);
        mContactTwo = (TextView) findViewById(R.id.contactTwo_branchDetail);
        mEmailId = (TextView) findViewById(R.id.emailId_branchDetail);
        mIfsCode = (TextView) findViewById(R.id.ifscCode_branchDetail);
        mMICR_CODE = (TextView) findViewById(R.id.micrCode_branchDetail);
    }
}