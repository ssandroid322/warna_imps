package com.waranabank.mobipro;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viewpagerindicator.CirclePageIndicator;

import Interface.DrawerConnectionInterface;
import Utility.MySharedPreferences;

public class DashBoardFragment extends com.waranabank.mobipro.BaseFragment implements ViewTreeObserver.OnPreDrawListener, View.OnClickListener, ViewClickHandler {
    private RelativeLayout mHeaderLayout;
    private FrameLayout mRootLayout;
    private ImageView mBlueImageView, mDrawerImage;
    private TextView mCustomerName;
    private ViewPager mViewPager;
    private DrawerConnectionInterface mDrawerConnectionInterface;
    private CirclePageIndicator mTitlePageIndicator;
    //private View mOperativeAccount, mJointAccount, mMiniStatement, mDetailedStatement, mChequeBook, mChequeSearch, mStopCheque, mBeneficiaries, mOurBranches;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dash_board, container, false);

        bindViews(v);

        mHeaderLayout.getViewTreeObserver().addOnPreDrawListener(this);
        mDrawerConnectionInterface = (DrawerConnectionInterface) getActivity();

        try {
            mCustomerName.setText(MySharedPreferences.getCustomerName(getActivity()), null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*mOperativeAccount.setOnClickListener(this);
        mJointAccount.setOnClickListener(this);
        mMiniStatement.setOnClickListener(this);
        mDetailedStatement.setOnClickListener(this);
        mChequeBook.setOnClickListener(this);
        mChequeSearch.setOnClickListener(this);
        mStopCheque.setOnClickListener(this);
        mBeneficiaries.setOnClickListener(this);
        mOurBranches.setOnClickListener(this);*/
        mDrawerImage.setOnClickListener(this);
        mViewPager.setAdapter(new DashBoardAdapter(getChildFragmentManager(), this));
        mTitlePageIndicator.setViewPager(mViewPager);

        return v;
    }

    @Override
    protected void bindViews(View v) {
        mHeaderLayout = (RelativeLayout) v.findViewById(R.id.headerLayout_dashBoard);
        mRootLayout = (FrameLayout) v.findViewById(R.id.root_dashBoard);
        mBlueImageView = (ImageView) v.findViewById(R.id.blueImage_dashBoard);
        mCustomerName = (TextView) v.findViewById(R.id.customerName_dashBoard);
        mViewPager = (ViewPager) v.findViewById(R.id.viewPager);
        mTitlePageIndicator = (CirclePageIndicator) v.findViewById(R.id.titles);
        /*mOperativeAccount = v.findViewById(R.id.operativeAc_dashBoard);
        mJointAccount = v.findViewById(R.id.jointAc_dashBoard);
        mMiniStatement = v.findViewById(R.id.miniStatement_dashBoard);
        mDetailedStatement = v.findViewById(R.id.detailedStatement_dashBoard);
        mChequeBook = v.findViewById(R.id.chequeBook_dashBoard);
        mChequeSearch = v.findViewById(R.id.chequeSearch_dashBoard);
        mStopCheque = v.findViewById(R.id.stopCheque_dashBoard);
        mBeneficiaries = v.findViewById(R.id.changePassword_dashBoard);
        mOurBranches = v.findViewById(R.id.ourBranches_dashBoard);*/
        mDrawerImage = (ImageView) v.findViewById(R.id.hamburger_dashBoard);
    }

    @Override
    public boolean onPreDraw() {
        mHeaderLayout.getViewTreeObserver().removeOnPreDrawListener(this);

        final int imageSize = mRootLayout.getWidth() / 3;

        Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.blue_header);
        mBlueImageView.setImageBitmap(Bitmap.createScaledBitmap(b, mHeaderLayout.getWidth(), mHeaderLayout.getHeight(), true));

        final ImageView imageView = new ImageView(getActivity());
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(imageSize, imageSize, Gravity.END);
        layoutParams.rightMargin = 30;
        layoutParams.topMargin = mHeaderLayout.getBottom() - (imageSize / 4);
        imageView.setLayoutParams(layoutParams);
        imageView.setImageResource(R.drawable.header_img);

        mRootLayout.addView(imageView);

        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.operativeAc_dashBoard:
                startActivity(new Intent(getActivity(), com.waranabank.mobipro.OperativeAccountActivity.class));
                break;
            case R.id.detailedStatement_dashBoard:
                startActivity(new Intent(getActivity(), com.waranabank.mobipro.DetailedStatementActivity.class));
                break;
            case R.id.miniStatement_dashBoard:
                startActivity(new Intent(getActivity(), com.waranabank.mobipro.MiniStatementActivity.class));
                break;
            case R.id.hamburger_dashBoard:
                mDrawerConnectionInterface.onDrawerOpenRequested();
                break;
            case R.id.chequeBook_dashBoard:
                startActivity(new Intent(getActivity(), com.waranabank.mobipro.ChequeBookIssuesActivity.class));
                break;
            case R.id.chequeSearch_dashBoard:
                startActivity(new Intent(getActivity(), SearchChequeActivity.class));
                break;
            case R.id.stopCheque_dashBoard:
                startActivity(new Intent(getActivity(), StopChequeRequestActivity.class));
                break;
            case R.id.blockAtm_dashBoard:
                startActivity(new Intent(getActivity(), BlockATMActivity.class));
                break;
            case R.id.ourBranches_dashBoard:
                startActivity(new Intent(getActivity(), OurBranchesActivity.class));
                break;
            case R.id.jointAc_dashBoard:
                startActivity(new Intent(getActivity(), com.waranabank.mobipro.JointDetailActivity.class));
                break;
            case R.id.beneficiary_dashBoard:
                startActivity(new Intent(getActivity(), BeneficiariesActivity.class));
                break;
            case R.id.mmid_dashBoard:
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, new String[] {"Generate MMID", "Cancel MMID"});
                builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent intent = new Intent(getActivity(), MmidActivity.class);
                        intent.putExtra(MmidActivity.ACTION, which == 0 ? MmidActivity.GENERATE : MmidActivity.CANCEL);
                        startActivity(intent);
                    }
                });
                builder.create().show();
                break;
            case R.id.fundTransfer_dashBoard:
                startActivity(new Intent(getActivity(), FundTransferOptionsActivity.class));
                break;
        }
        /*if(v == mOperativeAccount){
        }else if(v == mMiniStatement){
        }else if(v == mDetailedStatement){
        }else if(v == mDrawerImage){
        }else if(v == mBeneficiaries){
//            startActivity(new Intent(getActivity(), com.waranabank.mobipro.BeneficiariesActivity.class));
        }else if(v == mChequeBook){
        }else if(v == mJointAccount){

        }else if(v == mChequeSearch){
        }else if(v == mStopCheque){
        }else if(v == mOurBranches){
//            startActivity(new Intent(getActivity(), FundTransferActivity.class));
        }*/
    }

    @Override
    public void onViewClick(View view) {
        onClick(view);
    }

    @Override
    public void setViewPager(ViewPager viewPager) {

    }
}
