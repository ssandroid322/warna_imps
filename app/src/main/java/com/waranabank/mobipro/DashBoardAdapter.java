package com.waranabank.mobipro;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by Samvid Mistry on 21/07/2016.
 */

public class DashBoardAdapter extends FragmentPagerAdapter {
    private ViewClickHandler mViewClickHandler;

    public DashBoardAdapter(FragmentManager fm, ViewClickHandler viewClickHandler) {
        super(fm);
        mViewClickHandler = viewClickHandler;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                DashBoardGridOneFragment dashBoardGridOneFragment = DashBoardGridOneFragment.newInstance();
                dashBoardGridOneFragment.setViewClickHandler(mViewClickHandler);
                return dashBoardGridOneFragment;
            case 1:
                DashBoardGridTwoFragment dashBoardGridTwoFragment = DashBoardGridTwoFragment.newInstance();
                dashBoardGridTwoFragment.setViewClickHandler(mViewClickHandler);
                return dashBoardGridTwoFragment;
            default:
                return new Fragment();
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
