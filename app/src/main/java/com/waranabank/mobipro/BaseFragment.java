package com.waranabank.mobipro;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import org.json.JSONObject;

import Interface.onReply;
import Model.BaseModel;
import Parser.BaseParser;
import Utility.MySharedPreferences;
import Utility.URLGenerator;

/**
 * Created by chitan on 10/31/2015.
 */
public class BaseFragment  extends Fragment implements onReply {
    public String no_intenet_msg =  "Please check your internet connection or try again later";

    protected void showComingSoon(View view,String message) {
        Snackbar.make(view, message + " - Coming Soon !", Snackbar.LENGTH_LONG).show();
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)getActivity()
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Network[] networks = connectivityManager.getAllNetworks();

            if(networks == null || networks.length <= 0) return false;

            for(Network network : networks){
                if(connectivityManager.getNetworkInfo(network).isConnected()){
                    return true;
                }
            }

            return false;
        }else{
            //noinspection deprecation
            NetworkInfo wifi = connectivityManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            //noinspection deprecation
            NetworkInfo mobile = connectivityManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            return (wifi.isConnected() || mobile.isConnected());
        }
    }

    protected void bindViews(View v){}

    protected void displayErrorMessage(View view,String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {

    }

    @Override
    public void networkConnectionError(BaseModel reqModel) {

    }

    @Override
    public void updateDetail() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this.getActivity());
        alertDialogBuilder.setTitle("");

        // set dialog message
        alertDialogBuilder.setMessage("A new version of Car Trader is available, please update it.")
                .setCancelable(false)
                .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, close
                        // current activity
                        stopAppUsage();
                    }
                });
        alertDialogBuilder.show();

    }

    @Override
    public void onSessionExpired() {

    }

    @Override
    public void onRejectedService(String msg) {

    }

    @Override
    public void onRejectedServiceFinishActivity(String msg) {

    }

    protected void showKeyboard(View view){
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInputFromWindow(view.getWindowToken(), InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void stopAppUsage() {
        MySharedPreferences.clearSharedPreference(getActivity());
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(URLGenerator.getAppUrl())));
        getActivity().finish();
    }
}
