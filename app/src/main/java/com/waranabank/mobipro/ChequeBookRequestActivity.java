package com.waranabank.mobipro;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Model.BaseModel;
import Model.ChequeBookReqModel;
import Model.ChequeBookResModel;
import Model.GetNoOfLeafsReqModel;
import Model.GetNoOfLeafsResModel;
import Model.GetScreenMsg_ReqModel;
import Model.GetScreenMsg_ResModel;
import Model.OperativeAccountReqModel;
import Model.OperativeAccountResModel;
import Parser.BaseParser;
import Parser.ChequeBookParser;
import Parser.GetNoOfLeafsParser;
import Parser.GetScreenMsg_Parser;
import Parser.OperativeAccountParser;
import Request.BaseRequest;
import Request.ChequeBookRequest;
import Request.GetNoOfLeafsRequest;
import Request.GetScreenMsg_Request;
import Request.OperativeAccountRequest;
import Utility.Consts;
import Utility.ErrorMessage;
import Utility.MyEnum;
import Utility.MySharedPreferences;
import Utility.ShowProgressbar;

public class ChequeBookRequestActivity extends com.waranabank.mobipro.BaseActivity {
    private Spinner mSpinner, mNoOfLeafsSpinner;
    private ArrayList<String> mAccounts ;
    private ArrayList<String> mLeafs ;

    private View mBottomLayout;
    private TextView mErrorMessage, mNote;
    private Button mReqChequeBook;
    private GetNoOfLeafsResModel list_leafModel;
    private static final int REQUEST_CODE = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheque_book_request);

        bindViews();
        mNote.setVisibility(View.GONE);
        try
        {
            if(MySharedPreferences.getAccounts(this) == null || MySharedPreferences.getAccounts(this).size() <= 0)
            {
                OperativeAccountRequest operativeAccountRequest = new OperativeAccountRequest(this, MyEnum.displayProgress.Show);
                OperativeAccountReqModel operativeAccountReqModel = new OperativeAccountReqModel(MySharedPreferences.getUserId(this),
                        MySharedPreferences.getActivityCode(this));
                operativeAccountRequest.sendRequest(this, operativeAccountReqModel);
            }
            else
            {
                fillAdapterData();
            }
        }
        catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e)
        {
            e.printStackTrace();
        }
        sendRequestForNote(Consts.NOTE_CHQREQ);
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    @Override
    protected void bindViews()
    {
        mSpinner = (Spinner) findViewById(R.id.accountList_chequeBookRequest);
        mNoOfLeafsSpinner = (Spinner) findViewById(R.id.noofLeafList_chequeBookRequest);

        mBottomLayout = findViewById(R.id.bottomLayout_chequeBookRequest);
        mErrorMessage = (TextView) findViewById(R.id.errorText_chequeBookRequest);
        mNote = (TextView) findViewById(R.id.note);
        mReqChequeBook = (Button) findViewById(R.id.reqChequeBook_ChequeBookRequest);
    }
    private void sendRequestForNote(String screen){
        GetScreenMsg_ReqModel getScreenMsg_reqModel = new GetScreenMsg_ReqModel(screen);
        GetScreenMsg_Request getScreenMsg_request = new GetScreenMsg_Request(ChequeBookRequestActivity.this);
        getScreenMsg_request.sendRequest(ChequeBookRequestActivity.this,getScreenMsg_reqModel);
    }

    private void fillAdapterData()
    {
        try
        {
//            mAccounts = (ArrayList<String>) MySharedPreferences.getAccounts(this);
            mAccounts = (ArrayList<String>) MySharedPreferences.getChequeBookAccounts(this);

            if(mAccounts == null || mAccounts.size() <= 0)
                return;
            mAccounts.add(0, "Select Account Number");
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mAccounts);
            mSpinner.setAdapter(adapter);

            mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
            {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
                {
                    if(position == 0)
                    {
                        mErrorMessage.setVisibility(View.VISIBLE);
                        mErrorMessage.setText("Account not selected");
                        mBottomLayout.setVisibility(View.GONE);
                        mReqChequeBook.setVisibility(View.INVISIBLE);
                        mNote.setVisibility(View.GONE);
                        return;
                    }

                    requestGetNoOfLeafs(mAccounts.get(position));

                    mErrorMessage.setVisibility(View.GONE);
                    mBottomLayout.setVisibility(View.VISIBLE);
                    mReqChequeBook.setVisibility(View.VISIBLE);
                    mNote.setVisibility(View.VISIBLE);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }
        catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e)
        {
            e.printStackTrace();
        }
    }

    private void fillLeafsAdapterData()
    {
        try
        {
            mLeafs= new ArrayList<>();
            GetNoOfLeafsResModel.Response[] responses = list_leafModel.getResponse();
            for(GetNoOfLeafsResModel.Response response : responses)
            {
                mLeafs.add(response.getmChequeLeaf());
            }

            if(mLeafs == null || mLeafs.size() <= 0)
                return;

            mLeafs.add(0, "Select no of leafs");
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mLeafs);

            mNoOfLeafsSpinner.setAdapter(adapter);
        }
        catch (Exception  e)
        {
            e.printStackTrace();
        }
    }

    private void requestGetNoOfLeafs(String accountno)
    {
        try
        {
           String accountType =  MySharedPreferences.getAccountTypeFromNumber(this, accountno);

            if(!validateString(accountType))
                return;

            GetNoOfLeafsReqModel getNoOfLeafsReqModel = new GetNoOfLeafsReqModel(MySharedPreferences.getUserId(this),
                    MySharedPreferences.getActivityCode(this),accountType);

            GetNoOfLeafsRequest getNoOfLeafsRequest= new GetNoOfLeafsRequest(this, MyEnum.displayProgress.Show);
            getNoOfLeafsRequest.sendRequest(this, getNoOfLeafsReqModel);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser)
    {
        if(baseParser instanceof OperativeAccountParser)
        {
            OperativeAccountResModel superModel = (OperativeAccountResModel) baseParser.doParsing(objJson);

            OperativeAccountResModel operativeAccountResModel = (OperativeAccountResModel) superModel.getModelArray().get(0);

            if(operativeAccountResModel.getStatus() != OperativeAccountRequest.SUCCESS)
            {
                displayErrorMessage(mSpinner, ErrorMessage.getGenericError());
                return;
            }

            new CacheAccountsTask(this, operativeAccountResModel).execute();
        }
        else if(baseParser instanceof ChequeBookParser)
        {
            ChequeBookResModel superModel = (ChequeBookResModel) baseParser.doParsing(objJson);

            ChequeBookResModel chequeBookResModel = (ChequeBookResModel) superModel.getModelArray().get(0);
            ChequeBookResModel.Response response = null;

            if(chequeBookResModel.getStatus() == ChequeBookRequest.NOT_FOUND)
            {
                displayErrorMessage(mSpinner, "Unable to process request for this account.");
            }
            else if(chequeBookResModel.getStatus() == ChequeBookRequest.SUCCESS)
            {
                if(chequeBookResModel.getResponses() != null) {
                    for (int i = 0; i < chequeBookResModel.getResponses().length; i++) {
                        response = chequeBookResModel.getResponses()[i];
                    }
                }
                if(response != null){
                    try {
                        new MySharedPreferences().putUserId(this,response.getUserid());
                        new MySharedPreferences().putActivityCode(this,response.getActivitycode());
                    } catch (IllegalBlockSizeException e) {
                        e.printStackTrace();
                    } catch (InvalidKeyException e) {
                        e.printStackTrace();
                    } catch (BadPaddingException e) {
                        e.printStackTrace();
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (NoSuchPaddingException e) {
                        e.printStackTrace();
                    }
                }
                if(response != null) {
                    Intent intent = new Intent(this, ChequeBookReqVerify.class);
                    intent.putExtra(ChequeBookReqVerify.EXTRA_REQUEST_CD, response.getRequestcode());
                    intent.putExtra(ChequeBookReqVerify.EXTRA_NO_OF_LEAF,mLeafs.get(mNoOfLeafsSpinner.getSelectedItemPosition()));
                    intent.putExtra(ChequeBookReqVerify.EXTRA_ACCT_NO,mAccounts.get(mSpinner.getSelectedItemPosition()));
                    startActivityForResult(intent,REQUEST_CODE);
                }
//                showLongToast("Cheque book request sent successfully.");
//                finish();
            }
        }
        else if(baseParser instanceof GetNoOfLeafsParser)
        {
            GetNoOfLeafsResModel superModel = (GetNoOfLeafsResModel) baseParser.doParsing(objJson);

            list_leafModel = (GetNoOfLeafsResModel) superModel.getModelArray().get(0);

            if(list_leafModel.getStatus() != GetNoOfLeafsRequest.SUCCESS)
            {
                displayErrorMessage(mSpinner, ErrorMessage.getGenericError());
                return;
            }

            fillLeafsAdapterData();
        }
        else if(baseParser instanceof GetScreenMsg_Parser){
            GetScreenMsg_ResModel superModel = (GetScreenMsg_ResModel) baseParser.doParsing(objJson);
            GetScreenMsg_ResModel getScreenMsg_resModel = (GetScreenMsg_ResModel) superModel.getModelArray().get(0);
            if((getScreenMsg_resModel.getStatus().equals(GetScreenMsg_Request.SUCCESS)) && (getScreenMsg_resModel.getMessage() != null)){
                mNote.setText(getScreenMsg_resModel.getMessage());
            }
        }
    }

    @Override
    public void networkConnectionError(BaseModel reqModel)
    {
        if(reqModel instanceof OperativeAccountReqModel)
        {
            mErrorMessage.setVisibility(View.VISIBLE);
            mBottomLayout.setVisibility(View.GONE);
            mReqChequeBook.setVisibility(View.INVISIBLE);
            mErrorMessage.setText("Accounts not found.");
            displayErrorMessage(mSpinner, ErrorMessage.getNetworkConnectionError());
        }
        else if (reqModel instanceof  GetNoOfLeafsReqModel)
        {
            mErrorMessage.setVisibility(View.VISIBLE);
            mBottomLayout.setVisibility(View.GONE);
            mReqChequeBook.setVisibility(View.INVISIBLE);
            mErrorMessage.setText(ErrorMessage.getInvalidNoOfLeafs());
            mNoOfLeafsSpinner.setAdapter(null);
            displayErrorMessage(mSpinner, ErrorMessage.getInvalidNoOfLeafs());
        }
        else
        {
            displayErrorMessage(mSpinner, ErrorMessage.getNetworkConnectionError());
        }
    }
    public void requestChequeBook(View view)
    {
        hideKeybord(mSpinner);

        if(mSpinner.getSelectedItemPosition() == 0)
        {
            displayErrorMessage(mSpinner, "Please Select Account Number");
            return;
        }
        if(mNoOfLeafsSpinner.getSelectedItemPosition() == 0)
        {
            displayErrorMessage(mSpinner, "Please select no of leafs.");
            return;
        }

        try {
            ChequeBookRequest chequeBookRequest = new ChequeBookRequest(this, MyEnum.displayProgress.Show);
            ChequeBookReqModel chequeBookReqModel = new ChequeBookReqModel(MySharedPreferences.getUserId(this),
                    MySharedPreferences.getActivityCode(this),
                    mAccounts.get(mSpinner.getSelectedItemPosition()),
                    mLeafs.get(mNoOfLeafsSpinner.getSelectedItemPosition()));
            chequeBookRequest.sendRequest(this, chequeBookReqModel);
        } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    public void goBack(View view){
        super.onBackPressed();
    }

    private class CacheAccountsTask extends AsyncTask<Void, Void, Void>
    {
        private Activity mActivity;
        private OperativeAccountResModel mOperativeAccountResModel;

        private CacheAccountsTask(Activity activity, OperativeAccountResModel operativeAccountResModel)
        {
            mActivity = activity;
            mOperativeAccountResModel = operativeAccountResModel;
        }

        @Override
        protected void onPreExecute()
        {
            ShowProgressbar.showMaskProgress(mActivity, "Processing data");
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            OperativeAccountResModel.Response[] responses = mOperativeAccountResModel.getResponses();

            ArrayList<String> accounts = new ArrayList<>();

            for (int i = 0; i < responses.length; i++)
            {
                accounts.add(responses[i].getAccountNo());
            }

            try
            {
                MySharedPreferences.putAccounts(mActivity, accounts);
                MySharedPreferences.putAccounts(mActivity, responses);
            }
            catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e)
            {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid)
        {
            ShowProgressbar.dismissDialog();
            fillAdapterData();
        }
    }
}