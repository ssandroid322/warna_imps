package com.waranabank.mobipro;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import org.json.JSONObject;

import Adapters.BranchesAdapter;
import Model.BaseModel;
import Model.BranchDetailsResModel;
import Parser.BaseParser;
import Request.BranchDetailRequest;
import Utility.ErrorMessage;
import Utility.MyEnum;

public class OurBranchesActivity extends BaseActivity {
    private ListView mBranchList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_our_branches);

        bindViews();

        BranchDetailRequest branchDetailRequest = new BranchDetailRequest(this, MyEnum.displayProgress.Show);
        branchDetailRequest.sendRequest(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    @Override
    protected void bindViews() {
        mBranchList = (ListView) findViewById(R.id.branchList_ourBranches);
    }

    public void goBack(View view){
        super.onBackPressed();
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {
        BranchDetailsResModel superModel = (BranchDetailsResModel) baseParser.doParsing(objJson);

        BranchDetailsResModel branchDetailsResModel = (BranchDetailsResModel) superModel.getModelArray().get(0);

        if(branchDetailsResModel.getStatus() == BranchDetailRequest.SUCCESS){
            BranchesAdapter branchesAdapter = new BranchesAdapter(this, branchDetailsResModel);
            mBranchList.setAdapter(branchesAdapter);
        }else{
            networkConnectionError(null);
        }
    }

    @Override
    public void networkConnectionError(BaseModel reqModel) {
        displayErrorMessage(mBranchList, ErrorMessage.getNetworkConnectionError());
    }
}
