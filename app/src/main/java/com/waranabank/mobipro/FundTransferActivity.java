package com.waranabank.mobipro;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Model.BaseModel;
import Model.GetBeneficiariesReqModel;
import Model.GetBeneficiariesResModel;
import Model.GetScreenMsg_ReqModel;
import Model.GetScreenMsg_ResModel;
import Model.OperativeAccountReqModel;
import Model.OperativeAccountResModel;
import Parser.BaseParser;
import Parser.GetBeneficiariesParser;
import Parser.GetScreenMsg_Parser;
import Parser.OperativeAccountParser;
import Request.GetBeneficiariesRequest;
import Request.GetScreenMsg_Request;
import Request.OperativeAccountRequest;
import Utility.Consts;
import Utility.ErrorMessage;
import Utility.MyEnum;
import Utility.MySharedPreferences;
import Utility.ShowProgressbar;

public class FundTransferActivity extends BaseActivity {
    public static final String BENEFICIARY_NO = "beneficiary_no";
    public static final String TRANSACTION_TYPE = "transaction_type";
    private static final int FUND_TRANSFER_VERIFY = 733;

    private Spinner mAccountSpinner, mBeneficiarySpinner;
    private EditText mTransactionType, mIfscCode, mToAccount, mAmount, mRemarks;
    private ArrayList<String> mAccounts, mBeneficiaries;
    private TextView mErrorMessage, mNote;
    private View mBottomLayout;
    private Button mTransfer;
    private TextView mTitle;
    private boolean mGotBeneficiaries = false;
    private boolean mIsImps = false;
    private GetBeneficiariesResModel mGetBeneficiariesResModel;
    private List<GetBeneficiariesResModel.Response> mResponseList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fund_transfer);

        bindViews();

        if (getIntent().hasExtra(TRANSACTION_TYPE)) {
            switch (getIntent().getStringExtra(TRANSACTION_TYPE).toUpperCase()) {
                case "IMPS":
                    mNote.setText(getString(R.string.person_to_account));
                    mTitle.setText("IMPS Transfer");
                    sendRequestForNote(Consts.NOTE_IMPSP2A);
                    mIsImps = true;
                    break;
                case "I":
                    mNote.setText(getString(R.string.ift_note));
                    mTitle.setText("Within Bank Transfer");
                    sendRequestForNote(Consts.NOTE_IFT);
                    break;
                case "R":
                    mNote.setText(getString(R.string.rtgs_note));
                    mTitle.setText("RTGS Transfer");
                    sendRequestForNote(Consts.NOTE_RTGS);
                    break;
                case "N":
                    mNote.setText(getString(R.string.neft_note));
                    mTitle.setText("NEFT Transfer");
                    sendRequestForNote(Consts.NOTE_NEFT);
                    break;
            }
        }

        mNote.setVisibility(View.GONE);

        try {
            if (MySharedPreferences.getAccounts(this) == null || MySharedPreferences.getAccounts(this).size() <= 0) {
                OperativeAccountRequest operativeAccountRequest = new OperativeAccountRequest(this, MyEnum.displayProgress.Show);
                OperativeAccountReqModel operativeAccountReqModel = null;
                try {
                    operativeAccountReqModel = new OperativeAccountReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this));
                } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e) {
                    e.printStackTrace();
                }
                operativeAccountRequest.sendRequest(this, operativeAccountReqModel);
            } else {
                getBeneficiaries();
            }
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    private void getBeneficiaries() {
        GetBeneficiariesReqModel getBeneficiariesReqModel = null;
        try {
            getBeneficiariesReqModel = new GetBeneficiariesReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this));
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        GetBeneficiariesRequest getBeneficiariesRequest = new GetBeneficiariesRequest(this, MyEnum.displayProgress.Show);
        getBeneficiariesRequest.sendRequest(this, getBeneficiariesReqModel);
    }

    @Override
    protected void bindViews() {
        mAccountSpinner = (Spinner) findViewById(R.id.accountList_fundTransfer);
        mBeneficiarySpinner = (Spinner) findViewById(R.id.beneficiariesList_fundTransfer);
        mTransactionType = (EditText) findViewById(R.id.transactionType_fundTransfer);
        mIfscCode = (EditText) findViewById(R.id.toIfscCode_fundTransfer);
        mToAccount = (EditText) findViewById(R.id.toAccountNo_fundTransfer);
        mAmount = (EditText) findViewById(R.id.amount_fundTransfer);
        mErrorMessage = (TextView) findViewById(R.id.errorText_fundTransfer);
        mBottomLayout = findViewById(R.id.bottomLayout_fundTransfer);
        mTransfer = (Button) findViewById(R.id.transfer_fundTransfer);
        mNote = (TextView) findViewById(R.id.note);
        mTitle = (TextView) findViewById(R.id.title_fundTransfer);
        mRemarks = (EditText) findViewById(R.id.remarks_fundTransfer);
    }

    @Override
    public void networkConnectionError(BaseModel reqModel) {
        mErrorMessage.setVisibility(View.VISIBLE);
        mBottomLayout.setVisibility(View.GONE);
        mTransfer.setVisibility(View.GONE);

        if (reqModel instanceof OperativeAccountReqModel) {
            mErrorMessage.setText("Accounts not found.");
        } else if (reqModel instanceof GetBeneficiariesReqModel) {
            mErrorMessage.setText("Beneficiaries not found.");
            mGotBeneficiaries = false;
        }
        displayErrorMessage(mErrorMessage, ErrorMessage.getNetworkConnectionError());
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {
        if (baseParser instanceof OperativeAccountParser) {
            OperativeAccountResModel superModel = (OperativeAccountResModel) baseParser.doParsing(objJson);
            OperativeAccountResModel operativeAccountResModel = (OperativeAccountResModel) superModel.getModelArray().get(0);

            if (operativeAccountResModel.getStatus() != OperativeAccountRequest.SUCCESS) {
                displayErrorMessage(mAccountSpinner, ErrorMessage.getGenericError());
                return;
            }

            new CacheAccountsTask(this, operativeAccountResModel).execute();
        } else if (baseParser instanceof GetBeneficiariesParser) {
            GetBeneficiariesResModel superModel = (GetBeneficiariesResModel) baseParser.doParsing(objJson);

            mGetBeneficiariesResModel = (GetBeneficiariesResModel) superModel.getModelArray().get(0);

            fillAdapterData();
        }
        else if(baseParser instanceof GetScreenMsg_Parser){
            GetScreenMsg_ResModel superModel = (GetScreenMsg_ResModel) baseParser.doParsing(objJson);
            GetScreenMsg_ResModel getScreenMsg_resModel = (GetScreenMsg_ResModel) superModel.getModelArray().get(0);
            if((getScreenMsg_resModel.getStatus().equals(GetScreenMsg_Request.SUCCESS)) && (getScreenMsg_resModel.getMessage() != null)){
                mNote.setText(getScreenMsg_resModel.getMessage());
            }
        }
    }

    private void fillAdapterData() {
        mAccounts = new ArrayList<>();
        try {
            switch (getIntent().getStringExtra(TRANSACTION_TYPE).toUpperCase()) {
                case "I":
                    mAccounts = (ArrayList<String>) MySharedPreferences.getIftAccounts(this);
                    break;
                default:
                    mAccounts = (ArrayList<String>) MySharedPreferences.getOtherAccounts(this);
                    break;
            }
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }

        if (mAccounts == null) return;

        mAccounts.add(0, "Select Account Number");
        ArrayAdapter<String> accountAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mAccounts);
        mAccountSpinner.setAdapter(accountAdapter);

        mAccountSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    mErrorMessage.setVisibility(View.VISIBLE);
                    mErrorMessage.setText("Account not selected");
                    mBottomLayout.setVisibility(View.GONE);
                    mTransfer.setVisibility(View.GONE);
                    findViewById(R.id.noteDesc_fundTransfer).setVisibility(View.GONE);
                    mNote.setVisibility(View.GONE);
                    return;
                }

                if (!mGotBeneficiaries) {
                    mErrorMessage.setVisibility(View.VISIBLE);
                    mBottomLayout.setVisibility(View.GONE);
                    mTransfer.setVisibility(View.GONE);
                    mErrorMessage.setText("Beneficiaries not found");
                    return;
                }

                mErrorMessage.setVisibility(View.GONE);
                mBottomLayout.setVisibility(View.VISIBLE);
                mTransfer.setVisibility(View.VISIBLE);
                mNote.setVisibility(View.VISIBLE);
                findViewById(R.id.noteDesc_fundTransfer).setVisibility(View.VISIBLE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mBeneficiaries = new ArrayList<>();

        GetBeneficiariesResModel.Response[] responses = mGetBeneficiariesResModel.getResponse();

        mResponseList = new ArrayList<>();

        switch (getIntent().getStringExtra(TRANSACTION_TYPE).toUpperCase()) {
            case "I":
                for (int i = 0; i < responses.length; i++) {
                    if (responses[i].getmIFT() != null) {
                        if (responses[i].getTransactionType().equalsIgnoreCase("I") && responses[i].getmIFT().equalsIgnoreCase("Y")) {
                            mResponseList.add(responses[i]);

                        }
                    }
                }
                break;
//            case "R":
//                for (int i=0;i<responses.length;i++){
//                    if(responses[i].getTransactionType().equalsIgnoreCase("R") && responses[i].getmIFT().equalsIgnoreCase("Y")) {
//                        mResponseList.add(responses[i]);
//                    }
//                    break;
//                }
//            case "N":
//                for (int i=0;i<responses.length;i++){
//                    if(responses[i].getTransactionType().equalsIgnoreCase("N") && responses[i].getmIFT().equalsIgnoreCase("Y")) {
//                        mResponseList.add(responses[i]);
//                    }
//                    break;
//                }
            case "O":
            case "R":
            case "N":
            case "IMPS":
                for (int i = 0; i < responses.length; i++) {
                    if (((responses[i].getmIMPS() != null) || (responses[i].getmRTGS() != null) || (responses[i].getmNEFT() != null)) )
                    if (responses[i].getTransactionType().equalsIgnoreCase("O")
                            && ((responses[i].getmIMPS().equalsIgnoreCase("Y"))
                            || (responses[i].getmRTGS().equalsIgnoreCase("Y")) ||
                            (responses[i].getmNEFT().equalsIgnoreCase("Y")))) {
                        mResponseList.add(responses[i]);

                    }
                }
                break;

//            default:
//                for (int i = 0; i < responses.length; i++) {
//                    if(!responses[i].getTransactionType().equalsIgnoreCase("I")) {
//                        mResponseList.add(responses[i]);
//                    }
//                }
        }

        for (GetBeneficiariesResModel.Response response : mResponseList) {

            String beneficiary = response.getToAccountName() + " - " + response.getToAccountNo();
            mBeneficiaries.add(beneficiary);
        }

        mBeneficiaries.add(0, "Select a beneficiary");

        ArrayAdapter<String> beneficiaryAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mBeneficiaries);
        mBeneficiarySpinner.setAdapter(beneficiaryAdapter);
        mBeneficiarySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    mTransactionType.setText("");
                    mIfscCode.setText("");
                    mToAccount.setText("");
                    mAmount.setText("");
                    return;
                }

                mErrorMessage.setVisibility(View.GONE);
                mBottomLayout.setVisibility(View.VISIBLE);
                mTransfer.setVisibility(View.VISIBLE);

//                GetBeneficiariesResModel.Response response = mGetBeneficiariesResModel.getResponse()[position];
                GetBeneficiariesResModel.Response response = mResponseList.get(position-1);
                if (!getIntent().hasExtra(TRANSACTION_TYPE)) {
                    if (response.getTransactionType().equalsIgnoreCase("I")) {
                        mTransactionType.setText("IFT");
                    } else if (response.getTransactionType().equalsIgnoreCase("R")) {
                        mTransactionType.setText("RTGS");
                    } else if (response.getTransactionType().equalsIgnoreCase("N")) {
                        mTransactionType.setText("NEFT");
                    } else if (response.getTransactionType().equalsIgnoreCase("O")) {
                        mTransactionType.setText("RTGS/NEFT/IMPS");
                    }
                } else {
                    //Temp code to set transaction type
                    if (getIntent().getStringExtra(TRANSACTION_TYPE).equalsIgnoreCase("I")) {
                        mTransactionType.setText("IFT");
                    } else if (getIntent().getStringExtra(TRANSACTION_TYPE).equalsIgnoreCase("R")) {
                        mTransactionType.setText("RTGS/NEFT/IMPS");
                    } else if (getIntent().getStringExtra(TRANSACTION_TYPE).equalsIgnoreCase("N")) {
                        mTransactionType.setText("RTGS/NEFT/IMPS");
                    } else if (getIntent().getStringExtra(TRANSACTION_TYPE).equalsIgnoreCase("IMPS")) {
                        mTransactionType.setText("IMPS");
                    }
                }
                Log.e("Position",position+"");
                Log.e("mIfscCode",response.getToIfscCode());
                Log.e("mToAccount",response.getToAccountNo());
                mIfscCode.setText(response.getToIfscCode());
                mToAccount.setText(response.getToAccountNo());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mGotBeneficiaries = true;

        if (getIntent().hasExtra(BENEFICIARY_NO)) {
            mBeneficiarySpinner.setSelection(getIntent().getIntExtra(BENEFICIARY_NO, 0));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == FUND_TRANSFER_VERIFY && resultCode == RESULT_OK) {
            finish();
        }
    }

    public void goBack(View v) {
        super.onBackPressed();
    }

    public void fundTransfer(View v) {
        if (mAccountSpinner.getSelectedItemPosition() == 0) {
            displayErrorMessage(mAccountSpinner, "Please select an account type.");
            return;
        }

        if (mBeneficiarySpinner.getSelectedItemPosition() == 0) {
            displayErrorMessage(mBeneficiarySpinner, "Please select a beneficiary.");
            return;
        }

        if (getTextFromView(mAmount).length() <= 0) {
            mAmount.setError("Please enter an amount to be transferred.");
            return;
        }

        Intent intent = new Intent(this, FundTransferVerifyActivity.class);
        GetBeneficiariesResModel.Response response = mResponseList.get(mBeneficiarySpinner.getSelectedItemPosition() - 1);
        intent.putExtra(FundTransferVerifyActivity.ACCOUNT_NO, response.getToAccountNo());
        intent.putExtra(FundTransferVerifyActivity.AMOUNT, getTextFromView(mAmount));
        intent.putExtra(FundTransferVerifyActivity.BENEFICIARY, response.getToAccountName());
        intent.putExtra(FundTransferVerifyActivity.FROM_ACCOUNT, mAccounts.get(mAccountSpinner.getSelectedItemPosition()));
        intent.putExtra(FundTransferVerifyActivity.TITLE, getTextFromView(mTitle));
        intent.putExtra(FundTransferVerifyActivity.IFSC_CODE, response.getToIfscCode());
        intent.putExtra(FundTransferVerifyActivity.CONTACT_NO, response.getToContactNo());
        intent.putExtra(FundTransferVerifyActivity.TRANSACTION_TYPE, response.getTransactionType());
        intent.putExtra(FundTransferVerifyActivity.REMARKS, getTextFromView(mRemarks));
        if (mIsImps) {
            intent.putExtra(FundTransferVerifyActivity.IS_IMPS, true);
        }
        startActivityForResult(intent, FUND_TRANSFER_VERIFY);
    }

    private class CacheAccountsTask extends AsyncTask<Void, Void, Void> {
        private Activity mActivity;
        private OperativeAccountResModel mOperativeAccountResModel;

        private CacheAccountsTask(Activity activity, OperativeAccountResModel operativeAccountResModel) {
            mActivity = activity;
            mOperativeAccountResModel = operativeAccountResModel;
        }

        @Override
        protected void onPreExecute() {
            ShowProgressbar.showMaskProgress(mActivity, "Processing data");
        }

        @Override
        protected Void doInBackground(Void... params) {
            OperativeAccountResModel.Response[] responses = mOperativeAccountResModel.getResponses();

            ArrayList<String> accounts = new ArrayList<>();

            for (int i = 0; i < responses.length; i++) {
                accounts.add(responses[i].getAccountNo());
            }

            try {
                MySharedPreferences.putAccounts(FundTransferActivity.this, accounts);
                MySharedPreferences.putAccounts(FundTransferActivity.this, responses);
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (BadPaddingException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            ShowProgressbar.dismissDialog();
            getBeneficiaries();
        }
    }

    private void sendRequestForNote(String screen){
        GetScreenMsg_ReqModel getScreenMsg_reqModel = new GetScreenMsg_ReqModel(screen);
        GetScreenMsg_Request getScreenMsg_request = new GetScreenMsg_Request(FundTransferActivity.this);
        getScreenMsg_request.sendRequest(FundTransferActivity.this,getScreenMsg_reqModel);
    }
}
