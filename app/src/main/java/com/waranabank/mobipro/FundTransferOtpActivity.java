package com.waranabank.mobipro;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.TextView;

import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Model.BaseModel;
import Model.FundTransferVerifyReqModel;
import Model.FundTransferVerifyResModel;
import Model.P2AFundTransferReqModel;
import Model.P2AFundTransferVerifyReqModel;
import Model.P2AFundTransferVerifyResModel;
import Parser.BaseParser;
import Parser.FundTransferVerifyParser;
import Parser.P2AFundTransferVerifyParser;
import Request.FundTransferVerifyRequest;
import Request.P2AFundTransferVerifyRequest;
import Utility.ErrorMessage;
import Utility.MyEnum;
import Utility.MySharedPreferences;

public class FundTransferOtpActivity extends BaseActivity {
    public static final String REQUEST_CD = "request_cd";
    public static final String TITLE = "title";
    public static final String IS_IMPS = "is_imps";
    public static final String USER_ID = "user_id";
    public static final String ACTIVITY_CD = "activity_cd";
    public static final String ACCT_NO = "acct_no";
    public static final String IMPS_REF_CD = "imps_ref_cd";
    public static final String BENF_ACCT_NO = "benf_acct_no";
    public static final String BENF_IFSC = "benf_ifsc";
    public static final String TRN_AMT = "trn_amt";
    public static final String CONTACT_NO = "contact_no";
    public static final String PIN_NO = "pin_no";
    public static final String REMARKS = "remarks";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fund_transfer_otp);

        if (getIntent().hasExtra(TITLE)) {
            ((TextView) findViewById(R.id.title_otp)).setText(getIntent().getStringExtra(TITLE));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    public void fundTransfer(View view) {
        if (getTextFromView((TextView) findViewById(R.id.otp_fundTransferOtp)).length() <= 4) {
            ((TextView) findViewById(R.id.otp_fundTransferOtp)).setError("Please enter a valid OTP.");
            return;
        }

        try {
            if (!getIntent().hasExtra(IS_IMPS) && !getIntent().getBooleanExtra(IS_IMPS, false)) {
                FundTransferVerifyReqModel fundTransferVerifyReqModel = new FundTransferVerifyReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this), getIntent().getStringExtra(REQUEST_CD), getTextFromView(((TextView) findViewById(R.id.otp_fundTransferOtp))));
                FundTransferVerifyRequest fundTransferVerifyRequest = new FundTransferVerifyRequest(this, MyEnum.displayProgress.Show);
                fundTransferVerifyRequest.sendRequest(this, fundTransferVerifyReqModel);
            } else if (getIntent().hasExtra(IS_IMPS) && getIntent().getBooleanExtra(IS_IMPS, false)) {

                P2AFundTransferVerifyReqModel fundTransferReqModel = new P2AFundTransferVerifyReqModel(
                        getIntent().getStringExtra(USER_ID),
                        getIntent().getStringExtra(ACTIVITY_CD),
                        getIntent().getStringExtra(ACCT_NO),
                        "Mobile",
                        getIntent().getStringExtra(IMPS_REF_CD),
                        getTextFromView((TextView) findViewById(R.id.otp_fundTransferOtp)),
                        getIntent().getStringExtra(BENF_ACCT_NO),
                        getIntent().getStringExtra(BENF_IFSC),
                        getIntent().getStringExtra(TRN_AMT),
                        getIntent().getStringExtra(PIN_NO),
                        getIntent().getStringExtra(REMARKS) == null ? "" :
                                getIntent().getStringExtra(REMARKS),
                        getIntent().getStringExtra(CONTACT_NO)
                );
                P2AFundTransferVerifyRequest fundTransferVerifyRequest =
                        new P2AFundTransferVerifyRequest(this, MyEnum.displayProgress.Show);
                fundTransferVerifyRequest.sendRequest(this, fundTransferReqModel);
            }
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    public void goBack(View view) {
        super.onBackPressed();
    }

    @Override
    public void networkConnectionError(BaseModel reqModel) {
        // CHANGE BY RAKESH TO SHOW TIME OUT CONNECTION IN FUND TRANSFER :: 19-09-2017
        P2AFundTransferVerifyReqModel p2AFundTransferVerifyReqModel = (P2AFundTransferVerifyReqModel) reqModel;
        new AlertDialog.Builder(this)
                .setTitle("Connection Timeout")
                .setMessage(ErrorMessage.getConnectionTimeOutError(p2AFundTransferVerifyReqModel.getmImpsRefCode()))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                }).create().show();
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {
        if (baseParser instanceof FundTransferVerifyParser) {
            FundTransferVerifyResModel superModel = (FundTransferVerifyResModel) baseParser.doParsing(objJson);

            FundTransferVerifyResModel fundTransferVerifyResModel = (FundTransferVerifyResModel) superModel.getModelArray().get(0);

            if (fundTransferVerifyResModel.getStaus() == FundTransferVerifyRequest.REQ_NOT_FOUND) {
                displayErrorMessage(findViewById(R.id.otp_fundTransferOtp), "Request not found or request expired, please try again.");
            } else if (fundTransferVerifyResModel.getStaus() == FundTransferVerifyRequest.SUCCESS) {
                new AlertDialog.Builder(this)
                        .setTitle("Successful")
                        .setMessage("Fund transfer was successful.")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                setResult(RESULT_OK);
                                finish();
                            }
                        }).create().show();
            }
        } else if (baseParser instanceof P2AFundTransferVerifyParser) {
            P2AFundTransferVerifyResModel superModel =
                    (P2AFundTransferVerifyResModel) baseParser.doParsing(objJson);

            P2AFundTransferVerifyResModel fundTransferVerifyResModel =
                    (P2AFundTransferVerifyResModel) superModel.getModelArray().get(0);

            switch (fundTransferVerifyResModel.getStatus()) {
                case P2AFundTransferVerifyRequest.SUCCESS:
                    new AlertDialog.Builder(this)
                            .setTitle("Fund transfer successful")
                            .setMessage("Your transaction code is " +
                                    fundTransferVerifyResModel.getResponse()[0].getRespTrnRef())
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    setResult(RESULT_OK);
                                    finish();
                                }
                            }).create().show();
                    break;
                case P2AFundTransferVerifyRequest.INSUFFICIENT_BALANCE:
                    new AlertDialog.Builder(this)
                            .setTitle("Error")
                            .setMessage("The account you are trying to transfer from has " +
                                    "insufficient balance.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).create().show();
                    break;
                case P2AFundTransferVerifyRequest.INVALID_NUMBER:
                    new AlertDialog.Builder(this)
                            .setTitle("Error")
                            .setMessage("The number is not valid or it is not registered with " +
                                    "any account.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).create().show();
                    break;
                case P2AFundTransferVerifyRequest.NCPI_REJECT:
                    new AlertDialog.Builder(this)
                            .setTitle("Error")
                            .setMessage(fundTransferVerifyResModel.getResponse()[0].getRespDesc())
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).create().show();
                    break;
                case P2AFundTransferVerifyRequest.TIME_OUT:
                    new AlertDialog.Builder(this)
                            .setTitle("Error")
                            .setMessage("The request timed out, please try again.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).create().show();
                    break;
                default:
                    new AlertDialog.Builder(this)
                            .setTitle("Error")
                            .setMessage("There was an error processing your request, please try " +
                                    "again.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).create().show();
                    break;
            }
        }
    }

    public static Intent createImpsVerifyIntent(Context context, Intent intent,
                                                P2AFundTransferReqModel fundTransferReqModel,
                                                String impsRefCode) throws InvalidKeyException,
            BadPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException,
            NoSuchPaddingException {
        intent.putExtra(IS_IMPS, true);
        intent.putExtra(USER_ID, fundTransferReqModel.getUserId());
        intent.putExtra(ACTIVITY_CD, MySharedPreferences.getActivityCode(context));
        intent.putExtra(ACCT_NO, fundTransferReqModel.getAccountNo());
        intent.putExtra(IMPS_REF_CD, impsRefCode);
        intent.putExtra(BENF_ACCT_NO, fundTransferReqModel.getBenefAccountNo());
        intent.putExtra(BENF_IFSC, fundTransferReqModel.getBenefIfsc());
        intent.putExtra(TRN_AMT, fundTransferReqModel.getTransAmount());
        intent.putExtra(PIN_NO, fundTransferReqModel.getPin());
//        intent.putExtra(CONTACT_NO, fundTransferReqModel.getContactNo());
        intent.putExtra(REMARKS, fundTransferReqModel.getRemarks());
        return intent;
    }
}
