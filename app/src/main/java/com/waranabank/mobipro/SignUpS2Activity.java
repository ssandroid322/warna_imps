package com.waranabank.mobipro;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import Model.BaseModel;
import Model.SignUpS2ReqModel;
import Model.SignUpS2ResModel;
import Parser.BaseParser;
import Request.SignUpS2Request;
import Utility.ErrorMessage;
import Utility.MyEnum;

public class SignUpS2Activity extends BaseActivity {
    private static final int SIGN_UP_FINISH = 386;
    public static final String SHOULD_FINISH = "shouldfinish";

    public static final String CUSTOMER_ID = "customer_id";
    public static final String MOBILE_NO = "mobile_no";
    public static final String ACTIVATION_CODE = "activation_code";

    private Toolbar mToolbar;
    private EditText mCustomerId, mMobileNo, mOtp, mPassword, mConfirmPassword;
    private TextView mWelcomeString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_s2);

        bindViews();
        syncViewsWithIntents();

        mWelcomeString.setText(getString(R.string.welcome_string, getString(R.string.bank_name_welcome)));

        setSupportActionBar(mToolbar);

        getSupportActionBar().setTitle("");
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    private void syncViewsWithIntents() {
        mCustomerId.setText(getIntent().getStringExtra(CUSTOMER_ID));
        mMobileNo.setText(getIntent().getStringExtra(MOBILE_NO));
    }

    public void goBack(View view){
        super.onBackPressed();
    }

    @Override
    protected void bindViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar_signUpS2);
        mCustomerId = (EditText) findViewById(R.id.customerId_signUpS2);
        mMobileNo = (EditText) findViewById(R.id.mobileNo_signUpS2);
        mOtp = (EditText) findViewById(R.id.otp_signUpS2);
        mPassword = (EditText) findViewById(R.id.password_signUpS2);
        mConfirmPassword = (EditText) findViewById(R.id.confirmPassword_signUpS2);
        mWelcomeString = (TextView) findViewById(R.id.welcomeString_signUpS2);
    }

    public void doSignUp(View view){
        hideKeybord(mConfirmPassword);

        String customerId = mCustomerId.getText().toString();
        String mobileNo = mMobileNo.getText().toString();
        String otp = mOtp.getText().toString();
        String password = mPassword.getText().toString();
        String confirmPassword = mConfirmPassword.getText().toString();

        if(customerId.trim().length() <= 0 ){
            mCustomerId.setError("Please enter a valid Customer id.");
            return;
        }

        if(mobileNo.trim().length() <= 0){
            mMobileNo.setError("Please enter a valid mobile number.");
            return;
        }

        if(otp.trim().length() <= 4){
            mOtp.setError("Please enter a valid OTP.");
            return;
        }

        if(password.trim().length() <= 0){
            mPassword.setError("Please enter a valid password.");
            return;
        }

        if(confirmPassword.trim().length() <= 0){
            mConfirmPassword.setError("Please enter confirm password same as password.");
            return;
        }

        if(password.trim().length() < 8){
            mPassword.setError("The password must be at least 8 characters long.");
            return;
        }

        if(!confirmPassword.equals(password)){
            displayErrorMessage(mConfirmPassword, "Please enter confirm password same as password.");
            return;
        }

        SignUpS2ReqModel signUpS2ReqModel = new SignUpS2ReqModel(customerId, getIntent().getStringExtra(ACTIVATION_CODE), otp, password);

        SignUpS2Request signUpS2Request = new SignUpS2Request(this, MyEnum.displayProgress.Show);
        signUpS2Request.sendRequest(this, signUpS2ReqModel);
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {
        SignUpS2ResModel superModel = (SignUpS2ResModel) baseParser.doParsing(objJson);

        SignUpS2ResModel signUpS2ResModel = (SignUpS2ResModel) superModel.getModelArray().get(0);

        if(signUpS2ResModel.getStatus() == SignUpS2Request.SUCCESSFUL){
            Intent intent = new Intent(this, SignUpFinishActivity.class);
            SignUpS2ResModel.Response[] responses = signUpS2ResModel.getResponses();
            intent.putExtra(SignUpFinishActivity.USER_ID, responses[0].getUserId());
            startActivityForResult(intent, SIGN_UP_FINISH);
        }else{
            Toast.makeText(SignUpS2Activity.this, ErrorMessage.getInvalidResponse(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void networkConnectionError(BaseModel reqModel) {
        displayErrorMessage(mConfirmPassword, ErrorMessage.getNetworkConnectionError());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == SIGN_UP_FINISH && resultCode == RESULT_OK && data.getBooleanExtra(SHOULD_FINISH, false)){
            Intent intent = new Intent();
            intent.putExtra(com.waranabank.mobipro.SignUpS1Activity.SHOULD_FINISH, true);
            setResult(RESULT_OK, intent);
            finish();
        }
    }
}
