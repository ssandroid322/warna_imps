package com.waranabank.mobipro;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Model.OperativeAccountReqModel;
import Model.OperativeAccountResModel;
import Parser.BaseParser;
import Parser.OperativeAccountParser;
import Request.OperativeAccountRequest;
import Utility.ErrorMessage;
import Utility.MyEnum;
import Utility.MySharedPreferences;
import Utility.ShowProgressbar;

public class PersonToAdharCardActivity extends BaseActivity implements AdapterView.OnItemSelectedListener {
    private static final int FUND_TRANSFER = 12;
    private Spinner mSpinner;
    private ArrayList<String> mAccounts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_to_adhar_card);

        bindViews();

        findViewById(R.id.submit_p2pTransfer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mSpinner.getSelectedItemPosition() == 0) {
                    displayErrorMessage(mSpinner, "Please select an account");
                    return;
                }

                if(((TextView) findViewById(R.id.adharId_p2pTransfer)).getText().toString().trim().length() <= 0) {
                    displayErrorMessage(findViewById(R.id.submit_p2pTransfer), "Please enter a valid Adhar Id");
                    return;
                }

                if(((TextView) findViewById(R.id.amount_p2pTransfer)).getText().toString().trim().length() <= 0) {
                    displayErrorMessage(findViewById(R.id.submit_p2pTransfer), "Please enter amount to transfer");
                    return;
                }

                /*showLongToast("Fund Transfer successful");
                ActivityCompat.finishAfterTransition(PersonToAdharCardActivity.this);*/
                Intent intent = new Intent(PersonToAdharCardActivity.this, FundTransferOtpActivity.class);
                intent.putExtra(FundTransferOtpActivity.REQUEST_CD, "123456");
                intent.putExtra(FundTransferOtpActivity.TITLE, "IMPS Transfer");
                startActivityForResult(intent, FUND_TRANSFER);
            }
        });

        try {
            if(MySharedPreferences.getAccounts(this) == null || MySharedPreferences.getAccounts(this).size() <= 0){
                OperativeAccountRequest operativeAccountRequest = new OperativeAccountRequest(this, MyEnum.displayProgress.Show);
                OperativeAccountReqModel operativeAccountReqModel = null;
                try {
                    operativeAccountReqModel = new OperativeAccountReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this));
                } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e) {
                    e.printStackTrace();
                }
                operativeAccountRequest.sendRequest(this, operativeAccountReqModel);
            }else{
                fillAdapterData();
            }
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == FUND_TRANSFER && resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {
        if(baseParser instanceof OperativeAccountParser)
        {
            OperativeAccountResModel superModel = (OperativeAccountResModel) baseParser.doParsing(objJson);
            OperativeAccountResModel operativeAccountResModel = (OperativeAccountResModel) superModel.getModelArray().get(0);

            if(operativeAccountResModel.getStatus() != OperativeAccountRequest.SUCCESS)
            {
                displayErrorMessage(mSpinner, ErrorMessage.getGenericError());
                return;
            }

            new CacheAccountsTask(this, operativeAccountResModel).execute();
        }
    }

    private void fillAdapterData() {
        try {
            mAccounts = (ArrayList<String>) MySharedPreferences.getAccounts(this);
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        if (mAccounts == null) return;

        mAccounts.add(0, "Select Account Number");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mAccounts);
        mSpinner.setAdapter(arrayAdapter);
        mSpinner.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(position == 0) {
            findViewById(R.id.note_p2aTransfer).setVisibility(View.GONE);
            findViewById(R.id.noteDesc_p2aTransfer).setVisibility(View.GONE);
        } else {
            findViewById(R.id.note_p2aTransfer).setVisibility(View.VISIBLE);
            findViewById(R.id.noteDesc_p2aTransfer).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void goBack(View view) {
        ActivityCompat.finishAfterTransition(this);
    }

    @Override
    protected void bindViews() {
        mSpinner = (Spinner) findViewById(R.id.accountList_p2pTransfer);
    }

    private class CacheAccountsTask extends AsyncTask<Void, Void, Void> {
        private Activity mActivity;
        private OperativeAccountResModel mOperativeAccountResModel;

        private CacheAccountsTask(Activity activity, OperativeAccountResModel operativeAccountResModel) {
            mActivity = activity;
            mOperativeAccountResModel = operativeAccountResModel;
        }

        @Override
        protected void onPreExecute() {
            ShowProgressbar.showMaskProgress(mActivity, "Processing data");
        }

        @Override
        protected Void doInBackground(Void... params) {
            OperativeAccountResModel.Response[] responses = mOperativeAccountResModel.getResponses();

            ArrayList<String> accounts = new ArrayList<>();

            for (int i = 0; i < responses.length; i++) {
                accounts.add(responses[i].getAccountNo());
            }

            try {
                MySharedPreferences.putAccounts(mActivity, accounts);
                MySharedPreferences.putAccounts(mActivity, responses);
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (BadPaddingException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            ShowProgressbar.dismissDialog();
            fillAdapterData();
        }
    }
}
