package com.waranabank.mobipro;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Model.BaseModel;
import Model.ChequeStatusInqReqModel;
import Model.ChequeStatusInqResModel;
import Model.OperativeAccountReqModel;
import Model.OperativeAccountResModel;
import Parser.BaseParser;
import Parser.ChequeStatusInqParser;
import Parser.OperativeAccountParser;
import Request.ChequeStatusInqRequest;
import Request.OperativeAccountRequest;
import Utility.ErrorMessage;
import Utility.MyEnum;
import Utility.MySharedPreferences;
import Utility.ShowProgressbar;

public class SearchChequeActivity extends com.waranabank.mobipro.BaseActivity {
    private Spinner mSpinner;
    private TextView mChequeNo, mErrorMessage, mTransactionDate, mAmount, mRemarks;
    private View mChequeLayout;
    private ImageView mCheck;
    private ArrayList<String> mAccounts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_cheque);

        bindViews();

        try {
            if(MySharedPreferences.getAccounts(this) == null || MySharedPreferences.getAccounts(this).size() <= 0){
                OperativeAccountRequest operativeAccountRequest = new OperativeAccountRequest(this, MyEnum.displayProgress.Show);
                OperativeAccountReqModel operativeAccountReqModel = null;
                try {
                    operativeAccountReqModel = new OperativeAccountReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this));
                } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e) {
                    e.printStackTrace();
                }
                operativeAccountRequest.sendRequest(this, operativeAccountReqModel);
            }else{
                fillAdapterData();
            }
        } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    public void goBack(View v){
        super.onBackPressed();
    }

    private void fillAdapterData() {
        try {
//            mAccounts = (ArrayList<String>) MySharedPreferences.getAccounts(this);
            mAccounts = (ArrayList<String>) MySharedPreferences.getChequeBookAccounts(this);
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        if (mAccounts == null) return;

        mAccounts.add(0, "Select Account Number");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mAccounts);
        mSpinner.setAdapter(arrayAdapter);
        mChequeNo.setVisibility(View.VISIBLE);
        mErrorMessage.setVisibility(View.GONE);
    }

    public void getChequeDetails(View view){
        hideKeybord(mSpinner);

        if(mSpinner.getSelectedItemPosition() == 0){
            displayErrorMessage(mSpinner, "Please Select Account Number.");
            return;
        }

        if(mChequeNo.getText().toString().trim().length() <= 0){
            displayErrorMessage(mSpinner, "Please enter a valid cheque number.");
            return;
        }

        try {
            ChequeStatusInqRequest chequeStatusInqRequest = new ChequeStatusInqRequest(this, MyEnum.displayProgress.Show);
            ChequeStatusInqReqModel chequeStatusInqReqModel = new ChequeStatusInqReqModel(MySharedPreferences.getUserId(this), MySharedPreferences.getActivityCode(this), mAccounts.get(mSpinner.getSelectedItemPosition()), getTextFromView(mChequeNo));
            chequeStatusInqRequest.sendRequest(this, chequeStatusInqReqModel);
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void networkConnectionError(BaseModel reqModel) {
        mErrorMessage.setText("Cheque not found.");
        displayErrorMessage(mSpinner, ErrorMessage.getNetworkConnectionError());
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {
        if(baseParser instanceof OperativeAccountParser) {
            OperativeAccountResModel superModel = (OperativeAccountResModel) baseParser.doParsing(objJson);
            OperativeAccountResModel operativeAccountResModel = (OperativeAccountResModel) superModel.getModelArray().get(0);

            if(operativeAccountResModel.getStatus() != OperativeAccountRequest.SUCCESS) {
                displayErrorMessage(mSpinner, ErrorMessage.getGenericError());
                return;
            }

            new CacheAccountsTask(this, operativeAccountResModel).execute();
        }else if(baseParser instanceof ChequeStatusInqParser){
            ChequeStatusInqResModel superModel = (ChequeStatusInqResModel) baseParser.doParsing(objJson);

            ChequeStatusInqResModel chequeStatusInqResModel = (ChequeStatusInqResModel) superModel.getModelArray().get(0);

            /*mErrorMessage.setVisibility(View.GONE);
            mChequeLayout.setVisibility(View.VISIBLE);
            //ChequeStatusInqResModel.Response response = chequeStatusInqResModel.getResponses()[0];
            ChequeStatusInqResModel.Response response = new ChequeStatusInqResModel.Response("10-2-1212", "P", "Processing", "5000");
            mTransactionDate.setText(response.getTransactionDate());
            mRemarks.setText(response.getRemarks());
            mAmount.setText(response.getAmount());*/

            if(chequeStatusInqResModel.getStatus() == ChequeStatusInqRequest.NOT_FOUND){
                mErrorMessage.setVisibility(View.VISIBLE);
                mErrorMessage.setText("Cheque not found.");
                mChequeLayout.setVisibility(View.GONE);
                displayErrorMessage(mSpinner, "Cheque not found, please check cheque number.");
            }else if(chequeStatusInqResModel.getStatus() == ChequeStatusInqRequest.SUCCESSFUL){
                mErrorMessage.setVisibility(View.GONE);
                mChequeLayout.setVisibility(View.VISIBLE);
                ChequeStatusInqResModel.Response response = chequeStatusInqResModel.getResponses()[0];
                mTransactionDate.setText(response.getTransactionDate());
                mRemarks.setText(response.getRemarks());
                mAmount.setText(response.getAmount());
            }
        }
    }

    @Override
    protected void bindViews() {
        mSpinner = (Spinner) findViewById(R.id.accountList_searchCheque);
        mChequeNo = (TextView) findViewById(R.id.chequeNo_searchCheque);
        mErrorMessage = (TextView) findViewById(R.id.message_searchCheque);
        mChequeLayout = findViewById(R.id.chequeLayout_searchCheque);
        mTransactionDate = (TextView) findViewById(R.id.transactionDate_searchCheque);
        mAmount = (TextView) findViewById(R.id.amount_searchCheque);
        mRemarks = (TextView) findViewById(R.id.remarks_searchCheque);
        mCheck = (ImageView) findViewById(R.id.check_searchCheque);
    }

    private class CacheAccountsTask extends AsyncTask<Void, Void, Void> {
        private Activity mActivity;
        private OperativeAccountResModel mOperativeAccountResModel;

        private CacheAccountsTask(Activity activity, OperativeAccountResModel operativeAccountResModel) {
            mActivity = activity;
            mOperativeAccountResModel = operativeAccountResModel;
        }

        @Override
        protected void onPreExecute() {
            ShowProgressbar.showMaskProgress(mActivity, "Processing data");
        }

        @Override
        protected Void doInBackground(Void... params) {
            OperativeAccountResModel.Response[] responses = mOperativeAccountResModel.getResponses();

            ArrayList<String> accounts = new ArrayList<>();

            for (int i = 0; i < responses.length; i++) {
                accounts.add(responses[i].getAccountNo());
            }

            try {
                MySharedPreferences.putAccounts(SearchChequeActivity.this, accounts);
                MySharedPreferences.putAccounts(SearchChequeActivity.this, responses);
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (BadPaddingException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            ShowProgressbar.dismissDialog();
            fillAdapterData();
        }
    }
}
