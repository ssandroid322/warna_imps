package com.waranabank.mobipro;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Model.BaseModel;
import Model.BlockCardVerifyReqModel;
import Model.BlockCardVerifyResModel;
import Model.FundTransferVerifyReqModel;
import Model.FundTransferVerifyResModel;
import Parser.BaseParser;
import Request.BlockCardVerifyRequest;
import Request.FundTransferVerifyRequest;
import Utility.ErrorMessage;
import Utility.MyEnum;
import Utility.MySharedPreferences;

public class BlockATMOTPActivity extends BaseActivity {

    public static final String REQUEST_CD = "request_cd";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_block_atm_otp);
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseActivity = this;
    }

    public void blockCard(View view) {
        if (getTextFromView((TextView) findViewById(R.id.otp_blockCardOtp)).length() <= 4) {
            ((TextView) findViewById(R.id.otp_blockCardOtp)).setError("Please enter a valid OTP.");
            return;
        }
        try {
            BlockCardVerifyReqModel blockCardVerifyReqModel = new BlockCardVerifyReqModel(MySharedPreferences.getUserId(this),
                    MySharedPreferences.getActivityCode(this),
                    getIntent().getStringExtra(REQUEST_CD),
                    MySharedPreferences.getCustomerID(this),
                    getTextFromView(((TextView) findViewById(R.id.otp_blockCardOtp))));

            BlockCardVerifyRequest blockCardVerifyRequest = new BlockCardVerifyRequest(this, MyEnum.displayProgress.Show);
            blockCardVerifyRequest.sendRequest(this, blockCardVerifyReqModel);
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    public void goBack(View view) {
        super.onBackPressed();
    }

    @Override
    public void networkConnectionError(BaseModel reqModel) {
        displayErrorMessage(findViewById(R.id.otp_blockCardOtp), ErrorMessage.getNetworkConnectionError());
    }

    @Override
    public void onPopulate(JSONObject objJson, BaseParser baseParser) {
        BlockCardVerifyResModel superModel = (BlockCardVerifyResModel) baseParser.doParsing(objJson);

        BlockCardVerifyResModel blockCardVerifyResModel = (BlockCardVerifyResModel) superModel.getModelArray().get(0);

        if (blockCardVerifyResModel.getStaus() == BlockCardVerifyRequest.REQ_NOT_FOUND)
        {
            displayErrorMessage(findViewById(R.id.otp_blockCardOtp), "Request not found or request expired, please try again.");
        }
        else if (blockCardVerifyResModel.getStaus() == BlockCardVerifyRequest.SUCCESS)
        {
            showLongToast("Your request for blocking ATM Card is successfully recevied.");
            setResult(RESULT_OK);
            finish();
        }
    }
}