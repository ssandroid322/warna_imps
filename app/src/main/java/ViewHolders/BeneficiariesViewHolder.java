package ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.waranabank.mobipro.R;


/**
 * Created by Samvid Mistry on 23/02/2016.
 */
public class BeneficiariesViewHolder extends RecyclerView.ViewHolder {
    public View wholeView, mShowMore, mBottomLayout;
    public TextView mBeneficiaryName, mIfscCode, mTransactionType, mAccountNo, mContactNo, mAddressOne, mDeleteBeneficiary, mFundTransfer;
    public ImageView mShowMoreLess;
    public boolean mIsExpanded = false;

    public BeneficiariesViewHolder(View itemView) {
        super(itemView);
        wholeView = itemView;
        mBeneficiaryName = (TextView) itemView.findViewById(R.id.name_viewBeneficiaries);
        mIfscCode = (TextView) itemView.findViewById(R.id.ifsc_viewBeneficiaries);
        mTransactionType = (TextView) itemView.findViewById(R.id.transType_viewBeneficiaries);
        mAccountNo = (TextView) itemView.findViewById(R.id.accNo_viewBeneficiaries);
        mContactNo = (TextView) itemView.findViewById(R.id.contactNo_viewBeneficiaries);
        mAddressOne = (TextView) itemView.findViewById(R.id.addressOne_viewBeneficiaries);
        mDeleteBeneficiary = (TextView) itemView.findViewById(R.id.deleteBeneficiary_viewBeneficiaries);
        mFundTransfer = (TextView) itemView.findViewById(R.id.fundTransfer_viewBeneficiaries);
        mShowMore = itemView.findViewById(R.id.showMore_viewBeneficiaries);
        mShowMoreLess = (ImageView) itemView.findViewById(R.id.showMoreLess_viewBeneficiaries);
        mBottomLayout = itemView.findViewById(R.id.bottomLayout_viewBeneficiaries);
    }
}
