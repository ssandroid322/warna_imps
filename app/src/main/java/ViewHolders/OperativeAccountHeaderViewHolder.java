package ViewHolders;

import android.view.View;
import android.widget.TextView;
import com.waranabank.mobipro.R;


/**
 * Created by Samvid Mistry on 10/02/2016.
 */
public class OperativeAccountHeaderViewHolder extends OperativeAccountViewHolder {
    public TextView mAccountType;

    public OperativeAccountHeaderViewHolder(View itemView) {
        super(itemView);
        wholeView = itemView.findViewById(R.id.card_headerItemOperativeAccount);
        mAccountType = (TextView) itemView.findViewById(R.id.accountType_headerItemOperativeAccount);
    }
}
