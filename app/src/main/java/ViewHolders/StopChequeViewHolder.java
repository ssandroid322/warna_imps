package ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.waranabank.mobipro.R;


/**
 * Created by Samvid Mistry on 02/03/2016.
 */
public class StopChequeViewHolder extends RecyclerView.ViewHolder {
    public View wholeView;
    public TextView mChequeNo, mIssuedDate, mIssuedTo, mAmount;
    public Button mStopButton;

    public StopChequeViewHolder(View itemView) {
        super(itemView);
        wholeView = itemView;
        mChequeNo = (TextView) itemView.findViewById(R.id.chequeNo_stopCheque);
        mIssuedDate = (TextView) itemView.findViewById(R.id.issuedDate_stopCheque);
        mIssuedTo = (TextView) itemView.findViewById(R.id.issuedTo_stopCheque);
        mAmount = (TextView) itemView.findViewById(R.id.amount_stopCheque);
        mStopButton = (Button) itemView.findViewById(R.id.stopCheque_stopCheque);
    }
}
