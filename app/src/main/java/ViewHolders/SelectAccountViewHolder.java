package ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Spinner;
import com.waranabank.mobipro.R;


/**
 * Created by Samvid Mistry on 02/03/2016.
 */
public class SelectAccountViewHolder extends RecyclerView.ViewHolder {
    public Spinner mSpinner;

    public SelectAccountViewHolder(View itemView) {
        super(itemView);
        mSpinner = (Spinner) itemView.findViewById(R.id.accountList_selectAccount);
    }
}
