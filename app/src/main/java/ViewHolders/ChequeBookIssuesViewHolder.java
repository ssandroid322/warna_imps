package ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.waranabank.mobipro.R;


/**
 * Created by Samvid Mistry on 26/02/2016.
 */
public class ChequeBookIssuesViewHolder extends RecyclerView.ViewHolder {
    public TextView mIssueDate, mUsedCheques, mTotalCheques, mFromCheque, mToCheque, mRemarks;

    public ChequeBookIssuesViewHolder(View itemView) {
        super(itemView);
        mIssueDate = (TextView) itemView.findViewById(R.id.issueDate_chequeIssue);
        mUsedCheques = (TextView) itemView.findViewById(R.id.usedCheques_chequeIssue);
        mTotalCheques = (TextView) itemView.findViewById(R.id.totalCheques_chequeIssue);
        mFromCheque = (TextView) itemView.findViewById(R.id.fromCheque_chequeIssue);
        mToCheque = (TextView) itemView.findViewById(R.id.toCheque_chequeIssue);
        mRemarks = (TextView) itemView.findViewById(R.id.remarks_chequeIssue);
    }
}
