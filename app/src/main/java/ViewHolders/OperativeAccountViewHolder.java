package ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.waranabank.mobipro.R;

/**
 * Created by Samvid Mistry on 10/02/2016.
 */
public class OperativeAccountViewHolder extends RecyclerView.ViewHolder {
    public View wholeView, showMore, bottomLayout;
    public TextView mAccountNo, mAvailableBalance, mOpeningDate, mOpeningBal, mUnclearBal, mLienBal, mAvailableBal, mMiniStatement, mDetailedStatement, mFullName, mMoreDetailsText;
    public ImageView mShowMoreLess;

    public OperativeAccountViewHolder(View itemView) {
        super(itemView);
        wholeView = itemView;
        showMore = itemView.findViewById(R.id.showMore_itemOperativeAccount);
        bottomLayout = itemView.findViewById(R.id.bottomLayout_itemOperativeAccount);
        mAccountNo = (TextView) itemView.findViewById(R.id.accountNo_itemOperativeAccount);
        mAvailableBalance = (TextView) itemView.findViewById(R.id.balance_itemOperativeAccount);
        mOpeningDate = (TextView) itemView.findViewById(R.id.openingDate_itemOperativeAccount);
        mOpeningBal = (TextView) itemView.findViewById(R.id.openingBal_itemOperativeAccount);
        mUnclearBal = (TextView) itemView.findViewById(R.id.unclearBal_itemOperativeAccount);
        mLienBal = (TextView) itemView.findViewById(R.id.lienBal_itemOperativeAccount);
        mAvailableBal = (TextView) itemView.findViewById(R.id.availableBal_itemOperativeAccount);
        mMiniStatement = (TextView) itemView.findViewById(R.id.miniStatement_itemOperativeAccount);
        mDetailedStatement = (TextView) itemView.findViewById(R.id.detailedStatement_itemOperativeAccount);
        mFullName = (TextView) itemView.findViewById(R.id.fullName_itemOperativeAccount);
        mMoreDetailsText = (TextView) itemView.findViewById(R.id.moreDetailsText_itemOperativeAccount);
        mShowMoreLess = (ImageView) itemView.findViewById(R.id.showMoreLess_itemOperativeAccount);
    }
}
