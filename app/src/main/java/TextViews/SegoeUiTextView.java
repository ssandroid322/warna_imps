package TextViews;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by Samvid Mistry on 02/01/2016.
 */
public class SegoeUiTextView extends AppCompatTextView {
    public SegoeUiTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        //setFonts(context);
    }

    private void setFonts(Context context) {
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "Fonts/Helvetica-Normal.ttf"));
    }
}
