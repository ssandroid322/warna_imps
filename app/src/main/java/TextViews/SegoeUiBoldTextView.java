package TextViews;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by Samvid Mistry on 06/01/2016.
 */
public class SegoeUiBoldTextView extends AppCompatTextView {
    public SegoeUiBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        //setFonts(context);
    }

    private void setFonts(Context context) {
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "Fonts/Helvetica-Bold.ttf"));
    }
}
